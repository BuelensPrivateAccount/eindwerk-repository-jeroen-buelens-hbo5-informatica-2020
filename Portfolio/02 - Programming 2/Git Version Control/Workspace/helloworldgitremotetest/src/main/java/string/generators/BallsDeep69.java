/** DISCLAIMER: this beautiful method was created by Steven De Cock and is for non-commerical use only
 */

package string.generators;

public class BallsDeep69 {
    private BallsDeep69(){}

    //i made this to show branches do something and to commit something to master
    public static void printBallsDeep69(){
        String balls = "\t<^>\n"+
                        "\t| |\n"+
                        "\t| |\n"+
                        "  ()   ()";
        System.out.println(balls);
    }

    public static void main(String[] args) {
        printBallsDeep69();
    }
}
