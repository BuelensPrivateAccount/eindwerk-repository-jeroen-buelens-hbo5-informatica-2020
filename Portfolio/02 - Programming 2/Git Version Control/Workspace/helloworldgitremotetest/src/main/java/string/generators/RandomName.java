package string.generators;

import java.util.concurrent.ThreadLocalRandom;

public class RandomName {


    private static String personFirstNames[] = new String[]{"Steven",
            "Maxime",
            "Stijn",
            "Jeroen",
            "Yoni",
            "Seda",
            "Tim",
            "Alexander",
            "Arne"};
    private static String personLastNames[] = new String[]{"Esnol",
            "Buelens",
            "Coeckelbergh",
            "De Cock",
            "Hesters",
            "Gül",
            "Van Caekenbergh",
            "Vindelinckx",
            "Wauters"};

    private String output;

    public RandomName() {
        this.output = generateRandomName();
    }


    private static int getRandomNumber(int bound) {
        return ThreadLocalRandom.current().nextInt(0, bound);
    }

    public static String generateRandomName() {
        String firstname = personFirstNames[getRandomNumber(personFirstNames.length)];
        String lastname = personLastNames[getRandomNumber(personLastNames.length)];
        return firstname + " " + lastname;
    }

    public String getName() {
        return output;
    }
}
