package string.generators;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailGenerator {

    private EmailGenerator() {
    }

    public static String generateEmail(String input) throws NotAllowedException {
        if (checkOnlyContainsAllowed(input)) {
            StringBuilder sb = new StringBuilder();
            String[] split = input.split("\\W+");
            for (int i = 0; i < split.length; i++) {
                sb.append(split[i]);
                if (i == 0) {
                    sb.append(".");
                } else if (i == split.length - 1) {
                    sb.append("@realdolmen.com");
                } else ;
            }
            return sb.toString();
        } else {
            throw new NotAllowedException();
        }
    }

    private static boolean checkOnlyContainsAllowed(String s) {
        Pattern special = Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~;:'\"|-]");
        Matcher hasSpecial = special.matcher(s);
        return !hasSpecial.find();
    }
}
