package string.generators;

import java.util.concurrent.ThreadLocalRandom;

public class DefaultPassWordGenerator {
    private DefaultPassWordGenerator() {
    }

    private static final String ABC_LOWERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String ABC_UPPERCASE = "abcdefghijklmnopqrstuvxyz";
    private static final String SPECIAL_CHARACTERS = "1234567890-=!@#$%^&*()_+[]\\;'\"<>?:{}";
    private static final String COMBINED_TO_PULL_FROM = ABC_LOWERCASE + ABC_UPPERCASE + SPECIAL_CHARACTERS;

    public static String getDefaultPass(int length) {
        StringBuilder sb = new StringBuilder();
        if(length>0){
            sb.append(ABC_UPPERCASE.charAt((getRandomNumber(ABC_UPPERCASE.length()))));
            if(length>1){
                sb.append(SPECIAL_CHARACTERS.charAt(getRandomNumber(SPECIAL_CHARACTERS.length())));
            }
            if(length>2){
                sb.append(ABC_LOWERCASE.charAt(getRandomNumber(ABC_LOWERCASE.length())));
            }
        }
        for (int i = 2; i < length; i++) {
            sb.append(COMBINED_TO_PULL_FROM.charAt(getRandomNumber(COMBINED_TO_PULL_FROM.length())));
        }
        return sb.toString();
    }

    private static int getRandomNumber(int bound) {
        return ThreadLocalRandom.current().nextInt(0, bound);
    }
}
