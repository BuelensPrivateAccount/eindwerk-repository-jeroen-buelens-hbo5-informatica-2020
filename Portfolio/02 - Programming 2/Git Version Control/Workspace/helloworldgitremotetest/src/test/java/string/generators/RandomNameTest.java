package string.generators;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RandomNameTest {

    @Test
    public void emptyTest() {
    }

    @Test
    public void testRandomNameNotNull() {
        Assertions.assertNotNull(new RandomName().getName());
    }

    @Test
    public void testRandomNameNotEmpty() {
        Assertions.assertNotEquals("", new RandomName().getName().trim());
    }

}
