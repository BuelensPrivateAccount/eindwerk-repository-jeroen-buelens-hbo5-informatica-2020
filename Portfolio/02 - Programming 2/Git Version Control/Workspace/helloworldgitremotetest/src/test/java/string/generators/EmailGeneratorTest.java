package string.generators;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EmailGeneratorTest {

    @Test
    public void emptyTest() {
    }

    @Test
    public void testWrongInputInEmail() {
        Assertions.assertThrows(Exception.class, () -> {
            EmailGenerator.generateEmail("@#$!$%!@#!$%#^^%&$@&^$%");
        });
    }

    @Test
    public void testRandomEmailNormal() throws Exception {
        String input = "Jhon Doe";
        String output = EmailGenerator.generateEmail(input);
        Assertions.assertEquals("Jhon.Doe@realdolmen.com", output);
    }

    @Test
    public void testRandomEmailWithSpaceInSurName() throws Exception {
        String input = "Jack The Ripper";
        String output = EmailGenerator.generateEmail(input);
        Assertions.assertEquals("Jack.TheRipper@realdolmen.com", output);
    }
}

