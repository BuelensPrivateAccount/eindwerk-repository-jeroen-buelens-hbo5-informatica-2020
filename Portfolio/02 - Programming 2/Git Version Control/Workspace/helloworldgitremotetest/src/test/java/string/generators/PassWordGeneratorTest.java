package string.generators;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class PassWordGeneratorTest {


    @ParameterizedTest
    @ValueSource(ints = {8,9,10,11,126,1023,12,18,25})
    public void checkConstraintsLengthBiggerThan8Test(int i) throws Exception{
        String pass = DefaultPassWordGenerator.getDefaultPass(i);
        Assertions.assertTrue(constraintsChecking(pass));

    }
    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4,5,6,7})
    public void checkConstraintsLengthSmallerThan8ButNot0Test(int i) throws Exception{
        String pass = DefaultPassWordGenerator.getDefaultPass(i);
        Assertions.assertFalse(constraintsChecking(pass));

    }

    private boolean constraintsChecking(String s) {
        boolean upperCaseFlag = false;
        boolean lowerCaseFlag = false;
        boolean specialCharCaseFlag = false;
        if (s.length() <= 8) {
            return false;
        }
        for (char c : s.toCharArray()) {
            String singleCharString = "" + c;
            if (singleCharString.matches("^[a-z]")){
                lowerCaseFlag = true;
            } else if (singleCharString.matches("^[A-Z]")){
                upperCaseFlag = true;
            } else if (!singleCharString.matches("^[A-Za-z]")) {
                specialCharCaseFlag = true;
            }
        }
        if (upperCaseFlag && specialCharCaseFlag && lowerCaseFlag) {
            return true;
        } else {
            return false;
        }
    }

    @Test
    public void CheckNotEmptyTest() {
        String pass = DefaultPassWordGenerator.getDefaultPass(10);
        Assertions.assertTrue(!pass.isEmpty());

    }

    @Test
    public void CheckIsEmptyTest(){
        String pass = DefaultPassWordGenerator.getDefaultPass(0);
        Assertions.assertTrue(pass.isEmpty());
    }
}
