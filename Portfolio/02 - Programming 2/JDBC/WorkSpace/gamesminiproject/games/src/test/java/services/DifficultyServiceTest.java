package services;


import domain.Difficulty;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.DifficultyDAO;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DifficultyServiceTest {

    @Mock
    DifficultyDAO difficultyDAO;

    @InjectMocks
    DifficultyService difficultyService;


    @Test
    public void emptyTest() {
    }

    @Test
    void testFindDifficultysTest() throws NoRecordFoundException {
        Difficulty difficulty = new Difficulty();
        difficulty.setName("Test");
        difficulty.setId(1);
        ArrayList<Difficulty> difficultys = new ArrayList<Difficulty>();
        difficultys.add(difficulty);
        when(difficultyDAO.findDifficultys()).thenReturn(difficultys);
        List<Difficulty> output = difficultyService.findAllDifficultys();
        Assertions.assertEquals(difficultys, output);
    }

    @Test
    void findDifficultysTestThrowsException() {
        when(difficultyDAO.findDifficultys()).thenReturn(new ArrayList<Difficulty>());
        Assertions.assertThrows(NoRecordFoundException.class, () -> {
            difficultyService.findAllDifficultys();
        });
    }
}
