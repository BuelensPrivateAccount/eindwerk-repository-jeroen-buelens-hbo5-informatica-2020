package services;


import domain.Game;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.GameDAO;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GameServiceTest {

    @Mock
    GameDAO gameDAO;

    @InjectMocks
    GameService gameService;


    @Test
    public void emptyTest() {
    }

    @Test
    public void testThrowsNoRecordFoundException() {
        when(gameDAO.findGameById(anyInt())).thenReturn(null);
        Assertions.assertThrows(NoRecordFoundException.class, () -> {
            gameService.findGameById(1);
        });

    }

    @Test
    public void testReturnsACategory() throws NoRecordFoundException {
        Game game = new Game();
        game.setGameName("Test");
        game.setId(1);
        when(gameDAO.findGameById(anyInt())).thenReturn(game);
        Assertions.assertEquals(game, gameService.findGameById(1));
        verify(gameDAO, times(1)).findGameById(anyInt());
    }

    @Test
    void testFindGameStartsWith() throws NoRecordFoundException {
        Game game = new Game();
        game.setGameName("Test");
        game.setId(1);
        ArrayList<Game> games = new ArrayList<Game>();
        games.add(game);
        when(gameDAO.findGamesWhereNamehas(anyString(), anyString())).thenReturn(games);
        when(gameDAO.findGamesWhereNameStartsWith(anyString(), anyString())).thenReturn(games);
        when(gameDAO.findGamesWhereNameEndsWith(anyString(), anyString())).thenReturn(games);
        Game output = gameService.findSpecificGameStartswith("blah", 0);
        Assertions.assertEquals(game, output);
        verify(gameDAO, times(1)).findGamesWhereNameStartsWith(anyString(), anyString());
        verify(gameDAO, times(1)).findGamesWhereNamehas(anyString(), anyString());
        verify(gameDAO, times(1)).findGamesWhereNameEndsWith(anyString(), anyString());

    }

    @Test
    void findGameStartsWithNoRecordExceptionTest() {
        ArrayList<Game> games = new ArrayList<>();
        when(gameDAO.findGamesWhereNamehas(anyString(), anyString())).thenReturn(games);
        when(gameDAO.findGamesWhereNameStartsWith(anyString(), anyString())).thenReturn(games);
        when(gameDAO.findGamesWhereNameEndsWith(anyString(), anyString())).thenReturn(games);
        Assertions.assertThrows(NoRecordFoundException.class, () -> {
            gameService.findSpecificGameStartswith("blah", 999999999);
        });
    }

    @Test
    void testFindGamesByDifficultyTest() throws NoRecordFoundException {
        Game game = new Game();
        game.setGameName("Test");
        game.setId(1);
        ArrayList<Game> games = new ArrayList<Game>();
        games.add(game);
        when(gameDAO.findGamesByDifficulty(anyInt())).thenReturn(games);
        List<Game> output = gameService.findGameByDifficulty(4);
        Assertions.assertEquals(games, output);
    }

    @Test
    void findGamesByDifficultyTestThrowsException() {
        when(gameDAO.findGamesByDifficulty(anyInt())).thenReturn(new ArrayList<Game>());
        Assertions.assertThrows(NoRecordFoundException.class, () -> {
            gameService.findGameByDifficulty(1);
        });
    }
    @Test
    void testFindGamesTest() throws NoRecordFoundException {
        Game game = new Game();
        game.setGameName("Test");
        game.setId(1);
        ArrayList<Game> games = new ArrayList<Game>();
        games.add(game);
        when(gameDAO.findGames()).thenReturn(games);
        List<Game> output = gameService.findAllGames();
        Assertions.assertEquals(games, output);
    }

    @Test
    void findGamesTestThrowsException() {
        when(gameDAO.findGames()).thenReturn(new ArrayList<Game>());
        Assertions.assertThrows(NoRecordFoundException.class, () -> {
            gameService.findAllGames();
        });
    }
}
