package repo;


import domain.Borrower;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class BorrowerDAOTest {
    private Connection con;
    private domain.Borrower borrower = new Borrower();
    private BorrowerDAO borrowerDAO = new BorrowerDAO();

    @BeforeEach
    public void connection() {
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        borrower.setId(1);
        borrower.setName("Jan Peeters");


    }


    @DisplayName("Assert that if i want the database to look up for the borrower with id 1, that it actually finds the one")
    @Test
    public void findBorrowerByIdReturnsAllBorrowerDetailsTest() {
        Borrower result = borrowerDAO.findBorrowerById(1);
        Assertions.assertEquals(borrower.getName(), result.getName());
    }

    @DisplayName("Find a borrower from the database where the name ends with \"eeters\" and check the database found the right one")
    @Test
    public void findBorrowersWhereNameEndsWithTest() {
        List<Borrower> list = borrowerDAO.findBorrowersWhereNameEndsWith("borrower_name", "eeters");
        Borrower pulledborrower = list.get(0);
        Assertions.assertEquals(borrower.getBorrowerName(), pulledborrower.getBorrowerName());
    }

    @Test
    public void findBorrowersWhereNameStartsWithTest() {
        List<Borrower> list = borrowerDAO.findBorrowersWhereNameStartsWith("borrower_name", "Jan");
        Borrower pulledborrower = list.get(0);
        Assertions.assertEquals(borrower.getBorrowerName(), pulledborrower.getBorrowerName());
    }
    @Test
    void findBorrowerWhereNameHasTest(){
        List<Borrower> list = borrowerDAO.findBorrowerWhereNameHas("borrower_name", "an Peet");
        Borrower pulledborrower = list.get(0);
        Assertions.assertEquals(borrower.getBorrowerName(), pulledborrower.getBorrowerName());
    }


}
