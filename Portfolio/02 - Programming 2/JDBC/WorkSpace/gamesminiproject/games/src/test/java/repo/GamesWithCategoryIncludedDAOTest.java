package repo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class GamesWithCategoryIncludedDAOTest {
    private Connection con;
    private GamesWithCategoryIncludedDAO gamesWithCategoryIncludedDAO = new GamesWithCategoryIncludedDAO();
    private String gameWithCategoryIncludedString = "My first Labyrinth" + "\t" + "combination";

    @BeforeEach
    public void connection() {
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    @Test
    public void EmptyTest() {
    }

    @DisplayName("Asserts that when you fetch a list of all gamename with category has the specified game in it")
    @Test
    public void testFindGamesHasGameSpecified() {
        List<String> list = gamesWithCategoryIncludedDAO.findGameNameWithCategoryName();
        //I spied in the database by running the SQL directly, and made a output string of the first game from the list, now i assert that this game and what i get from the list of output is the same
        String s = list.get(0);
        Assertions.assertEquals(gameWithCategoryIncludedString, s);
    }

    @DisplayName("Asserts that when i look for \"My first Labyrinth\" i actually find it ")
    @Test
    public void testFindStringWhereNameStartsWith() {
        List<String> list = gamesWithCategoryIncludedDAO.findStringsWhereNameStartsWith("game_name","My");
        String s = list.get(0);
        Assertions.assertEquals(gameWithCategoryIncludedString,s);
    }

    @DisplayName("Asserts that when i look for \"My first Labyrinth\" i actually find it ")
    @Test
    public void testFindStringWhereNameEndssWith() {
        List<String> list = gamesWithCategoryIncludedDAO.findStringsWhereNameEndsWith("game_name","labyrinth");
        String s = list.get(0);
        Assertions.assertEquals(gameWithCategoryIncludedString,s);
    }
    @DisplayName("Asserts that when i look for \"My first Labyrinth\" i actually find it ")
    @Test
    public void testFindStringWhereNameHas() {
        List<String> list = gamesWithCategoryIncludedDAO.findStringsWhereNamehas("game_name","rst Laby");
        String s = list.get(0);
        Assertions.assertEquals(gameWithCategoryIncludedString,s);
    }


}
