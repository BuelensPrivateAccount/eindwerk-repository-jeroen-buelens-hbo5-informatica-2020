package services;


import domain.Category;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.CategoryDAO;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

   @Mock
   CategoryDAO categoryDAO;

   @InjectMocks
   CategoryService categoryService;


   @Test
   public void emptyTest(){}

   @Test
    public void testThrowsNoRecordFoundException() {
       when(categoryDAO.findCategoryById(anyInt())).thenReturn(null);
       Assertions.assertThrows(NoRecordFoundException.class, () -> {
           categoryService.findCategoryById(1);
       });

   }

   @Test
   public void testReturnsACategory() throws NoRecordFoundException {
      Category category = new Category();
      category.setCategoryName("Test");
      category.setId(1);
      when(categoryDAO.findCategoryById(anyInt())).thenReturn(category);
      Assertions.assertEquals(category,categoryService.findCategoryById(1));
      verify(categoryDAO,times(1)).findCategoryById(anyInt());
   }

   @Test
   void testReturnsACategoryList() throws NoRecordFoundException{
      ArrayList<Category> categories = new ArrayList<>();
      Category category = new Category();
      category.setCategoryName("test");
      category.setId(1);
      categories.add(category);
      when(categoryDAO.findCategories()).thenReturn(categories);
      List<Category> output = categoryService.findAllCategories();
      Assertions.assertEquals(categories,output);
   }

   @Test
   void findAllCategoriesTestThrowsException(){
      when(categoryDAO.findCategories()).thenReturn(new ArrayList<Category>());
      Assertions.assertThrows(NoRecordFoundException.class,()->{
         categoryService.findAllCategories();
      });
   }



}
