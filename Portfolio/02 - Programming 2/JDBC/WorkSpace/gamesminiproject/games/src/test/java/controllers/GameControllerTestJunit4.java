package controllers;

import utilities.UtilRequestingInfo;
import domain.Borrower;
import domain.Difficulty;
import domain.Game;
import exceptions.NoRecordFoundException;
import org.junit.Test;


import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import services.BorrowWithBorrowerAndGameService;
import services.BorrowerService;
import services.DifficultyService;
import services.GameService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UtilRequestingInfo.class)
public class GameControllerTestJunit4 {
    @Mock
    private GameService gameService;
    @Mock
    private DifficultyService difficultyService;
    @Mock
    private BorrowerService borrowerService;
    @Mock
    private BorrowWithBorrowerAndGameService borrowWithBorrowerAndGameService;
    @InjectMocks
    private GameController gameController;

    @Test
    public void ExecuteOption4Test() throws NoRecordFoundException {
        mockStatic(UtilRequestingInfo.class);
        Game game = new Game();
        game.setId(1);
        game.setName("test");
        when(UtilRequestingInfo.requestSearchQuery()).thenReturn("test");
        when(gameService.findSpecificGameStartswith("test",0)).thenReturn(game);
        Game output = (Game) gameController.executeOption(4).get(0);
        Assertions.assertEquals(game,output);
        verify(gameService, times(1)).findSpecificGameStartswith(anyString(),anyInt());
    }
    @Test
    public void ExecuteOption4ThrowsExceptionTest() throws NoRecordFoundException {
        mockStatic(UtilRequestingInfo.class);
        Game game = new Game();
        game.setId(1);
        game.setName("test");
        when(UtilRequestingInfo.requestSearchQuery()).thenReturn("test");
        when(gameService.findSpecificGameStartswith("test",0)).thenThrow(NoRecordFoundException.class);
       Assertions.assertThrows(NoRecordFoundException.class,()->{
           gameController.executeOption(4);
       });
    }
    @Test
    public void ExecuteOption8Test()throws NoRecordFoundException{
        mockStatic(UtilRequestingInfo.class);
        Game game = new Game();
        game.setId(1);
        game.setName("test");
        List<Game> games = new ArrayList<>();
        games.add(game);
        when(UtilRequestingInfo.requestAIntFromUser()).thenReturn(1);
        when(gameService.findGameByDifficulty(1)).thenReturn(games);
        when(difficultyService.findAllDifficultys()).thenReturn(new ArrayList<Difficulty>());
        Game output = (Game) gameController.executeOption(8).get(0);
        Assertions.assertEquals(game,output);
        verify(gameService, times(1)).findGameByDifficulty(anyInt());
    }
    @Test
    public void ExecuteOption8ThrowsExceptionTest() throws NoRecordFoundException {
        mockStatic(UtilRequestingInfo.class);
        when(UtilRequestingInfo.requestAIntFromUser()).thenReturn(1);
        when(gameService.findGameByDifficulty(1)).thenThrow(NoRecordFoundException.class);
        when(difficultyService.findAllDifficultys()).thenReturn(new ArrayList<Difficulty>());
        Assertions.assertThrows(NoRecordFoundException.class,()->{
            gameController.executeOption(8);
        });
    }
    @Test
    public void ExecuteOption9Test() throws NoRecordFoundException {
        mockStatic(UtilRequestingInfo.class);
        Borrower borrower = new Borrower();
        borrower.setId(1);
        borrower.setName("test");
        List<Borrower> list = new ArrayList<>();
        list.add(borrower);
        when(UtilRequestingInfo.requestSearchQuery()).thenReturn("test");
        when(borrowerService.findBorrowersWithSearchTerm("test")).thenReturn(list);
        Borrower output = (Borrower) gameController.executeOption(9).get(0);
        Assertions.assertEquals(borrower,output);
        verify(borrowerService,times(1)).findBorrowersWithSearchTerm(anyString());
    }
    @Test
    public void ExecuteOption9TestThrowsException() throws NoRecordFoundException {
        mockStatic(UtilRequestingInfo.class);
        when(UtilRequestingInfo.requestSearchQuery()).thenReturn("test");
        when(borrowerService.findBorrowersWithSearchTerm("test")).thenThrow(NoRecordFoundException.class);
        Assertions.assertThrows(NoRecordFoundException.class,()->{
            gameController.executeOption(9);
        });
    }
    @Test
    public void ExecuteOption10Test() throws ParseException, NoRecordFoundException {
        mockStatic(UtilRequestingInfo.class);
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse("2017/01/01");
        List<String> list = new ArrayList<>();
        String s = "Ann Lenaerts" + "\t" + "Valkuil" + "\t" + "2018-04-22";
        list.add(s);
        when(UtilRequestingInfo.requestDateFromUser()).thenReturn(date);
        when(borrowWithBorrowerAndGameService.findBorrowersByBorrowDate(date)).thenReturn(list);
        String output = (String) gameController.executeOption(10).get(0);
        Assertions.assertEquals(s,output);
        verify(borrowWithBorrowerAndGameService,times(1)).findBorrowersByBorrowDate(any());


    }
    @Test
    public void ExecuteOption10TestThrowsException() throws NoRecordFoundException, ParseException {
        mockStatic(UtilRequestingInfo.class);
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse("2017/01/01");
        when(UtilRequestingInfo.requestDateFromUser()).thenReturn(date);
        when(borrowWithBorrowerAndGameService.findBorrowersByBorrowDate(date)).thenThrow(NoRecordFoundException.class);
        Assertions.assertThrows(NoRecordFoundException.class,()->{
            gameController.executeOption(10);
        });
    }

}
