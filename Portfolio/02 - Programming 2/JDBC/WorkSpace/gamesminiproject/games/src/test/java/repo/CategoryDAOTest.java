package repo;


import domain.Category;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class CategoryDAOTest {
    private Connection con;
    private domain.Category category = new Category();
    private CategoryDAO categoryDAO = new CategoryDAO();

    @BeforeEach
    public void connection() {
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        category.setId(1);
        category.setName("combination");

    }


    @Test
    public void findCategoryByIdReturnsAllCategoryDetailsTest() {
        Category result = categoryDAO.findCategoryById(1);
        Assertions.assertEquals(category.toString(), result.toString());
    }

    @Test
    public void findCategorysWhereNameEndsWithTest() {
        List<Category> list = categoryDAO.findCategoriesWhereNameEndsWith("category_name", "ombination");
        Category pulledcategory = list.get(0);
        Assertions.assertEquals(category.getCategoryName(),pulledcategory.getCategoryName());
    }

    @Test
    public void findCategorysWhereNameStartsWithTest() {
        List<Category> list = categoryDAO.findCategoriesWhereNameStartsWith("category_name", "comb");
        Category pulledcategory = list.get(0);
        Assertions.assertEquals(category.getCategoryName(),pulledcategory.getCategoryName());
    }
}
