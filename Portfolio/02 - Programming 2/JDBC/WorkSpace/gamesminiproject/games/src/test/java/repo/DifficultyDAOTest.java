package repo;


import domain.Difficulty;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DifficultyDAOTest {
    private Connection con;
    private domain.Difficulty difficulty = new Difficulty();
    private DifficultyDAO difficultyDAO = new DifficultyDAO();

    @BeforeEach
    public void connection() {
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        difficulty.setId(1);
        difficulty.setName("very easy");

    }


    @Test
    public void findDifficultyByIdReturnsAllDifficultyDetailsTest() {
        Difficulty result = new ArrayList<Difficulty>(difficultyDAO.findDifficultys()).get(0);
        Assertions.assertEquals(difficulty.getName(), result.getName());
    }

}
