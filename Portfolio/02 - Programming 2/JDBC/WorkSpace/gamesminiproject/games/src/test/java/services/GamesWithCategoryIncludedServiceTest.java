package services;



import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.GamesWithCategoryIncludedDAO;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GamesWithCategoryIncludedServiceTest {

    @Mock
    GamesWithCategoryIncludedDAO gamesWithCategoryIncludedDAO;

    @InjectMocks
    GamesWithCategoryIncludedService gamesWithCategoryIncludedService;


    @Test
    public void emptyTest() {
    }

    @Test
    void testFindGamesWithCategoryIncludedsTest() throws NoRecordFoundException {
        ArrayList<String> strings = new ArrayList<>();
        strings.add( "test");
        when(gamesWithCategoryIncludedDAO.findGameNameWithCategoryName()).thenReturn(strings);
        List<String> output = gamesWithCategoryIncludedService.findAllStrings();
        Assertions.assertEquals(strings,output);

    }

    @Test
    void findGamesWithCategoryIncludedsTestThrowsException() {
        when(gamesWithCategoryIncludedDAO.findGameNameWithCategoryName()).thenReturn(new ArrayList<String>());
        Assertions.assertThrows(NoRecordFoundException.class, () -> {
            gamesWithCategoryIncludedService.findAllStrings();
        });
    }
}
