package services;


import domain.Borrower;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.BorrowerDAO;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BorrowerServiceTest {

    @Mock
    BorrowerDAO borrowerDAO;

    @InjectMocks
    BorrowerService borrowerService;


    @Test
    public void emptyTest() {
    }

    @Test
    public void testThrowsNoRecordFoundException() {
        when(borrowerDAO.findBorrowerById(anyInt())).thenReturn(null);
        Assertions.assertThrows(NoRecordFoundException.class, () -> {
            borrowerService.findBorrowerById(1);
        });

    }

    @Test
    public void testReturnsABorrower() throws NoRecordFoundException {
        Borrower Borrower = new Borrower();
        Borrower.setBorrowerName("Test");
        Borrower.setId(1);
        when(borrowerDAO.findBorrowerById(anyInt())).thenReturn(Borrower);
        Assertions.assertEquals(Borrower, borrowerService.findBorrowerById(1));
        verify(borrowerDAO, times(1)).findBorrowerById(anyInt());
    }
@Test
    void testFindBorrowerStartsWith() throws NoRecordFoundException {
        Borrower borrower = new Borrower();
        borrower.setBorrowerName("Test");
        borrower.setId(1);
        ArrayList<Borrower> borrowers = new ArrayList<Borrower>();
        borrowers.add(borrower);
        when(borrowerDAO.findBorrowersWhereNameStartsWith(anyString(), anyString())).thenReturn(borrowers);
        Borrower output = borrowerService.findFirstBorrowerWithString("blah", 0);
        Assertions.assertEquals(borrower, output);
        verify(borrowerDAO, times(1)).findBorrowersWhereNameStartsWith(anyString(), anyString());
        verify(borrowerDAO, times(1)).findBorrowersWhereNameEndsWith(anyString(), anyString());

    }

    @Test
    void findBorrowerStartsWithNoRecordExceptionTest() {
        ArrayList<Borrower> borrowers = new ArrayList<>();
        when(borrowerDAO.findBorrowersWhereNameStartsWith(anyString(), anyString())).thenReturn(borrowers);
        Assertions.assertThrows(NoRecordFoundException.class, () -> {
            borrowerService.findFirstBorrowerWithString("blah", 9999);
        });
    }
@Test
    void findBorrowerWithSearchTermTest()throws NoRecordFoundException{
    Borrower borrower = new Borrower();
    borrower.setBorrowerName("Test");
    borrower.setId(1);
    ArrayList<Borrower> borrowers = new ArrayList<Borrower>();
    borrowers.add(borrower);
    when(borrowerDAO.findBorrowerWhereNameHas(anyString(),anyString())).thenReturn(borrowers);
    when(borrowerDAO.findBorrowersWhereNameStartsWith(anyString(),anyString())).thenReturn(borrowers);
    when(borrowerDAO.findBorrowersWhereNameEndsWith(anyString(),anyString())).thenReturn(borrowers);
    Borrower output = borrowerService.findBorrowersWithSearchTerm("test").get(0);
    Assertions.assertEquals(borrower,output);
    verify(borrowerDAO,times(1)).findBorrowersWhereNameStartsWith(anyString(),anyString());
    verify(borrowerDAO,times(1)).findBorrowersWhereNameEndsWith(anyString(),anyString());
    verify(borrowerDAO,times(1)).findBorrowerWhereNameHas(anyString(),anyString());
}
@Test
void findBorrowerWithSearchTermThrowsExceptionTest()throws NoRecordFoundException{
    ArrayList<Borrower> borrowers = new ArrayList<Borrower>();
    when(borrowerDAO.findBorrowerWhereNameHas(anyString(),anyString())).thenReturn(borrowers);
    Assertions.assertThrows(NoRecordFoundException.class,()->{
        borrowerService.findBorrowersWithSearchTerm("test");
    });

}

}
