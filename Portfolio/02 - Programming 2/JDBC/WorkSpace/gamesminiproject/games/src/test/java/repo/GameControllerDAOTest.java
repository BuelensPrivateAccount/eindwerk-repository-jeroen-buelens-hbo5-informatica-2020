package repo;


import domain.Game;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class GameControllerDAOTest {
    private Connection con;
    private domain.Game game = new Game();
    private GameDAO gameDAO = new GameDAO();

    @BeforeEach
    public void connection() {
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        game.setId(2);
        game.setName("Sabotage");
        game.setEditor("Franjos");
        game.setAuthor("Abbott Robert");
        game.setYearEdition(1996);
        game.setAge("as of 9y");
        game.setMinPlayers(2);
        game.setMaxPlayers(2);
        game.setCategoryID(12);
        game.setPlayDuration("16 min to 45 min");
        game.setDifficultyID(2);
        game.setPrice(17.50);
        game.setImage("sabotage.jpg");

    }

    @Test
    public void EmptyTest() {
    }

    @Test
    void findGamesTest(){
        List<Game> games = gameDAO.findGames();
       Game output = games.get(1);
       Assertions.assertEquals(game.toString(),output.toString());
    }
    @Test
    public void findGameByIdReturnsAllGameDetailsTest() {
        Game result = gameDAO.findGameById(2);
        Assertions.assertEquals(game.toString(), result.toString());
    }

    @Test
    public void findGamesWhereNameWithTest() {
        List<Game> list = gameDAO.findGamesWhereNamehas("game_name", "sabot");
        Game pulledgame = list.get(0);
            Assertions.assertEquals(game.getGameName(),pulledgame.getGameName());
    }

    @Test
    void testFindGamesWhereNameEndsWith(){
        List<Game> list = gameDAO.findGamesWhereNameEndsWith("game_name", "abotage");
        Game pulledgame = list.get(0);
        Assertions.assertEquals(game.getGameName(),pulledgame.getGameName());
    }
    @Test
    void testFindGamesWhereNameStartsWith(){
        List<Game> list = gameDAO.findGamesWhereNameStartsWith("game_name", "sabot");
        Game pulledgame = list.get(0);
        Assertions.assertEquals(game.getGameName(),pulledgame.getGameName());
    }

    @Test
    public void findgamesByDifficultytest(){
        List<Game> list = gameDAO.findGamesByDifficulty(2);
        Game pulledgame = list.get(0);
        Assertions.assertTrue(game.getDifficultyID() <= pulledgame.getDifficultyID());
    }


}
