package repo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BorrowWithBorrowerAndGameControllerDAOTest {

    private Connection con;
    private BorrowWithBorrowerAndGameDAO borrowWithBorrowerAndGameDAO = new BorrowWithBorrowerAndGameDAO();
    private String gamesWithCategoryIncludedString = "Ann Lenaerts" + "\t" + "Valkuil" + "\t" + "2018-04-22";

    @BeforeEach
    public void connection() {
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }


    @DisplayName("Assets that the list fetched has the right item on the first slot")
    @Test
    void findBorrowedGamesBorrowDateReturnDateAnd() {
        List<String> list = borrowWithBorrowerAndGameDAO.findBorrowedGamesBorrowDateReturnDateAnd();
        String s = list.get(0);
        Assertions.assertEquals(gamesWithCategoryIncludedString, s);
    }
}