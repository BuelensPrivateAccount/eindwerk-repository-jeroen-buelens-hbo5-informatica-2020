package services;



import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.BorrowWithBorrowerAndGameDAO;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BorrowWithBorrowerAndGameControllerServiceTest {

    @Mock
    BorrowWithBorrowerAndGameDAO borrowWithBorrowerAndGameDAO;

    @InjectMocks
    BorrowWithBorrowerAndGameService borrowWithBorrowerAndGameService;


    @Test
    public void emptyTest() {
    }

    @Test
    void testFindBorrowWithBorrowerAndGamesTest() throws NoRecordFoundException {
        ArrayList<String> strings = new ArrayList<>();
        strings.add( "test");
        when(borrowWithBorrowerAndGameDAO.findBorrowedGamesBorrowDateReturnDateAnd()).thenReturn(strings);
        List<String> output = borrowWithBorrowerAndGameService.findAllStrings();
        Assertions.assertEquals(strings,output);

    }

    @Test
    void findBorrowWithBorrowerAndGamesTestThrowsException() {
        when(borrowWithBorrowerAndGameDAO.findBorrowedGamesBorrowDateReturnDateAnd()).thenReturn(new ArrayList<String>());
        Assertions.assertThrows(NoRecordFoundException.class, () -> {
            borrowWithBorrowerAndGameService.findAllStrings();
        });
    }
}
