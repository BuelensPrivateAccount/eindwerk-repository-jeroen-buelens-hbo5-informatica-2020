package utilities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserInputHandlerTest {
    private UserInputHandler userInputHandler;

    @BeforeEach
    void setUp() {
    }

    @DisplayName("checks if when i ask for a int, it returns said int from the input")
    @Test
    void fetchIntFromUserTest() {
        userInputHandler = new UserInputHandler("1");
        int intFromUser = userInputHandler.fetchIntFromUser("Mijn error", "mijn query");
        Assertions.assertEquals(1, intFromUser);

    }
    @DisplayName("test that a input text returns said text")
    @Test
    void fetchStringFromUserTest(){
        userInputHandler = new UserInputHandler("Pineapple does not belong on pizza");
        String s = userInputHandler.fetchStringFromUser("","");
        Assertions.assertEquals("Pineapple does not belong on pizza",s);
    }

    @DisplayName("test that when i ask for a true he actually gets said boolean")
    @Test
    void fetchBooleanTrueFromUserTest(){
        userInputHandler = new UserInputHandler("YES");
        Boolean b = userInputHandler.fetchBooleanFromUser("","");
        Assertions.assertEquals(true,b);
    }
    @DisplayName("test that when i ask for a true he actually gets said boolean")
    @Test
    void fetchBooleanFalseFromUserTest(){
        userInputHandler = new UserInputHandler("NO");
        Boolean b = userInputHandler.fetchBooleanFromUser("","");
        Assertions.assertEquals(false,b);
    }

    @DisplayName("test that if i enter a Date, i get the right date from it")
    @Test
    void fetchDateFromUserTest() throws ParseException {
        userInputHandler = new UserInputHandler("1999/12/31");
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse("1999/12/31");
        Date output = userInputHandler.fetchDateFromUser("","");

    }
}