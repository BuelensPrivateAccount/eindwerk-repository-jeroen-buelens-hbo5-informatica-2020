package controllers;

import domain.Borrower;
import domain.Category;
import domain.Game;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import services.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class GameControllerTest {

    @Mock
    private CategoryService categoryService;
    @Mock
    private GameService gameService;
    @Mock
    private BorrowerService borrowerService;

    @InjectMocks
    GameController gameController;


    @Test
    void executeOption1Test() throws NoRecordFoundException {
        Category category = new Category();
        category.setId(1);
        category.setName("test");
        when(categoryService.findCategoryById(anyInt())).thenReturn(category);
        Category output = (Category) gameController.executeOption(1).get(0);
        assertEquals(category, output);
        verify(categoryService, times(1)).findCategoryById(anyInt());
    }

    @Test
    void ExecuteOption1ThrowsExceptionTest() throws NoRecordFoundException {
        when(categoryService.findCategoryById(anyInt())).thenThrow(NoRecordFoundException.class);
        assertThrows(NoRecordFoundException.class, () -> {
            gameController.executeOption(1);
        });
    }

    @Test
    void executeOption2Test() throws NoRecordFoundException {
        Game game = new Game();
        game.setId(1);
        game.setName("test");
        when(gameService.findGameById(anyInt())).thenReturn(game);
        Game output = (Game)  gameController.executeOption(2).get(0);
        assertEquals(game, output);
        verify(gameService, times(1)).findGameById(anyInt());
    }

    @Test
    void ExecuteOption2ThrowsExceptionTest() throws NoRecordFoundException {
        when(gameService.findGameById(anyInt())).thenThrow(NoRecordFoundException.class);
        assertThrows(NoRecordFoundException.class, () -> {
            gameController.executeOption(2);
        });
    }

    @Test
    void executeOption3Test() throws NoRecordFoundException {
        Borrower borrower = new Borrower();
        borrower.setId(1);
        borrower.setName("test");
        when(borrowerService.findBorrowerById(anyInt())).thenReturn(borrower);
        Borrower output = (Borrower) gameController.executeOption(3).get(0);
        assertEquals(borrower, output);
        verify(borrowerService, times(1)).findBorrowerById(anyInt());
    }

    @Test
    void ExecuteOption3ThrowsExceptionTest() throws NoRecordFoundException {
        when(borrowerService.findBorrowerById(anyInt())).thenThrow(NoRecordFoundException.class);
        assertThrows(NoRecordFoundException.class, () -> {
            gameController.executeOption(3);
        });
    }
    @Test
    void executeOption5Test() throws NoRecordFoundException {
        Game game = new Game();
        game.setId(1);
        game.setName("test");
        List<Game> list = new ArrayList<>();
        list.add(game);
        when(gameService.findAllGames()).thenReturn(list);
        Game output = (Game) gameController.executeOption(5).get(0);
        assertEquals(game, output);
        verify(gameService, times(1)).findAllGames();
    }

    @Test
    void ExecuteOption5ThrowsExceptionTest() throws NoRecordFoundException {
        when(gameService.findAllGames()).thenThrow(NoRecordFoundException.class);
        assertThrows(NoRecordFoundException.class, () -> {
            gameController.executeOption(5);
        });
    }

}