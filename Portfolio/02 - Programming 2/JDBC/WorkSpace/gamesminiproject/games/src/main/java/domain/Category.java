package domain;

/**
 * This is the Category Class, this is the java object belonging with the Category table on the database, with it's getters and setters
 */
public class Category extends DomainObject {
    private int id;
    private String categoryName;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return getCategoryName();
    }

    @Override
    public void setName(String s) {
        setCategoryName(s);
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}


