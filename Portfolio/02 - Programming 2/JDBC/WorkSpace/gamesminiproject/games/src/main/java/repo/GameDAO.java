package repo;

import domain.Game;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GameDAO {
    private static final String URL = "jdbc:mysql://localhost:3306/games";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    public List<Game> findGames() {
        List<Game> games = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.createStatement();
            statement.execute("SELECT * FROM game");
            ResultSet set = statement.getResultSet();
            while(set.next()){
                games.add(setToData(set));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return games;
    }

    private Game setToData(ResultSet set) throws SQLException{
        Game game = new Game();
            game.setId(set.getInt("id"));
            game.setGameName(set.getString("game_name"));
            game.setEditor(set.getString("editor"));
            game.setAuthor(set.getString("author"));
            game.setYearEdition(set.getInt("year_edition"));
            game.setAge(set.getString("age"));
            game.setMinPlayers(set.getInt("min_players"));
            game.setMaxPlayers(set.getInt("max_players"));
            game.setCategoryID(set.getInt("category_id"));
            game.setPlayDuration(set.getString("play_duration"));
            game.setDifficultyID(set.getInt("difficulty_id"));
            game.setPrice(set.getDouble("price"));
            game.setImage(set.getString("image"));
        return game;
    }

    public Game findGameById(int id) {
        Game game = new Game();
        Connection connection;
        PreparedStatement statement;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.prepareStatement("SELECT * FROM game WHERE id = ?");
            statement.setInt(1,id);
            statement.execute();
            ResultSet set = statement.getResultSet();
            if(set.next()){
                game = setToData(set);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return game;
    }

    public List<Game> findGamesWhereNameStartsWith(String columname,String begin){
        begin = begin + "%";
        return findGamesWhereNameStartsOREndsWith(columname, begin);
    }

    public List<Game> findGamesWhereNameEndsWith(String columname,String end){
        end = "%"+end;
        return findGamesWhereNameStartsOREndsWith(columname, end);
    }
    public List<Game> findGamesWhereNamehas(String columname,String text){
        text = "%"+text+"%";
        return findGamesWhereNameStartsOREndsWith(columname, text);
    }

    private List<Game> findGamesWhereNameStartsOREndsWith(String columname,String where){
        List<Game> games = new ArrayList<>();
        Connection connection;
        PreparedStatement statement;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.prepareStatement("SELECT * FROM Game WHERE "+columname+ " LIKE ?");
            statement.setString(1,where);
            statement.execute();
            ResultSet set = statement.getResultSet();
            if(set.next()){
                games.add(setToData(set));
            }
            while(set.next()){
                games.add(setToData(set));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return games;
    }

    public List<Game> findGamesByDifficulty(int difficulty){
        List<Game> output = new ArrayList<>();
        Connection connection;
        PreparedStatement statement;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.prepareStatement("SELECT * FROM game WHERE difficulty_id >= ?");
            statement.setInt(1,difficulty);
            statement.execute();
            ResultSet set = statement.getResultSet();
            if(set.next()){
                output.add(setToData(set));
            }
            while(set.next()){
                output.add(setToData(set));
            }
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return output;
    }

}




