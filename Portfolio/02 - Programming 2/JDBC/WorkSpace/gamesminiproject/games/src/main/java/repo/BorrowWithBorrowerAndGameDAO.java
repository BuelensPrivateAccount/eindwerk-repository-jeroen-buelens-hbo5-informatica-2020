/*I ran into Datbase connection issues if i tried to fetch each table individually and work it out in program, this is why i made extra DAO's to do it with inner joins*/

package repo;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BorrowWithBorrowerAndGameDAO {

    private static final String URL = "jdbc:mysql://localhost:3306/games";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    public List<String> findBorrowedGamesBorrowDateReturnDateAnd() {
        Connection connection;
        Statement statement;
        List<String> s = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.createStatement();
            statement.execute("SELECT B.borrow_date,\n" +
                    "                    B.return_date,\n" +
                    "                    A.borrower_name,\n" +
                    "                    G.game_name\n" +
                    "                    from borrow B\n" +
                    "                    INNER JOIN game G\n" +
                    "                    ON B.game_id = G.id\n" +
                    "                    INNER JOIN borrower A\n" +
                    "                    On A.id = B.borrower_id\n" +
                    "                    ORDER BY A.borrower_name,B.borrow_date");
            ResultSet set = statement.getResultSet();
            if(set.next()){
                s.add(setToData(set));
            }
            while (set.next()) {
                s.add(setToData(set));
            }
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return s;
    }

    private String setToData(ResultSet set) throws SQLException {
        String s = set.getString("return_date");
        String returnValue = set.getString("borrower_name")
                + "\t"
                +  set.getString("game_name")
                +"\t"
                +set.getDate("borrow_date");
        if(s != null){
            returnValue += "\t"+s;
        }
        return returnValue;
    }

    public List<String> findBorrowerByBorrowDate(String borrowDate, Date case10Date) {
        Connection connection = null;
        PreparedStatement statement = null;
        List<String> s = new ArrayList<>();
        java.sql.Date dateSql = new java.sql.Date(case10Date.getTime());
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.prepareStatement("SELECT B.borrow_date,\n" +
                    "                    B.return_date,\n" +
                    "                    A.borrower_name,\n" +
                    "                    G.game_name\n" +
                    "                    from borrow B\n" +
                    "                    INNER JOIN game G\n" +
                    "                    ON B.game_id = G.id\n" +
                    "                    INNER JOIN borrower A\n" +
                    "                    On A.id = B.borrower_id\n" +
                    "                   Where "+borrowDate+" > ?");
            statement.setDate(1,dateSql);
            statement.execute();
            ResultSet set = statement.getResultSet();
            if(set.next()){
                s.add(setToData(set));
            }
            while (set.next()) {
                s.add(setToData(set));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return s;
    }
}
