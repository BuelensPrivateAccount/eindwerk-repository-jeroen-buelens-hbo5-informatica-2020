package controllers;

import domain.Borrower;
import domain.Game;
import exceptions.NoRecordFoundException;
import services.*;

import java.util.*;

import static utilities.UtilRequestingInfo.*;

/**
 * this is the controller of minigamesproject
 */
public class GameController {

    private Map<Integer, String> menuMap = new HashMap<>();

    private CategoryService categoryService;
    private GameService gameService;
    private BorrowerService borrowerService;
    private GamesWithCategoryIncludedService gamesWithCategoryIncludedService;
    private BorrowWithBorrowerAndGameService borrowWithBorrowerAndGameService;
    private DifficultyService difficultyService;

    /**
     * GameController constructor creates a object of the controller with all the services the controller uses.
     * @param categoryService this is a instance of the service class categoryService
     * @param gameService this is a instance of the service class GameService
     * @param borrowerService this is a instance of the service class BorrowerService
     * @param gamesWithCategoryIncludedService this is a instance of the class GamesWithCategoryIncludedService
     * @param borrowWithBorrowerAndGameService this is a instance of the class BorrowWithBorrowerAndGameService
     * @param difficultyService this is a instance of the class DifficultyService
     */
    public GameController(CategoryService categoryService, GameService gameService, BorrowerService borrowerService, GamesWithCategoryIncludedService gamesWithCategoryIncludedService, BorrowWithBorrowerAndGameService borrowWithBorrowerAndGameService, DifficultyService difficultyService) {
        createMenu();
        this.categoryService = categoryService;
        this.gameService = gameService;
        this.borrowerService = borrowerService;
        this.gamesWithCategoryIncludedService = gamesWithCategoryIncludedService;
        this.borrowWithBorrowerAndGameService = borrowWithBorrowerAndGameService;
        this.difficultyService = difficultyService;
    }

    /**
     * createMenu is a part of the constructor to fill menuMap with the options to choose
     */
    private void createMenu() {
        menuMap.put(1, "show first category");
        menuMap.put(2, "show 5th game");
        menuMap.put(3, "show the first borrower");
        menuMap.put(4, "show a game of your choice ");
        menuMap.put(5, "show all games");
        menuMap.put(6, "show games and let users pick one");
        menuMap.put(7, "show borrowed games");
        menuMap.put(8, "Advanced search: difficulty");
        menuMap.put(9, "Complex search: Borrowers");
        menuMap.put(10, "show all borrowers who borrowed since \"Date\"");
    }

    /**
     * this is the boot-section of the controller, this shows a opening printout and starts the method:
     */
    public void startGame() {
        System.out.println("===================================");
        System.out.println("Hello,  This APP was made by Jeroen");
        System.out.println("===================================");
        System.out.println("=-=-=-=-=-= Game Options =-=-=-=-=-=");
        runGame();
    }

    /**
     * this is the final method that will be ran by the gameController, this exists to properly close off the project.
     */
    private void stopGame() {
        closeScanner();
    }

    /**
     * this is the Body of the application, this is what will run every time until the user wishes to exit the application
     */
    private synchronized void runGame() {
        boolean b = true;
        while (b) {
            try {
                showMenu();
                int i = chooseOption();
                List<Object> objectList = executeOption(i);
                printObjectList(objectList);
            } catch (NoRecordFoundException e) {
                e.printUserFriendlyMessage();
            } finally {
                boolean conti = requestContinue();
                if (!conti) {
                    stopGame();
                    b = false;
                }
            }
        }

    }

    /**
     * this prints the menu created in: createMenu();
     * @see GameController#createMenu()
     */
    private void showMenu() {
        for (Map.Entry<Integer, String> m : menuMap.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
    }

    /**
     * this executes the option given with runGame();
     * @see GameController#runGame()
     * @param option this is the integer given by the RunGame method to choose what option of the switchcase you need to run
     * @return objectList this is a list with the objects created in the method, returned to be printed out later
     * @throws NoRecordFoundException {@link NoRecordFoundException} this exception will be thrown when the JDBC can't find a record of what the program was trying to look up.
     */
    List<Object> executeOption(int option) throws NoRecordFoundException { //protected so i can call in tests
        List<Object> objectsList = new ArrayList<>();
        switch (option) {
            case 1:
                objectsList.add(categoryService.findCategoryById(1));
                break;
            case 2:
                objectsList.add(gameService.findGameById(5));
                break;
            case 3:
                objectsList.add(borrowerService.findBorrowerById(5));
                break;
            case 5:
                List<Game> case5List = gameService.findAllGames();
                for (Game case5game : case5List) {
                    objectsList.add(case5game);
                }
                break;
            case 6:
                List<String> case6List = gamesWithCategoryIncludedService.findAllStrings();
                for (String case6String : case6List) {
                    objectsList.add(case6String);
                }
                printObjectList(objectsList);
                objectsList = new ArrayList<>();
            case 4:
                objectsList = findSpecificGameStartWith(objectsList);
                break;
            /*
              Case 7 had a weird printout, and this is Legacy from when this printout was still done in the switch case.
              unfortunately this printout could not be lifted like the rest of them and stays here, safe.
             */
            case 7:
                List<String> case7StringList = borrowWithBorrowerAndGameService.findAllStrings();
                System.out.printf("%-50s %-50s %-50s %s\n", "Name", "Game Name", "Borrow Date", "Return Date");
                for (String case7String : case7StringList) {
                    String[] case7strings = case7String.split("\t");
                    if (case7strings.length < 4) {
                        System.out.printf("%-50s %-50s %s\n", case7strings[0], case7strings[1], case7strings[2]);
                    } else {
                        System.out.printf("%-50s %-50s %-50s %s\n", case7strings[0], case7strings[1], case7strings[2], case7strings[3]);
                    }
                }
                if (requestfindBorrower()) {
                    objectsList = findSpecificGameStartWith(objectsList);
                }
                break;
            case 8:
                findDifficulties();
                System.out.println("please enter the number of the difficulty you want to limit by");
                int case8int = requestAIntFromUser();
                List<domain.Game> case8GamesList = gameService.findGameByDifficulty(case8int);
                for (domain.Game case8game : case8GamesList) {
                    objectsList.add(case8game);
                }
                break;
            case 9:
                String case9String = requestSearchQuery();
                List<Borrower> case9List = borrowerService.findBorrowersWithSearchTerm(case9String);
                for (Borrower case9Borrower : case9List) {
                    objectsList.add(case9Borrower);
                }
                break;
            case 10:
                Date case10Date = requestDateFromUser();
                List<String> case10List = borrowWithBorrowerAndGameService.findBorrowersByBorrowDate(case10Date);
                for (String case10String : case10List) {
                    objectsList.add(case10String);
                }
                break;
            default:
                System.err.println("This number was not a option");
                chooseOption();
        }
        return objectsList;
    }

    /**
     * this is a method created as it needs to be called multiple times in multiple cases in the switch case
     * @see GameController#executeOption(int)
     * @param objectList this is the objectList created in executeOption and returned to it.
     * @return List of {@link Object}
     * @throws NoRecordFoundException {@link NoRecordFoundException} this exception will be thrown when the JDBC can't find a record of what the program was trying to look up.
     */
    private List<Object> findSpecificGameStartWith(List<Object> objectList) throws NoRecordFoundException {
        String s = requestSearchQuery();
        objectList.add(gameService.findSpecificGameStartswith(s, 0));
        return objectList;
    }

    /**
     * this prints out everything in the objectList given to it.
     * @param objectsList this is the objectList generated in
     * @see GameController#executeOption(int)
     */
    private void printObjectList(List<Object> objectsList) {
        if (!objectsList.isEmpty()) {
            for (Object o : objectsList) {
                System.out.println(o);
            }
        }
    }

    /**
     * findDifficulties is a Legacy method used in one of the switch cases, as it does this than something else.
     * @throws NoRecordFoundException {@link NoRecordFoundException} this exception will be thrown when the JDBC can't find a record of what the program was trying to look up.
     */
    private void findDifficulties() throws NoRecordFoundException {
        List<domain.Difficulty> difficulties = difficultyService.findAllDifficultys();
        for (domain.Difficulty difficulty : difficulties) {
            System.out.println(difficulty.toString());
        }
    }

}
