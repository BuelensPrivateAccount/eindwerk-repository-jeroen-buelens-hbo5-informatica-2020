package repo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import domain.Difficulty;
public class DifficultyDAO {

        private static final String URL = "jdbc:mysql://localhost:3306/games";
        private static final String USERNAME = "root";
        private static final String PASSWORD = "";

        public List<Difficulty> findDifficultys() {
            List<Difficulty> difficultys = new ArrayList<>();
            Connection connection =null;
            Statement statement = null;
            try {
                connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
                statement = connection.createStatement();
                statement.execute("SELECT * FROM difficulty");
                ResultSet set = statement.getResultSet();
                while(set.next()){
                    difficultys.add(setToData(set));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    statement.close();
                    connection.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            return difficultys;
        }

    private Difficulty setToData(ResultSet set) throws SQLException {
            Difficulty difficulty = new Difficulty();
            difficulty.setId(set.getInt("id"));
            difficulty.setName(set.getString("difficulty_name"));
            return difficulty;
    }


}
