package services;

import domain.Category;
import exceptions.NoRecordFoundException;
import repo.CategoryDAO;

import java.util.List;

public class CategoryService {

    private CategoryDAO categoryDAO;

    public CategoryService(CategoryDAO categoryDAO){
        this.categoryDAO = categoryDAO;

    }

    public Category findCategoryById(int id) throws NoRecordFoundException {
      Category category =  this.categoryDAO.findCategoryById(id);
        if(category ==null){
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }
        return category;
    }


    public List<Category> findAllCategories() throws NoRecordFoundException {

        if (!categoryDAO.findCategories().isEmpty()) {
            return categoryDAO.findCategories();
        } else {
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }
    }
}
