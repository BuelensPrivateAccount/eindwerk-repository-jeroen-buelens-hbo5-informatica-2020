package services;

import domain.Borrower;
import exceptions.NoRecordFoundException;
import repo.BorrowerDAO;

import java.util.Date;
import java.util.List;


public class BorrowerService {

    private BorrowerDAO borrowerDAO;

    public BorrowerService(BorrowerDAO borrowerDAO) {
        this.borrowerDAO = borrowerDAO;

    }


    public Borrower findBorrowerById(int id) throws NoRecordFoundException {
        Borrower borrower = this.borrowerDAO.findBorrowerById(id);
        if (borrower == null) {
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }
        return borrower;
    }

    public Borrower findFirstBorrowerWithString(String text, int index) throws NoRecordFoundException {
        String borrowerName = "borrower_name";
        Borrower borrower;
        List<Borrower> listEnd = this.borrowerDAO.findBorrowersWhereNameEndsWith(borrowerName, text);
        List<Borrower> listMiddle = this.borrowerDAO.findBorrowerWhereNameHas(borrowerName, text);
        List<Borrower> listStart = this.borrowerDAO.findBorrowersWhereNameStartsWith(borrowerName, text);
        if (listStart.size() >= index) {
            borrower = listStart.get(index);
        } else if (listMiddle.size() >= index) {
            borrower = listMiddle.get(index);
        } else if (listEnd.size() >= index) {
            borrower = listEnd.get(index);
        } else {
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }
        return borrower;
    }

    public List<Borrower> findBorrowersWithSearchTerm(String search) throws NoRecordFoundException {
        String borrowerName = "borrower_name";
        List<Borrower> listEnd = this.borrowerDAO.findBorrowersWhereNameEndsWith(borrowerName, search);
        List<Borrower> listMiddle = this.borrowerDAO.findBorrowerWhereNameHas(borrowerName, search);
        List<Borrower> listStart = this.borrowerDAO.findBorrowersWhereNameStartsWith(borrowerName, search);
        if (!listMiddle.isEmpty()) {
            return listMiddle;
        } else if (!listStart.isEmpty()) {
            return listStart;
        } else if (!listEnd.isEmpty()) {
            return listEnd;
        } else {
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }
    }



}
