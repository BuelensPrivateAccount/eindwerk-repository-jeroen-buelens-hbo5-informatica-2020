package services;

import domain.Game;
import exceptions.NoRecordFoundException;
import repo.GameDAO;

import java.util.*;

public class GameService {

    private GameDAO gameDAO;

    public GameService(GameDAO gameDAO) {
        this.gameDAO = gameDAO;

    }


    public Game findGameById(int id) throws NoRecordFoundException {
        Game game = this.gameDAO.findGameById(id);
        if (game == null) {
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }
        return game;
    }

    public Game findSpecificGameStartswith(String text, int index) throws NoRecordFoundException {
        String columnName = "game_name";
        Game game;
        List<Game> listMiddle = this.gameDAO.findGamesWhereNamehas(columnName, text);
        List<Game> listEnd = this.gameDAO.findGamesWhereNameStartsWith(columnName, text);
        List<Game> listStart = this.gameDAO.findGamesWhereNameEndsWith(columnName, text);
        if (listMiddle.size() >= index+1) {
            game = listMiddle.get(index);
        } else if (listStart.size() >= index+1) {
            game = listStart.get(index);
        } else if (listEnd.size() >= index+1) {
            game = listEnd.get(index);
        } else {
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }
        return game;
    }

    public List<Game> findAllGames() throws NoRecordFoundException {

        if (!gameDAO.findGames().isEmpty()) {
            return gameDAO.findGames();
        } else {
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }
    }

    public List<Game> findGameByDifficulty(int difficulty) throws NoRecordFoundException {
        if (gameDAO.findGamesByDifficulty(difficulty).size() != 0) {
            return gameDAO.findGamesByDifficulty(difficulty);
        } else {
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }
    }
}