package application;

import controllers.GameController;
import repo.*;
import services.*;

/**
 * Java Documentation on GamesApp
 * @author Jeroen Buelens
 * @version 1.0
 */
public class GamesApp {

    public static void main(String[] args) {

        CategoryDAO categoryDAO = new CategoryDAO();
        CategoryService categoryService = new CategoryService(categoryDAO);
        GameDAO gameDAO = new GameDAO();
        GameService gameService = new GameService(gameDAO);
        BorrowerDAO borrowerDAO = new BorrowerDAO();
        BorrowerService borrowerService = new BorrowerService(borrowerDAO);
        GamesWithCategoryIncludedDAO gamesWithCategoryIncludedDAO = new GamesWithCategoryIncludedDAO();
        GamesWithCategoryIncludedService gamesWithCategoryIncludedService = new GamesWithCategoryIncludedService(gamesWithCategoryIncludedDAO);
        BorrowWithBorrowerAndGameDAO borrowWithBorrowerAndGameDAO = new BorrowWithBorrowerAndGameDAO();
        BorrowWithBorrowerAndGameService borrowWithBorrowerAndGameService = new BorrowWithBorrowerAndGameService(borrowWithBorrowerAndGameDAO);
        DifficultyDAO difficultyDAO = new DifficultyDAO();
        DifficultyService difficultyService = new DifficultyService(difficultyDAO);
        GameController gameController = new GameController(categoryService, gameService, borrowerService, gamesWithCategoryIncludedService, borrowWithBorrowerAndGameService, difficultyService);
        gameController.startGame();

    }
}
