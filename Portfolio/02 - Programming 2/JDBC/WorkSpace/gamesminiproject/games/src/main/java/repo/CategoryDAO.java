package repo;


import domain.Category;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CategoryDAO {
    private static final String URL = "jdbc:mysql://localhost:3306/games";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    public List<Category> findCategories() {
        List<Category> categories = new ArrayList<>();
        Connection connection =null;
        Statement statement = null;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.createStatement();
            statement.execute("SELECT * FROM category");
            ResultSet set = statement.getResultSet();
            while (set.next()) {
                categories.add(setToData(set));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return categories;
    }

    public Category findCategoryById(int id) {
        Category category = new Category();
        Connection connection;
        PreparedStatement statement;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.prepareStatement("SELECT * FROM category WHERE id = ?");
            statement.setInt(1, id);
            statement.execute();
            ResultSet set = statement.getResultSet();
            if (set.next()) {
                category = setToData(set);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return category;
    }

    private Category setToData(ResultSet set) throws SQLException {
        Category category = new Category();
        category.setId(set.getInt("id"));
        category.setCategoryName(set.getString("category_name"));
        return category;
    }

    public List<Category> findCategoriesWhereNameStartsWith(String columname,String begin){
        begin = begin + "%";
        return findCategoriesWhereNameStartsOREndsWith(columname, begin);
    }

    public List<Category> findCategoriesWhereNameEndsWith(String columname,String end){
        end = "%"+end;
        return findCategoriesWhereNameStartsOREndsWith(columname, end);
    }

    private List<Category> findCategoriesWhereNameStartsOREndsWith(String columname,String where){
        List<Category> categories = new ArrayList<>();
        Connection connection;
        PreparedStatement statement;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.prepareStatement("SELECT * FROM Category WHERE "+columname+" LIKE ?");
            statement.setString(1,where);
            statement.execute();
            ResultSet set = statement.getResultSet();
            if(set.next()){
                categories.add(setToData(set));
            }
            while(set.next()){
                categories.add(setToData(set));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categories;
    }

}





