package utilities;

import java.util.Date;

public class UtilRequestingInfo {
    private UtilRequestingInfo() {
    }

    public static boolean requestContinue() {
        return UserInputHandler.fetchBooleanFromUser("you have not entered a valid input, please enter \"Yes\" or \"No\"", "Do you want to continue?");
    }

    public static int requestID() {
        return UserInputHandler.fetchIntFromUser("you have not entered a valid input, please enter a valid input", "what userID ae you looking for?");
    }

    public  static int chooseOption() {
        return UserInputHandler.fetchIntFromUser("You have not entered a valid input, please enter a number", "Please choose a option");

    }
    public  static String requestSearchQuery() {
        return UserInputHandler.fetchStringFromUser("You have not entered a valid input","please enter the \"text\" you're looking for");

    }
    public static int requestAIntFromUser(){
        return UserInputHandler.fetchIntFromUser("You have not entered a valid input, please enter a number","please enter the \"number\" you're looking for");
    }

    public static boolean requestfindBorrower(){
        return UserInputHandler.fetchBooleanFromUser("you have not entered a valid input, please enter \"Yes\" or \"No\"", "Do you want to find a specific borrower?");
    }
    public static void closeScanner(){
        UserInputHandler.closeScannerMethod();
    }
    public static Date requestDateFromUser(){
        return  UserInputHandler.fetchDateFromUser("You have not entered a valid input, please enter a date with the format\"yyyy/MM/dd\"","please enter a date [UseFormat: \"yyyy/MM/dd\"]");
    }
}
