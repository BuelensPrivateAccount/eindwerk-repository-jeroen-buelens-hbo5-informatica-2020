package domain;

/**
 * this is the interface that dictates that all domain objects need to have getters and setters for iD and a Name
 */
public interface DomainInterface {
    int getId();
    void setId(int id);
    String getName();
    void setName(String s);
}
