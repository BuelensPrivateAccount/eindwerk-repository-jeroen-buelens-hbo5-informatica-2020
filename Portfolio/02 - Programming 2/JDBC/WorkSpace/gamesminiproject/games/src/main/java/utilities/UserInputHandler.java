package utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

class UserInputHandler {
    private static Scanner scanner;
    private  static String input ="";

     UserInputHandler(String input) {// only for test
        UserInputHandler.scanner = new Scanner(input);
        this.input = input;
    }

    private static void initScanner() {
        if (UserInputHandler.scanner == null || (input.isEmpty()) ) {
            UserInputHandler.scanner = new Scanner(System.in);
        }
    }

     static int fetchIntFromUser(String errorMessage, String userQuery) {
        while(true) {
            initScanner();
            try {
                System.out.println(userQuery);
                return scanner.nextInt();
            } catch (InputMismatchException e) {
                System.err.println(errorMessage);
            }
        }
    }

     static String fetchStringFromUser(String errorMessage, String userQuery) {
         while(true) {
             initScanner();
             System.out.print(userQuery);
             String input = scanner.nextLine();
             if (input.isEmpty()) {
                 System.err.println(errorMessage);
             } else {
                 return input;
             }
         }
    }

     static Boolean fetchBooleanFromUser(String errorMessage, String userQuery) {
        while(true) {
            String s = fetchStringFromUser(errorMessage, userQuery);
            if (s.toUpperCase().contains("YES") || s.toUpperCase().contains("Y") || s.toUpperCase().contains("1")) {
                return true;
            } else if (s.toUpperCase().contains("NO") || s.toUpperCase().contains("N") || s.toUpperCase().contains("0")) {
                return false;
            } else {
                System.err.println(errorMessage);
            }
        }

    }

     static Date fetchDateFromUser(String errorMessage, String userQuery) {
        while(true) {
            String s = fetchStringFromUser(errorMessage, userQuery);
            try {
                return new SimpleDateFormat("yyyy/MM/dd").parse(s);
            } catch (InputMismatchException | ParseException e) {
                System.err.println(errorMessage);
            }
        }
    }

     static void closeScannerMethod() {
        if (scanner != null)
        {
            scanner.reset();
            scanner.close();
        }
    }

}
