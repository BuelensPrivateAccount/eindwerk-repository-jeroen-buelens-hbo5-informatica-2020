package services;

import domain.Borrower;
import exceptions.NoRecordFoundException;

import java.util.Date;
import java.util.List;
import repo.BorrowWithBorrowerAndGameDAO;

public class BorrowWithBorrowerAndGameService {
    repo.BorrowWithBorrowerAndGameDAO borrowWithBorrowerAndGameDAO;

    public BorrowWithBorrowerAndGameService(BorrowWithBorrowerAndGameDAO borrowWithBorrowerAndGameDAO) {
        this.borrowWithBorrowerAndGameDAO = borrowWithBorrowerAndGameDAO;

    }

    public List<String> findAllStrings() throws NoRecordFoundException {

        if (!borrowWithBorrowerAndGameDAO.findBorrowedGamesBorrowDateReturnDateAnd().isEmpty()) {
            return borrowWithBorrowerAndGameDAO.findBorrowedGamesBorrowDateReturnDateAnd();
        } else {
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }
    }
        public List<String> findBorrowersByBorrowDate(Date case10Date) throws NoRecordFoundException {
            String borrowDate = "borrow_date";
            List<String> list = this.borrowWithBorrowerAndGameDAO.findBorrowerByBorrowDate(borrowDate, case10Date);
            if (!list.isEmpty()) {
                return list;
            } else {
                throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
            }
        }
}
