/*I ran into Database connection issues if i tried to fetch each table individually and work it out in program, this is why i made extra DAO's to do it with inner joins*/

package repo;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GamesWithCategoryIncludedDAO {

    private static final String URL = "jdbc:mysql://localhost:3306/games";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    public List<String> findGameNameWithCategoryName() {
        Connection connection;
        Statement statement;
        List<String> s = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.createStatement();
            statement.execute("SELECT G.game_name, C.category_name\n" +
                    "    FROM game G\n" +
                    "    INNER JOIN category C\n" +
                    "        ON C.id = G.category_id");
            ResultSet set = statement.getResultSet();
            while (set.next()) {
                s.add(setToData(set));
            }
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return s;
    }

    private String setToData(ResultSet set) throws SQLException {
        return set.getString("game_name") + "\t" + set.getString("category_name");
    }

    public List<String> findStringsWhereNameStartsWith(String columname, String begin) {
        begin = begin + "%";
        return findStringsWhereNameStartsOREndsWith(columname, begin);
    }

    public List<String> findStringsWhereNameEndsWith(String columname, String end) {
        end = "%" + end;
        return findStringsWhereNameStartsOREndsWith(columname, end);
    }

    public List<String> findStringsWhereNamehas(String columname, String text) {
        text = "%" + text + "%";
        return findStringsWhereNameStartsOREndsWith(columname, text);
    }

    private List<String> findStringsWhereNameStartsOREndsWith(String columname, String where) {
        List<String> strings = new ArrayList<>();
        Connection connection;
        PreparedStatement statement;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.prepareStatement("SELECT G.game_name, C.category_name\n" +
                    "FROM game G\n" +
                    "INNER JOIN category C\n" +
                    "ON C.id = G.category_id" + " WHERE "+columname+" LIKE ?");
            statement.setString(1, where);
            statement.execute();
            ResultSet set = statement.getResultSet();
            while (set.next()) {
                strings.add(setToData(set));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return strings;
    }
}
