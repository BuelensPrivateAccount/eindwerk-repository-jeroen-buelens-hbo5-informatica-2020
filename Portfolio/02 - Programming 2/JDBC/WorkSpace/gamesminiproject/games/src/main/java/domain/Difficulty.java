package domain;
/**
 * This is the Difficulty Class, this is the java object belonging with the Difficulty table on the database, with it's getters and setters
 */
public class Difficulty extends DomainObject{
    private int id;
    private String name;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Difficulty{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
