package domain;

/**
 * This is the Abstract method that all other domain objects extend to implement the interface.
 */
abstract class DomainObject implements DomainInterface {

}
