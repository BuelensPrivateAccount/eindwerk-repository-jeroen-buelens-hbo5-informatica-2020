package repo;

import domain.Borrower;


import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This is the Database Access Object for the Borrower table
 * @see Borrower
 */
public class BorrowerDAO {
    private static final String URL = "jdbc:mysql://localhost:3306/games";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    /**
     * this looks on the database to find all borrowers on the table with all their rows.
     * this method uses the setToData Method to actually process the data from the set to something fitting in a {@link Borrower} Object.
     * @return List of {@link Borrower}
     * @see  BorrowerDAO#setToData(ResultSet)
     */
    public List<Borrower> findBorrowers() {
        List<Borrower> borrowers = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.createStatement();
            statement.execute("SELECT * FROM borrower");
            ResultSet set = statement.getResultSet();
            while (set.next()) {
                borrowers.add(setToData(set));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return borrowers;
    }

    /**
     * This method will take a {@link com.mysql.cj.protocol.Resultset} and returns it as a {@link Borrower}
     * @param set this is the set fetched from the data this will parse to a {@link Borrower}
     * @return {@link Borrower}
     * @throws SQLException this exception will be thrown to the method above in case there's a issue with the contains of the set
     */
    private Borrower setToData(ResultSet set) throws SQLException {
        Borrower borrower = new Borrower();
        borrower.setId(set.getInt("id"));
        borrower.setBorrowerName(set.getString("borrower_name"));
        borrower.setStreet(set.getString("street"));
        borrower.setHouseNumber(set.getString("house_number"));
        borrower.setBusNumber(set.getString("bus_number"));
        borrower.setPostcode(set.getInt("postcode"));
        borrower.setCity(set.getString("city"));
        borrower.setTelephone(set.getString("telephone"));
        borrower.setEmail(set.getString("email"));
        return borrower;
    }

    /**
     * this method takes a {@link Integer} to add to the SQL Query and return a {@link Borrower}
     * @param id this is the {@link Integer} id of the borrower you want
     * @return {@link Borrower}
     */
    public Borrower findBorrowerById(int id) {
        Borrower borrower = new Borrower();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.prepareStatement("SELECT * FROM borrower WHERE id = ?");
            statement.setInt(1, id);
            statement.execute();
            ResultSet set = statement.getResultSet();
            if (set.next()) {
                borrower = setToData(set);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return borrower;
    }

    /**
     * This method will use the BorrowerDAO#findBorrowersWhereNameStartsOREndsWith method to look up a {@link Borrower} where his name will start with  begin.
     * @param columnName this is the column name that will be looked on in the database
     * @param begin this is the {@link String} part that will be looked with on the database
     * @return List of {@link Borrower}
     * @see BorrowerDAO#findBorrowersWhereNameStartsOREndsWith
     */
    public List<Borrower> findBorrowersWhereNameStartsWith(String columnName, String begin) {
        begin = begin + "%";
        return findBorrowersWhereNameStartsOREndsWith(columnName, begin);
    }
    /**
     * This method will use the BorrowerDAO#findBorrowersWhereNameStartsOREndsWith method to look up a {@link Borrower} where his name will end with  end.
     * @param columnName this is the column name that will be looked on in the database
     * @param end this is the {@link String} part that will be looked with on the database
     * @return List of {@link Borrower}
     * @see BorrowerDAO#findBorrowersWhereNameStartsOREndsWith
     */
    public List<Borrower> findBorrowersWhereNameEndsWith(String columnName, String end) {
        end = "%" + end;
        return findBorrowersWhereNameStartsOREndsWith(columnName, end);
    }
    /**
     * This method will use the BorrowerDAO#findBorrowersWhereNameStartsOREndsWith method to look up a {@link Borrower} where his name will have text in it.
     * @param columnName this is the column name that will be looked on in the database
     * @param text this is the {@link String} part that will be looked with on the database
     * @return List of {@link Borrower}
     * @see BorrowerDAO#findBorrowersWhereNameStartsOREndsWith
     */
    public List<Borrower> findBorrowerWhereNameHas(String columnName, String text) {
        text = "%" + text + "%";
        return findBorrowersWhereNameStartsOREndsWith(columnName, text);
    }

    /**
     *This is the method that will look for  where on the columName column to return a {@link Borrower}
     * @param columName this is the {@link String} of the Column name that will be looked up on the database.
     * @param where this is the {@link String} used to look up on the database to know what to look for.
     * @return List of {@link Borrower}
     */
    private List<Borrower> findBorrowersWhereNameStartsOREndsWith(String columName, String where) {
        List<Borrower> borrowers = new ArrayList<>();
        Connection connection;
        PreparedStatement statement;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.prepareStatement("SELECT * FROM borrower WHERE " + columName + " LIKE ?");
            statement.setString(1, where);
            statement.execute();
            ResultSet set = statement.getResultSet();
            while (set.next()) {
                borrowers.add(setToData(set));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return borrowers;
    }

    /**
     * this method will look for a borrower depending on the Date given to it, this is still unfinished and this holds the @Depricated tag to prevent misuse
     * @param columnName this is the {@link String} of the Column name that will be looked up on the database.
     * @param case10Date this is the {@link Date} that will be used by the Query to compare it to
     * @return List of {@link Borrower}
     */
    @Deprecated
    public List<Borrower> findBorrowerByBorrowDate(String columnName, Date case10Date) {
        List<Borrower> borrowers = new ArrayList<>();
        Connection connection;
        PreparedStatement statement;
        java.sql.Date datesql = new java.sql.Date(case10Date.getTime());
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.prepareStatement("SELECT * FROM borrower WHERE " + columnName + " > ?");
            statement.setDate(1, datesql);
            statement.execute();
            ResultSet set = statement.getResultSet();
            while (set.next()) {
                borrowers.add(setToData(set));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return borrowers;
    }
}



