package services;

import domain.Difficulty;
import exceptions.NoRecordFoundException;
import repo.DifficultyDAO;

import java.util.List;

public class DifficultyService {

        private DifficultyDAO difficultyDAO;

        public DifficultyService(DifficultyDAO difficultyDAO) {
            this.difficultyDAO = difficultyDAO;

        }

        public List<Difficulty> findAllDifficultys() throws NoRecordFoundException {

            if (difficultyDAO.findDifficultys().size() != 0) {
                return difficultyDAO.findDifficultys();
            } else {
                throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
            }
        }
}
