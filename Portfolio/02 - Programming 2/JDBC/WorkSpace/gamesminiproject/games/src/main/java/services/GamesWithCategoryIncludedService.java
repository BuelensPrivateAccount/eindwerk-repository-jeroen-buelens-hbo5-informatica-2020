package services;

import exceptions.NoRecordFoundException;
import repo.GamesWithCategoryIncludedDAO;

import java.util.List;

public class GamesWithCategoryIncludedService {
    repo.GamesWithCategoryIncludedDAO gamesWithCategoryIncludedDAO;

    public GamesWithCategoryIncludedService(GamesWithCategoryIncludedDAO gamesWithCategoryIncludedDAO) {
        this.gamesWithCategoryIncludedDAO = gamesWithCategoryIncludedDAO;

    }

    public String findFirstStringStartswithtest(String text, int index) throws NoRecordFoundException {
        String columnName = "game_name";
        String output;
        List<String> listMiddle = this.gamesWithCategoryIncludedDAO.findStringsWhereNamehas(columnName, text);
        List<String> listEnd = this.gamesWithCategoryIncludedDAO.findStringsWhereNameStartsWith(columnName, text);
        List<String> listStart = this.gamesWithCategoryIncludedDAO.findStringsWhereNameStartsWith(columnName, text);
        if (listMiddle.size() >= index+1) {
            output = listMiddle.get(index);
        } else if (listStart.size() >= index+1) {
            output = listStart.get(index);
        } else if (listEnd.size() >= index+1) {
            output = listEnd.get(index);
        } else {
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }
        return output;
    }

    public List<String> findAllStrings() throws NoRecordFoundException {

        if (gamesWithCategoryIncludedDAO.findGameNameWithCategoryName().size() != 0) {
            return gamesWithCategoryIncludedDAO.findGameNameWithCategoryName();
        } else {
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }
    }
}
