package com.realdolmen;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class TestCalculator {

    @Mock
    MathUtil mathUtil;

    @InjectMocks
    Calculator calculator;

    @BeforeEach
    void setUp() {


    }

    @Test
    public void testSumAndMultiply() {
        when(mathUtil.getSum(anyInt(), anyInt())).thenReturn(10);
        when(mathUtil.multiply(eq(10), anyInt())).thenReturn(100);
        int result = calculator.sumAndMultiply(5, 5, 10);
        Assertions.assertAll(() -> {
            assertEquals(100, result);
            verify(mathUtil, times(1)).getSum(anyInt(), anyInt());
            verify(mathUtil, times(1)).multiply(anyInt(), anyInt());
        });


    }

    @Test
    public void testPersonHasCorrectName() {
        ArgumentCaptor<Person> argumentCaptor = ArgumentCaptor.forClass(Person.class);
        calculator.printPerson();
        verify(mathUtil).showPerson(argumentCaptor.capture());
        assertEquals("Jimmi", argumentCaptor.getValue().getName());
    }

}
