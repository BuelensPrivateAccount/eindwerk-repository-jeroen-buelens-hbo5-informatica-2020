package com.realdolmen;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class PersonServiceTest {
    @Mock
    PersonRepository personRepository;

    @InjectMocks
    PersonService personService;

    @BeforeEach
    void setUp() {
    }

    @Test
    public void emptyTest() {
    }

    @Test
    public void testSavePersonMockedPersonRepository() {
        Date newdate = new Date();
        Person person = new Person("John", "Doe", newdate , new Address("kleinestraat", "5", new City("City", "9999")));
        person.setId(123);
        ArgumentCaptor<Person> argumentCaptor = ArgumentCaptor.forClass(Person.class);

        personService.savePerson(person);
        verify(personRepository).save(argumentCaptor.capture());
        verifyNoMoreInteractions(personRepository);
        Assertions.assertAll(() ->{
            assertEquals(123, argumentCaptor.getValue().getId().intValue());
            assertEquals("John",argumentCaptor.getValue().getFirstName());
            assertEquals(newdate,argumentCaptor.getValue().getBirthDate());
            //...........
        });


    }

    @Test
    public void testFindPersonMockedPersonRepository() {
        Person person = new Person("John", "Doe", new Date(), new Address("kleinestraat", "5", new City("City", "9999")));
        person.setId(123);
        ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);
        when(personRepository.find(argumentCaptor.capture())).thenReturn(person);
        Person person2 = personService.findPerson(123);
        Assertions.assertAll(() -> {
            assertEquals(person, person2);
            assertEquals(person.getId(), argumentCaptor.getValue());
        });
    }

    @Test
    public void verifyFindPersonMockedPersonRepository() {
        Person person = new Person("John", "Doe", new Date(), new Address("kleinestraat", "5", new City("City", "9999")));
        person.setId(123);
        when(personRepository.find(anyInt())).thenReturn(person);
        personService.findPerson(123);
        verify(personRepository, times(1)).find(anyInt());
        verifyNoMoreInteractions(personRepository);
    }


    @Test
    public void testRemovePersonMockedPersonRepository() {
        Person person = new Person("Test", "Doe", new Date(), new Address("kleinestraat", "5020202", new City("City", "9999")));
        person.setId(123);
        ArgumentCaptor<Person> argumentCaptor = ArgumentCaptor.forClass(Person.class);
        personService.removePerson(person);
        verify(personRepository).remove(argumentCaptor.capture());
        verifyNoMoreInteractions(personRepository);
        assertEquals(person, argumentCaptor.getValue());
    }
}