package com.realdolmen;

import java.util.Date;

public class PersonService {
    private PersonRepository personRepository;
    private Person person = new Person("John", "Doe", new Date(), new Address("kleinestraat", "5", new City(null, "9999")));

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    //deze methode saved een persoon
    public void savePerson(Person person) {
        person.setId(5);
        personRepository.save(person);
    }

    //deze methode zoekt een persoon op id en returned die
    public Person findPerson(int id) {
        personRepository.remove(personRepository.find(1));
        return personRepository.find(1);
    }

    //deze methode removed een persoon
    public void removePerson(Person deletePerson) {
        personRepository.remove(this.person);
    }

}
