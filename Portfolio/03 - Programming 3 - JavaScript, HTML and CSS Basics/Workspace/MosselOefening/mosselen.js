function mussles() {
    let date = new Date();
    let currentmonth = date.getMonth();
    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    switch (currentmonth) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 8:
        case 9:
        case 10:
        case 11:
            console.log("het is " + monthNames[currentmonth] + ", je mag mosselen eten.");
            document.getElementById("maand").innerHTML = "<p>het is " + monthNames[currentmonth] + ", je mag mosselen eten.</p>";
            break;
        case 4:
        case 5:
        case 6:
        case 7:
            console.log("blijf van de mosselen af!");
            document.getElementById("maand").innerHTML = "<p>het is " + monthNames[currentmonth] + ", no mosselen for you!</p>";
            break;
        default:
            document.getElementById("maand").innerHTML = "<p>geen geldige maand</p>"
            break;
    }

    let string = "";
    for (let i = 1; i < 13; i++) {
        if (i > 4 && i < 9) {
            continue;
        }
        console.log("in " + monthNames[i - 1] + ", mag je mosselen eten"); //i-1 omdat mijn loop loopt van 1-12, maar mijn constanten van 0-11 lopen
        string += "in " + monthNames[i - 1] + ", mag je mosselen eten<br/>";
    }
    document.getElementById("mosselen").innerHTML = string;
}