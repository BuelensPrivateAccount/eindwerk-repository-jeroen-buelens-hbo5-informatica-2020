function switchtagline() {
    let date = new Date();
    let theDayAsNumber = date.getDay();
    let msg;
    switch (theDayAsNumber) {
        case 1: msg = "it's almost friday"; break;
        case 2: msg = "tuesday, what day"; break;
        case 3: msg = "92 is half of 99"; break;
        case 4: msg = "on thursday we drink, because tomorrow is the last workday"; break;
        case 5: msg = "Yeet your boss, because it's friday"; break;
        default: msg = "weekend!"; break;
    }
    document.getElementById("punchline").innerHTML = msg;
}

let fontSize = 11;

function fontsizesmaller(){
    fontSize -= 1;
   document.getElementsByTagName("body")[0].style.fontSize = fontSize + "px" 
}

function fontsizenormal(){
    fontSize = 11
    document.getElementsByTagName("body")[0].style.fontSize = fontSize + "px" 
}

function fontsizebigger(){
    fontSize += 1;
    document.getElementsByTagName("body")[0].style.fontSize = fontSize + "px" 
}