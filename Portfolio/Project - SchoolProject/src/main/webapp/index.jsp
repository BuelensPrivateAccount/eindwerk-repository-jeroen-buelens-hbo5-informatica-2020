<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Start Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Page</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
<main>
    <h1>SchoolProject Login Page</h1>
    <form action="LoginServlet" method="post">
        <%
            if (null != request.getAttribute("errormessage")) { %>
        <div class="alert alert-danger" role="alert">
            <%= request.getAttribute("errormessage")%>
        </div>
        <%}%>
        <div class="form-group">
            <label for="UName"><b>Username: </b></label>
            <input type="text" class="form-control" placeholder="Enter Username" name="UName" id="UName" required/>
        </div>
        <div class="form-group">
            <label for="PWord"><b>Password: </b></label>
            <input type="password" class="form-control" placeholder="Enter Password" name="PWord" id="PWord" required>
        </div>
        <button type="submit" name="login" class="btn btn-primary">Login</button>
    </form>
</main>
</div>
</body>
</html>
