<%@ page import="com.realdolmen.schoolproject.beans.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="com.realdolmen.schoolproject.beans.abstractClasses.Person" %>
<%@ page import="com.realdolmen.schoolproject.beans.Teacher" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="com.realdolmen.schoolproject.beans.SchoolClass" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin homepage</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Page</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <link rel="stylesheet" href="css/style.css">
</head>
<body onload="addclassdropdown()">
<div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li>
                <a href="AdminServlet?action=showStudents">Students</a>
            </li>
            <li>
                <a href="AdminServlet?action=showTeachers">Teachers</a>
            </li>
            <li>
                <a href="#">Classes (under construction)</a>
            </li>
            <li>
                <a href="#">Courses (under construction)</a>
            </li>
            <li>
                <a href=#">Years (under construction)</a>
            </li>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Editing or creating a user</h1>

                    <% Person person = null;
                        if (null != request.getAttribute("student")) {
                            person = (Student) request.getAttribute("student");
                        } else if (null != request.getAttribute("teacher")) {
                            person = (Teacher) request.getAttribute("teacher");
                        }
                    %>
                    <form action="AdminServlet">
                        <input type="hidden" name="OGUserName" value="<%if(person != null){%><%=person.getUsername()%><%}%>"/>
                        <label for="type">Student or Teacher : </label>
                        <select id="type" name="type" class="form-control" onchange="addclassdropdown()">
                            <option value="Teacher" <%if (person instanceof Teacher) {%> selected <%}%>>Teacher</option>
                            <option value="Student" <%if (person instanceof Student) {%> selected <%}%>>Student</option>
                        </select>
                        <label for="class" id="classlabel"> Class: </label>
                        <select id="class" name="class" class="form-control" hidden>
                            <% List<SchoolClass> classes = (List<SchoolClass>) request.getAttribute("classes");
                                for (SchoolClass schoolClass : classes) {%>
                            <option value="<%=schoolClass.getPk()%>" <%
                                SchoolClass studentclass = null;
                                if(person instanceof Student){
                               studentclass =  ((Student) person).getSchoolClass();
                            } if(null != studentclass && schoolClass.getPk() == studentclass.getPk()){%> selected <%}%>>
                                Class: <%=schoolClass.getClassgradeId().getYear()%><%=schoolClass.getClassnumber()%>
                            </option>
                            <%
                                }
                            %>
                        </select>
                        <label for="userName">Username : </label><input type="text" class="form-control" id="userName"
                                                                        name="userName"
                                                                        <%if(person != null){%>value="<%=person.getUsername()%>"
                            <%} else {%> placeholder="User Name - must be unique" <%}%> required/>
                        <label for="firstName">FirstName : </label><input type="text" class="form-control"
                                                                          id="firstName"
                                                                          name="firstName"
                                                                          <%if(person != null){%>value="<%=person.getFirstName()%>"
                            <%} else {%> placeholder="First Name" <%}%> required/>
                        <label for="surName">Surname : </label><input type="text" class="form-control" id="surName"
                                                                      name="surName"
                                                                      <%if(person != null){%>value="<%=person.getSurName()%>"
                            <%} else {%> placeholder="Last Name" <%}%>
                                                                      required/>
                        <label for="passWord">Password : </label><input type="text" class="form-control" id="passWord"
                                                                        name="passWord"
                                                                        <%if(person != null){%>placeholder="unchanged"
                                                                        <%} else {%>placeholder="Password"
                                                                        required <%}%>/>
                        <label for="birthdate">Birthdate : </label><input type="date" min="1900-01-01"
                                                                          max="<%=LocalDate.now().toString()%>"
                                                                          <%if(person != null){%>value="<%=person.getBirthDate().toString()%>"
                        <%}else {%> placeholder="YYYY-MM-DD" <%}%> name="birthdate" id="birthdate">
                        <button type="submit" name="SaveTOS" class="btn btn-primary">Save</button>
                        <a class="btn btn-primary" href="AdminServlet?action=homepage">Discard changes</a>
                        <a class="btn btn-primary" href="AdminServlet?action=deleteTOS<%if(person != null){%>&OGUserName=<%=person.getUsername()%><%}%>">Delete User</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->
<script>
    $(" #menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    function addclassdropdown() {
        let box = document.getElementById("type");
        let classbox = document.getElementById("class");
        let classlabel = document.getElementById("classlabel");
        let selectedvalue = box.options[box.selectedIndex].value;
        if (selectedvalue === "Student") {
            classlabel.style.display = "flex";
            classbox.style.display = "flex";
        } else {
            classlabel.style.display = "none";
            classbox.style.display = "none";
        }

    }
</script>
</body>
</html>
