<%@ page import="com.realdolmen.schoolproject.beans.CourseSubject" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin homepage</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Page</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li>
                <a href="MasterServlet?action=teacherhomepage">Teachers</a>
            </li>
            <li>
                <a href="AdminServlet?action=changepassword">Change Password</a>
            </li>
            <li>
                <a href="index.jsp">logout</a>
            </li>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Editing or creating a test</h1>
                    <form action="MasterServlet">
                        <label for="subject" id="classlabel"> Course: </label>
                        <select id="subject" name="subject" class="form-control" hidden>
                            <% List<CourseSubject> subjects = (List<CourseSubject>) request.getAttribute("subjects");
                                for (CourseSubject subject : subjects) {%>
                            <option value="<%=subject.getPk()%>"><%=subject.getCoursesubject()%></option>
                            <%
                                }
                            %>
                        </select>
                        <label for="maxscore">Max Score: </label>
                        <input type="number" class="form-control" id="maxscore" name="maxscore" min="1" max="100" placeholder="Max score" value="10" required/>
                        <label for="testname">Test Name: </label>
                        <input type="text" class="form-control" id="testname" name="testname" placeholder="TestName" required/>
                        <label for="testDate">Test Date: </label>
                        <input type="date" class="form-control" id="testDate" name="testDate" placeholder="YYYY-MM-DD" min="2000-01-01" required/>
                        <button type="submit" name="SaveTest" class="btn btn-primary">Save</button>
                        <a class="btn btn-primary" href="MasterServlet?action=teacherhomepage">Discard changes</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->
<script>
    $(" #menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
</body>
</html>
