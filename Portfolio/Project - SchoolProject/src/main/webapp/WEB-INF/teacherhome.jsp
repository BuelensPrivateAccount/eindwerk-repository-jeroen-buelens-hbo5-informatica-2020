<%@ page import="java.util.List" %>
<%@ page import="com.realdolmen.schoolproject.beans.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Teacher homepage</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Page</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li>
                <a href="MasterServlet?action=createnewtest">create a new test</a>
            </li>
            <li>
                <a href="AdminServlet?action=changepassword">Change Password</a>
            </li>
            <li>
                <a href="index.jsp">logout</a>
            </li>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1>SchoolProject Teacher home</h1>
                    <%
                        List<CourseClassTeacher> courseClassTeachers = (List<CourseClassTeacher>) request.getAttribute("data");
                        for (CourseClassTeacher courseClassTeacher : courseClassTeachers) {
                    %>
                    <label for="<%=courseClassTeacher.getCourseClassTeachers_Subject().getCoursesubject()+courseClassTeacher.getCourseClassTeachers_Class().getClassnumber()%>">Class: <%=courseClassTeacher.getCourseClassTeachers_Class().getClassgradeId().getYear() + " " + courseClassTeacher.getCourseClassTeachers_Class().getClassnumber()%>
                        Course: <%=courseClassTeacher.getCourseClassTeachers_Subject().getCoursesubject()%>
                    </label>
                    <div>
                        <table class="table table-bordered"
                               id="<%=courseClassTeacher.getCourseClassTeachers_Subject().getCoursesubject()+courseClassTeacher.getCourseClassTeachers_Class().getClassnumber()%>">
                            <%List<Student> classStudents = courseClassTeacher.getCourseClassTeachers_Class().getStudents();%>
                            <thead>
                            <tr>
                                <th>Student Name</th>
                                <%
                                    for (Student s : classStudents) {
                                        for (StudentScore score : s.getStudentScoreCollection()) {
                                %>
                                <th scope="col"><%=score.getTestdateId().getTestId().getTestname()%>
                                </th>
                                <%}%>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row"><%=s.getFirstName()%> <%=s.getSurName()%>
                                </th>
                                <%for (StudentScore score : s.getStudentScoreCollection()) {%>
                                <td><%=score.getScore()%>/<%=score.getTestdateId().getTestId().getMaxscore()%>
                                </td>
                                <%}%>
                            </tr>
                            </tbody>
                            <%}%>
                        </table>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->
<script>
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
</body>
</html>
