<%@ page import="com.realdolmen.schoolproject.beans.Teacher" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin homepage</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li>
                <a href="AdminServlet?action=showStudents">Students</a>
            </li>
            <li>
                <a href="AdminServlet?action=showTeachers">Teachers</a>
            </li>
            <li>
                <a href="#">Classes (under construction)</a>
            </li>
            <li>
                <a href="#">Courses (under construction)</a>
            </li>
            <li>
                <a href=#">Years (under construction)</a>
            </li>
            <li>
                <a href="AdminServlet?action=changepassword">Change Password</a>
            </li>
            <li>
                <a href="index.jsp">logout</a>
            </li>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Teachers</h1>
                    <div class="list-group">
                        <%
                            List<Teacher> teachers = (List<Teacher>) request.getAttribute("teachers");
                            for (Teacher s : teachers) {
                        %>
                        <p>
                            <a href="AdminServlet?action=showteacherbyid&id=<%=s.getPk()%>"
                               class="list-group-item list-group-item-action">
                                <%=s.getSurName()%> <%=s.getFirstName()%>
                            </a>
                        </p>
                        <%}%>
                        <p><a href="AdminServlet?action=create">Create a new teacher</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->
<script>
    $(" #menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
</body>
</html>
