<%@ page import="com.realdolmen.schoolproject.beans.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="com.realdolmen.schoolproject.beans.abstractClasses.Person" %>
<%@ page import="com.realdolmen.schoolproject.beans.Teacher" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="com.realdolmen.schoolproject.beans.Administrator" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin homepage</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Page</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<!-- Page Content -->
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1>Editing or creating a user</h1>

                <% Person person = null;
                    if (null != request.getAttribute("user")) {
                        person = (Person) request.getAttribute("user");
                    }
                %>
                <form action="AdminServlet">
                    <input type="hidden" name="OGUserName" value="<%if(person != null){%><%=person.getUsername()%><%}%>"/>
                    <label for="passWord">Password : </label><input type="text" class="form-control" id="passWord"
                                                                    name="passWord"
                                                                    <%if(person != null){%>placeholder="unchanged"
                                                                    <%} else {%>placeholder="Password" <%}%> required/>
                    <button type="submit" name="SavePassword" class="btn btn-primary">Save</button>
                    <a class="btn btn-primary" href="index.jsp">Discard changes</a>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->
<script>
    $(" #menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
</body>
</html>
