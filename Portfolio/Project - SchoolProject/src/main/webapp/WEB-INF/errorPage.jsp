<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
<h1>A Error has occured</h1>
<%
    String errormessage = (String) request.getAttribute("errormessage");
    Exception e = (Exception) request.getAttribute("devstacktrace");
%>
<p><%=errormessage%>
</p>
<p><a href="../index.jsp">Return to start</a></p>
<hr>
<script type="text/javascript">
    <%if(e != null){
        StackTraceElement[] stacktrace = e.getStackTrace();
    for(StackTraceElement element : stacktrace){
    String s = element.toString();%>
    console.log("<%=s%>");
    <%}%>
    console.log("   ");
    console.log("<%=e.getMessage()%>")
    <%}%>
</script>
</body>
</html>