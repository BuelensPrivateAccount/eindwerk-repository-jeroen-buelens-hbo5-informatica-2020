/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.schoolproject.beans;

import com.realdolmen.schoolproject.beans.abstractClasses.Person;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

/**
 * @author JBLBL75
 */
@Entity
@Table(name = "admin", schema = "schoolproject")
@NamedQueries({
        @NamedQuery(name = "Administrator.findAll", query = "SELECT t FROM Administrator t"),
        @NamedQuery(name = "Administrator.findByPk", query = "SELECT t FROM Administrator t WHERE t.pk = :pk"),
        @NamedQuery(name = "Administrator.findByUsername", query = "SELECT t FROM Administrator t WHERE t.username = :username")})
public class Administrator extends Person {
       public Administrator() {
        super();
    }

    public Administrator(Integer pk, String username, String password, String firstname, String surname, Date birthdate) {
        super();
        this.pk = pk;
        this.username = username;
        this.password = password;
        this.surName = surname;
        this.firstName = firstname;
        this.birthDate = birthdate;
    }

    @Override
    public String toString() {
        return "Administrator{" +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", surName='" + surName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }

}
