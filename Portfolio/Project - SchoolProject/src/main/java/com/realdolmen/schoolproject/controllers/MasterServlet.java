package com.realdolmen.schoolproject.controllers;

import com.realdolmen.schoolproject.beans.*;
import com.realdolmen.schoolproject.beans.abstractClasses.Person;
import com.realdolmen.schoolproject.utilities.JPAUtility;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

import static com.realdolmen.schoolproject.utilities.ServletExceptionHandeling.*;


@WebServlet(name = "MasterServlet", urlPatterns = "/MasterServlet")
public class MasterServlet extends HttpServlet {

    private static JPAUtility jpaUtility;

    @Override
    public void init() throws ServletException {
        jpaUtility = JPAUtility.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        RequestDispatcher rd = null;
        if (session == null || null == session.getAttribute("username") || session.getAttribute("username").equals("")) {
            rd = request.getRequestDispatcher("index.jsp");
        } else {
            if (request.getParameter("SaveTest") != null) {
                rd = saveTest(request, response);
            } else if (request.getParameter("action") != null) {
                String action = request.getParameter("action");
                switch (action) {
                    case "teacherhomepage":
                        session = request.getSession(false);
                        LoginServlet.initTeacherHome(request, response, (Teacher) session.getAttribute("fulluser"));
                        rd = request.getRequestDispatcher("/WEB-INF/teacherhome.jsp");
                        break;
                    case "createnewtest":
                        rd = teacherCreateNEWTest(request, response);
                        break;
                }
            }
        }
        if (rd != null) {
            rd.forward(request, response);
        } else {
            throw new ServletException("you tried to do a a method/option that isn't implemented or available yet, please contact staff.");
        }


    }

    private RequestDispatcher saveTest(HttpServletRequest request, HttpServletResponse response) {
        EntityManager em = jpaUtility.createEntityManager();
        EntityTransaction tr = em.getTransaction();
        StudentTest studentTest = new StudentTest();
        TestDate testDate = new TestDate();
        testDate.setTestId(studentTest);
        studentTest.setMaxscore(Integer.parseInt(request.getParameter("maxscore")));
        testDate.setTestdate(request.getParameter("testDate"));
        studentTest.setTestname(request.getParameter("testname"));
        studentTest.setSubject(em.createNamedQuery("Coursesubject.findByPk", CourseSubject.class).setParameter("pk", Integer.parseInt(request.getParameter("subject"))).getSingleResult());
        tr.begin();
        em.persist(studentTest);
        em.persist(testDate);
        tr.commit();
        HttpSession session = request.getSession(false);
        LoginServlet.initTeacherHome(request, response, (Teacher) session.getAttribute("fulluser"));
        return request.getRequestDispatcher("/WEB-INF/teacherhome.jsp");
    }

    private RequestDispatcher teacherCreateNEWTest(HttpServletRequest request, HttpServletResponse response) {
        EntityManager em = jpaUtility.createEntityManager();
        request.setAttribute("subjects", em.createNamedQuery("Coursesubject.findAll", CourseSubject.class).getResultList());
        return request.getRequestDispatcher("/WEB-INF/teachercreatenewtest.jsp");
    }


}
