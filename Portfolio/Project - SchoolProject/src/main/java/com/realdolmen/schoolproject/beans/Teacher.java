/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.schoolproject.beans;

import com.realdolmen.schoolproject.beans.abstractClasses.Person;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

/**
 * @author JBLBL75
 */
@Entity
@Table(name = "teacher", schema = "schoolproject")
@NamedQueries({
        @NamedQuery(name = "Teacher.findAll", query = "SELECT t FROM Teacher t"),
        @NamedQuery(name = "Teacher.findByPk", query = "SELECT t FROM Teacher t WHERE t.pk = :pk"),
        @NamedQuery(name = "Teacher.findByUsername", query = "SELECT t FROM Teacher t WHERE t.username = :username"),
        @NamedQuery(name = "Teacher.findByPassword", query = "SELECT t FROM Teacher t WHERE t.password = :password"),
        @NamedQuery(name = "Teacher.findByFirstname", query = "SELECT t FROM Teacher t WHERE t.firstName = :firstname"),
        @NamedQuery(name = "Teacher.findBySurname", query = "SELECT t FROM Teacher t WHERE t.surName = :surname"),
        @NamedQuery(name = "Teacher.findByBirthdate", query = "SELECT t FROM Teacher t WHERE t.birthDate = :birthdate")})
public class Teacher extends Person {
    @OneToMany(mappedBy = "courseClassTeachers_Teacher",fetch = FetchType.EAGER)
    private List<CourseClassTeacher> courseClassTeachers;

    public Teacher() {
        super();
    }

    public Teacher(Integer pk, String username, String password, String firstname, String surname, Date birthdate) {
        super();
        this.pk = pk;
        this.username = username;
        this.password = password;
        this.surName = surname;
        this.firstName = firstname;
        this.birthDate = birthdate;
    }

    public List<CourseClassTeacher> getCourseClassTeachers() {
        return courseClassTeachers;
    }

    public void setCourseClassTeachers(List<CourseClassTeacher> courseClassTeachers) {
        this.courseClassTeachers = courseClassTeachers;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "courseClassTeachers=" + courseClassTeachers +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", surName='" + surName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }
}
