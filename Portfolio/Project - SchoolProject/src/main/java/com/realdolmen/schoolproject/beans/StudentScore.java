/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.schoolproject.beans;

import com.realdolmen.schoolproject.beans.abstractClasses.AbstractBean;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author JBLBL75
 */
@Entity
@Table(name = "studentscore", schema = "schoolproject")
@NamedQueries({
        @NamedQuery(name = "Studentscore.findAll", query = "SELECT s FROM StudentScore s"),
        @NamedQuery(name = "Studentscore.findByPk", query = "SELECT s FROM StudentScore s WHERE s.pk = :pk"),
        @NamedQuery(name = "Studentscore.findByScore", query = "SELECT s FROM StudentScore s WHERE s.score = :score")})
public class StudentScore extends AbstractBean {

    @Basic(optional = false)
    @NotNull
    @Column(name = "score")
    private int score;
    @JoinColumn(name = "student_id", referencedColumnName = "PK")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private Student studentId;
    @JoinColumn(name = "testdate_id", referencedColumnName = "PK")
    @ManyToOne(optional = false, cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private TestDate testdateId;

    public StudentScore() {
    }

    public StudentScore(Integer pk) {
        this.pk = pk;
    }

    public StudentScore(Integer pk, int score) {
        this.pk = pk;
        this.score = score;
    }


    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Student getStudentId() {
        return studentId;
    }

    public void setStudentId(Student studentId) {
        this.studentId = studentId;
    }

    public TestDate getTestdateId() {
        return testdateId;
    }

    public void setTestdateId(TestDate testdateId) {
        this.testdateId = testdateId;
    }


    @Override
    public String toString() {
        return "StudentScore{" +
                "score=" + score +
                ", studentId=" + studentId +
                ", testdateId=" + testdateId +
                '}';
    }
}
