/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.schoolproject.beans;

import com.realdolmen.schoolproject.beans.abstractClasses.AbstractBean;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

/**
 * @author JBLBL75
 */
@Entity
@Table(name = "testdate", schema = "schoolproject")
@NamedQueries({
        @NamedQuery(name = "Testdate.findAll", query = "SELECT t FROM TestDate t"),
        @NamedQuery(name = "Testdate.findByPk", query = "SELECT t FROM TestDate t WHERE t.pk = :pk"),
        @NamedQuery(name = "Testdate.findByTestdate", query = "SELECT t FROM TestDate t WHERE t.testdate = :testdate")})
public class TestDate extends AbstractBean {


    @Basic(optional = false)
    @NotNull
    @Access(AccessType.FIELD)
    @Column(name = "testdate")
    private Date testdate;
    @OneToMany(mappedBy = "testdateId")
    private List<StudentScore> studentScoreCollection;
    @JoinColumn(name = "test_id", referencedColumnName = "PK")
    @ManyToOne(optional = false,fetch = FetchType.EAGER)
    private StudentTest testId;

    public TestDate() {
    }

    public TestDate(Integer pk) {
        this.pk = pk;
    }

    public TestDate(Integer pk, Date testdate) {
        this.pk = pk;
        this.testdate = testdate;
    }


    public LocalDate getTestdate() {
        return testdate.toLocalDate();
    }

    public void setTestdate(LocalDate testdate) {
        this.testdate = Date.valueOf(testdate);
    }

    public void setTestdate(String testdate) {
        setTestdate(LocalDate.parse(testdate));
    }

    public List<StudentScore> getStudentScoreCollection() {
        return studentScoreCollection;
    }

    public void setStudentScoreCollection(List<StudentScore> studentScoreCollection) {
        this.studentScoreCollection = studentScoreCollection;
    }

    public StudentTest getTestId() {
        return testId;
    }

    public void setTestId(StudentTest testId) {
        this.testId = testId;
    }


    @Override
    public String toString() {
        return "TestDate{" +
                "testdate=" + testdate +
                ", studentScoreCollection=" + studentScoreCollection +
                ", testId=" + testId +
                '}';
    }
}
