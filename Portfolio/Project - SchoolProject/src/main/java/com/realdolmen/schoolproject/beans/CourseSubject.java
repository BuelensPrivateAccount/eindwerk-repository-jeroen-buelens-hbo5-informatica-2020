/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.schoolproject.beans;

import com.realdolmen.schoolproject.beans.abstractClasses.AbstractBean;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author JBLBL75
 */
@Entity
@Table(name = "coursesubject", schema = "schoolproject")
@NamedQueries({
        @NamedQuery(name = "Coursesubject.findAll", query = "SELECT c FROM CourseSubject c"),
        @NamedQuery(name = "Coursesubject.findByPk", query = "SELECT c FROM CourseSubject c WHERE c.pk = :pk"),
        @NamedQuery(name = "Coursesubject.findByCouresubject", query = "SELECT c FROM CourseSubject c WHERE c.coursesubject = :couresubject")})
public class CourseSubject extends AbstractBean {

    @OneToMany(mappedBy = "courseClassTeachers_Subject")
    List<CourseClassTeacher> courseClassTeachers;
    @Size(max = 255)
    @Column(name = "coursesubject")
    private String coursesubject;
    @OneToMany(mappedBy = "subject")
    private List<StudentTest> tests;

    public CourseSubject() {
    }

    public String getCoursesubject() {
        return coursesubject;
    }

    public void setCoursesubject(String coursesubject) {
        this.coursesubject = coursesubject;
    }

    public List<StudentTest> getTests() {
        return tests;
    }

    public void setTests(List<StudentTest> tests) {
        this.tests = tests;
    }

    public List<CourseClassTeacher> getCourseClassTeachers() {
        return courseClassTeachers;
    }

    public void setCourseClassTeachers(List<CourseClassTeacher> courseClassTeachers) {
        this.courseClassTeachers = courseClassTeachers;
    }

    @Override
    public String toString() {
        return "CourseSubject{" +
                "courseClassTeachers=" + courseClassTeachers +
                ", coursesubject='" + coursesubject + '\'' +
                ", tests=" + tests +
                '}';
    }
}
