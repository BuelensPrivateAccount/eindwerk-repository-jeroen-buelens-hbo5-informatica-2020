package com.realdolmen.schoolproject.controllers;

import com.realdolmen.schoolproject.beans.*;
import com.realdolmen.schoolproject.beans.abstractClasses.Person;
import com.realdolmen.schoolproject.utilities.JPAUtility;
import org.eclipse.persistence.jpa.JpaCache;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.realdolmen.schoolproject.utilities.ServletExceptionHandeling.defaultException;

@WebServlet(name = "LoginServlet", urlPatterns = "/LoginServlet")
public class LoginServlet extends HttpServlet {

    private static JPAUtility jpaUtility;

    @Override
    public void init() throws ServletException {
        jpaUtility = JPAUtility.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            RequestDispatcher rd = null;
            if (request.getParameter("login") != null) {
                rd = login(request, response);

            }
            if (rd != null) {
                rd.forward(request, response);
            } else {
                throw new ServletException("you tried to do a a method/option that isn't implemented or available yet, please contact staff.");
            }
        } catch (SQLException e) {
            if (e.getMessage().equals("Sorry, your username or password wasn't correct")) {
                request.setAttribute("errormessage", e.getMessage());
                RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
                rd.forward(request, response);
            } else {
                defaultException(request.getRequestDispatcher("errorPage.jsp"), request, response);
            }
        }
    }

    private RequestDispatcher login(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        EntityManager em = jpaUtility.createEntityManager();
        String username = request.getParameter("UName");
        String password = request.getParameter("PWord");
        HttpSession session = request.getSession(true);
        String path = "";
        Person user = null;
        try {
            TypedQuery<Student> query = em.createNamedQuery("Student.findByUsername", Student.class).setParameter("username", username);
            user = query.getSingleResult();
        } catch (NoResultException ignored) {
        }
        try {
            TypedQuery<Teacher> query = em.createNamedQuery("Teacher.findByUsername", Teacher.class).setParameter("username", username);
            user = query.getSingleResult();
        } catch (NoResultException ignored) {

        }
        try {
            TypedQuery<Administrator> query = em.createNamedQuery("Administrator.findByUsername", Administrator.class).setParameter("username", username);
            user = query.getSingleResult();
        } catch (NoResultException ignored) {

        }
        if (user == null || !user.checkPassword(password)) {
            throw new SQLException("Sorry, your username or password wasn't correct");
        }
        if (user instanceof Teacher) {
            path = "/WEB-INF/teacherhome.jsp";
            Teacher teacher = (Teacher) user;
            session.setAttribute("username", teacher.getUsername());
            session.setAttribute("fulluser", teacher);
            initTeacherHome(request, response, teacher);
        }
        if (user instanceof Student) {
            path = "/WEB-INF/studenthome.jsp";
            Student student = (Student) user;
            session.setAttribute("username", student.getUsername());
            session.setAttribute("fulluser", student);
        }
        if (user instanceof Administrator) {
            Administrator administrator = (Administrator) user;
            session.setAttribute("username", administrator.getUsername());
            session.setAttribute("fulluser", administrator);
            path = "/WEB-INF/adminhome.jsp";
        }
        return request.getRequestDispatcher(path);


    }

    public static void initTeacherHome(HttpServletRequest request, HttpServletResponse response, Teacher teacher) {
        List<CourseClassTeacher> courseClassTeachers = teacher.getCourseClassTeachers();
        request.setAttribute("data", courseClassTeachers);
    }


}
