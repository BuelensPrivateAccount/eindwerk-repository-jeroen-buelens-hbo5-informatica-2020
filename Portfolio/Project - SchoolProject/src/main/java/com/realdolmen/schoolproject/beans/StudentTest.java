/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.schoolproject.beans;

import com.realdolmen.schoolproject.beans.abstractClasses.AbstractBean;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author JBLBL75
 */
@Entity
@Table(name = "studenttest", schema = "schoolproject")
@NamedQueries({
    @NamedQuery(name = "Studenttest.findAll", query = "SELECT s FROM StudentTest s"),
    @NamedQuery(name = "Studenttest.findByPk", query = "SELECT s FROM StudentTest s WHERE s.pk = :pk"),
    @NamedQuery(name = "Studenttest.findByMaxscore", query = "SELECT s FROM StudentTest s WHERE s.maxscore = :maxscore"),
    @NamedQuery(name = "Studenttest.findByTestname", query = "SELECT s FROM StudentTest s WHERE s.testname = :testname")})
public class StudentTest extends AbstractBean {

    @Basic(optional = false)
    @NotNull
    @Column(name = "maxscore")
    private int maxscore;
    @Size(max = 255)
    @Column(name = "testname")
    private String testname;
    @OneToMany(mappedBy = "testId")
    private List<TestDate> testdateCollection;
    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "coursesubjectid",referencedColumnName = "PK")
    private CourseSubject subject;

    public StudentTest() {
    }

    public StudentTest(Integer pk) {
        this.pk = pk;
    }

    public StudentTest(Integer pk, int maxscore) {
        this.pk = pk;
        this.maxscore = maxscore;
    }


    public int getMaxscore() {
        return maxscore;
    }

    public void setMaxscore(int maxscore) {
        this.maxscore = maxscore;
    }

    public String getTestname() {
        return testname;
    }

    public void setTestname(String testname) {
        this.testname = testname;
    }

    public Collection<TestDate> getTestdateCollection() {
        return testdateCollection;
    }

    public void setTestdateCollection(List<TestDate> testdateCollection) {
        this.testdateCollection = testdateCollection;
    }


    @Override
    public String toString() {
        return "StudentTest{" +
                "maxscore=" + maxscore +
                ", testname='" + testname + '\'' +
                ", testdateCollection=" + testdateCollection +
                '}';
    }

    public CourseSubject getSubject() {
        return subject;
    }

    public void setSubject(CourseSubject subject) {
        this.subject = subject;
    }
}
