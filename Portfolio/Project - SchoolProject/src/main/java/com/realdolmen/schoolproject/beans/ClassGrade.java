/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.schoolproject.beans;

import com.realdolmen.schoolproject.beans.abstractClasses.AbstractBean;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author JBLBL75
 */
@Entity
@Table(name = "classgrade", schema = "schoolproject")
@NamedQueries({
    @NamedQuery(name = "Classgrade.findAll", query = "SELECT c FROM ClassGrade c"),
    @NamedQuery(name = "Classgrade.findByPk", query = "SELECT c FROM ClassGrade c WHERE c.pk = :pk"),
    @NamedQuery(name = "Classgrade.findByYear", query = "SELECT c FROM ClassGrade c WHERE c.year = :year")})
public class ClassGrade extends AbstractBean {

    @Column(name = "year")
    @Access(AccessType.FIELD)
    private int year;
    @OneToMany(mappedBy = "classgradeId")
    private List<SchoolClass> schoolClassCollection;

    public ClassGrade() {
    }

    public ClassGrade(Integer pk) {
        this.pk = pk;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Collection<SchoolClass> getSchoolClassCollection() {
        return schoolClassCollection;
    }

    public void setSchoolClassCollection(List<SchoolClass> schoolClassList) {
        this.schoolClassCollection = schoolClassList;
    }

    @Override
    public String toString() {
        return "ClassGrade{" +
                "year=" + year +
                ", schoolClassCollection=" + schoolClassCollection +
                '}';
    }
}
