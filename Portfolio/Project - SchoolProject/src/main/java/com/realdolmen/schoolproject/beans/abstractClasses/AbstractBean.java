package com.realdolmen.schoolproject.beans.abstractClasses;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;


/**
 * @author JBLBL75
 */
@MappedSuperclass
public abstract class AbstractBean implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK")
    @Access(AccessType.FIELD)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int pk;

    public int getPk() {
        return pk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractBean)) return false;
        AbstractBean that = (AbstractBean) o;
        return getPk() == that.getPk();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPk());
    }


}
