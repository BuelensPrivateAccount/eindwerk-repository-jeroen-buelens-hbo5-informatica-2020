/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.schoolproject.beans;

import com.realdolmen.schoolproject.beans.abstractClasses.AbstractBean;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author JBLBL75
 */
@Entity
@Table(name = "courseclassteacher", schema = "schoolproject")
@NamedQueries({
        @NamedQuery(name = "CourseClassTeacher.findAll", query = "SELECT c FROM CourseClassTeacher c"),
        @NamedQuery(name = "CourseClassTeacher.findByPk", query = "SELECT c FROM CourseClassTeacher c WHERE c.pk = :pk")})
public class CourseClassTeacher extends AbstractBean {

    @ManyToOne(cascade = CascadeType.ALL , fetch = FetchType.EAGER)
    @JoinColumn(name = "subject_id",referencedColumnName = "PK")
    private CourseSubject courseClassTeachers_Subject;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "teacher_id",referencedColumnName = "PK")
    private Teacher courseClassTeachers_Teacher;
    @ManyToOne(cascade = CascadeType.ALL , fetch = FetchType.EAGER)
    @JoinColumn(name = "schoolclass_id",referencedColumnName = "PK")
    private SchoolClass courseClassTeachers_Class;

    public CourseSubject getCourseClassTeachers_Subject() {
        return courseClassTeachers_Subject;
    }

    public void setCourseClassTeachers_Subject(CourseSubject courseClassTeachers_Subject) {
        this.courseClassTeachers_Subject = courseClassTeachers_Subject;
    }

    public Teacher getCourseClassTeachers_Teacher() {
        return courseClassTeachers_Teacher;
    }

    public void setCourseClassTeachers_Teacher(Teacher courseClassTeachers_Teacher) {
        this.courseClassTeachers_Teacher = courseClassTeachers_Teacher;
    }

    public SchoolClass getCourseClassTeachers_Class() {
        return courseClassTeachers_Class;
    }

    public void setCourseClassTeachers_Class(SchoolClass courseClassTeachers_Class) {
        this.courseClassTeachers_Class = courseClassTeachers_Class;
    }

    @Override
    public String toString() {
        return "CourseClassTeacher{" +
                "courseClassTeachers_Subject=" + courseClassTeachers_Subject +
                ", courseClassTeachers_Teacher=" + courseClassTeachers_Teacher +
                ", courseClassTeachers_Class=" + courseClassTeachers_Class +
                '}';
    }
}
