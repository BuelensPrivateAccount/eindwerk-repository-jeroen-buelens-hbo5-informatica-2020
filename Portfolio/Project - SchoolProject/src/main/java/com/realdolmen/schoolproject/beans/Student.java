/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.schoolproject.beans;

import com.realdolmen.schoolproject.beans.abstractClasses.Person;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;


/**
 *
 * @author JBLBL75
 */
@Entity
@Table(name = "student", schema = "schoolproject")
@NamedQueries({
    @NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s"),
    @NamedQuery(name = "Student.findByPk", query = "SELECT s FROM Student s WHERE s.pk = :pk"),
    @NamedQuery(name = "Student.findByUsername", query = "SELECT s FROM Student s WHERE s.username = :username"),
    @NamedQuery(name = "Student.findByPassword", query = "SELECT s FROM Student s WHERE s.password = :password"),
    @NamedQuery(name = "Student.findBySurname", query = "SELECT s FROM Student s WHERE s.surName = :surname"),
    @NamedQuery(name = "Student.findByFirstname", query = "SELECT s FROM Student s WHERE s.firstName = :firstname"),
    @NamedQuery(name = "Student.findByBirthdate", query = "SELECT s FROM Student s WHERE s.birthDate = :birthdate")})
public class Student extends Person {

    @OneToMany(mappedBy = "studentId",fetch = FetchType.EAGER)
    private List<StudentScore> studentScoreCollection;
    @ManyToOne(optional = false)
    @JoinColumn(name="schoolclassid",referencedColumnName = "PK")
    @NotNull
    private SchoolClass schoolClass;

    public Student() {
        super();
    }


    public Student( String username, String password, String surname, String firstname, LocalDate birthdate) {
        super();
        this.username = username;
        this.password = password;
        this.surName = surname;
        this.firstName = firstname;
        this.setBirthDate(birthdate);
    }



    public List<StudentScore> getStudentScoreCollection() {
        return studentScoreCollection;
    }

    public void setStudentScoreCollection(List<StudentScore> studentScoreCollection) {
        this.studentScoreCollection = studentScoreCollection;
    }


    @Override
    public String toString() {
        return "Student{" +
                "studentScoreCollection=" + studentScoreCollection +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", surName='" + surName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }

    public SchoolClass getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(SchoolClass schoolClass) {
        this.schoolClass = schoolClass;
    }
}
