/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.schoolproject.beans;

import com.realdolmen.schoolproject.beans.abstractClasses.AbstractBean;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 *
 * @author JBLBL75
 */
@Entity
@Table(name = "schoolclass", schema = "schoolproject")
@NamedQueries({
    @NamedQuery(name = "Schoolclass.findAll", query = "SELECT s FROM SchoolClass s"),
    @NamedQuery(name = "Schoolclass.findByPk", query = "SELECT s FROM SchoolClass s WHERE s.pk = :pk"),
    @NamedQuery(name = "Schoolclass.findByClassnumber", query = "SELECT s FROM SchoolClass s WHERE s.classnumber = :classnumber")})
public class SchoolClass extends AbstractBean {

    @Size(max = 255)
    @Column(name = "classnumber")
    private String classnumber;
    @JoinColumn(name = "classgrade_id", referencedColumnName = "PK")
    @ManyToOne
    private ClassGrade classgradeId;
    @OneToMany(mappedBy = "schoolClass" ,fetch = FetchType.EAGER)
    private List<Student> students;
   @OneToMany(mappedBy = "courseClassTeachers_Class")
   private List<CourseClassTeacher> courseClassTeachers;

    public SchoolClass() {
    }

    public SchoolClass(Integer pk) {
        this.pk = pk;
    }

    public String getClassnumber() {
        return classnumber;
    }

    public void setClassnumber(String classnumber) {
        this.classnumber = classnumber;
    }

    public ClassGrade getClassgradeId() {
        return classgradeId;
    }

    public void setClassgradeId(ClassGrade classgradeId) {
        this.classgradeId = classgradeId;
    }


    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public List<CourseClassTeacher> getCourseClassTeachers() {
        return courseClassTeachers;
    }

    public void setCourseClassTeachers(List<CourseClassTeacher> courseClassTeachers) {
        this.courseClassTeachers = courseClassTeachers;
    }

    @Override
    public String toString() {
        return "SchoolClass{" +
                "classnumber='" + classnumber + '\'' +
                ", classgradeId=" + classgradeId +
                ", students=" + students +
                ", courseClassTeachers=" + courseClassTeachers +
                '}';
    }
}
