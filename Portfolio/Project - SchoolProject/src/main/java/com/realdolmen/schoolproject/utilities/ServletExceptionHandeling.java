package com.realdolmen.schoolproject.utilities;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletExceptionHandeling {
    private ServletExceptionHandeling() {
    }

    public static void defaultSQLException(Exception e, RequestDispatcher rd, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = e.getMessage();
        message = "SQLException: " + message;
        request.setAttribute("errorMessage",message);
        rd.forward(request,response);
    }

    public static void defaultException(RequestDispatcher rd, HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("errorMessage","A error has occurred, we're sorry for the inconvenience");
        rd.forward(request,response);
    }
}
