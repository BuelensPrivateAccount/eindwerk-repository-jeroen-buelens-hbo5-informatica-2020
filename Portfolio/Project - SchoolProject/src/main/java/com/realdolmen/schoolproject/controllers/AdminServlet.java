package com.realdolmen.schoolproject.controllers;

import com.realdolmen.schoolproject.beans.Administrator;
import com.realdolmen.schoolproject.beans.SchoolClass;
import com.realdolmen.schoolproject.beans.Student;
import com.realdolmen.schoolproject.beans.Teacher;
import com.realdolmen.schoolproject.beans.abstractClasses.Person;
import com.realdolmen.schoolproject.utilities.JPAUtility;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "AdminServlet", urlPatterns = "/AdminServlet")
public class AdminServlet extends HttpServlet {

    private static JPAUtility jpaUtility;

    @Override
    public void init() throws ServletException {
        jpaUtility = JPAUtility.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = null;
        HttpSession session = request.getSession(false);
        if (session == null || null == session.getAttribute("username") || session.getAttribute("username").equals("")) {

            rd = request.getRequestDispatcher("index.jsp");

        } else {
            if (request.getParameter("SaveTOS") != null) {
                rd = saveTeacherORStudent(request, response);
            } else if (request.getParameter("SavePassword") != null) {
                rd = savePassword(request, response);
            } else if (request.getParameter("action") != null) {
                String action = request.getParameter("action");
                int id;
                EntityManager em = jpaUtility.createEntityManager();
                List<SchoolClass> classes;
                switch (action) {
                    case "homepage":
                        rd = request.getRequestDispatcher("/WEB-INF/adminhome.jsp");
                        break;
                    case "showStudents":
                        rd = showAllStudents(request, response);
                        break;
                    case "showstudentbyid":
                        classes = em.createNamedQuery("Schoolclass.findAll", SchoolClass.class).getResultList();
                        request.setAttribute("classes", classes);
                        id = Integer.parseInt(request.getParameter("id"));
                        rd = showStudentByID(id, request, response);
                        break;
                    case "create":
                        classes = em.createNamedQuery("Schoolclass.findAll", SchoolClass.class).getResultList();
                        request.setAttribute("classes", classes);
                        rd = openCreatePage(request, response);
                        break;
                    case "showTeachers":
                        rd = showAllTeachers(request, response);
                        break;
                    case "showteacherbyid":
                        classes = em.createNamedQuery("Schoolclass.findAll", SchoolClass.class).getResultList();
                        request.setAttribute("classes", classes);
                        id = Integer.parseInt(request.getParameter("id"));
                        rd = showTeacherByID(id, request, response);
                        break;
                    case "changepassword":
                        String username = (String) session.getAttribute("username");
                        rd = changepassword(username, request, response);
                        break;
                    case "deleteTOS":
                        rd = deleteTOS(request, response);
                        break;
                }
            }
        }
        if (rd != null) {
            rd.forward(request, response);
        } else {
            throw new ServletException("you tried to do a a method/option that isn't implemented or available yet, please contact staff.");
        }
    }

    private RequestDispatcher deleteTOS(HttpServletRequest request, HttpServletResponse response) {
        EntityManager em = jpaUtility.createEntityManager();
        String ogUN = request.getParameter("OGUserName");
        Person user = null;
        try {
            TypedQuery<Student> query = em.createNamedQuery("Student.findByUsername", Student.class).setParameter("username", ogUN);
            user = query.getSingleResult();
        } catch (NoResultException ignored) {
        }
        try {
            TypedQuery<Teacher> query = em.createNamedQuery("Teacher.findByUsername", Teacher.class).setParameter("username", ogUN);
            user = query.getSingleResult();
        } catch (NoResultException ignored) {
        }
        if (null != user) {
            EntityTransaction tr = em.getTransaction();
            tr.begin();
            em.remove(user);
            tr.commit();
        }
        return request.getRequestDispatcher("/WEB-INF/adminhome.jsp");
    }

    private RequestDispatcher changepassword(String username, HttpServletRequest request, HttpServletResponse response) {
        Person user = null;
        EntityManager em = jpaUtility.createEntityManager();
        try {
            TypedQuery<Student> query = em.createNamedQuery("Student.findByUsername", Student.class).setParameter("username", username);
            user = query.getSingleResult();
        } catch (NoResultException ignored) {
        }
        try {
            TypedQuery<Teacher> query = em.createNamedQuery("Teacher.findByUsername", Teacher.class).setParameter("username", username);
            user = query.getSingleResult();
        } catch (NoResultException ignored) {

        }
        try {
            TypedQuery<Administrator> query = em.createNamedQuery("Administrator.findByUsername", Administrator.class).setParameter("username", username);
            user = query.getSingleResult();
        } catch (NoResultException ignored) {

        }
        request.setAttribute("user", user);
        return request.getRequestDispatcher("/WEB-INF/changepassword.jsp");
    }

    private RequestDispatcher savePassword(HttpServletRequest request, HttpServletResponse response) {
        EntityManager em = jpaUtility.createEntityManager();
        String password = request.getParameter("passWord");
        String username = (String) request.getSession().getAttribute("username");
        Person user = null;
        try {
            TypedQuery<Student> query = em.createNamedQuery("Student.findByUsername", Student.class).setParameter("username", username);
            user = query.getSingleResult();
        } catch (NoResultException ignored) {
        }
        try {
            TypedQuery<Teacher> query = em.createNamedQuery("Teacher.findByUsername", Teacher.class).setParameter("username", username);
            user = query.getSingleResult();
        } catch (NoResultException ignored) {

        }
        try {
            TypedQuery<Administrator> query = em.createNamedQuery("Administrator.findByUsername", Administrator.class).setParameter("username", username);
            user = query.getSingleResult();
        } catch (NoResultException ignored) {

        }
        if (user != null) {
            user.setPassword(password);
        }
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
        return request.getRequestDispatcher("index.jsp");
    }

    private RequestDispatcher showAllTeachers(HttpServletRequest request, HttpServletResponse response) {
        EntityManager em = jpaUtility.createEntityManager();
        TypedQuery<Teacher> query = em.createNamedQuery("Teacher.findAll", Teacher.class);
        List<Teacher> teachers = query.getResultList();
        request.setAttribute("teachers", teachers);
        return request.getRequestDispatcher("/WEB-INF/adminteachers.jsp");
    }

    private RequestDispatcher showTeacherByID(int id, HttpServletRequest request, HttpServletResponse response) {
        EntityManager em = jpaUtility.createEntityManager();
        TypedQuery<Teacher> query = em.createNamedQuery("Teacher.findByPk", Teacher.class).setParameter("pk", id);
        Teacher teacher = query.getSingleResult();
        request.setAttribute("teacher", teacher);
        return request.getRequestDispatcher("/WEB-INF/adminusercreationandeditpage.jsp");
    }

    private RequestDispatcher saveTeacherORStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        EntityManager em = jpaUtility.createEntityManager();
        String ogUN = request.getParameter("OGUserName");
        String type = request.getParameter("type");
        Person user = null;
        try {
            TypedQuery<Student> query = em.createNamedQuery("Student.findByUsername", Student.class).setParameter("username", ogUN);
            user = query.getSingleResult();
        } catch (NoResultException ignored) {
        }
        try {
            TypedQuery<Teacher> query = em.createNamedQuery("Teacher.findByUsername", Teacher.class).setParameter("username", ogUN);
            user = query.getSingleResult();
        } catch (NoResultException ignored) {
        }
        if (user == null) {
            if (type.equals("Teacher")) {
                user = new Teacher();
            } else if (type.equals("Student")) {
                user = new Student();
            } else {
                throw new ServletException("HELP something went wrong, you're absolutely not supposed to see this, i don't know why you were able to do something youré absolutely not supposed to, i give up...");
            }
        }
        EntityTransaction tr = em.getTransaction();
        tr.begin();
        if (user instanceof Teacher && type.equals("Student")) {
            String pw = user.getPassword();
            em.remove(user);
            user = new Student();
            user.setHashedPassword(pw);
        } else if (user instanceof Student && type.equals("Teacher")) {
            String pw = user.getPassword();
            em.remove(user);
            user = new Teacher();
            user.setHashedPassword(pw);
        }
        if (!request.getParameter("passWord").isEmpty()) {
            user.setPassword(request.getParameter("passWord"));
        }
        if (request.getParameter("birthdate") != null && !request.getParameter("birthdate").isEmpty()) {
            user.setBirthDate(request.getParameter("birthdate"));
        }
        user.setFirstName(request.getParameter("firstName"));
        user.setSurName(request.getParameter("surName"));
        user.setUsername(request.getParameter("userName"));
        if (user instanceof Student) {
            int schoolClassId = Integer.parseInt(request.getParameter("class"));
            SchoolClass schoolClass = em.createNamedQuery("Schoolclass.findByPk", SchoolClass.class).setParameter("pk", schoolClassId).getSingleResult();
            ((Student) user).setSchoolClass(schoolClass);
        }
        em.persist(user);
        tr.commit();
        return request.getRequestDispatcher("/WEB-INF/adminhome.jsp");
    }

    private RequestDispatcher openCreatePage(HttpServletRequest request, HttpServletResponse response) {
        return request.getRequestDispatcher("/WEB-INF/adminusercreationandeditpage.jsp");
    }

    private RequestDispatcher showStudentByID(int id, HttpServletRequest request, HttpServletResponse response) {
        EntityManager em = jpaUtility.createEntityManager();
        TypedQuery<Student> query = em.createNamedQuery("Student.findByPk", Student.class).setParameter("pk", id);
        Student student = query.getSingleResult();
        request.setAttribute("student", student);
        return request.getRequestDispatcher("/WEB-INF/adminusercreationandeditpage.jsp");
    }

    private RequestDispatcher showAllStudents(HttpServletRequest request, HttpServletResponse response) {
        EntityManager em = jpaUtility.createEntityManager();
        TypedQuery<Student> query = em.createNamedQuery("Student.findAll", Student.class);
        List<Student> students = query.getResultList();
        request.setAttribute("students", students);
        return request.getRequestDispatcher("/WEB-INF/adminstudents.jsp");
    }


}
