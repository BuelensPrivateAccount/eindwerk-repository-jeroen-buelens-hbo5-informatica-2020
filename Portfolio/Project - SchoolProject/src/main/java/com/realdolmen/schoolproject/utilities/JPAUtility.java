package com.realdolmen.schoolproject.utilities;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtility {
    private final EntityManagerFactory emf;
    private static JPAUtility jpaUtility;


    public void close() {
        emf.close();
    }

    public EntityManager createEntityManager() {
        return emf.createEntityManager();
    }

    private JPAUtility() {
        emf = Persistence.createEntityManagerFactory("com.realdolmen_SchoolProject_war_1.0-SNAPSHOTPU");
    }

    public static JPAUtility getInstance() {
        if (jpaUtility == null) {
            jpaUtility = new JPAUtility();
        }
        return jpaUtility;
    }
}
