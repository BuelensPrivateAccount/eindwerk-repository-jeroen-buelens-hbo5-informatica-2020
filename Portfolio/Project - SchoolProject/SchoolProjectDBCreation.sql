
Drop database `schoolproject`;
CREATE DATABASE `schoolproject` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `schoolproject`;
CREATE TABLE `admin` (
  `PK` int(11) NOT NULL AUTO_INCREMENT,
  `birthdate` date DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PK`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
CREATE TABLE `classgrade` (
  `PK` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) DEFAULT NULL,
  PRIMARY KEY (`PK`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
CREATE TABLE `coursesubject` (
  `PK` int(11) NOT NULL AUTO_INCREMENT,
  `coursesubject` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PK`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
CREATE TABLE `schoolclass` (
  `PK` int(11) NOT NULL AUTO_INCREMENT,
  `classnumber` varchar(255) DEFAULT NULL,
  `classgrade_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`PK`),
  KEY `FK_schoolclass_classgrade_id` (`classgrade_id`),
  CONSTRAINT `FK_schoolclass_classgrade_id` FOREIGN KEY (`classgrade_id`) REFERENCES `classgrade` (`PK`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
CREATE TABLE `student` (
  `PK` int(11) NOT NULL AUTO_INCREMENT,
  `birthdate` date DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `schoolclassid` int(11) DEFAULT NULL,
  PRIMARY KEY (`PK`),
  UNIQUE KEY `username` (`username`),
  KEY `FK_student_schoolclassid` (`schoolclassid`),
  CONSTRAINT `FK_student_schoolclassid` FOREIGN KEY (`schoolclassid`) REFERENCES `schoolclass` (`PK`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
CREATE TABLE `teacher` (
  `PK` int(11) NOT NULL AUTO_INCREMENT,
  `birthdate` date DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PK`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `studenttest` (
  `PK` int(11) NOT NULL AUTO_INCREMENT,
  `maxscore` int(11) DEFAULT NULL,
  `testname` varchar(255) DEFAULT NULL,
  `coursesubjectid` int(11) DEFAULT NULL,
  PRIMARY KEY (`PK`),
  KEY `FK_studenttest_coursesubjectid` (`coursesubjectid`),
  CONSTRAINT `FK_studenttest_coursesubjectid` FOREIGN KEY (`coursesubjectid`) REFERENCES `coursesubject` (`PK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `testdate` (
  `PK` int(11) NOT NULL AUTO_INCREMENT,
  `testdate` date DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`PK`),
  KEY `FK_testdate_test_id` (`test_id`),
  CONSTRAINT `FK_testdate_test_id` FOREIGN KEY (`test_id`) REFERENCES `studenttest` (`PK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `studentscore` (
  `PK` int(11) NOT NULL AUTO_INCREMENT,
  `score` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `testdate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`PK`),
  KEY `FK_studentscore_testdate_id` (`testdate_id`),
  KEY `FK_studentscore_student_id` (`student_id`),
  CONSTRAINT `FK_studentscore_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`PK`),
  CONSTRAINT `FK_studentscore_testdate_id` FOREIGN KEY (`testdate_id`) REFERENCES `testdate` (`PK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `courseclassteacher` (
  `PK` int(11) NOT NULL AUTO_INCREMENT,
  `schoolclass_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`PK`),
  KEY `FK_courseclassteacher_schoolclass_id` (`schoolclass_id`),
  KEY `FK_courseclassteacher_teacher_id` (`teacher_id`),
  KEY `FK_courseclassteacher_subject_id` (`subject_id`),
  CONSTRAINT `FK_courseclassteacher_schoolclass_id` FOREIGN KEY (`schoolclass_id`) REFERENCES `schoolclass` (`PK`),
  CONSTRAINT `FK_courseclassteacher_subject_id` FOREIGN KEY (`subject_id`) REFERENCES `coursesubject` (`PK`),
  CONSTRAINT `FK_courseclassteacher_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`PK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into schoolproject.admin (firstname,password,surname,username) values ("Admin","$2a$10$HDENQnngAnOGp4YGKJtlV.G/MGwuTcKXdFbBExxUnrALEpHPIQtf2","Admin","Admin");
insert into schoolproject.classgrade (PK,year) values (1,1),(2,2),(3,3),(4,4),(5,5),(6,6);
insert into schoolproject.coursesubject (PK,coursesubject) values (1,"testCourse");
insert into schoolproject.schoolclass (PK,classnumber,classgrade_id) values (1,"A",1),(2,"B",1),(3,"A",2),(4,"B",2),(5,"A",3),(6,"B",3),(7,"A",4),(8,"B",4),(9,"A",5),(10,"B",5),(11,"A",6),(12,"B",6);
insert into schoolproject.student (PK,birthdate,firstname,password,surname,username,schoolclassid) values (1,"1900-01-01","TestStudentFirstName","$2a$10$GK4/k4c6XJ9ZxHM9YHcR0.J2/Y//mO9eOfet8FWpmw8wuOMaHA0pa","TestStudentSurName","TestStudent",1);
insert into schoolproject.teacher (PK,birthdate,firstname,password,surname,username) values (1,"1900-01-01","TestTeacherFirstName","$2a$10$3z6EBEq0AXMmH.8b0m3eP.PB0s/nVVkCCbeDJuyeOGA2CWfI86eOG","TestTeacherSurName","TestTeacher");
insert into schoolproject.studenttest (PK,maxscore,testname,coursesubjectid) values (1,10,"een Test die een leerkracht afnam",1);
insert into schoolproject.testdate (PK,testdate,test_id) values (1,CURRENT_DATE(),1);
insert into schoolproject.courseclassteacher (PK,schoolclass_id,subject_id,teacher_id) values (1,1,1,1);
insert into schoolproject.studentscore (PK,score,student_id,testdate_id) values (1,8,1,1);