@echo off
set batdir=%~dp0
pushd "%batdir%"
cd %batdir%out\artifacts\auctionhouse_jar
java -jar auctionhouse.jar
PAUSE