CREATE DATABASE `wow` /*!40100 DEFAULT CHARACTER SET latin1 */;
use wow;
CREATE TABLE `auctiontype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `faction` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `money` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `alliance_ah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL,
  `ItemName` varchar(45) NOT NULL,
  `Amount` int(11) NOT NULL DEFAULT '1',
  `postage_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `buyout_price` int(11) NOT NULL,
  `current_minimal_bid` int(11) NOT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `auctiontype_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID_UNIQUE` (`id`),
  KEY `id_idx` (`auctiontype_id`),
  KEY `id_idx1` (`seller_id`),
  KEY `id_idx2` (`buyer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `neutral_ah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL,
  `ItemName` varchar(45) NOT NULL,
  `Amount` int(11) NOT NULL DEFAULT '1',
  `postage_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `buyout_price` int(11) NOT NULL,
  `current_minimal_bid` int(11) NOT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `auctiontype_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID_UNIQUE` (`id`),
  KEY `id_idx` (`auctiontype_id`),
  KEY `id_idx1` (`seller_id`),
  KEY `id_idx2` (`buyer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `horde_ah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL,
  `ItemName` varchar(45) NOT NULL,
  `Amount` int(11) NOT NULL DEFAULT '1',
  `postage_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `buyout_price` int(11) NOT NULL,
  `current_minimal_bid` int(11) NOT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `auctiontype_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID_UNIQUE` (`id`),
  KEY `id_idx` (`auctiontype_id`),
  KEY `id_idx1` (`seller_id`),
  KEY `id_idx2` (`buyer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
