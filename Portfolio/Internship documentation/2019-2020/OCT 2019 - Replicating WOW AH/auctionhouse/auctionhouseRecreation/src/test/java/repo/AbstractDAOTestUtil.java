package repo;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;

import java.io.File;
import java.net.MalformedURLException;
import java.sql.SQLException;

/**
 * This testClass was made to make a impelementable super.setUp and super.powerDown to be able to be used in every single DAOTest
 */
abstract class AbstractDAOTestUtil {
    IDatabaseConnection iDatabaseConnection;
    IDataSet iDataSet;

    public void setUp() throws SQLException, DatabaseUnitException, MalformedURLException {
        AbstractDAO.OpenConnection();
        iDatabaseConnection = new DatabaseConnection(AbstractDAO.connection);
        iDataSet = new FlatXmlDataSetBuilder().build(new File("src/test/mockData.xml"));
        DatabaseOperation.CLEAN_INSERT.execute(iDatabaseConnection, iDataSet);
    }

    public void powerDown() throws SQLException {
        AbstractDAO.CloseConnection();
    }

    abstract Object fetchFromITable(ITable table, int row);
}
