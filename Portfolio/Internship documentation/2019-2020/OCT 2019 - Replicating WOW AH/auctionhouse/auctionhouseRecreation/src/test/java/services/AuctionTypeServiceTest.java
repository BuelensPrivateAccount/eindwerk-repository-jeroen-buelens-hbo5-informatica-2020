package services;

import domain.AuctionType;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.AuctionTypeDAO;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AuctionTypeServiceTest {

    @Mock
    AuctionTypeDAO auctionTypeDAO;

    @InjectMocks
    AuctionTypeService auctionTypeService;

    @Test
    public void emptyTest() {
    }

    @DisplayName("Asserts that FindAllAuctionTypes finds something.")
    @Test
    public void findAllAuctionTypesTest() throws SQLException, NoRecordFoundException {
        AuctionType auctionType = new AuctionType();
        auctionType.setName("test");
        List<AuctionType> list = new ArrayList<>();
        list.add(auctionType);
        when(auctionTypeDAO.findAllAuctionTypes()).thenReturn(list);
        List<AuctionType> output = auctionTypeService.findAllAuctionTypes();
        assertEquals(list,output);
    }

    @DisplayName("Asserts that norecordfound is thrown correctly")
    @Test
    public void findAllAuctionTypesTestThrowNoRecordFoundException() throws SQLException {
        when(auctionTypeDAO.findAllAuctionTypes()).thenReturn(new ArrayList<>());
        assertThrows(NoRecordFoundException.class,()->{
            auctionTypeService.findAllAuctionTypes();
        });
    }
    
    @DisplayName("Asserts that findAuctionTypeById does return the correct auctionType")
    @Test
    public void findAuctionTypeByIdTest() throws SQLException, NoRecordFoundException {
        AuctionType auctionType = new AuctionType();
        auctionType.setName("test");
        auctionType.setId(1);
        when(auctionTypeDAO.findAuctionTypeById(anyInt())).thenReturn(auctionType);
        AuctionType output = auctionTypeService.findAuctionTypeByID(1);
        assertEquals(auctionType,output);
    }

    @DisplayName("Asserts that NoRecordFoundException is thrown correctly")
    @Test
    public void findAuctionTypeByIdTestThrowNoRecordFoundException() throws SQLException {
        when(auctionTypeDAO.findAuctionTypeById(anyInt())).thenReturn(new AuctionType());
        assertThrows(NoRecordFoundException.class,()->{
            auctionTypeService.findAuctionTypeByID(123456);
        });
    }
}