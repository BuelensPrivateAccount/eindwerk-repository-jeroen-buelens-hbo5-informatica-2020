package repo;

import domain.Player;


import exceptions.NoRecordFoundException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.ITable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.dbunit.DatabaseUnitException;
import org.junit.jupiter.api.*;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This is the class that tests the {@link PlayerDAO}
 */
class PlayerDAOTest extends AbstractDAOTestUtil {
    private PlayerDAO playerDAO = new PlayerDAO();

    /**
     * Implementation of the generic Setup from {@link AbstractDAOTestUtil}
     *
     * @throws SQLException          {@link SQLException}
     * @throws DatabaseUnitException {@link DatabaseUnitException}
     * @throws MalformedURLException {@link MalformedURLException}
     */
    @Override
    @BeforeEach
    public void setUp() throws SQLException, DatabaseUnitException, MalformedURLException {
        super.setUp();
    }

    /**
     * Implementation of the generic powerDown from {@link AbstractDAOTestUtil}
     *
     * @throws SQLException {@link SQLException}
     */
    @Override
    @AfterEach
    public void powerDown() throws SQLException {
        super.powerDown();
    }

    /**
     * generic empty test to verify the class works during creation of the class
     */
    @Test
    public void EmptyTest() {
    }

    @DisplayName("Assert that FindPlayerWhereNameHas Returns Correct player")
    @Test
    public void findPlayerWhereNameHasTest() throws DataSetException, SQLException {
        Player output = playerDAO.findPlayerWhereNameHas("ugZu").get(0);
        Player reference = (Player) fetchFromITable(super.iDataSet.getTable("player"), 0);
        assertAll(() -> {
            assertEquals(reference.getId(), output.getId());
            assertEquals(reference.getName(), output.getName());
            assertEquals(reference.getPassword(), output.getPassword());
            assertEquals(reference.getFaction(), output.getFaction());
        });
    }


    @DisplayName("Asserts that findAllPlayers does indeed return something")
    @Test
    public void assertPlayerExistsTest() throws SQLException {
        List<Player> players = playerDAO.findAllPlayers();
        assertAll(() -> {
            assertFalse(players.isEmpty());
            Player player = players.get(0);
            assertTrue(player.getId() > 0);
            assertFalse(player.getName().isEmpty());
            assertFalse(player.getFaction().isEmpty());
            assertFalse(player.getPassword().isEmpty());
        });
    }

    @DisplayName("Asserts that findAllHordePlayers does indeed return something")
    @Test
    public void assertHordePlayerExistsTest() throws SQLException {
        List<Player> players = playerDAO.findAllHordePlayers();
        assertAll(() -> {
            assertFalse(players.isEmpty());
            Player player = players.get(0);
            assertTrue(player.getId() > 0);
            assertFalse(player.getName().isEmpty());
            assertFalse(player.getFaction().isEmpty());
            assertFalse(player.getPassword().isEmpty());
        });
    }

    @DisplayName("Asserts that findAllAlliancePlayers does indeed return something")
    @Test
    public void assertAlliancePlayerExistsTest() throws SQLException {
        List<Player> players = playerDAO.findAllAlliancePlayers();
        assertAll(() -> {
            assertFalse(players.isEmpty());
            Player player = players.get(0);
            assertTrue(player.getId() > 0);
            assertFalse(player.getName().isEmpty());
            assertFalse(player.getFaction().isEmpty());
            assertFalse(player.getPassword().isEmpty());
        });
    }


    @DisplayName("Asserts that FindById Finds the correct Player")
    @Test
    public void assertFindPlayerByIdTest() throws DataSetException, SQLException {
        Player output = playerDAO.findPlayerByID(1);
        Player reference = (Player) fetchFromITable(super.iDataSet.getTable("player"), 0);
        assertAll(() -> {
            assertEquals(reference.getId(), output.getId());
            assertEquals(reference.getName(), output.getName());
            assertEquals(reference.getPassword(), output.getPassword());
            assertEquals(reference.getFaction(), output.getFaction());
        });
    }

    @DisplayName("Asserts that find by specific name Finds the correct Player")
    @Test
    public void assertFindPlayerBynameTest() throws DataSetException, SQLException {
        Player reference = (Player) fetchFromITable(super.iDataSet.getTable("player"), 0);
        Player output = playerDAO.findSpecificPlayerByName(reference.getName());
        assertAll(() -> {
            assertEquals(reference.getId(), output.getId());
            assertEquals(reference.getName(), output.getName());
            assertEquals(reference.getPassword(), output.getPassword());
            assertEquals(reference.getFaction(), output.getFaction());
        });
    }

    @DisplayName("Asserts that Create works")
    @Test
    public void createTest() {
        Player player = new Player();
        player.setId(23);
        player.setName("asdf");
        player.setFaction("Horde");
        player.setPassword("qwt");
        assertDoesNotThrow(() -> {
            playerDAO.create(player);
        });

    }

    @DisplayName("Asserts that update money works")
    @Test
    public void UpdateMoneyTest() throws DataSetException {
        Player updateTestPlayer = (Player) fetchFromITable(super.iDataSet.getTable("player"), 1);
        updateTestPlayer.setMoney(123456);
        assertDoesNotThrow(()-> {
            playerDAO.updatePlayerPass(updateTestPlayer);
        });

    }
    @DisplayName("Asserts that update pass works")
    @Test
    public void UpdatePassTest() throws DataSetException {
        Player updateTestPlayer = (Player) fetchFromITable(super.iDataSet.getTable("player"), 1);
        updateTestPlayer.setPassword("test");
        assertDoesNotThrow(()-> {
            playerDAO.updatePlayerMoney(updateTestPlayer);
        });

    }
    @DisplayName("Asserts that Delete works")
    @Test
    public void DeleteTest() throws DataSetException {
        Player updateTestPlayer = (Player) fetchFromITable(super.iDataSet.getTable("player"), 1);
        assertDoesNotThrow(()-> {
            playerDAO.delete(updateTestPlayer);
        });
    }

    @Override
    Object fetchFromITable(ITable table, int row) {
        try {
            Player player = new Player();
            player.setName(table.getValue(row, "name").toString());
            player.setPassword(table.getValue(row, "password").toString());
            player.setFaction(table.getValue(row, "faction").toString());
            player.setId(Integer.parseInt(table.getValue(row, "id").toString()));
            return player;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}