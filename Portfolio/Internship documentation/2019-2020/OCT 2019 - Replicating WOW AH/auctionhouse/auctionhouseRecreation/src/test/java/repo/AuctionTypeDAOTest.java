package repo;

import domain.AuctionType;


import exceptions.NoRecordFoundException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.ITable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.dbunit.DatabaseUnitException;
import org.junit.jupiter.api.*;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 * This is the class that tests the {@link AuctionTypeDAO}
 */
public class AuctionTypeDAOTest extends AbstractDAOTestUtil {

    private AuctionTypeDAO auctionTypeDAO = new AuctionTypeDAO();

    /**
     * Implementation of the generic Setup from {@link AbstractDAOTestUtil}
     *
     * @throws SQLException          {@link SQLException}
     * @throws DatabaseUnitException {@link DatabaseUnitException}
     * @throws MalformedURLException {@link MalformedURLException}
     */
    @Override
    @BeforeEach
    public void setUp() throws SQLException, DatabaseUnitException, MalformedURLException {
        super.setUp();
    }

    /**
     * Implementation of the generic powerDown from {@link AbstractDAOTestUtil}
     *
     * @throws SQLException {@link SQLException}
     */
    @Override
    @AfterEach
    public void powerDown() throws SQLException {
        super.powerDown();
    }

    /**
     * generic empty test to verify the class works during creation of the class
     */
    @Test
    public void EmptyTest() {
    }

    @DisplayName("Asserts that findAllAuctionTypes does indeed return something")
    @Test
    public void assertAuctionTypeExistsTest() throws SQLException, NoRecordFoundException {
        List<AuctionType> auctionTypes = auctionTypeDAO.findAllAuctionTypes();
        assertAll(() -> {
            assertFalse(auctionTypes.isEmpty());
            AuctionType auctionType = auctionTypes.get(0);
            assertTrue(auctionType.getId() > 0);
            assertFalse(auctionType.getType().isEmpty());
        });
    }

    @DisplayName("Asserts that FindById Finds the correct AuctionType")
    @Test
    public void assertFindAuctionTypeByIdTest() throws DataSetException, SQLException, NoRecordFoundException {
        AuctionType output = auctionTypeDAO.findAuctionTypeById(1);
        AuctionType reference = (AuctionType) fetchFromITable(super.iDataSet.getTable("auctiontype"), 0);
        assertAll(() -> {
            assertEquals(reference.getId(), output.getId());
            assertEquals(reference.getType(), output.getType());
        });
    }

    @Override
    Object fetchFromITable(ITable table, int row) {
        try {
            AuctionType auctionType = new AuctionType();
            auctionType.setId(Integer.parseInt((table.getValue(row, "id").toString())));
            auctionType.setType(table.getValue(row, "type").toString());
            return auctionType;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
