package repo;

import domain.Auction;


import exceptions.GotDarnITFuckingUserException;
import exceptions.NoRecordFoundException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.ITable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.dbunit.DatabaseUnitException;
import org.junit.jupiter.api.*;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static repo.AuctionDAO.*;


class AuctionDAOTest extends AbstractDAOTestUtil {
    private AuctionDAO auctionDAO = new AuctionDAO();

    /**
     * Implementation of the generic Setup from {@link AbstractDAOTestUtil}
     *
     * @throws SQLException          {@link SQLException}
     * @throws DatabaseUnitException {@link DatabaseUnitException}
     * @throws MalformedURLException {@link MalformedURLException}
     */
    @Override
    @BeforeEach
    public void setUp() throws SQLException, DatabaseUnitException, MalformedURLException {
        super.setUp();
    }

    /**
     * Implementation of the generic powerDown from {@link AbstractDAOTestUtil}
     *
     * @throws SQLException {@link SQLException}
     */
    @Override
    @AfterEach
    public void powerDown() throws SQLException {
        super.powerDown();
    }

    /**
     * generic empty test to verify the class works during creation of the class
     */
    @Test
    public void EmptyTest() {
    }


    @DisplayName("Asserts that findAllAuctions does indeed return something")
    @Test
    public void assertAuctionExistsTest() throws SQLException, NoRecordFoundException, GotDarnITFuckingUserException {
        List<Auction> auctions = auctionDAO.findAllAuctions(HORDE);
        assertAll(() -> {
            assertFalse(auctions.isEmpty());
            Auction auction = auctions.get(0);
            assertTrue(auction.getId() > 0);
            assertFalse(auction.getName().isEmpty());
        });
    }

    @DisplayName("Asserts that FindById Finds the correct Auction")
    @Test
    public void assertFindAuctionByIdTest() throws DataSetException, SQLException, NoRecordFoundException, GotDarnITFuckingUserException {
        Auction output = auctionDAO.findAuctionById(HORDE, 1);
        Auction reference = (Auction) fetchFromITable(super.iDataSet.getTable(HORDE), 1);
        assertAll(() -> {
            assertEquals(reference.getId(), output.getId());
            assertEquals(reference.getName(), output.getName());
        });
    }

    @DisplayName("Asserts that FindByQueryLikeness Finds the correct Auction")
    @Test
    public void assertFindAuctionByQueryLikenessTest() throws DataSetException, SQLException, NoRecordFoundException, GotDarnITFuckingUserException {
        List<Auction> outputList = auctionDAO.findAuctionsByQueryLikeness(HORDE, "test");
        Auction output = outputList.get(0);
        Auction reference = (Auction) fetchFromITable(super.iDataSet.getTable(HORDE), 1);
        assertAll(() -> {
            assertEquals(reference.getId(), output.getId());
            assertEquals(reference.getName(), output.getName());
        });
    }

    @DisplayName("Asserts that FindAuctionBySellerId Finds the correct Auction")
    @Test
    public void assertFindAuctionBySellerIDTest() throws DataSetException, SQLException, NoRecordFoundException, GotDarnITFuckingUserException {
        List<Auction> outputList = auctionDAO.findAuctionsBySellerId(HORDE,1);
        Auction output = outputList.get(0);
        Auction reference = (Auction) fetchFromITable(super.iDataSet.getTable(HORDE), 1);
        assertAll(() -> {
            assertEquals(reference.getId(), output.getId());
            assertEquals(reference.getName(), output.getName());
        });
    }

    @DisplayName("Asserts that Create works")
    @Test
    public void createTest() {
        Auction auction = new Auction();
        auction.setSellerID(1);
        auction.setName("asdf");
        auction.setAmount(123);
        auction.setBuyoutPrice(1);
        auction.setType(1);
        auction.setCurrentMinimalBid(122);
        auction.setId(23);
            assertDoesNotThrow(()->{
                auctionDAO.create(auction,HORDE);
            });

    }

    @DisplayName("Asserts that update works")
    @Test
    public void UpdateTest() throws DataSetException {
        Auction updateTestAuction = (Auction) fetchFromITable(super.iDataSet.getTable(HORDE), 1);
        updateTestAuction.setBuyerID(1);
        updateTestAuction.setCurrentMinimalBid(123456);
        assertDoesNotThrow(()-> {
            auctionDAO.updateAuction(updateTestAuction,HORDE);
        });

    }

    @DisplayName("Asserts that Delete works")
    @Test
    public void DeleteTest() throws DataSetException {
        Auction updateTestAuction = (Auction) fetchFromITable(super.iDataSet.getTable(HORDE), 1);
        assertDoesNotThrow(()-> {
            auctionDAO.delete(updateTestAuction,HORDE);
        });
    }

    @DisplayName("Asserts that Exception is thrown when not using correct input")
    @Test
    public void assertAuctionThrowsTest() {
        assertAll(() -> {
            assertThrows(GotDarnITFuckingUserException.class, () -> {
                auctionDAO.findAuctionById("ssdjkajkdf", 0);
            });
            assertThrows(GotDarnITFuckingUserException.class, () -> {
                auctionDAO.findAllAuctions("ssdjkajkdf");
            });
            assertThrows(GotDarnITFuckingUserException.class, () -> {
                auctionDAO.findAuctionsByQueryLikeness("sdfhjk", "sdfghj");
            });
            assertThrows(GotDarnITFuckingUserException.class, () -> {
                auctionDAO.create(new Auction(),"sdgh");
            });
            assertThrows(GotDarnITFuckingUserException.class, () -> {
                auctionDAO.updateAuction(new Auction(),"sdgh");
            });
            assertThrows(GotDarnITFuckingUserException.class, () -> {
                auctionDAO.delete(new Auction(),"sdgh");
            });
        });
    }

    @Override
    Object fetchFromITable(ITable table, int row) {
        try {
            Auction auction = new Auction();
            auction.setId(Integer.parseInt((table.getValue(row, "id").toString())));
            auction.setItemName(table.getValue(row, "ItemName").toString());
            auction.setCurrentMinimalBid(Integer.parseInt((table.getValue(row, "current_minimal_bid").toString())));
            auction.setBuyoutPrice(Integer.parseInt((table.getValue(row, "current_minimal_bid").toString())));
            auction.setBuyerID(Integer.parseInt((table.getValue(row, "current_minimal_bid").toString())));
            auction.setSellerID(Integer.parseInt((table.getValue(row, "current_minimal_bid").toString())));
            auction.setAmount(Integer.parseInt((table.getValue(row, "current_minimal_bid").toString())));
            auction.setType(Integer.parseInt((table.getValue(row, "current_minimal_bid").toString())));
            auction.setPostageDate(new Date());
            return auction;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}