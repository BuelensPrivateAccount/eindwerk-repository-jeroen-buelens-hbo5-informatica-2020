package domain;

/**
 * this is the AuctionType object.
 * @see DomainInterface
 */
public class AuctionType implements DomainInterface {
    private int id;
    private String Type;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return getType();
    }

    @Override
    public void setName(String s) {
        setType(s);
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    @Override
    public String toString() {
        return "AuctionType{" +
                "id=" + id +
                ", Type='" + Type + '\'' +
                '}';
    }
}
