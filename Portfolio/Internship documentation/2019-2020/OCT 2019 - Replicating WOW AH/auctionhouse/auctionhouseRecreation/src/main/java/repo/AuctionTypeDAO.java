package repo;

import domain.AuctionType;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AuctionTypeDAO extends AbstractDAO {
    /**
     * Method that will find all AuctionTypes
     * @return List of {@link AuctionType}
     * @throws SQLException sql handling
     */
    public List<AuctionType> findAllAuctionTypes() throws SQLException {
        List<AuctionType> auctionTypes = new ArrayList<>();
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * from auctiontype");
        statement.execute();
        ResultSet set = statement.getResultSet();
        while (set.next()) {
            auctionTypes.add((AuctionType) setToData(set));
        }
        CloseConnection();
        statement.close();
            return auctionTypes;
    }

    /**
     * Method will look up a specific {@link AuctionType} and returns it.
     * @param id {@link Integer}
     * @return {@link AuctionType}
     * @throws SQLException thrown when no records are found
     */
    public AuctionType findAuctionTypeById(int id) throws SQLException {
        AuctionType auctionType = new AuctionType();
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * from auctiontype WHERE id = ?");
        statement.setInt(1, id);
        statement.execute();
        ResultSet set = statement.getResultSet();
        if (set.next()) {
            auctionType = (AuctionType) setToData(set);
        }
        CloseConnection();
        statement.close();
        return auctionType;
    }

    /**
     * @see AbstractDAO#setToData(ResultSet)
     * @param set this is the resultSet gotten by a method and makes a usable Object with it
     * @return {@link AuctionType} as superClass {@link Object}
     * @throws SQLException sql handling.
     */
    @Override
    protected Object setToData(ResultSet set) throws SQLException {
        AuctionType auctionType = new AuctionType();
        auctionType.setId(set.getInt("id"));
        auctionType.setType(set.getString("type"));
        return auctionType;
    }


}
