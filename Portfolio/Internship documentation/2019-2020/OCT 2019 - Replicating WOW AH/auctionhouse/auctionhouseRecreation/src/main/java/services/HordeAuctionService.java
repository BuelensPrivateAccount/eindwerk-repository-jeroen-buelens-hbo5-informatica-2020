package services;

import domain.Auction;
import exceptions.GotDarnITFuckingUserException;
import exceptions.NoRecordFoundException;
import repo.AuctionDAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HordeAuctionService {
    private AuctionDAO auctionDAO;
    private String type = AuctionDAO.HORDE;

    public HordeAuctionService(AuctionDAO auctionDAO) {
        this.auctionDAO = auctionDAO;
    }

    /**
     * method finds all auctions
     *
     * @return list of {@link Auction}
     * @throws NoRecordFoundException thrown when nothing is found
     * @throws SQLException           sql handling
     */
    public List<Auction> findAllAuctions() throws NoRecordFoundException, SQLException {
        List<Auction> auctions = new ArrayList<>();
        try {
            auctions = auctionDAO.findAllAuctions(type);
        } catch (GotDarnITFuckingUserException e) {
            e.printStackTrace();
        }
        if (auctions.isEmpty()) {
            throw new NoRecordFoundException("No records found");
        } else {
            return auctions;
        }

    }

    /**
     * method finds all auctions from a specific seller
     *
     * @param id the id of the seller
     * @return list of {@link Auction}
     * @throws SQLException           sql handling
     * @throws NoRecordFoundException sql handling
     */
    public List<Auction> findAuctionsBySellerID(int id) throws SQLException, NoRecordFoundException {
        List<Auction> auctions = new ArrayList<>();
        try {
            auctions = auctionDAO.findAuctionsBySellerId(type, id);
        } catch (GotDarnITFuckingUserException e) {
            e.printStackTrace();
        }
        if (auctions.isEmpty()) {
            throw new NoRecordFoundException("No records found");
        } else {
            return auctions;
        }
    }

    /**
     * method finds all auctions that have query as part of the name
     *
     * @param query the userQuery the user inputs
     * @return list of {@link Auction}
     * @throws SQLException           sql handling
     * @throws NoRecordFoundException sql handling
     */
    public List<Auction> findAuctionsByQueryLikeness(String query) throws SQLException, NoRecordFoundException {
        List<Auction> auctions = new ArrayList<>();
        try {
            auctions = auctionDAO.findAuctionsByQueryLikeness(type, query);
        } catch (GotDarnITFuckingUserException e) {
            e.printStackTrace();
        }
        if (auctions.isEmpty()) {
            throw new NoRecordFoundException("No records found");
        } else {
            return auctions;
        }
    }

    /**
     * method finds a specific auction by it's auctionID
     *
     * @param id the id the method is looking by
     * @return {@link Auction}
     * @throws SQLException           sql handling
     * @throws NoRecordFoundException sql handling
     */
    public Auction findAuctionById(int id) throws SQLException, NoRecordFoundException {
        Auction auction = new Auction();
        try {
            auction = auctionDAO.findAuctionById(type, id);
        } catch (GotDarnITFuckingUserException e) {
            e.printStackTrace();
        }
        if (auction.getId() == 0) {
            throw new NoRecordFoundException("No records found");
        } else {
            return auction;
        }
    }

    /**
     * method creates a new auction on the database
     *
     * @param auction {@link Auction}
     * @throws SQLException sql handling
     */
    public void createNewAuction(Auction auction) throws SQLException {
        try {
            auctionDAO.create(auction, type);
        } catch (GotDarnITFuckingUserException e) {
            e.printStackTrace();
        }
    }

    /**
     * method deletes a auction from the database
     *
     * @param auction {@link Auction}
     * @throws SQLException sql handling
     */
    public void deleteAuction(Auction auction) throws SQLException {
        try {
            auctionDAO.delete(auction, type);
        } catch (GotDarnITFuckingUserException e) {
            e.printStackTrace();
        }
    }
    /**
     * method updates a auction's buyer_id and current bid
     *
     * @param auction {@link Auction}
     * @throws SQLException sql handling
     */
    public void updateAuction(Auction auction)throws SQLException{
        try {
            auctionDAO.updateAuction(auction,type);
        } catch (GotDarnITFuckingUserException e) {
            e.printStackTrace();
        }
    }
}
