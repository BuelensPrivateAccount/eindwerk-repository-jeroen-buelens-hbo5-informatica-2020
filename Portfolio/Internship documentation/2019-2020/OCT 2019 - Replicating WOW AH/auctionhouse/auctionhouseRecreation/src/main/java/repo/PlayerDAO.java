package repo;

import domain.Player;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PlayerDAO extends AbstractDAO {
    /**
     * Method that will look for all {@link Player} in the DataBase
     *
     * @return list of {@link Player}
     * @throws SQLException sql handling
     */
    public List<Player> findAllPlayers() throws SQLException {
        List<Player> players = new ArrayList<>();
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * from player");
        statement.execute();
        ResultSet set = statement.getResultSet();
        if (set.next()) {
            players.add((Player) setToData(set));
        }
        while (set.next()) {
            players.add((Player) setToData(set));
        }
        CloseConnection();
        statement.close();
        return players;
    }

    /**
     * Method that finds All Alliance Players
     *
     * @return List of {@link Player}
     * @throws SQLException sql handling
     */
    public List<Player> findAllAlliancePlayers() throws SQLException {
        String factionName = "Alliance";
        return findAllPlayersOFAFaction(factionName);
    }

    /**
     * Method that finds All Horde Players
     *
     * @return List of {@link Player}
     * @throws SQLException sql handling
     */
    public List<Player> findAllHordePlayers() throws SQLException {
        String factionName = "Horde";
        return findAllPlayersOFAFaction(factionName);
    }

    /**
     * This method will find all players of a specific faction
     *
     * @param factionName This is the factionname it's searching on
     * @return list of {@link Player}
     * @throws SQLException sql handling
     */
    private List<Player> findAllPlayersOFAFaction(String factionName) throws SQLException {
        List<Player> players = new ArrayList<>();
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * from player WHERE faction = ?");
        statement.setString(1, factionName);
        statement.execute();
        ResultSet set = statement.getResultSet();
        if (set.next()) {
            players.add((Player) setToData(set));
        }
        while (set.next()) {
            players.add((Player) setToData(set));
        }
        CloseConnection();
        statement.close();
        return players;

    }

    /**
     * Method will look up a specific {@link Player} and returns it.
     *
     * @param id {@link Integer}
     * @return {@link Player}
     * @throws SQLException sql handling
     */
    public Player findPlayerByID(int id) throws SQLException {
        Player player = new Player();
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * from player WHERE id = ?");
        statement.setInt(1, id);
        statement.execute();
        ResultSet set = statement.getResultSet();
        if (set.next()) {
            player = (Player) setToData(set);
        }
        CloseConnection();
        statement.close();
        return player;
    }

    /**
     * Method will look up for all players with "a string" in their name
     *
     * @param query the sting to match to
     * @return list of {@link Player}
     * @throws SQLException sql handling
     * @see PlayerDAO#findPlayersWhereName(String)
     */
    public List<Player> findPlayerWhereNameHas(String query) throws SQLException {
        query = "%" + query + "%";
        return findPlayersWhereName(query);
    }

    /**
     * Method will look up for all players with "a string" in their name
     *
     * @param query the sting to match to
     * @return list of {@link Player}
     * @throws SQLException sql handling
     */
    private List<Player> findPlayersWhereName(String query) throws SQLException {
        List<Player> players = new ArrayList<>();
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * from player WHERE name LIKE ?");
        statement.setString(1, query);
        statement.execute();
        ResultSet set = statement.getResultSet();
        if (set.next()) {
            players.add((Player) setToData(set));
        }
        while (set.next()) {
            players.add((Player) setToData(set));
        }
        CloseConnection();
        statement.close();
        return players;
    }

    public Player findSpecificPlayerByName(String query) throws SQLException {
      Player player = new Player();
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * from player WHERE name = ?");
        statement.setString(1, query);
        statement.execute();
        ResultSet set = statement.getResultSet();
        if (set.next()) {
            player = (Player) setToData(set);
        }
        CloseConnection();
        statement.close();
        return player;
    }
    
    /**
     * @param set this is the resultSet gotten by a method and makes a usable Object with it
     * @return {@link Player} as SuperClass {@link Object}
     * @throws SQLException sql handling.
     * @see AbstractDAO#setToData(ResultSet)
     */
    @Override
    protected Object setToData(ResultSet set) throws SQLException {
        Player player = new Player();
        player.setId(set.getInt("id"));
        player.setFaction(set.getString("faction"));
        player.setPassword(set.getString("password"));
        player.setName(set.getString("name"));
        player.setMoney(set.getLong("money"));
        return player;
    }

    /**
     * This method will create a new entry on the database
     *
     * @param object {@link Player}
     * @throws SQLException sql handling
     */
    public void create(Object object) throws SQLException {
        String query = "INSERT INTO " + "player" + " (name, faction, password)" +
                " values ( ?, ?, ?)";
        Player player = (Player) object;
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, player.getName());
        statement.setString(2, player.getFaction());
        statement.setLong(3, player.getMoney());
        statement.execute();
        statement.close();
        CloseConnection();
    }

    /**
     * this method will update the players money
     *
     * @param replacement {@link Player}
     * @throws SQLException sql handling
     */
    public void updatePlayerMoney(Object replacement) throws SQLException {
        Player player = (Player) replacement;
        String query = "UPDATE " + "player" + " SET " +
                "money= ?" +
                " WHERE id = ?";
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(2, player.getId());
        statement.setLong(1, player.getMoney());
        statement.execute();
        statement.close();
        CloseConnection();
    }

    /**
     * this method will update the players password
     *
     * @param replacement {@link Player}
     * @throws SQLException sql handling
     */
    public void updatePlayerPass(Object replacement) throws SQLException {
        Player player = (Player) replacement;
        String query = "UPDATE " + "wow.player" + " SET " +
                "password= ?" +
                " WHERE id = ?";
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(2, player.getId());
        statement.setString(1, player.getPassword());
        statement.execute();
        statement.close();
        CloseConnection();
    }

    /**
     * this method will delete a specific player
     *
     * @param object {@link Player}
     * @throws SQLException sql handling
     */
    public void delete(Object object) throws SQLException {
        String query = "DELETE FROM " + "wow.player" + " WHERE id = ?";
        Player playerInsert = (Player) object;
        OpenConnection();
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, playerInsert.getId());
        statement.execute();
        statement.close();
        CloseConnection();
    }
}
