package services;

import domain.AuctionType;
import exceptions.NoRecordFoundException;
import repo.AuctionTypeDAO;

import java.sql.SQLException;
import java.util.List;

public class AuctionTypeService {
    private AuctionTypeDAO auctionTypeDAO;

    /**
     * Constructor
     *
     * @param auctionTypeDAO this is the database access object of auctionType that this service uses
     */
    public AuctionTypeService(AuctionTypeDAO auctionTypeDAO) {
        this.auctionTypeDAO = auctionTypeDAO;
    }

    /**
     * method finds all auctionTypes
     *
     * @return List of {@link AuctionType}
     * @throws SQLException           sql handling
     * @throws NoRecordFoundException returns when no auctionTypes are on the database
     */
    public List<AuctionType> findAllAuctionTypes() throws SQLException, NoRecordFoundException {
        List<AuctionType> auctionTypes = auctionTypeDAO.findAllAuctionTypes();
        if (auctionTypes.isEmpty()) {
            throw new NoRecordFoundException("there are no auctionTypes to be found");
        } else {
            return auctionTypes;
        }
    }

    /**
     * method finds a single auctionType by the primary key id
     *
     * @param id id it's looking for, primary key of database
     * @return {@link AuctionType}
     * @throws SQLException           sql handling
     * @throws NoRecordFoundException returns when no auctionTypes are on the database
     */

    public AuctionType findAuctionTypeByID(int id) throws SQLException, NoRecordFoundException {
        AuctionType auctionType = auctionTypeDAO.findAuctionTypeById(id);
        if (auctionType.getId() ==0) {
            throw new NoRecordFoundException("This auctionType was not found");
        } else {
            return auctionType;
        }
    }

}
