package controllers;


import domain.Auction;
import domain.Player;
import exceptions.NoRecordFoundException;
import services.*;

import java.sql.SQLException;
import java.util.*;

import static utilities.UtilRequestingInfo.*;

/**
 * this is the controller of the auctionhouse project.
 */
public class ApplicationController {

    private Map<Integer, String> menuMap = new HashMap<>();

    private AllianceAuctionService allianceAuctionService;
    private HordeAuctionService hordeAuctionService;
    private NeutralAuctionService neutralAuctionService;
    private PlayerService playerService;
    private AuctionTypeService auctionTypeService;
    private Player user;

    /**
     * controller constructor
     *
     * @param allianceAuctionService alliance auction service
     * @param hordeAuctionService    horde auction service
     * @param neutralAuctionService  neutral auction service
     * @param playerService          player service
     * @param auctionTypeService     auctiontype service
     */
    public ApplicationController(AllianceAuctionService allianceAuctionService, HordeAuctionService hordeAuctionService, NeutralAuctionService neutralAuctionService, PlayerService playerService, AuctionTypeService auctionTypeService) {
        this.allianceAuctionService = allianceAuctionService;
        this.hordeAuctionService = hordeAuctionService;
        this.neutralAuctionService = neutralAuctionService;
        this.playerService = playerService;
        this.auctionTypeService = auctionTypeService;
        createMenu();
    }

    /**
     * createMenu is a part of the constructor to fill menuMap with the options to choose
     */
    private void createMenu() {
        menuMap.put(1, "log in");
        menuMap.put(2, "create a new account");
    }

    /**
     * wipes menumap to be re-used again
     */
    private void wipeMenu() {
        menuMap = new HashMap<>();
    }

    /**
     * this is the boot-section of the controller, this shows a opening printout and starts the method:
     */
    public void startGame() {
        System.out.println("===================================");
        System.out.println("Hello,  This APP was made by Jeroen");
        System.out.println("===================================");
        runGame();
    }

    /**
     * this is the final method that will be ran by the ApplicationController, this exists to properly close off the project.
     */
    private void stopGame() {
        closeScanner();
    }

    /**
     * this is the Body of the application, this is what will run every time until the user wishes to exit the application
     */
    private synchronized void runGame() {
        showMenu();
        int i = chooseOption();
        loginExecute(i);
        boolean b = true;
        if (!user.getFaction().equals("Horde") && !user.getFaction().equals("Alliance")) {
            System.out.println("Exiting the application.");
        } else {
            while (b) {
                wipeMenu();
                menuMap.put(1, "Change password");
                menuMap.put(2, "view " + user.getFaction() + " auction house");
                menuMap.put(3, "view Neutral auction house");
                menuMap.put(4, "see your own money");
                try {
                    showMenu();
                    i = chooseOption();
                    List<Object> objectList = executeOption(i);
                    printObjectList(objectList);
                } catch (NoRecordFoundException e) {
                    e.printUserFriendlyMessage();
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    boolean conti = requestContinue();
                    if (!conti) {
                        stopGame();
                        b = false;
                    }
                }
            }
        }

    }

    /**
     * this method will execute a login and fill {@link ApplicationController#user} with a player to be used
     *
     * @param i the choice of either logging in or new account
     */
    private void loginExecute(int i) {
        String username = requestUsername();
        String pass = requestPass();
        String factionString;
        switch (i) {
            case 1:
                try {
                    Player player = playerService.findPlayerByName(username);
                    if (player.getPassword().equals(pass)) {
                        this.user = player;
                    } else {
                        throw new NoRecordFoundException("");
                    }
                } catch (NoRecordFoundException e) {
                    System.out.println("your username or password wasn't correct, ending application.");
                    this.user = new Player();
                    this.user.setName("");
                    this.user.setFaction("");
                } catch (SQLException e) {
                    System.err.println("something went wrong on our end, we're sorry. \n please try and re-start the application.");
                    this.user = new Player();
                    this.user.setName("");
                }
                break;
            case 2:
                int faction = requestFaction();
                if (faction == 1) {
                    factionString = "Alliance";
                } else if (faction == 2) {
                    factionString = "Horde";
                } else {
                    System.out.println("there were only 2 options, ending application.");
                    return;
                }
                user = new Player();
                user.setFaction(factionString);
                user.setName(username);
                try {
                    playerService.createNewPlayer(user);
                    this.user = playerService.findPlayerByName(user.getName());
                    user.setPassword(pass);
                    playerService.updatePlayerMoney(user);
                    playerService.updatePlayerPassword(user);
                } catch (SQLException | NoRecordFoundException e) {
                    System.err.println("something went wrong on our end, we're sorry. \n please try and re-start the application.");
                }
                break;
            default:
                System.err.println("This number was not a option");

        }
    }

    /**
     * this prints the menu created in: createMenu();
     *
     * @see ApplicationController#createMenu()
     */
    private void showMenu() {
        for (Map.Entry<Integer, String> m : menuMap.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
    }

    /**
     * this executes the option given with runGame();
     *
     * @param option this is the integer given by the RunGame method to choose what option of the switchcase you need to run
     * @return objectList this is a list with the objects created in the method, returned to be printed out later
     * @throws NoRecordFoundException {@link NoRecordFoundException} this exception will be thrown when the JDBC can't find a record of what the program was trying to look up.
     * @throws SQLException sql handling
     * @see ApplicationController#runGame()
     */
    List<Object> executeOption(int option) throws NoRecordFoundException, SQLException { // package protected so i can call in tests
        List<Object> objectsList = new ArrayList<>();
        wipeMenu();
        menuMap.put(1, "Post a auction");
        menuMap.put(2, "search for a auction by itemname");
        menuMap.put(3, "find all your auctions");
        switch (option) {
            case 1:
                System.out.println("please enter your new password");
                String newPassword = requestPass();
                user.setPassword(newPassword);
                playerService.updatePlayerPassword(user);
                break;
            case 2:
                if (user.getFaction().equals("Alliance")) {
                    objectsList = executeAllianceAH();
                } else {
                    objectsList = executeHordeAH();
                }
                break;
            case 3:
                objectsList = executeNeutralAH();
                break;
            case 4:
                String money = Long.toString(user.getMoney());
                String copper;
                String silver = "0";
                String gold = "0";
                if(money.length() >1){
                    copper = money.substring(money.length() - 2);
                } else{
                    copper = money;
                }
                if(money.length() >3){
                    silver = money.substring(money.length()-4,money.length()-2);
                    gold = money.substring(0,money.length()-4);
                }
                objectsList.add("you have " + gold +" gold, "+silver+" silver, "+ copper+" copper.");
                break;
            default:
                System.err.println("This number was not a option");
        }
        return objectsList;
    }

    /**
     * this is the neutral auction house execute
     *
     * @return List of {@link Object}
     * @throws SQLException sql handling
     * @throws NoRecordFoundException thrown when no record of your search is found
     */
    private List<Object> executeNeutralAH() throws SQLException, NoRecordFoundException {
        String type = "Neutral";
        List<Object> objectsList = new ArrayList<>();
        System.out.println("====================================");
        System.out.println("Welcome to the Neutral Auction house");
        System.out.println("====================================");
        showMenu();
        int i = chooseOption();
        switch (i) {
            case 1:
                postANewAuction(type);
                break;
            case 2:
                objectsList.addAll(neutralAuctionService.findAuctionsByQueryLikeness(requestAString("what item are you looking for")));
                break;
            case 3:
                objectsList.addAll(neutralAuctionService.findAuctionsBySellerID(user.getId()));
                break;
            default:
                System.out.println("this was not a option");
        }
        return objectsList;
    }

    /**
     * this is the horde auction house execute
     *
     * @return List of {@link Object}
     * @throws SQLException sql handling
     * @throws NoRecordFoundException thrown when no record is found.
     */
    private List<Object> executeHordeAH() throws SQLException, NoRecordFoundException {
        String type = "Horde";
        List<Object> objectsList = new ArrayList<>();
        System.out.println("==================================");
        System.out.println("Welcome to the Horde Auction house");
        System.out.println("==================================");
        showMenu();
        int i = chooseOption();
        switch (i) {
            case 1:
                postANewAuction(type);
                break;
            case 2:
                objectsList.addAll(hordeAuctionService.findAuctionsByQueryLikeness(requestAString("what item are you looking for")));
                break;
            case 3:
                objectsList.addAll(hordeAuctionService.findAuctionsBySellerID(user.getId()));
                break;
            default:
                System.out.println("this was not a option");
        }
        return objectsList;
    }

    /**
     * this is the Alliance auction house execute
     *
     * @return List of {@link Object}
     * @throws SQLException sql handling
     * @throws NoRecordFoundException thrown when your search resulted in no results
     */
    private List<Object> executeAllianceAH() throws SQLException, NoRecordFoundException {
        String type = "Alliance";
        List<Object> objectsList = new ArrayList<>();
        System.out.println("=====================================");
        System.out.println("Welcome to the Alliance Auction house");
        System.out.println("=====================================");
        showMenu();
        int i = chooseOption();
        switch (i) {
            case 1:
                postANewAuction(type);
                break;
            case 2:
                objectsList.addAll(allianceAuctionService.findAuctionsByQueryLikeness(requestAString("what item are you looking for")));
                break;
            case 3:
                objectsList.addAll(allianceAuctionService.findAuctionsBySellerID(user.getId()));
            default:
                System.out.println("this was not a option");
        }
        return objectsList;
    }

    private void postANewAuction(String s) throws SQLException {
        Auction auction = new Auction();
        auction.setSellerID(user.getId());
        auction.setItemName(requestAString("what item do you want to sell?"));
        auction.setAmount(requestInt("how many are you selling?"));
        auction.setCurrentMinimalBid(requestMoney("what is the minimal bid you want to set your auction at?"));
        auction.setBuyoutPrice(requestMoney("what would you have the buyout price be?"));
        menuMap.put(1, "4 hour duration");
        menuMap.put(2, "8 hour duration");
        menuMap.put(3, "12 hour duration");
        menuMap.put(4, "24 hour duration");
        showMenu();
        int type = chooseOption();
        if (type > 5) {
            System.out.println("that wasn't a option, exiting.");
            return;
        }
        auction.setType(type);
        if (s.equals("Neutral")) {
            neutralAuctionService.createNewAuction(auction);
        } else if (s.equals("Alliance")) {
            allianceAuctionService.createNewAuction(auction);
        } else if (s.equals("Horde")) {
            hordeAuctionService.createNewAuction(auction);
        }
    }

    /**
     * this prints out everything in the objectList given to it.
     *
     * @param objectsList this is the objectList generated in
     * @see ApplicationController#executeOption(int)
     */
    private void printObjectList(List<Object> objectsList) {
        if (!objectsList.isEmpty()) {
            for (Object o : objectsList) {
                System.out.println(o);
            }
        }
    }


}
