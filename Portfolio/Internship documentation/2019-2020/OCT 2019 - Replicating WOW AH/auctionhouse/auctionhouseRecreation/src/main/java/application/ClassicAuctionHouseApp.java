package application;

import controllers.ApplicationController;
import repo.AuctionDAO;
import repo.AuctionTypeDAO;
import repo.PlayerDAO;
import services.*;


/**
 * Java Documentations on ClassicAuctionHouseApp
 *
 * @author Jeroen Buelens
 * @version 1.0
 * <p>
 * Added in a if args.length ==0 to make the jar runAble by itself
 */
public class ClassicAuctionHouseApp {
    public static void main(String[] args) {
        AuctionDAO auctionDAO = new AuctionDAO();
        AuctionTypeDAO auctionTypeDAO = new AuctionTypeDAO();
        PlayerDAO playerDAO = new PlayerDAO();
        AllianceAuctionService allianceAuctionService = new AllianceAuctionService(auctionDAO);
        HordeAuctionService hordeAuctionService = new HordeAuctionService(auctionDAO);
        NeutralAuctionService neutralAuctionService = new NeutralAuctionService(auctionDAO);
        AuctionTypeService auctionTypeService = new AuctionTypeService(auctionTypeDAO);
        PlayerService playerService = new PlayerService(playerDAO);
        ApplicationController applicationController = new ApplicationController(allianceAuctionService, hordeAuctionService, neutralAuctionService, playerService, auctionTypeService);
        applicationController.startGame();
    }


}
