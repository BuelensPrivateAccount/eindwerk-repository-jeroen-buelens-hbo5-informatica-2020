package exceptions;

/**
 * This is the GotDarnITFuckingUserException, this exception will be thrown every time we can't find a record of what you wanted to look up
 */
public class GotDarnITFuckingUserException extends Exception {


    public GotDarnITFuckingUserException(String message){
        super(message);
    }

    public void printUserFriendlyMessage(){
        System.err.println(getMessage());
    }
}
