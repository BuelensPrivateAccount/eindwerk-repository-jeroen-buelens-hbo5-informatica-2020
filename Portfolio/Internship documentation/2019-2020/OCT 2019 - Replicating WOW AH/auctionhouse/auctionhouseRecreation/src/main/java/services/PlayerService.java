package services;

import domain.Player;
import exceptions.NoRecordFoundException;
import repo.PlayerDAO;

import java.sql.SQLException;
import java.util.List;

public class PlayerService {
    private PlayerDAO playerDAO;

    /**
     * Constructor
     *
     * @param playerDAO this is the database access object of player that this service uses
     */
    public PlayerService(PlayerDAO playerDAO) {
        this.playerDAO = playerDAO;
    }

    /**
     * method finds all players
     *
     * @return List of {@link Player}
     * @throws SQLException           sql handling
     * @throws NoRecordFoundException returns when no players are on the database
     */
    public List<Player> findAllPlayers() throws SQLException, NoRecordFoundException {
        List<Player> players = playerDAO.findAllPlayers();
        if (players.isEmpty()) {
            throw new NoRecordFoundException("there are no players to be found");
        } else {
            return players;
        }
    }

    /**
     * method finds all alliance players
     *
     * @return List of {@link Player}
     * @throws SQLException           sql handling
     * @throws NoRecordFoundException returns when no players are on the database
     */
    public List<Player> findAllAlliancePlayers() throws SQLException, NoRecordFoundException {
        List<Player> players = playerDAO.findAllAlliancePlayers();
        if (players.isEmpty()) {
            throw new NoRecordFoundException("there are no players to be found");
        } else {
            return players;
        }
    }

    /**
     * method finds all horde players
     *
     * @return List of {@link Player}
     * @throws SQLException           sql handling
     * @throws NoRecordFoundException returns when no players are on the database
     */
    public List<Player> findAllHordePlayers() throws SQLException, NoRecordFoundException {
        List<Player> players = playerDAO.findAllHordePlayers();
        if (players.isEmpty()) {
            throw new NoRecordFoundException("there are no players to be found");
        } else {
            return players;
        }
    }

    /**
     * method finds a single player by the primary key id
     *
     * @param id id it's looking for, primary key of database
     * @return {@link Player}
     * @throws SQLException           sql handling
     * @throws NoRecordFoundException returns when no players are on the database
     */

    public Player findPlayerByID(int id) throws SQLException, NoRecordFoundException {
        Player player = playerDAO.findPlayerByID(id);
        if (player.getId() == 0) {
            throw new NoRecordFoundException("This player was not found");
        } else {
            return player;
        }
    }

    /**
     * method finds a single player by the player name specific
     *
     * @param name the specific name it's looking for, primary key of database
     * @return {@link Player}
     * @throws SQLException           sql handling
     * @throws NoRecordFoundException returns when no players are on the database
     */
    public Player findPlayerByName(String name) throws SQLException, NoRecordFoundException {
        Player player = playerDAO.findSpecificPlayerByName(name);
        if (player.getId() == 0) {
            throw new NoRecordFoundException("This player was not found");
        } else {
            return player;
        }
    }

    /**
     * method finds all players where query is in their name
     *
     * @param query the query that needs to be in their name
     * @return List of {@link Player}
     * @throws SQLException           sql handling
     * @throws NoRecordFoundException returns when no players are on the database
     */
    public List<Player> findPlayerWhereName(String query) throws SQLException, NoRecordFoundException {
        List<Player> players = playerDAO.findPlayerWhereNameHas(query);
        if (players.isEmpty()) {
            throw new NoRecordFoundException("there are no players to be found");
        } else {
            return players;
        }
    }

    /**
     * this method creates a new player on the database from a player
     *
     * @param player {@link Player}
     * @throws SQLException sql handling
     */
    public void createNewPlayer(Player player) throws SQLException {
        playerDAO.create(player);
    }

    /**
     * this method updates the player's money on the database
     *
     * @param player {@link Player}
     * @throws SQLException sql handling
     */
    public void updatePlayerMoney(Player player) throws SQLException {
        playerDAO.updatePlayerMoney(player);
    }

    /**
     * this method updates the player's password on the database
     *
     * @param player {@link Player}
     * @throws SQLException sql handling
     */
    public void updatePlayerPassword(Player player) throws SQLException {
        playerDAO.updatePlayerPass(player);
    }

    /**
     * this method deletes a player on the database
     *
     * @param player {@link Player}
     * @throws SQLException sql handling
     */
    public void deletePlayer(Player player) throws SQLException {
        playerDAO.delete(player);
    }
}
