import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import Colors from "../constants/Colors";

import Home from '../screens/Home'
import Orders from '../screens/Orders'


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function MainTabNavigator() {

    return (
        <Tab.Navigator
            tabBarOptions={{
                activeTintColor: Colors.normalText,
                style: {
                    backgroundColor: Colors.primary
                }
            }}
            screenOptions={({ route }) => ({
                tabBarIcon: ({ color, size }) => {
                    let iconName
                    if (route.name == 'Home') {
                        iconName = 'ios-home'
                    } else if (route.name == 'Orders') {
                        iconName = 'ios-person'
                    }
                    return <Ionicons name={iconName} color={color} size={size} />
                }
            })}>
            <Tab.Screen name='Home' component={Home} />
            <Tab.Screen name='Orders' component={Orders} />
        </Tab.Navigator>

    )
}

function AppNavigator() {
    return (
        <NavigationContainer>
            <Stack.Navigator
                initialRouteName='Home'
                screenOptions={{
                    gestureEnabled: true,
                    headerStyle: {
                        backgroundColor: Colors.primary

                    },
                    headerTitleStyle: {
                        fontWeight: 'bold',
                        textAlign:'center',
                        fontSize: 25
                    },
                    headerTintColor: Colors.normalText,
                    headerBackTitleVisible: false
                }}
                headerMode='float'>
                <Stack.Screen
                    name='Home'
                    component={MainTabNavigator}
                    options={{ title: 'Home Screen' }}
                />
                <Stack.Screen
                    name='Orders'
                    component={Orders}
                    options={{ title: 'Orders Screen' }}
                    />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default AppNavigator;
