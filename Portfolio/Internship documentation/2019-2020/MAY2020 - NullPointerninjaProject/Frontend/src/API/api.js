const URL = "http://10.10.110.7:8080/project1_war_exploded";

export const getProductById = async (id) => {
    let request = await fetch(URL + "/itemByID?id=" + id);
    return await request.json();
};

export const getAllItems = async (page,amount,sort) =>{
    let request = await fetch(URL+"/allItems?page="+page+"&sort="+sort+"&amount="+amount);
    return await request.json();
};

export const getProductsByName = async (name, page,sort) => {
    let request = await fetch(URL + "/itemSearchName?name=" + encodeURI(name) + "&page=" + page+"&sort="+sort);
    return await request.json();
};

export const getProductsByCategory = async (category, page,sort) => {
    let request = await fetch(URL + "/itemSearchName?category=" + encodeURI(category) + "&page=" + page+"&sort="+sort);
    return await request.json();
};

