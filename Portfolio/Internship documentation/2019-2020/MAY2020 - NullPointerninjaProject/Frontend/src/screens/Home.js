import React, {useState} from 'react';
import {StyleSheet, View, Text, Picker, Image, TouchableOpacity, ScrollView,} from 'react-native';
import {Card, CardItem,Thumbnail, Left, Right,Title, Subtitle,Content} from "native-base";
import SearchInput from '../components/SearchInput';
import Colors from "../constants/Colors";
import FontAwesome from "react-native-vector-icons/FontAwesome";

import ShowItems from "../components/ShowItems";


function Home(props) {
    const [selectedValue, setSelectedValue] = useState("");
    const {navigation} = props;

    return (
        <View style={styles.container}>
            <Text style={styles.text}>Onze producten</Text>
            <SearchInput
                style={styles.input}
            />
            <View style={styles.picker}>
                <Picker
                    style={{height: 30, width: "100%"}}
                    color='white'
                    selectedValue={selectedValue}
                    onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                >
                    <Picker.Item label="-- Sorteer op --" value=""></Picker.Item>
                    <Picker.Item label="Sorteren op prijs" value="Sorteren op prijs"></Picker.Item>
                    <Picker.Item label="Sorteren op categorie" value="Sorteren op categorie"></Picker.Item>
                    <Picker.Item label="Sorteren op prijs" value="Sorteren op prijs"></Picker.Item>
                    <Picker.Item label="Sorteren op categorie" value="Sorteren op categorie"></Picker.Item>
                </Picker>
            </View>
            <ScrollView>
                <Card transparent>
                    <CardItem style={styles.card}>
                        <Left>
                            <Thumbnail
                                style={{width: 160, height: 130, padding: 5, marginRight: 5}}
                            source={{uri: 'https://tse4.mm.bing.net/th?id=OIP.o9iyT0ViPfxj2bE4MyHcuQHaHa&pid=Api&P=0&w=300&h=300'}}
                            />
                            <View style={{alignItems: 'flex-start', justifyContent:'center', padding:10}}>
                                <Title style={{color: 'black'}}> Product Title </Title>
                                <Subtitle style={{color: 'black'}}> Categorie</Subtitle>
                                <Subtitle style={{color: 'black'}}> € Prijs</Subtitle>
                            </View>
                        </Left>
<Right>
<View style={{alignItems: 'flex-end', justifyContent:'center'}}>
    <FontAwesome name='eye' size={30}/>
    <FontAwesome style={{marginTop: 30}}name='cart-plus' size={30}/>
</View>
</Right>
                    </CardItem>

                </Card>

            </ScrollView>
            <TouchableOpacity
                style={styles.buttonContainer}
                onPress={() => navigation.navigate('Orders')}>
                <Text style={{color: 'white'}}>Orders</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: Colors.accent
    },
    card:{
        width:370,
        borderRadius: 20,
        borderWidth: 1,
        backgroundColor: Colors.secondary,
        alignItems:'center',
        marginTop: 10
    },
    cardContainer: {
        backgroundColor: Colors.secondary
    },
    text: {
        color: '#ffffff',
        fontSize: 24,
        padding: 10,
        borderWidth: 2,
        borderColor: 'black',
        borderRadius: 5,
        marginTop: 15,
        backgroundColor: Colors.primary,
        width: '90%',
        textAlign: 'center'
    },
    itemTitle: {
        color: 'black',

    },
    buttonContainer: {
        backgroundColor: '#222',
        borderRadius: 5,
        padding: 10,
        margin: 20
    },
    buttonText: {
        fontSize: 20,
        color: '#fff'
    },
    input: {
        width: '90%'
    },
    picker: {
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 5,
        width: '90%',
        backgroundColor: 'transparent',
        opacity: 0.8
    },
    pickerItem: {
        textAlign: 'center',
        alignItems: 'center',
        color: 'white'
    }
})

export default Home
