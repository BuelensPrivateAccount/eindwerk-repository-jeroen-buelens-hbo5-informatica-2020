import React from 'react'
import {StyleSheet, View, Text, TouchableOpacity, ScrollView} from 'react-native'
import OrdersCards from '../components/OrdersCards';
import Colors from "../constants/Colors";
import {Card, CardItem, Left, Right, Subtitle, Thumbnail, Title} from "native-base";
import FontAwesome from "react-native-vector-icons/FontAwesome";

function Orders(props) {
    const { navigation } = props
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Uw bestellingen</Text>
            <ScrollView>
                <Card transparent style={styles.cardContainer}>
                    <CardItem style={styles.card}>
                        <Left>
                            <Thumbnail
                                style={{width: 160, height: 130, padding: 5, marginRight: 5}}
                                source={{uri: 'https://tse4.mm.bing.net/th?id=OIP.o9iyT0ViPfxj2bE4MyHcuQHaHa&pid=Api&P=0&w=300&h=300'}}
                            />
                            <View style={{alignItems: 'flex-start', justifyContent:'center', padding:10}}>
                                <Title style={{color: 'black'}}> Product Title </Title>
                                <Subtitle style={{color: 'black'}}> Categorie</Subtitle>
                                <Subtitle style={{color: 'black'}}> € Prijs</Subtitle>
                            </View>
                        </Left>
                        <Right>
                            <View style={{alignItems: 'flex-end', justifyContent:'center'}}>
                                <FontAwesome name='eye' size={30}/>
                                <FontAwesome style={{marginTop: 30}}name='cart-plus' size={30}/>
                            </View>
                        </Right>
                    </CardItem>
                </Card>
                <Text style={styles.totalAmount}>Uw totaalbedrag: € bedrag</Text>
            </ScrollView>
        </View>
    )
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: Colors.accent,
        textAlign: 'center',
        justifyContent: 'center'
    },
    text: {
        color: '#ffffff',
        fontSize: 24,
        padding: 10,
        borderWidth: 2,
        borderColor: 'black',
        borderRadius: 5,
        marginTop: 15,
        backgroundColor: Colors.primary,
        width: '90%',
        textAlign: 'center'

    },
    totalAmount:{
        color: '#ffffff',
        fontSize: 24,
        padding: 10,
        borderWidth: 2,
        borderColor: 'black',
        borderRadius: 5,
        marginTop: 15,
        backgroundColor: Colors.primary,
        width: '100%',
        textAlign: 'center'
    },
    buttonContainer: {
        backgroundColor: '#222',
        borderRadius: 5,
        padding: 10,
        margin: 20
    },
    buttonText: {
        fontSize: 20,
        color: '#fff'
    },
    card:{
        width:370,
        borderRadius: 20,
        borderWidth: 1,
        backgroundColor: Colors.secondary,
        alignItems:'center',
        marginTop: 10
    }

})

export default Orders;
