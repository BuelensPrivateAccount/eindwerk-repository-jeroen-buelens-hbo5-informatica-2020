import React from 'react';
import {StyleSheet,Text,View,FlatList,TouchableOpacity,ActivityIndicator} from 'react-native';
import {Card} from 'native-base';


export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            dataSource:[]
        };
    }
    componentDidMount(){
        fetch("http://192.168.0.170:8080/project1_war/allItems?page=1&sort=1&amount=20")
            .then(response => response.json())
            .then((responseJson)=> {
                this.setState({
                    loading: false,
                    dataSource: responseJson
                })

            })
    }
    FlatListItemSeparator = () => {
        return (
            <View style={{
                height: .5,
                width:"100%",
                backgroundColor:"rgba(0,0,0,0.5)",
            }}
            />
        );
    }
    renderItem=(data) =>
        <TouchableOpacity style={{...styles.list , ...this.props}}>
            <Text>ArtikelNummer: {data.item.artikelNr}</Text>
            <Text>EanCode: {data.item.eanCode}</Text>
            <Text>Merk: {data.item.merk}</Text>
            <Text>Naam: {data.item.naam}</Text>
            <Text>Omschrijving NL: {data.item.omschrijvingNL}</Text>
            <Text>Omschrijving EN: {data.item.omschrijvingEN}</Text>
            <Text>Categorie NL: {data.item.categorieNL}</Text>
            <Text>Categorie EN: {data.item.categorieEN}</Text>
            <Text>Groep 1: {data.item.groep1}</Text>
            <Text>Groep 2: {data.item.groep2}</Text>
            <Text>Groep 3: {data.item.groep3}</Text>
            <Text>Prijs: €{data.item.prijs}</Text>
            <Text>Aantal: {data.item.aantal}</Text>
            <Text>Creatie Datum: {data.item.creatieDatum}</Text>
        </TouchableOpacity>

    render(){
        if(this.state.loading){
            return(
                <View style={styles.loader}>
                    <ActivityIndicator size="large" color="#0c9"/>
                </View>
            )}
        return(
            <View style={styles.container}>
                <FlatList
                    data= {this.state.dataSource}
                    renderItem= {item=> this.renderItem(item)}
                    ItemSeparatorComponent = {this.FlatListItemSeparator}
                    keyExtractor= {item=>item.pk.toString()}
                />
            </View>
        )}
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    loader:{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff"
    },
    list:{
        paddingVertical: 4,
        margin: 15,
        backgroundColor: "#fff"
    }
});