package com.realdolmen.hbo5.t1.helper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.mockito.stubbing.Answer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public abstract class ServletTestHelper {
    protected final Map<String, Object> attributes = new ConcurrentHashMap<String, Object>();
    @Mock
    protected HttpServletRequest request;
    @Mock
    protected HttpServletResponse response;

    @BeforeEach
    public void mockitoOverride() {
        // Mock setAttribute
        Mockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                String key = invocation.getArgument(0, String.class);
                Object value = invocation.getArgument(1, Object.class);
                attributes.put(key, value);
                System.out.println("put attribute key=" + key + ", value=" + value);
                return null;
            }
        }).when(request).setAttribute(Mockito.anyString(), Mockito.anyObject());

        // Mock getAttribute
        Mockito.doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                String key = invocation.getArgument(0, String.class);
                Object value = attributes.get(key);
                System.out.println("get attribute value for key=" + key + " : " + value);
                return value;
            }
        }).when(request).getAttribute(Mockito.anyString());
        //to make sure requestdispatcher doesnt give errors
        when(request.getRequestDispatcher(anyString())).thenReturn(new RequestDispatcher() {
            @Override
            public void forward(ServletRequest request, ServletResponse response) throws ServletException, IOException {
            }

            @Override
            public void include(ServletRequest request, ServletResponse response) throws ServletException, IOException {
            }
        });

    }
}
