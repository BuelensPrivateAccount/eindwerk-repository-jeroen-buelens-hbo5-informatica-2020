package com.realdolmen.hbo5.t1.domain.objects;

import com.realdolmen.hbo5.t1.helper.AssertAnnotations;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.*;

import static com.realdolmen.hbo5.t1.helper.AssertAnnotations.assertField;
import static com.realdolmen.hbo5.t1.helper.ReflectTool.getFieldAnnotation;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class ItemTest {

    @Test
    public void ItemHasCorrectAnnotationsTest() {
        AssertAnnotations.assertType(Item.class, Entity.class, Table.class, NamedQueries.class);
    }

    @Test
    public void artikelNRVerify() {
        assertField(Item.class, "artikelNr", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "artikelNr", Access.class);
        Column column = getFieldAnnotation(Item.class, "artikelNr", Column.class);
        assertEquals("artikelNr", column.name());
        assertTrue(column.unique());
        assertEquals(AccessType.FIELD, access.value());
    }
    
    @Test
    public void eanCodeVerify() {
        assertField(Item.class, "eanCode", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "eanCode", Access.class);
        Column column = getFieldAnnotation(Item.class, "eanCode", Column.class);
        assertEquals("eancode", column.name());
        assertTrue(column.unique());
        assertEquals(AccessType.FIELD, access.value());
    }

    @Test
    public void merkVerify() {
        assertField(Item.class, "merk", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "merk", Access.class);
        Column column = getFieldAnnotation(Item.class, "merk", Column.class);
        assertEquals("merk", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }
    @Test
    public void naamVerify() {
        assertField(Item.class, "naam", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "naam", Access.class);
        Column column = getFieldAnnotation(Item.class, "naam", Column.class);
        assertEquals("naam", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }
    @Test
    public void omschrijvingNLVerify() {
        assertField(Item.class, "omschrijvingNL", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "omschrijvingNL", Access.class);
        Column column = getFieldAnnotation(Item.class, "omschrijvingNL", Column.class);
        assertEquals("omschrijvingNL", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }

    @Test
    public void omschrijvingENVerify() {
        assertField(Item.class, "omschrijvingEN", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "omschrijvingEN", Access.class);
        Column column = getFieldAnnotation(Item.class, "omschrijvingEN", Column.class);
        assertEquals("omschrijvingEN", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }
    @Test
    public void categorieNLVerify() {
        assertField(Item.class, "categorieNL", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "categorieNL", Access.class);
        Column column = getFieldAnnotation(Item.class, "categorieNL", Column.class);
        assertEquals("categorieNL", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }
    @Test
    public void categorieENVerify() {
        assertField(Item.class, "categorieEN", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "categorieEN", Access.class);
        Column column = getFieldAnnotation(Item.class, "categorieEN", Column.class);
        assertEquals("categorieEn", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }
    @Test
    public void groep1Verify() {
        assertField(Item.class, "groep1", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "groep1", Access.class);
        Column column = getFieldAnnotation(Item.class, "groep1", Column.class);
        assertEquals("groep1", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }
    @Test
    public void groep2Verify() {
        assertField(Item.class, "groep2", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "groep2", Access.class);
        Column column = getFieldAnnotation(Item.class, "groep2", Column.class);
        assertEquals("groep2", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }
    @Test
    public void groep3Verify() {
        assertField(Item.class, "groep3", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "groep3", Access.class);
        Column column = getFieldAnnotation(Item.class, "groep3", Column.class);
        assertEquals("groep3", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }
    @Test
    public void prijsVerify() {
        assertField(Item.class, "prijs", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "prijs", Access.class);
        Column column = getFieldAnnotation(Item.class, "prijs", Column.class);
        assertEquals("prijs", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }
    @Test
    public void aantalVerify() {
        assertField(Item.class, "aantal", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "aantal", Access.class);
        Column column = getFieldAnnotation(Item.class, "aantal", Column.class);
        assertEquals("aantal", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }
    @Test
    public void creatieDatumVerify() {
        assertField(Item.class, "creatieDatum", Basic.class, Access.class, Column.class);
        Access access = getFieldAnnotation(Item.class, "creatieDatum", Access.class);
        Column column = getFieldAnnotation(Item.class, "creatieDatum", Column.class);
        assertEquals("creatiedatum", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }
}
