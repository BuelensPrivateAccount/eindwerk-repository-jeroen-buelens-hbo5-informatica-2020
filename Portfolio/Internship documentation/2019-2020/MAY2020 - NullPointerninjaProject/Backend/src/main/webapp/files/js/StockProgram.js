async function loadAllResults(element, page, amount, order, onElementsLoaded = null) {
    let resp = await fetch("async?method=showAll&page=" + page + "&amount=" + amount + "&order=" + order);
    let results = await resp.text();

    element.innerHTML = results;

    if (onElementsLoaded != null) {
        onElementsLoaded();
    }

    updatePageNumber(page);
    showTotalNumberOfPages();
}

async function loadSearchResults(element, page, query, onElementsLoaded = null) {
    let resp = await fetch("async?method=q&page=" + page + "&q=" + query);
    let searchResults = await resp.text();

    element.innerHTML = searchResults;
    if (onElementsLoaded != null) {
        onElementsLoaded();
    }

    updatePageNumber(page);
    showTotalNumberOfPages();
}

function showTotalNumberOfPages(){
    let totalPages = document.getElementById("results-metadata").getAttribute("data-total-pages");
    document.getElementById("totalPages").innerHTML = " " + totalPages + " ";
}

function updatePageNumber(newPageNumber) {
    document.getElementById('page-number').value = newPageNumber;
}

function nextPage() {
    let infoElem = document.getElementById('results-metadata');
    let methodUsed = infoElem.getAttribute("data-method-used");
    let currentPage = parseInt(infoElem.getAttribute("data-current-page"));

    switch (methodUsed) {
        case "all":
            goToPageForAllResults(currentPage + 1);
            break;
        case "search":
            goToPageForSearchResults(currentPage + 1);
            break;
    }
}

function goToPage(page) {
    let infoElem = document.getElementById("results-metadata");
    let methodUsed = infoElem.getAttribute("data-method-used");

    switch (methodUsed) {
        case "all":
            goToPageForAllResults(page);
            break;
        case "search":
            goToPageForSearchResults(page);
            break;
    }

    updatePageNumber(page);
}

function previousPage() {
    let infoElem = document.getElementById('results-metadata');
    let methodUsed = infoElem.getAttribute("data-method-used");
    let currentPage = parseInt(infoElem.getAttribute("data-current-page"));

    if (currentPage > 1) {
        switch (methodUsed) {
            case "all":
                goToPageForAllResults(currentPage - 1);
                break;
            case "search":
                goToPageForSearchResults(currentPage - 1);
                break;
        }
    }
}

function goToPageForAllResults(page) {
    let selectElement = document.getElementById('orderSelect');
    let order = selectElement.options[selectElement.selectedIndex].value;

    loadAllResults(
        document.getElementById('results'),
        page,
        20,
         order,
        function () {
            langParser.update("lp");
        }
    );
}

function goToPageForSearchResults(page) {
    loadSearchResults(
        document.getElementById('results'),
        page,
        document.getElementById('q').value,
        function () {
            langParser.update("lp");
        }
    );
}

function setCookie(name, value, expiresAfterDays) {
    let date = new Date();
    date.setTime(date.getTime() + (expiresAfterDays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + date.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

function getCookie(name) {
    let cookies = document.cookie.split(";");
    for (let i = 0; i < cookies.length; i++) {
        if (cookies[i].split("=")[0] == name) {
            return cookies[i].split("=")[1];
        }
    }
    return undefined;
}

function getLangParserLanguage(languageString) {
    let language;
    switch (languageString) {
        case "nl":
        case "en":
        case "fr":
            language = languageString;
            break;
        default:
            language = "en";
            break;
    }

    return language;
}

function getUserLanguage() {
    if (getCookie("language") == undefined) {
        let browserLanguage = navigator.language.substr(0, 2);
        return getLangParserLanguage(browserLanguage);
    } else {
        let cookieLanguage = getCookie("language");
        return getLangParserLanguage(cookieLanguage);
    }
}

function setupLangParser(directory) {
    let langParser = new LangParser(getUserLanguage(), directory);
    langParser.parse("lp");
    showSelectedLanguage(document.getElementById("languageSelect"));
    return langParser;
}

function updateLanguage(selectElement) {
    let lang = selectElement.options[selectElement.selectedIndex].value;
    setCookie("language", lang, 365 * 20);
    location.reload();
}

function showSelectedLanguage(selectElement) {
    let language = getUserLanguage();

    for(let i = 0; i < selectElement.options.length; i++){
        if(selectElement.options[i].value == language){
            selectElement.options[i].setAttribute("selected", "selected");
        }
    }
}

function confirmDelete(id){
    let msg = document.getElementById('results-metadata').getAttribute("deletion-message");
    if(confirm(msg)){
        window.location="edit?action=delete&id="+id;
    };
}