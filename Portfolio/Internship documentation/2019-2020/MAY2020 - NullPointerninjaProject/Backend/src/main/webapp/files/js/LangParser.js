class LangParser {
    /**
     * Sets up a LangParser object to allow text on a web page to be translated by different JSON files containing
     * the translations.
     * This can be done in two ways.
     * 1. Create <span></span> elements that indicate the translated text needs to be inserted between these two tags.
     * 2. Attribute translation, if for example the 'placeholder' attribute needs to be translated, use
     * the 'lp-custom-attribute="<attribute that needs translation>" attribute to indicate the provided attribute
     * will be translated. For example: <input lp-custom-attribute="placeholder" data-message="your_message"/>
     * @param language
     * @param languageFilesDirectory
     */
    constructor(language, languageFilesDirectory) {
        this.language = language;
        this.languageFilesDirectory = languageFilesDirectory;
        this.json = "";
    }

    /**
     * Reads the JSON file by navigating to the 'languageFilesDirectory' and searching for a file named 'language'.json
     * where language is the value given when constructing the LangParser object.
     * For example: calling new LangParser("en", "path/to/my/dir/"); will try to read the file
     * "path/to/my/dir/en.json" and save its contents so they can be parsed later on.
     * @returns {Promise<any>} JSON contents of the file.
     */
    async readJson() {
        let resp = await fetch(this.languageFilesDirectory + this.language + ".json");
        let json = await resp.json();
        this.json = json;
    }

    /**
     * Parses an HTML document for elements containing the 'elementClass' class. Elements containing
     * this class will be checked for the "data-message" attribute.
     * If "data-message" is defined and its value is a key in the language JSON file, the value matching this key
     * in the JSON file will be inserted in the HTML code.
     * Additionally, if an 'lp-custom-attribute' attribute is defined, the value of the JSON file will be inserted in the
     * custom attribute.
     * For example: if 'lp-custom-attribute' is 'placeholder', then the translation will not be printed in HTML, but will
     * be inserted in this element's 'placeholder' attribute.
     * @param elementClass The class of the elements that contain text that can be found in a JSON file.
     * @returns void
     */
    async parse(elementClass) {
        let elems = document.getElementsByClassName(elementClass);
        await this.readJson();
        let messages = this.json;

        for (let i = 0; i < elems.length; i++) {
            this.parseThisElement(elems[i]);
        }
    }

    /**
     * Translates all the elements from the provided class that haven't been translated yet.
     * This is useful when loading in new elements that need translation via AJAX.
     * When the XHR request is completed, you could call the LangParser.update(elementClass) function
     * to updated possibly newly added elements.
     * It is better to use the update() function because this will only translate elements that haven't been translated,
     * whereas the parse() function updates all elements that need translation.
     * @param elementClass The class of the elements that contain text that can be found in a JSON file.
     */
    update(elementClass) {
        let elems = document.getElementsByClassName(elementClass);
        for (let i = 0; i < elems.length; i++) {
            let element = elems[i];

            if (element.getAttribute("lp-parsed") == null) {
                this.parseThisElement(element);
            }
        }
    }

    /**
     * Translates the specified element only. The element needs to have at least
     * a 'data-message' attribute. The 'lp-custom-attribute' can be used if the translation needs to be
     * done on a specific attribute of this element, such as a placeholder or title attribute.
     * @param element The HTMLElement to translate.
     */
    parseThisElement(element) {
        let messageCustomAttribute = element.getAttribute("lp-custom-attribute");
        let message = element.getAttribute("data-message");

        if (typeof this.json[message] != "undefined") {
            if (messageCustomAttribute != null) {
                element.setAttribute(messageCustomAttribute, this.json[message]);
            } else {
                element.innerHTML = this.json[message];
            }
            element.setAttribute("lp-parsed", "true");
        } else {
            console.error("[LangParser] (error): Element [" + element + "] does not have a valid 'data-message' attribute. The value [" + element.getAttribute('data-message') + "] is unrecognized.");
        }
    }
}