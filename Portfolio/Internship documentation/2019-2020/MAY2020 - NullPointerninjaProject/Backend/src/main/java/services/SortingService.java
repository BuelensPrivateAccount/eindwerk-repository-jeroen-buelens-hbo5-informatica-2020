package com.realdolmen.hbo5.t1.services;

import com.realdolmen.hbo5.t1.domain.objects.Item;

import java.util.Comparator;
import java.util.List;

/**
 * this helper class sorts a list of items based on specific needs
 */
public class SortingService {
    private SortingService(){}

   public static void sortAlphabeticallyOnName(List<Item> list){
       list.sort(Comparator.comparing(item -> item.getNaam().toLowerCase()));
    }

    public static void sortAlphabeticallyOnCategoryNL(List<Item> list){
        list.sort(Comparator.comparing(item -> item.getCategorieNL().toLowerCase()));
    }

    public static void sortNumericallyOnID(List<Item> list){
        list.sort(Comparator.comparing(Item::getArtikelNr));
    }

    public static void sortNumericallyOnEANCode(List<Item> list){
        list.sort(Comparator.comparing(item -> item.getEanCode().toLowerCase()));
    }

    public static void sortAlphabeticallyOnCategoryEN(List<Item> list){
        list.sort(Comparator.comparing(item -> item.getCategorieEN().toLowerCase()));
    }

    public static void sortAlphabeticallyOnBrand(List<Item> list){
        list.sort(Comparator.comparing(item -> item.getMerk().toLowerCase()));
    }


}
