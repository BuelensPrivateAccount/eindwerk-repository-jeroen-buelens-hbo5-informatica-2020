package com.realdolmen.hbo5.t1.exceptions;

public class FileToBigException extends Exception {
    public FileToBigException(String message) {
        super(message);
    }
}
