package com.realdolmen.hbo5.t1.dao;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.domain.objects.Orders;
import com.realdolmen.hbo5.t1.exceptions.NoEntryFoundException;
import com.realdolmen.hbo5.t1.utilities.JPAUtility;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import java.util.List;

public class OrderDAO {
    JPAUtility jpaUtility ;
    EntityManager em;
    
   public boolean saveToDB(Orders order){
       em = jpaUtility.createEntityManager();
       EntityTransaction tr = em.getTransaction();
       try {
           tr.begin();
           em.persist(order);
           tr.commit();
           return true;
       } catch (RollbackException e) {
           return false;
       } finally {
           em.close();
       }
   }

    public boolean saveToDB(List<Orders> orders) {
        em = jpaUtility.createEntityManager();
        if (orders.size() == 1) {
            return saveToDB(orders.get(0));
        } else {
            EntityTransaction tr = em.getTransaction();
            try {
                tr.begin();
                for (Orders i : orders) {
                    em.persist(i);
                }
                tr.commit();
                return true;
            } catch (RollbackException e) {
                return false;
            } finally {
                em.close();
            }
        }
    }
   
    
    public Orders findOrderById(int id){
        em = jpaUtility.createEntityManager();
        TypedQuery<Orders> query = em.createNamedQuery("Order.findbyid", Orders.class);
        query.setParameter("id", id);
        Orders order = query.getSingleResult();
        em.close();
        return order;
    }

}
