package com.realdolmen.hbo5.t1.exceptions;

public class EndsOnCSVException extends Throwable {
    public EndsOnCSVException(String message){
        super(message);
    }
}
