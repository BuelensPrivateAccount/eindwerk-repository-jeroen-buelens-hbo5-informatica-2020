package com.realdolmen.hbo5.t1.services;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.exceptions.ColumnNameNotOkException;
import com.realdolmen.hbo5.t1.utilities.validation.ImportValidation;

import java.io.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * this class is designed and created to aid with the importation of csv files of a specific header and turn them into items
 */
public class CSVReader {

    private List<String> importExceptions;
    BufferedReader reader;
    int addedItems;

    public CSVReader(String fullPath) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(fullPath));
    }

    public CSVReader(File file) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(file));
    }


    public List<Item> read() throws ColumnNameNotOkException, ParseException, IOException, NumberFormatException {
        String row;
        List<Item> items = new ArrayList<>();
        long counter = 1;
        importExceptions = new ArrayList<>();
        addedItems = 0;
        while ((row = reader.readLine()) != null) {
            if (!row.isEmpty()) {
                if (ImportValidation.checkCharacters(row)) {
                    if (ImportValidation.checkEmptyRow(row)) {
                        if (counter > 1) {
                            Item item = csvToItem(row);
                            items.add(item);
                            addedItems++;
                        } else {
                            if (!ImportValidation.compatibleColumns(row)) {
                                throw new ColumnNameNotOkException("Dit CSV bestand is niet compatibel met dit programma.");
                            }
                        }
                    } else {
                        importExceptions.add("[Waarschuwing] Lijn " + counter + ": Deze lijn bevat geen data.");
                    }
                } else {
                    importExceptions.add("[Waarschuwing] Lijn " + counter + ": Ongeldige tekens op deze lijn.");
                }
            }
            counter++;
        }
        return items;


    }

    protected Item csvToItem(String data) throws ParseException, NumberFormatException {
        String[] dataArray = data.split(";");
        long articleNr = Long.parseLong(dataArray[0]);
        String eanCode = dataArray[1];
        String brand = dataArray[2];
        String name = dataArray[3];
        String descriptionNL = dataArray[4];
        String descriptionEN = dataArray[5];
        String categoryNL = dataArray[6];
        String categoryEN = dataArray[7];
        String group1 = dataArray[8];
        String group2 = dataArray[9];
        String group3 = dataArray[10];

        NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
        Number priceString = format.parse(dataArray[11]);
        double price = priceString.doubleValue();

        Number amountString = format.parse(dataArray[12]);
        int amount = (int) amountString.doubleValue();
        Item item = new Item(articleNr, eanCode, brand, name, descriptionNL, descriptionEN, categoryNL, categoryEN, group1, group2, group3, price, amount);
        LocalDate creationDate;
        if (Integer.parseInt(dataArray[13]) != 0) {
            creationDate = LocalDate.parse(dataArray[13], DateTimeFormatter.ofPattern("yyyyMMdd"));
            item.setCreatieDatum(creationDate);
        }
        return item;
    }

    public List<String> getImportExceptions() {
        return importExceptions;
    }

    public int getAddedItems() {
        return addedItems;
    }
}
