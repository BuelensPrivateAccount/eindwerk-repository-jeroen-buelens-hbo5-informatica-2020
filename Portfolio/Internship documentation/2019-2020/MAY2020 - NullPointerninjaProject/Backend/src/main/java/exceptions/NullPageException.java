package com.realdolmen.hbo5.t1.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class NullPageException extends Exception {
    public NullPageException(String message){
        super(message);
    }
}
