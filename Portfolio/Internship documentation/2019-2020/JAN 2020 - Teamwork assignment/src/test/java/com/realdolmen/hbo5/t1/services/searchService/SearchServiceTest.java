package com.realdolmen.hbo5.t1.services.searchService;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class SearchServiceTest {

    private static SearchService searchService;
    private static List<Item> list;

    @BeforeAll
    public static void setup() {
        list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            LocalDateTime localDateTime = Instant.ofEpochSecond(Long.parseLong("20140502"))
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime();
            Item item = new Item(12L, "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", 1, 1, localDateTime);
            list.add(item);
        }
        searchService = new SearchService(list);
    }

    public static String randomStringGenerator(int length) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    @Test
    public void searchByNameTest() {
        List<Item> result = searchService.searchByName("NullPointer");
        assertArrayEquals(list.toArray(), result.toArray());
    }

    @Test
    public void searchByNameNoResultsTest() {
        List<Item> result = searchService.searchByName("sdfghjkjhedfsfjhkrfedcfghvadejk.sdjfrd.scjhkdffvsh.C bfv>c jJ C KVVJHJK JBXJ,BVJKLSDJVXDNKLFHV/JKLJ");
        assertTrue(result.isEmpty());
    }

    @Test
    public void searchByNameNullTest() {
        assertDoesNotThrow(() -> {
            List<Item> result1 = searchService.searchByName(null);
            List<Item> result2 = searchService.searchByName("");
            assertTrue(result1.isEmpty());
            assertTrue(result2.isEmpty());
        });

    }

    @Test
    public void searchByCategoryNLTest() {
        List<Item> result = searchService.searchByCategoryNL("NullPointer");
        assertArrayEquals(list.toArray(), result.toArray());
    }

    @Test
    public void searchByCategoryNLNoResultsTest() {
        List<Item> result = searchService.searchByCategoryNL("sdfghjkjhedfsfjhkrfedcfghvadejk.sdjfrd.scjhkdffvsh.C bfv>c jJ C KVVJHJK JBXJ,BVJKLSDJVXDNKLFHV/JKLJ");
        assertTrue(result.isEmpty());
    }

    @Test
    public void searchByCategoryNLNullTest() {
        assertDoesNotThrow(() -> {
            List<Item> result1 = searchService.searchByCategoryNL(null);
            List<Item> result2 = searchService.searchByCategoryNL("");
            assertTrue(result1.isEmpty());
            assertTrue(result2.isEmpty());
        });

    }

    @Test
    public void searchByBrandTest() {
        List<Item> result = searchService.searchByBrand("NullPointer");
        assertArrayEquals(list.toArray(), result.toArray());
    }

    @Test
    public void searchByIDTest() {
        List<Item> result = searchService.searchByID(12L);
        assertArrayEquals(list.toArray(), result.toArray());
    }

    @Test
    public void searchByIDNoResultsTest() {
        List<Item> result = searchService.searchByID(13L);
        assertTrue(result.isEmpty());
    }

    @Test
    public void searchByEANCodeTest() {
        List<Item> result = searchService.searchByEAN("NullPointerNinja");
        assertArrayEquals(list.toArray(), result.toArray());
    }

    @Test
    public void searchByEANCodeNoResultsTest() {
        List<Item> result = searchService.searchByEAN("sdfghjkjhedfsfjhkrfedcfghvadejk.sdjfrd.scjhkdffvsh.C bfv>c jJ C KVVJHJK JBXJ,BVJKLSDJVXDNKLFHV/JKLJ");
        assertTrue(result.isEmpty());
    }

    @Test
    public void searchByEANCodeNullTest() {
        assertDoesNotThrow(() -> {
            List<Item> result1 = searchService.searchByEAN(null);
            List<Item> result2 = searchService.searchByEAN("");
            assertTrue(result1.isEmpty());
            assertTrue(result2.isEmpty());
        });

    }

}