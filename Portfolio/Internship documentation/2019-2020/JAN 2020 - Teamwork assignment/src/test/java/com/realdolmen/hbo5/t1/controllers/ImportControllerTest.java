package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.exceptions.ColumnNameNotOkException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.text.ParseException;

class ImportControllerTest {

    @Test
    public void testTrowFileNotFoundExceptionGetImportEx() {
        Assertions.assertThrows(FileNotFoundException.class, () -> {
            String defaultDirectory = ".\\src\\test\\rescourses\\";
            ImportController reader = new ImportController(defaultDirectory + "expected.sv");
            reader.getImportExceptions();
        });
    }

    @Test
    public void testTrowFileNotFoundExceptionGetAddedItems() {
        Assertions.assertThrows(FileNotFoundException.class, () -> {
            String defaultDirectory = ".\\testRescources\\";
            ImportController reader = new ImportController(defaultDirectory + "expected.sv");
            reader.getAddedItems();
        });
    }

    @Test
    public void testTrowFileNotFoundExceptionGetFile() {
        Assertions.assertThrows(FileNotFoundException.class, () -> {
            String defaultDirectory = ".\\testRescources\\";
            ImportController reader = new ImportController(defaultDirectory + "expected.sv");
            reader.getFile();
        });
    }

    @Test
    public void testTrowAssertNoCSV() {
        Assertions.assertThrows(FileNotFoundException.class, () -> {
            String defaultDirectory = ".\\testRescources\\";
            ImportController reader = new ImportController(defaultDirectory + "expected.sv");
            reader.read();
        });
    }

    @Test
    public void testTrowAssertNoHead() {
        Assertions.assertThrows(ColumnNameNotOkException.class, () -> {
            String defaultDirectory = ".\\testRescources\\";
            ImportController reader = new ImportController(defaultDirectory + "nohead.csv");
            reader.read();
        });
    }


    @Test
    public void testTrowAssertNumberFormat() {
        Assertions.assertThrows(NumberFormatException.class, () -> {
            String defaultDirectory = ".\\testRescources\\";
            ImportController reader = new ImportController(defaultDirectory + "importNumberFormat.csv");
            reader.read();
        });
    }


}