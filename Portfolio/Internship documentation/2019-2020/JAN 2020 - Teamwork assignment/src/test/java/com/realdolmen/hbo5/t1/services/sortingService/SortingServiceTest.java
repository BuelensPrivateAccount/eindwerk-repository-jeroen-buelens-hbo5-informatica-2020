package com.realdolmen.hbo5.t1.services.sortingService;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class SortingServiceTest {
    Item item;

    public SortingServiceTest() {
        LocalDateTime localDateTime = Instant.ofEpochSecond(Long.parseLong("20140502"))
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();

        item = new Item(0, "NullPointerNinja", "NullPointerNinja", "NullPointerNinja", "NullPointerNinja", "NullPointerNinja", "NullPointerNinja", "NullPointerNinja", "NullPointerNinja", "NullPointerNinja", "NullPointerNinja", 1, 1, localDateTime);
    }

    @Test
    public void sortAlphabeticallyOnNameTest() throws CloneNotSupportedException {
        List<Item> expected = new ArrayList<>();
        List<Item> actual = new ArrayList<>();
        Item itemA =  (Item) item.clone();
        Item itemB = (Item) item.clone();
        Item itemC = (Item) item.clone();
        itemA.setNaam("A");
        itemB.setNaam("B");
        itemC.setNaam("C");
        actual.add(itemB);
        actual.add(itemA);
        actual.add(itemC);
        SortingService.sortAlphabeticallyOnName(actual);
        expected.add(itemA);
        expected.add(itemB);
        expected.add(itemC);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void sortAlphabeticallyOnCategoryNLTest() throws CloneNotSupportedException {
        List<Item> expected = new ArrayList<>();
        List<Item> actual = new ArrayList<>();
        Item itemA =  (Item) item.clone();
        Item itemB = (Item) item.clone();
        Item itemC = (Item) item.clone();
        itemA.setCategorieNL("A");
        itemB.setCategorieNL("B");
        itemC.setCategorieNL("C");
        actual.add(itemB);
        actual.add(itemA);
        actual.add(itemC);
        SortingService.sortAlphabeticallyOnCategoryNL(actual);
        expected.add(itemA);
        expected.add(itemB);
        expected.add(itemC);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void sortAlphabeticallyOnCategoryENTest() throws CloneNotSupportedException {
        List<Item> expected = new ArrayList<>();
        List<Item> actual = new ArrayList<>();
        Item itemA =  (Item) item.clone();
        Item itemB = (Item) item.clone();
        Item itemC = (Item) item.clone();
        itemA.setCategorieEN("A");
        itemB.setCategorieEN("B");
        itemC.setCategorieEN("C");
        actual.add(itemB);
        actual.add(itemA);
        actual.add(itemC);
        SortingService.sortAlphabeticallyOnCategoryEN(actual);
        expected.add(itemA);
        expected.add(itemB);
        expected.add(itemC);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }
    @Test
    public void sortNumericallyOnIDTest() throws CloneNotSupportedException {
        List<Item> expected = new ArrayList<>();
        List<Item> actual = new ArrayList<>();
        Item itemA =  (Item) item.clone();
        Item itemB = (Item) item.clone();
        Item itemC = (Item) item.clone();
        itemA.setArtikelNr(1);
        itemB.setArtikelNr(2);
        itemC.setArtikelNr(3);
        actual.add(itemB);
        actual.add(itemA);
        actual.add(itemC);
        SortingService.sortNumericallyOnID(actual);
        expected.add(itemA);
        expected.add(itemB);
        expected.add(itemC);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void sortNumericallyOnEANCodeTest() throws CloneNotSupportedException {
        List<Item> expected = new ArrayList<>();
        List<Item> actual = new ArrayList<>();
        Item itemA =  (Item) item.clone();
        Item itemB = (Item) item.clone();
        Item itemC = (Item) item.clone();
        itemA.setEanCode("1");
        itemB.setEanCode("2");
        itemC.setEanCode("3");
        actual.add(itemB);
        actual.add(itemA);
        actual.add(itemC);
        SortingService.sortNumericallyOnEANCode(actual);
        expected.add(itemA);
        expected.add(itemB);
        expected.add(itemC);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void sortAlphabeticallyOnBrandTest() throws CloneNotSupportedException {
        List<Item> expected = new ArrayList<>();
        List<Item> actual = new ArrayList<>();
        Item itemA =  (Item) item.clone();
        Item itemB = (Item) item.clone();
        Item itemC = (Item) item.clone();
        itemA.setMerk("A");
        itemB.setMerk("B");
        itemC.setMerk("C");
        actual.add(itemB);
        actual.add(itemA);
        actual.add(itemC);
        SortingService.sortAlphabeticallyOnBrand(actual);
        expected.add(itemA);
        expected.add(itemB);
        expected.add(itemC);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }
}