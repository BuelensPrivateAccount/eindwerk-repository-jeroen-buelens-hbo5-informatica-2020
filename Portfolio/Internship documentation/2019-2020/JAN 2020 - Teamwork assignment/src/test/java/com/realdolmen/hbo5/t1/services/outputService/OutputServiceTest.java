package com.realdolmen.hbo5.t1.services.outputService;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.utilities.UserInputInterface;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OutputServiceTest {

    @Mock
    private UserInputInterface userInputInterface;

    @InjectMocks
    private OutputService outputService =  new OutputService(new ArrayList<>());


    @Test
    void getFileName() {
        String testString = "NullPointerNinja";
        when(userInputInterface.requestAString(anyString())).thenReturn(testString);
        String output = outputService.getFileName();
        assertEquals(testString,output);
    }

    @Test
    void writeHashMapToFile() throws IOException {
        String defaultDirectory = ".\\testRescources\\";
        List<Item> list = new ArrayList<>();
        LocalDateTime localDateTime = Instant.ofEpochSecond(Long.parseLong("20140502"))
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();

        Item item = new Item(0,"123456","NullPointerNinja","NullPointerNinja","NullPointerNinja","NullPointerNinja","NullPointerNinja","NullPointerNinja","NullPointerNinja","NullPointerNinja","NullPointerNinja",1,1,localDateTime);
        for(int i=0;i<25;i++){
            list.add(item);
        }
        OutputService outputService = new OutputService(list);

        outputService.outputCSV(defaultDirectory,"actual");
        Path file1 = new File(defaultDirectory+"actual.csv").toPath();
        Path file2 = new File(defaultDirectory+"expected.csv").toPath();
        byte[] actual = Files.readAllBytes(file1);
        byte[] expected = Files.readAllBytes(file2);
        assertArrayEquals(expected,actual);
    }
}