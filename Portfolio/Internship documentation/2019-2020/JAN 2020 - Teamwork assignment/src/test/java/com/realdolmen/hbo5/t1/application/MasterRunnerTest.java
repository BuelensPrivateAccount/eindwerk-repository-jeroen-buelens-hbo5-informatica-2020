package com.realdolmen.hbo5.t1.application;


import com.realdolmen.hbo5.t1.controllers.ImportController;
import com.realdolmen.hbo5.t1.controllers.OutputController;
import com.realdolmen.hbo5.t1.controllers.SearchController;
import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.exceptions.ColumnNameNotOkException;
import com.realdolmen.hbo5.t1.exceptions.FileToBigException;
import com.realdolmen.hbo5.t1.exceptions.FileValidationException;
import com.realdolmen.hbo5.t1.utilities.ConsoleRequestUserInput;
import com.realdolmen.hbo5.t1.utilities.UserInputInterface;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MasterRunnerTest {
    static List<Item> list;
    static MasterRunner masterRunnerNL = new MasterRunner("nl");
    static ImportController importController = Mockito.mock(ImportController.class);
    static OutputController outputController = Mockito.mock(OutputController.class);
    static SearchController searchController = Mockito.mock(SearchController.class);
    static UserInputInterface userInputInterface = Mockito.mock(ConsoleRequestUserInput.class);

    @BeforeAll
    public static void setup() {
        list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            LocalDateTime localDateTime = Instant.ofEpochSecond(Long.parseLong("20140502"))
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime();
            Item item = new Item(12L, "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", randomStringGenerator(2) + "NullPointerNinja", 1, 1, localDateTime);
            list.add(item);
        }
        masterRunnerNL.setImportControllerBootUp(importController);
        masterRunnerNL.setImportController(importController);
        masterRunnerNL.setUserInputInterface(userInputInterface);
        masterRunnerNL.setRunningDatabase(list);
        masterRunnerNL.setSearchController(searchController);
        masterRunnerNL.setOutputController(outputController);
    }

    public static String randomStringGenerator(int length) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    @Test
    @DisplayName("high level overview test")
    public void MasterRunnerFunctionalityTestWorkCorrectlyNL() throws ColumnNameNotOkException, FileValidationException, ParseException, IOException, FileToBigException {
        when(userInputInterface.requestInt("Geef het nummer van de optie die u wilt uitvoeren in: ")).thenReturn(1, 2, 3, 4, 1, 2, 3, 4, 0, 5, 0);
        when(userInputInterface.requestAString(anyString())).thenReturn("blah");
        when(userInputInterface.requestAString("[X] om te stoppen")).thenReturn("x");
        when(userInputInterface.requestLong("Geef de EAN-code in (alleen cijfers): ")).thenReturn(123L);
        when(userInputInterface.requestInt("Geef het aantal beschikbare artikelen in: ")).thenReturn(1);
        when(userInputInterface.requestAString("[V] om verder te gaan [B] om terug te gaan.")).thenReturn("V");
        when(userInputInterface.requestDouble("Geef de prijs in (zonder euroteken, met punt)")).thenReturn(1.0);
        when(userInputInterface.requestLong(anyString())).thenReturn(1L);
        when(outputController.fetchFileInCaseOfDupe(any(),any())).thenReturn(new File("NullPointerNinjas"));
        Assertions.assertDoesNotThrow(() -> {
            masterRunnerNL.run();
        });
        verify(importController, times(2)).read();
        verify(outputController, times(2)).outputCSV(anyString(), anyString());
        verify(searchController).searchByBrand(anyString());
        verify(searchController).searchByName(anyString());
        verify(searchController).searchByCategoryNL(anyString());
        verify(searchController).searchByID(anyLong());


    }
}
