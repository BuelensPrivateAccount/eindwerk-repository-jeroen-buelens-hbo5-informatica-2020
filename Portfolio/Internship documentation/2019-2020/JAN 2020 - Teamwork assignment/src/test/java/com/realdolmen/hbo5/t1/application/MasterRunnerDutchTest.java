package com.realdolmen.hbo5.t1.application;


import com.realdolmen.hbo5.t1.utilities.UserInputInterface;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MasterRunnerDutchTest {
    private static String language = "nl";
    @InjectMocks
    MasterRunner masterRunner = new MasterRunner(language);
    @Mock
    private UserInputInterface userInputInterface;

    @Test
    public void emptyTest() {
    }

    @Test
    @DisplayName("Assert no exceptions are thrown when program is ran")
    public void TestRunDutch() {
        when(userInputInterface.requestInt(anyString())).thenReturn(0);
        Assertions.assertDoesNotThrow(() -> {
            masterRunner.run();
        });

    }

    @Test
    public void TestimportCSV() {

    }


}