package com.realdolmen.hbo5.t1.services.validation;

import com.realdolmen.hbo5.t1.services.validation.ImportValidation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;

@ExtendWith(MockitoExtension.class)
public class ValdidationTest {

    @Test
    public void endsOnCSVTestLukt() {
        File fTest = new File(".\\testRescources\\test.csv");

        Boolean result = ImportValidation.endsOnCSV(fTest);

        Assertions.assertTrue(result);
    }

    @Test
    public void endsOnCSVTestLuktNiet() {
        File fTest = new File(".\\testRescources\\test.txt");

        Boolean result = ImportValidation.endsOnCSV(fTest);

        Assertions.assertFalse(result);
    }

    @Test
    public void contentCSVTestLuktNiet() {//omdat het een bestand is en niet bestaat (bij testing), dus is het nul
        File fTest = new File(".\\testRescources\\afilethatdoesnotexist.csv");//geen \ omdat anders de test niet wilt runnen

        Boolean result = ImportValidation.contentCSV(fTest);

        Assertions.assertFalse(result);
    }

    @Test
    public void contentCSVTestLukt() {//omdat het een bestand is en bestaat (bij testing) en niet nul is, daarom werkt de test
        File fTest = new File(".\\testRescources\\test.csv");
        Boolean result = ImportValidation.contentCSV(fTest);

        Assertions.assertTrue(result);
    }

    @Test
    public void compatibleColumnsTestLukt() {
        String sTest = "artikelnr;eancode;merk;naam;omschrijving_nl;omschrijving_e;categorie_nl;categorie_e;groep1;groep2;groep3;prijs;aantal;creatiedatum\n" +
                "140120001;4012074147426;SCANPART;LEKBAK;lekbak wasmachine / vaatwasser;drip tray washing machine / dishwasher;Witgoed\\Universeel\\Diverse accessoires;White goods\\Universal\\Various accessories;10;1010;101099;19,99;519,00;0\n" +
                "140120002;4012074371869;SCANPART;TRILLINGSDEMPER;anti trillingsmat 60x60x0,8cm;anti-vibration mat 60x60x0.8cm;Witgoed\\Wasmachines\\Diverse accessoires;White goods\\Washing machines\\Various accessories;10;1015;101599;7,99;2760,00;20120305\n";

        Boolean result = ImportValidation.compatibleColumns(sTest.toLowerCase());

        Assertions.assertTrue(result);
    }

    @Test
    public void compatibleColumnsTestLuktNiet() {
        String sTest = "artikelnummer;eancode;merk;naam;omschrijving_nl;omschrijving_e;categorie_nl;categorie_e;groep1;groep2;groep3;prijs;aantal;creatiedatum\n" +
                "140120001;4012074147426;SCANPART;LEKBAK;lekbak wasmachine / vaatwasser;drip tray washing machine / dishwasher;Witgoed\\Universeel\\Diverse accessoires;White goods\\Universal\\Various accessories;10;1010;101099;19,99;519,00;0\n" +
                "140120002;4012074371869;SCANPART;TRILLINGSDEMPER;anti trillingsmat 60x60x0,8cm;anti-vibration mat 60x60x0.8cm;Witgoed\\Wasmachines\\Diverse accessoires;White goods\\Washing machines\\Various accessories;10;1015;101599;7,99;2760,00;20120305\n";

        Boolean result = ImportValidation.compatibleColumns(sTest.toLowerCase());

        Assertions.assertFalse(result);
    }

    @Test
    public void checkCharactersTestLukt() {
        String sTest = "artikelnr;eancode;merk;naam;omschrijving_nl;omschrijving_e;categorie_nl;categorie_e;groep1;groep2;groep3;prijs;aantal;creatiedatum";
        String sTest2 = "140120001;4012074147426;SCANPART;LEKBAK;lekbak wasmachine / vaatwasser;drip tray washing machine / dishwasher;Witgoed\\Universeel\\Diverse accessoires;White goods\\Universal\\Various accessories;10;1010;101099;19,99;519,00;0";
        String sTest3 = "140120002;4012074371869;SCANPART;TRILLINGSDEMPER;anti trillingsmat 60x60x0,8cm;anti-vibration mat 60x60x0.8cm;Witgoed\\Wasmachines\\Diverse accessoires;White goods\\Washing machines\\Various accessories;10;1015;101599;7,99;2760,00;20120305\n";


        Boolean result = ImportValidation.checkEmptyRow(sTest);
        Boolean result2 = ImportValidation.checkEmptyRow(sTest2);
        Boolean result3 = ImportValidation.checkEmptyRow(sTest3);

        Assertions.assertTrue(result);
        Assertions.assertTrue(result2);
        Assertions.assertTrue(result3);
    }

    @Test
    public void checkCharactersTestLuktNiet() {
        String sTest = "140120001;4012074147426;SCANPART;�;lekbak wasmachine / vaatwasser;drip tray washing machine / dishwasher;Witgoed\\Universeel\\Diverse accessoires;White goods\\Universal\\Various accessories;10;1010;101099;19,99;519,00;0";

        Boolean result = ImportValidation.checkCharacters(sTest);

        Assertions.assertFalse(result);
    }

    @Test
    public void checkEmptyRowTestLukt() {
        String sTest = "artikelnr;eancode;merk;naam;omschrijving_nl;omschrijving_e;categorie_nl;categorie_e;groep1;groep2;groep3;prijs;aantal;creatiedatum";
        String sTest2 = "140120001;4012074147426;SCANPART;LEKBAK;lekbak wasmachine / vaatwasser;drip tray washing machine / dishwasher;Witgoed\\Universeel\\Diverse accessoires;White goods\\Universal\\Various accessories;10;1010;101099;19,99;519,00;0";
        String sTest3 = ";;;;;;Witgoed\\Wasmachines\\Diverse accessoires;;;;;,;;";

        Boolean result = ImportValidation.checkEmptyRow(sTest);
        Boolean result2 = ImportValidation.checkEmptyRow(sTest2);
        Boolean result3 = ImportValidation.checkEmptyRow(sTest3);

        Assertions.assertTrue(result);
        Assertions.assertTrue(result2);
        Assertions.assertTrue(result3);
    }

    @Test
    public void checkEmptyRowTestLuktNiet() {
        String sTest = ";;;;;;;;;;;;;";
        Boolean result = ImportValidation.checkEmptyRow(sTest);

        Assertions.assertFalse(result);
    }
}
