package com.realdolmen.hbo5.t1.application;


import com.realdolmen.hbo5.t1.utilities.UserInputInterface;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MasterRunnerEnglishTest {
    private static String language = "en";

    @Mock
    private UserInputInterface userInputInterface;

    @InjectMocks
    MasterRunner masterRunner = new MasterRunner(language);

    @Test
    public void emptyTest() {
    }

    @Test
    @DisplayName("Assert no exceptions are thrown when program is ran")
    public void TestRunDutch() {
        when(userInputInterface.requestInt(anyString())).thenReturn(0);
        Assertions.assertDoesNotThrow(() -> {
            masterRunner.run();
        });

    }

}