package com.realdolmen.hbo5.t1.services.displayService;

import com.realdolmen.hbo5.t1.services.displayService.ItemDisplayer;
import com.realdolmen.hbo5.t1.domain.objects.Item;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class ItemDisplayerTest {
     List<Item> items;
    Item i;

    private final String TABLE_BORDER = "+============+==================================================+==========+==================================================+====================+\n";
    private final String BREAK_LINE = "\n";

    public ItemDisplayerTest(){
        i = new Item(0,
            "0",
            "A",
            "B",
            "C",
            "C",
            "D",
            "D",
            "E",
            "F",
            "G",
            7.99,
            10, LocalDateTime.now());
        items = new ArrayList<>();
        for(int integer = 0; integer < 10;integer++){
            items.add(i);
        }

    }

    @Test
    public void testFormatMessage(){
        ItemDisplayer displayer = new ItemDisplayer(items);
        String outp = i.formatSimple();
        NumberFormat f = NumberFormat.getCurrencyInstance();
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("|%-12s", "0"));
        sb.append(String.format("|%-50s", "B"));
        sb.append(String.format("|%-10s", f.format(7.99).replace(".",",")));
        sb.append(String.format("|%-50s", "D"));
        sb.append(String.format("|%-20s|", "A"));

        String expected = sb.toString();
        assertEquals(expected, outp);
    }

    @Test
    public void testSplit(){
        ItemDisplayer display = new ItemDisplayer(items);
        List<List<Item>> pages = display.split();
        List<List<Item>> expectedPages = new ArrayList<>();
        List<Item> expectedMap = items;
        expectedPages.add(expectedMap);
        assertEquals(expectedPages, pages);
    }

    @Test
    public void testItemsTable(){
        ItemDisplayer displayer = new ItemDisplayer(items);
        List<Item> itemMap = items;
        StringBuilder sb = new StringBuilder();
        sb.append(TABLE_BORDER);
        sb.append(String.format("|%-12s", "Artikelnr"));
        sb.append(String.format("|%-50s", "Naam"));
        sb.append(String.format("|%-10s", "Prijs"));
        sb.append(String.format("|%-50s", "Categorie"));
        sb.append(String.format("|%-20s|", "Merknaam"));
        sb.append(BREAK_LINE + TABLE_BORDER);
        String table = displayer.getItemsTable(itemMap);
        String starting = sb.toString();
        assertTrue(table.startsWith(starting));
    }


}
