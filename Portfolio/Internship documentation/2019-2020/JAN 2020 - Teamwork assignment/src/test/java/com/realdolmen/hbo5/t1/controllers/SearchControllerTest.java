package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.services.searchService.SearchService;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;

import static org.mockito.Mockito.*;

import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class SearchControllerTest {

    List<Item> inProgressDB;
    SearchController searchController;
    SearchService service;

    @BeforeEach
    public void setup(){
        LocalDateTime dt = LocalDateTime.now();
        inProgressDB = new ArrayList<>();
        Item i = new Item(1, "1", "NPN", "NPN", "NPN", "NPN", "NPN", "NPN", "NPN", "NPN", "NPN", 1.99, 10, dt);
        inProgressDB.add(i);

        service = mock(SearchService.class);

        searchController = new SearchController(inProgressDB);
        searchController.setSearchService(service);
    }

    @Test
    public void searchByName() {
        String searchQuery = "LEKBAK";
        ArgumentCaptor<String> searchQueryCaptor = ArgumentCaptor.forClass(String.class);

        searchController.searchByName(searchQuery);
        verify(service, times(1)).searchByName(searchQueryCaptor.capture());

        Assertions.assertEquals(searchQuery, searchQueryCaptor.getValue());
    }

    @Test
    public void searchByCategory(){
        String searchQuery = "LEKBAK";
        ArgumentCaptor<String> searchQueryCaptor = ArgumentCaptor.forClass(String.class);

        searchController.searchByCategoryNL(searchQuery);
        verify(service, times(1)).searchByCategoryNL(searchQueryCaptor.capture());

        Assertions.assertEquals(searchQuery, searchQueryCaptor.getValue());
    }

    @Test
    public void searchByID(){
        long searchQuery = 1;
        ArgumentCaptor<Long> searchQueryCaptor = ArgumentCaptor.forClass(Long.class);

        searchController.searchByID(searchQuery);
        verify(service, times(1)).searchByID(searchQueryCaptor.capture());

        Assertions.assertEquals(searchQuery, (long)searchQueryCaptor.getValue());
    }

    @Test
    public void searchByBrand(){
        String searchQuery = "NPN";
        ArgumentCaptor<String> searchQueryCaptor = ArgumentCaptor.forClass(String.class);

        searchController.searchByBrand(searchQuery);
        verify(service, times(1)).searchByBrand(searchQueryCaptor.capture());

        Assertions.assertEquals(searchQuery, searchQueryCaptor.getValue());
    }
}
