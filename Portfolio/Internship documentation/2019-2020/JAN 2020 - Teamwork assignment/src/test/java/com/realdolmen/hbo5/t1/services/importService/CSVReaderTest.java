package com.realdolmen.hbo5.t1.services.importService;

import com.realdolmen.hbo5.t1.application.MasterRunner;
import com.realdolmen.hbo5.t1.controllers.ImportController;
import com.realdolmen.hbo5.t1.exceptions.ColumnNameNotOkException;
import com.realdolmen.hbo5.t1.exceptions.FileValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.text.ParseException;

@ExtendWith(MockitoExtension.class)
public class CSVReaderTest {

    @Test
    public void testRead(){
        Assertions.assertDoesNotThrow(()->{
            String defaultDirectory = ".\\testRescources\\";
            CSVReader reader = new CSVReader(defaultDirectory+"expected.csv");
            reader.read();
        });
    }

    @Test
    public void testTrowAssertNoCSVReader(){
        Assertions.assertThrows(ColumnNameNotOkException.class, () ->{
            String defaultDirectory = ".\\testRescources\\";
            CSVReader reader = new CSVReader(defaultDirectory+"nohead.csv");
            reader.read();
        });
    }

}