package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.services.outputService.OutputService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class OutputControllerTest {

    private List<Item> trueItems;
    @Mock
    private OutputService outputService;
    @InjectMocks
    private OutputController outputController;

    public OutputControllerTest(){
        LocalDateTime localDateTime = LocalDateTime.now();
        Item item = new Item(0,"NullPointerNinja","NullPointerNinja","NullPointerNinja","NullPointerNinja","NullPointerNinja","NullPointerNinja","NullPointerNinja","NullPointerNinja","NullPointerNinja","NullPointerNinja",1,1,localDateTime);
        trueItems = new ArrayList<>();
        for(long i = 0L;i<99L;i++){
            trueItems.add(item);
        }
        outputController = new OutputController(trueItems);
    }


    @Test
    void outputCSV() throws IOException {
        ArgumentCaptor<String> path = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> filename = ArgumentCaptor.forClass(String.class);
        String truePath = "NullPointerNinja";
        String trueFilename = "NullPointerNinja";
        outputController.outputCSV(truePath,trueFilename);
        verify(outputService).outputCSV(path.capture(),filename.capture());
        assertAll(()->{
            assertEquals(truePath,path.getValue());
            assertEquals(trueFilename,filename.getValue());
        });

    }
}