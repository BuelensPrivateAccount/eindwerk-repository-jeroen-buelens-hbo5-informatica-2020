package com.realdolmen.hbo5.t1.services.displayService;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.exceptions.InvalidPageException;
import com.realdolmen.hbo5.t1.exceptions.NullPageException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ItemDisplayer {
    private List<Item> filteredItems;
    private int itemsPerPage;

    private List<List<Item>> pages;
    private int totalPages;

    private final String TABLE_BORDER = "+============+==================================================+==========+==================================================+====================+\n";
    private final String BREAK_LINE = "\n";

    public ItemDisplayer(List<Item> filteredItems, int itemsPerPage) {
        this(filteredItems);
        this.itemsPerPage = itemsPerPage;
        pages = split();
        totalPages = pages.size();
    }

    public ItemDisplayer(List<Item> filteredItems){
        this.filteredItems = filteredItems;
        itemsPerPage = 30;
        pages = split();
        totalPages = pages.size();
    }

    /**
     * Splitst de items in HashMaps die elk ten meeste 30 items bevatten. De HashMaps van 30 items
     * worden in een ArrayList van HashMaps geplaatst.
     *
     * @return pagedItems. Dit is de ArrayList die de verschillende HashMaps bevat.
     */
    protected List<List<Item>> split() {
        List<List<Item>> pagedItems = new ArrayList<>();
        List<Item> buffer = new ArrayList<>();

        Iterator<Item> iterator = filteredItems.iterator();
        int counter = 0;
        while (iterator.hasNext()) {
            Item value = iterator.next();
            buffer.add(value);
            if (counter == itemsPerPage-1 || !iterator.hasNext()) {
                pagedItems.add(buffer);
                buffer = new ArrayList<>();
                counter = 0;
            } else {
                counter++;
            }
        }

        return pagedItems;
    }
    protected String getTableHeader(){
        StringBuilder sb = new StringBuilder();
        sb.append(TABLE_BORDER);
        sb.append(String.format("|%-12s", "Artikelnr"));
        sb.append(String.format("|%-50s", "Naam"));
        sb.append(String.format("|%-10s", "Prijs"));
        sb.append(String.format("|%-50s", "Categorie"));
        sb.append(String.format("|%-20s|", "Merknaam"));
        sb.append(BREAK_LINE + TABLE_BORDER);

        return sb.toString();
    }
    protected String getItemsTable(List<Item> items) {
        StringBuilder sb = new StringBuilder();
        sb.append(getTableHeader());

        for (Item entry : items) {
            sb.append(BREAK_LINE);
            sb.append(entry.formatSimple());
        }

        sb.append(BREAK_LINE + TABLE_BORDER);
        return sb.toString();
    }

    /**
     * Weergeeft de geselecteerde pagina, mooi geformateert in een tabel als String. De tabel bevat een header en
     * alle items die op de gekozen pagina staan. De pageOndex begint vanaf 0, dat wil zeggen dat om pagina 1 te selecteren,
     * je 0 moet ingeven als pageIndex.
     * @param pageIndex De index van de pagina, beginende van 0. Om pagina 1 te verkrijgen, moet het getal 0 worden ingegeven.
     * @return Een String die geformateerd als tabel is waarin alle items van de huidige pagina in staan.
     * @throws InvalidPageException Wanneer de pageIndex gelijk of groter is dan het eigenlijke aantal pagina's.
     */
    public String display(int pageIndex) throws InvalidPageException {
        StringBuilder sb = new StringBuilder();

        if((totalPages)>0){
            if(pageIndex < totalPages){
                List<Item> page = pages.get(pageIndex);

                int startItem = (pageIndex+1) * itemsPerPage - itemsPerPage;
                int endItem = startItem + page.size();

                sb.append(getItemsTable(page));
                sb.append("Pagina " + (pageIndex+1) + " van " + totalPages + ". Toont items " + (startItem+1) + " tot " + endItem + ". Totaal: " + getTotalItems() );

            } else {
                throw new InvalidPageException("U heeft de laatste pagina bereikt.");
            }
        } else {
            sb.append(getTableHeader());
            sb.append("- Niets om weer te geven. -");
        }

        return sb.toString();
    }


    public List<Item> getFilteredItems() {
        return filteredItems;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public int getTotalItems(){
        int items = itemsPerPage * pages.size();

        return items - (itemsPerPage - (pages.get(pages.size()-1).size()));
    }
}
