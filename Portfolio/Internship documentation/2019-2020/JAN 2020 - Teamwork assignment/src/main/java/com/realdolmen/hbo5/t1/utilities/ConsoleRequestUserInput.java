package com.realdolmen.hbo5.t1.utilities;

import static com.realdolmen.hbo5.t1.utilities.UserInputHandler.*;

public class ConsoleRequestUserInput implements UserInputInterface {

    private static final ConsoleRequestUserInput instance = new ConsoleRequestUserInput();

    private ConsoleRequestUserInput() {
    }

    public static ConsoleRequestUserInput getInstance() {
        return instance;
    }

    /**
     * requests a string from user
     *
     * @param query the question aimed at the user
     * @return {@link String}
     */
    public String requestAString(String query) {
        return fetchStringFromUser("you have not entered a valid input", query);
    }

    /**
     * requests a integer from user
     *
     * @param query the question aimed at the user
     * @return {@link Integer}
     */
    public int requestInt(String query) {

        return fetchIntFromUser("you have not entered a valid input", query);
    }

    /**
     * requests a double from user
     *
     * @param query the question aimed at the user
     * @return {@link Double}
     */
    public double requestDouble(String query) {

        return fetchDoubleFromUser("you have not entered a valid input", query);
    }

    public long requestLong(String query) {

        return fetchLongFromUser("you have not entered a valid input", query);
    }

    public boolean requestBoolean(String query) {
        return fetchBooleanFromUser("you have not entered a valid",query);
    }



}
