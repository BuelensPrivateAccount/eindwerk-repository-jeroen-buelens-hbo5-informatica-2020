package com.realdolmen.hbo5.t1.utilities;

public interface UserInputInterface {
    public String requestAString(String query);
    public int requestInt(String query);
    public boolean requestBoolean(String query);
    public double requestDouble(String query);
    public long requestLong(String query);
}
