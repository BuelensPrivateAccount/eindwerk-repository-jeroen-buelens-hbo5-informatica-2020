package com.realdolmen.hbo5.t1.application;
import com.realdolmen.hbo5.t1.utilities.ConsoleRequestUserInput;
import com.realdolmen.hbo5.t1.utilities.UserInputInterface;

/**
 * this is the launcher, to launch the application
 */
public class App
{
    /**
     * main method, launcher
     * @param args this are the arguments you could pass through, just main stuff
     */
    public static void main( String[] args )
    {
        UserInputInterface userInputInterface = ConsoleRequestUserInput.getInstance();
        System.out.println("English has not yet been implemented and the program will always default to dutch.");
        String language = userInputInterface.requestAString("In what language do you want to run this program?/ In welke taal wil je dit programma laten draaien? (Nederlands/English): ");
        MasterRunner masterRunner = new MasterRunner(language);
        masterRunner.run();
    }
}
