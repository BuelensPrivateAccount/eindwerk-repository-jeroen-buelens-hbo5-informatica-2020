package com.realdolmen.hbo5.t1.services.validation;


import java.io.File;

public class ImportValidation {
    private ImportValidation() {
    }

    /**
     * endsOnCSV is een methode die controleert dat het bestand de juiste extentie heeft (.csv)
     *
     * @param f is het filepath dat verwijst naar het bestand
     * @return true als de filenaam eindigd met .csv
     */
    public static boolean endsOnCSV(File f) {
        String naam = f.getName();
        if (naam.endsWith(".csv")) {
            return true;
        } else {
            return false;
            //error melding file is geen csv bestand
        }
    }

    /**
     * contentCSV is een methode die controleert als het csv bestand niet leeg is
     *
     * @param f is het filepath dat verwijst naar het bestand
     * @return true als de file groter is dan 0
     */
    public static boolean contentCSV(File f) {
        if (f.length() > 0) {
            return true;
        } else {
            return false;
            //error melding file bevat niks
        }
    }

    /**
     * contentCSV is een methode die controleert als het csv bestand niet leeg is
     *
     * @param f is het filepath dat verwijst naar het bestand
     * @return true als de file kleiner is dan 1 000 000
     */
    public static boolean contentCSVToBig(File f) {
        if (f.length() > 1000000000) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * compatibleColumns is een methode die controleert dat een csv file begint met de vooropgegeven colomnamen
     *
     * @param s string die alle data bevat die in het bestand staan
     * @return true als de data in ons csv bestand begint met de vooropgegeven colomnamen
     */
    public static boolean compatibleColumns(String s) {
        return s.trim().startsWith("artikelnr;eancode;merk;naam;omschrijving_nl;omschrijving_e;categorie_nl;categorie_e;groep1;groep2;groep3;prijs;aantal;creatiedatum");
    }

    /**
     * checkCharacters is een methode die nakijkt dat er geen caraters zijn die verandert zijn in vraagtekens
     *
     * @param s string die alle data bevat die in het bestand staan
     * @return true als het bestand alle data kan lezen
     */
    public static boolean checkCharacters(String s) {// & �
        if (s.contains("\u007F") || (s.contains("�"))) {
            return false;
        } else {
            return true;
            //err
        }
    }

    /**
     *checkEmptyRow is een methode die nakijkt dat er een lijn leeg is of niet
     *
     * @param s string die alle data bevat die in het bestand staan
     * @return true als de rij niet leeg is
     */
    public static boolean checkEmptyRow(String s) {
        if (s.contentEquals(";;;;;;;;;;;;;")) {
            return false;
        } else {
            return true;
        }
    }
}
