package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.exceptions.ColumnNameNotOkException;
import com.realdolmen.hbo5.t1.exceptions.FileToBigException;
import com.realdolmen.hbo5.t1.exceptions.FileValidationException;
import com.realdolmen.hbo5.t1.interfaces.ICSVReader;
import com.realdolmen.hbo5.t1.services.importService.CSVReader;
import com.realdolmen.hbo5.t1.services.validation.ImportValidation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public class ImportController implements ICSVReader {

    private CSVReader importer;
    private File file;
    private String path;

    public ImportController(String path){
        this.path = path;
    }

    @Override
    public List<Item> read() throws IOException, ParseException, FileValidationException, ColumnNameNotOkException, NumberFormatException, ArrayIndexOutOfBoundsException, FileToBigException {
        if(importer == null){
            importer = new CSVReader(path);
            file = importer.getFile();
        }

        if(ImportValidation.endsOnCSV(file) && ImportValidation.contentCSV(file)){
            if (ImportValidation.contentCSVToBig(file)){
                return importer.read();
            }else {
                throw new FileToBigException("Het bestand dat u probeerde te importeren is te groot om zonder problemen te runnen");
            }

        } else{
            throw new FileValidationException("Het bestand is niet herkent als CSV bestand of bevat geen inhoud.");
        }

    }

    @Override
    public File getFile() throws FileNotFoundException {
        if(importer == null){
            importer = new CSVReader(path);
            file = importer.getFile();
        }

        return importer.getFile();
    }

    @Override
    public int getAddedItems() throws FileNotFoundException {
        if(importer == null){
            importer = new CSVReader(path);
        }

        return importer.getAddedItems();
    }

    @Override
    public String getImportExceptions() throws FileNotFoundException {
        if(importer == null){
            importer = new CSVReader(path);
        }

        return importer.getImportExceptions();
    }

}
