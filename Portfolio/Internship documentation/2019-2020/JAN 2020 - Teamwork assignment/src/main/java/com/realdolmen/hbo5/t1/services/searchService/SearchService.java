package com.realdolmen.hbo5.t1.services.searchService;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.interfaces.ISearch;

import java.util.ArrayList;
import java.util.List;

public class SearchService implements ISearch {

    private List<Item> inProgressDB;

    public SearchService(List<Item> inProgressDB) {
        this.inProgressDB = inProgressDB;
    }

    @Override
    public List<Item> searchByName(String name) {
        List<Item> filteredList = new ArrayList<>();
        for (Item item : inProgressDB) {
            if (name != null && !name.isEmpty()) {
                if (item.getNaam().toLowerCase().contains(name.toLowerCase())) {
                    filteredList.add(item);
                }
            }
        }
        return filteredList;
    }

    @Override
    public List<Item> searchByCategoryNL(String category){
        List<Item> filteredList = new ArrayList<>();
        for (Item item : inProgressDB) {
            if (category != null && !category.isEmpty()) {
                if (item.getCategorieNL().toLowerCase().contains(category.toLowerCase())) {
                    filteredList.add(item);
                }
            }
        }
        return filteredList;
    }

    @Override
    public List<Item> searchByID(long id) {
        List<Item> filteredList = new ArrayList<>();
        for (Item item : inProgressDB) {
                if (item.getArtikelNr() == id) {
                    filteredList.add(item);
            }
        }
        return filteredList;
    }

    @Override
    public List<Item> searchByBrand(String brand){
        List<Item> filteredList = new ArrayList<>();
        for(Item item : inProgressDB){
            if(brand != null && !brand.isEmpty()){
                if(item.getMerk().toLowerCase().contains(brand.toLowerCase())){
                    filteredList.add(item);
                }
            }
        }
        return filteredList;
    }

    @Override
    public List<Item> searchByEAN(String eanCode){
        List<Item> filteredList = new ArrayList<>();
        for(Item item : inProgressDB){
            if(eanCode != null && !eanCode.isEmpty()){
                if(item.getEanCode().toLowerCase().equals(eanCode.toLowerCase())){
                    filteredList.add(item);
                }
            }
        }
        return filteredList;
    }

}
