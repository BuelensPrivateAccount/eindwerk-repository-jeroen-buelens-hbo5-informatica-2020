package com.realdolmen.hbo5.t1.application;


import com.mysql.cj.protocol.ExportControlled;
import com.realdolmen.hbo5.t1.controllers.ImportController;
import com.realdolmen.hbo5.t1.controllers.OutputController;
import com.realdolmen.hbo5.t1.controllers.SearchController;
import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.exceptions.*;
import com.realdolmen.hbo5.t1.services.displayService.ItemDisplayer;
import com.realdolmen.hbo5.t1.services.validation.DirectoryValidation;
import com.realdolmen.hbo5.t1.utilities.ConsoleRequestUserInput;
import com.realdolmen.hbo5.t1.utilities.UserInputInterface;
import com.realdolmen.hbo5.t1.services.validation.ImportValidation;
import com.realdolmen.hbo5.t1.utilities.UserInputMenu;


import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is the master runner file
 */
public class MasterRunner {

    private final String DEFAULT_DIR = FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "\\StockProgram\\";
    private boolean userWantsToStop = false;
    private Map<Integer, String> menuMap;
    private List<Item> runningDatabase;
    private UserInputInterface userInputInterface;
    private int languageInteger;
    private ImportController importController;
    private ImportController importControllerBootUp;
    private OutputController outputController;
    private SearchController searchController;

    public MasterRunner(String language) {
        if (language.toLowerCase().contains("nl") || language.toLowerCase().contains("nederlands") || language.toLowerCase().contains("0") || language.contains("dutch")) {
            this.languageInteger = 0;
        } else {
            this.languageInteger = 1;
        }
        this.userInputInterface = ConsoleRequestUserInput.getInstance();
        this.runningDatabase = new ArrayList<>();
        this.importController = new ImportController(DEFAULT_DIR + "Import\\import.csv");
        this.outputController = new OutputController(runningDatabase);
        this.searchController = new SearchController(runningDatabase);
        this.importControllerBootUp = new ImportController(DEFAULT_DIR + "Resources\\production.csv");
        initialiseMenu();
    }

    private void initialiseMenu() {
        menuMap = new HashMap<>();
        menuMap.put(0, "Afsluiten");
        menuMap.put(1, "CSV importeren");
        menuMap.put(2, "CSV exporteren");
        menuMap.put(3, "Alle rijen weergeven");
        menuMap.put(4, "Zoeken");
        menuMap.put(5, "Item toevoegen");
    }

    public void run() {
        bootUpSequence();
        do {
            showMenu();
            int choice = userInputInterface.requestInt("Geef het nummer van de optie die u wilt uitvoeren in: ");
            executeSwitchStatement(choice);
        } while (!userWantsToStop);
        shutDownSequence();
    }

    private void executeSwitchStatement(int choice) {
        switch (choice) {
            case 1:
                importCSV();
                break;
            case 2:
                exportCSV();
                break;
            case 3:
                tonen(runningDatabase);
                break;
            case 4:
                bootSearchFunction();
                break;
            case 5:
                createItem();
                break;
            default:
                exitProgram();
        }
    }

    private void createItem() {
        List<String> inputMenuItems = new ArrayList<>();
        inputMenuItems.add("Artikelnummer");
        inputMenuItems.add("EAN-code");
        inputMenuItems.add("Merk");
        inputMenuItems.add("Naam");
        inputMenuItems.add("Omschrijving (NL)");
        inputMenuItems.add("Omschrijving (EN)");
        inputMenuItems.add("Categorie (NL)");
        inputMenuItems.add("Categorie (EN)");
        inputMenuItems.add("Groep (1)");
        inputMenuItems.add("Groep (2)");
        inputMenuItems.add("Groep (3)");
        inputMenuItems.add("Prijs");
        inputMenuItems.add("Aantal");

        List<String> inputQuestions = new ArrayList<>();
        inputQuestions.add("Geef het artikelnummer in (alleen cijfers): ");
        inputQuestions.add("Geef de EAN-code in (alleen cijfers): ");
        inputQuestions.add("Geef de merknaam in: ");
        inputQuestions.add("Geef de naam van het artikel in: ");
        inputQuestions.add("Geef de Nederlandse omschrijving van het artikel in: ");
        inputQuestions.add("Geef de Engelse omschrijving van het artikel in: ");
        inputQuestions.add("Geef de Nederlandse categorie van het artikel in: ");
        inputQuestions.add("Geef de Engelse categorie van het artikel in: ");
        inputQuestions.add("Geef de groep (1) in: ");
        inputQuestions.add("Geef de groep (2) in: ");
        inputQuestions.add("Geef de groep (3) in: ");
        inputQuestions.add("Geef de prijs in (zonder euroteken, met komma): ");
        inputQuestions.add("Geef het aantal beschikbare artikelen in: ");

        List<String> dataTypes = new ArrayList<>();
        dataTypes.add("long");
        dataTypes.add("long");
        dataTypes.add("String");
        dataTypes.add("String");
        dataTypes.add("String");
        dataTypes.add("String");
        dataTypes.add("String");
        dataTypes.add("String");
        dataTypes.add("String");
        dataTypes.add("String");
        dataTypes.add("String");
        dataTypes.add("double");
        dataTypes.add("int");

        UserInputMenu createItem = new UserInputMenu(userInputInterface, inputMenuItems, inputQuestions, dataTypes);

        createItem.start();
        List<Object> answers = createItem.getInputAnswers();
        try{
            Item newItem = new Item();
            newItem.create(Long.parseLong(answers.get(0).toString()),
                    answers.get(1).toString(),
                    answers.get(2).toString(),
                    answers.get(3).toString(),
                    answers.get(4).toString(),
                    answers.get(5).toString(),
                    answers.get(6).toString(),
                    answers.get(7).toString(),
                    answers.get(8).toString(),
                    answers.get(9).toString(),
                    answers.get(10).toString(),
                    Double.parseDouble(answers.get(11).toString()),
                    Integer.parseInt(answers.get(12).toString()));

            runningDatabase.add(newItem);

            System.out.println("Het item, met artikelnummer " + newItem.getArtikelNr() + ", werd met succes toegevoegd.");
        } catch (IllegalArgumentException e){
            System.err.println(e.getMessage());
        }
    }

    private void editMenuForSearchFunction() {
        menuMap = new HashMap<>();
        menuMap.put(0, "Afsluiten");
        menuMap.put(1, "Zoek op naam");
        menuMap.put(2, "Zoek op categorie");
        menuMap.put(3, "Zoek op ID");
        menuMap.put(4, "Zoek op merknaam");
    }

    private void bootSearchFunction() {
        searchController.setInProgressDB(runningDatabase);
        String query = "";
        editMenuForSearchFunction();
        while (!userWantsToStop) {
            List<Item> results = new ArrayList<>();
            showMenu();
            int choice = userInputInterface.requestInt("Geef het nummer van de optie die u wilt uitvoeren in: ");
            switch (choice) {
                case 1:
                    query = userInputInterface.requestAString("Op welke naam wenst u te zoeken: ");
                    results = searchController.searchByName(query);
                    break;
                case 2:
                    query = userInputInterface.requestAString("Op welke categorie wenst u te zoeken: ");
                    if (languageInteger == 0) {
                        results = searchController.searchByCategoryNL(query);
                    }
                    break;
                case 3:
                    int id = userInputInterface.requestInt("Op welke id wenst u te zoeken: ");
                    results = searchController.searchByID(id);
                    break;
                case 4:
                    query = userInputInterface.requestAString("Op welk merknaam wenst u te zoeken:");
                    results = searchController.searchByBrand(query);
                    break;
                case 0:
                default:
                    exitProgram();
            }
            if (!userWantsToStop) {
                tonen(results);

                int wantsToExport = userInputInterface.requestInt("Wilt u deze zoekresultaten exporteren?\n[1] Ja\n[2] Neen");
                if (wantsToExport == 1){
                    outputController.setItems(results);
                    startExport(outputController);
                }
            }
        }
        userWantsToStop = false;
        initialiseMenu();
    }

    private void tonen(List<Item> dataToshow) {
        ItemDisplayer display = new ItemDisplayer(dataToshow, 20);
        int pageToShow = 0;
        try {
            do {

                System.out.println(display.display(pageToShow));
                String pageMenu = (display.getTotalPages() > 0) ? "[N] voor de volgende pagina\n[P] voor de vorige pagina\n[X] om te stoppen: " : "[X] om te stoppen";
                String userInput = userInputInterface.requestAString(pageMenu);
                switch (userInput.toLowerCase()) {
                    case "p":
                        pageToShow--;
                        break;
                    case "x":
                        exitProgram();
                        break;
                    default:
                        pageToShow++;
                        break;
                }

            } while (!userWantsToStop);
        } catch (InvalidPageException e) {
            System.err.println(e.getMessage());
        } catch (IndexOutOfBoundsException e){
            System.err.println("Er kan niet teruggegaan worden bij de eerste pagina.");
        } finally {
            userWantsToStop = false;
        }
    }

    private void exportCSV() {
        outputController.setItems(runningDatabase);
        startExport(outputController);
    }

    private void startExport(OutputController outputController) {
        String exportFileName = userInputInterface.requestAString("Kies een naam voor het export bestand: ");
        try {
            File f = outputController.fetchFileInCaseOfDupe(exportFileName,DEFAULT_DIR);
            if (f.exists()) {
                int overrideFile = userInputInterface.requestInt("Opgelet: het bestand '" + exportFileName + ".csv' bestaat al op deze locatie. Wilt u het overschrijven?\n[1] Ja, overschrijven\n[2] Neen, stoppen");
                if (overrideFile == 1) {
                    outputController.outputCSV(DEFAULT_DIR + "Export\\", exportFileName);
                    System.out.println("Exporteren voltooid. Het bestand bevindt zich in \"" + DEFAULT_DIR + "Export\\" + exportFileName + ".csv" + "\".");
                } else {
                    System.out.println("Exporteren gestopt. Het bestand werd niet overschreven.");
                }
            } else {
                outputController.outputCSV(DEFAULT_DIR + "Export\\", exportFileName);
                System.out.println("Exporteren voltooid. Het bestand bevindt zich in \"" + DEFAULT_DIR + "Export\\" + exportFileName + ".csv" + "\".");
            }
        } catch (IOException e) {
            System.err.println("Door een onbekende fout tijdens het schrijven naar het CSV bestand, konden de gegevens niet geëxporteert worden.");
        }
    }

    private void importCSV() {
        boolean continueImport = false;
        try {
            importController.getFile();
            continueImport = true;
        } catch (FileNotFoundException e) {
            System.out.println("- Hoe importeren? -\n1) Plaats het bestand dat u wilt importeren in het mapje: Mijn Documenten/StockProgram/Import\n2) Hernoem het bestand 'import.csv'");
            int isReady = userInputInterface.requestInt("[1] Klaar, begin importeren\n[2] Stoppen");
            if (isReady == 1) {
                importCSV();
                return;
            }
        }

        if (continueImport) {
            System.out.println("Bezig met het importeren van de gegevens... Even geduld...");
            try {
                runningDatabase = importController.read();
                System.out.println(importController.getImportExceptions());
                System.out.println("Importeren voltooid. " + importController.getAddedItems() + " items ingeladen. ");
            } catch (IOException e) {
                System.err.println("Door een onbekende fout kon het bestand niet geopend worden.");
            } catch (ParseException e) {
                System.err.println("Er ging iets mis bij het berekenen van de datum van één van de producten.");
            } catch (NumberFormatException e) {
                System.err.println("Er is data die niet juist staat, er staan letter(s) bij het artikelnr");
            } catch (ArrayIndexOutOfBoundsException e) {
                System.err.println("Er is te weinig data in een van de rijen");
            } catch (FileValidationException | ColumnNameNotOkException | FileToBigException e) {
                System.err.println(e.getMessage());
            }
        } else {
            System.out.println("Importeren gestopt.");
        }
    }


    private void showMenu() {
        for (Map.Entry<Integer, String> entry : menuMap.entrySet()) {
            System.out.println("[" + entry.getKey() + "]  " + entry.getValue());
        }
    }

    private void bootUpSequence() {
        if (!DirectoryValidation.checkIfInputOutPutDirectoryExists()) {
            new File(DEFAULT_DIR + "Import").mkdirs();
            new File(DEFAULT_DIR + "Export").mkdirs();
            new File(DEFAULT_DIR + "Resources").mkdirs();
        }
        try {
            runningDatabase = importControllerBootUp.read();
        } catch (IOException e) {
            System.err.println("U moet nog een bestand importeren.");
        } catch (ParseException e) {
            System.err.println("Er ging iets mis bij het omzetten van de datums.");
        } catch (FileValidationException | ColumnNameNotOkException | FileToBigException e) {
            System.err.println(e.getMessage());
        }
    }

    private void shutDownSequence() {
        try {
            outputController.setItems(runningDatabase);
            outputController.outputCSV(DEFAULT_DIR + "Resources\\", "production");
        } catch (IOException e) {
            System.err.println("Er liep iets mis bij het opslaan.");
        }
    }

    private void exitProgram() {
        userWantsToStop = true;
    }

    void setRunningDatabase(List<Item> runningDatabase) {
        this.runningDatabase = runningDatabase;
    }

    void setUserInputInterface(UserInputInterface userInputInterface) {
        this.userInputInterface = userInputInterface;
    }

    void setImportController(ImportController importController) {
        this.importController = importController;
    }

    void setOutputController(OutputController outputController) {
        this.outputController = outputController;
    }

    void setImportControllerBootUp(ImportController importControllerBootUp) {
        this.importControllerBootUp = importControllerBootUp;
    }

    void setSearchController(SearchController searchController) {
        this.searchController = searchController;
    }
}
