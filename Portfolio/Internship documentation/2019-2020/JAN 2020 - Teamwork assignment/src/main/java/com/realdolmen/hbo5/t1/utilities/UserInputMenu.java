package com.realdolmen.hbo5.t1.utilities;

import java.util.ArrayList;
import java.util.List;

public class UserInputMenu {

    private UserInputInterface inputHandler;
    private List<String> inputItemsMenu;
    private List<String> inputQuestions;
    private List<String> expectedDataTypes;
    private List<Object> inputAnswers = new ArrayList<>();

    private int currentIteration = 0;

    public UserInputMenu(UserInputInterface inputHandler, List<String> inputItemsMenu, List<String> inputQuestions, List<String> expectedDataTypes){
        this.inputHandler = inputHandler;
        this.inputItemsMenu = inputItemsMenu;
        this.inputQuestions = inputQuestions;
        this.expectedDataTypes = expectedDataTypes;

        for(int i = 0; i < inputQuestions.size(); i++){
            inputAnswers.add(null);
        }
    }

    private String getInputHeader(){
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < inputItemsMenu.size(); i++){
            if(i > 0){
                sb.append("->");
            }
            if(i == currentIteration){
                sb.append("  [ " + inputItemsMenu.get(i) + " ]  ");
            } else {
                sb.append("  " + inputItemsMenu.get(i) + "  ");
            }
        }

        return sb.toString();
    }

    private String getNextQuestion(){
        return inputQuestions.get(currentIteration);
    }

    private int getNextIteration(){
        String nextOrPrev = inputHandler.requestAString("[V] om verder te gaan [B] om terug te gaan.");
        int i = currentIteration;

        if(nextOrPrev.toLowerCase().equals("v")){
            return ++i;
        } else {
            if(i > 0){
                return --i;
            } else {
                return i;
            }
        }
    }

    public void start(){
        while(currentIteration < inputItemsMenu.size()){
            showQuestion();
        }
    }

    public Object showQuestion(){
        int questionIteration = currentIteration;
        StringBuilder sb = new StringBuilder();
        sb.append(getInputHeader());
        sb.append("\n\n");
        sb.append(getNextQuestion());

        if(expectedDataTypes.get(currentIteration) == "String"){
            inputAnswers.set(currentIteration, inputHandler.requestAString(sb.toString()));
        } else if(expectedDataTypes.get(currentIteration) == "int"){
            inputAnswers.set(currentIteration, inputHandler.requestInt(sb.toString()));
        } else if(expectedDataTypes.get(currentIteration) == "boolean"){
            inputAnswers.set(currentIteration, inputHandler.requestBoolean(sb.toString()));
        } else if(expectedDataTypes.get(currentIteration) == "long"){
            inputAnswers.set(currentIteration, inputHandler.requestLong(sb.toString()));
        } else if(expectedDataTypes.get(currentIteration) == "double"){
            inputAnswers.set(currentIteration, inputHandler.requestDouble(sb.toString()));
        } else {
            throw new IllegalArgumentException("Niet ondersteunde datatype ingegeven.");
        }

        currentIteration = getNextIteration();

        return inputAnswers.get(questionIteration);
    }

    public List<Object> getInputAnswers(){
        return inputAnswers;
    }

    public int getCurrentIteration() {
        return currentIteration;
    }
}
