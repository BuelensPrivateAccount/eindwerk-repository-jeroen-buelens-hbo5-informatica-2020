package com.realdolmen.hbo5.t1.utilities;

import java.util.InputMismatchException;
import java.util.Scanner;

class UserInputHandler {
    private static Scanner scanner;
    private static String input ="";

    /**
     * This Constructor exists for testing purposes.
     * @param input this is a static string so tests can get a scanner with a static string as input from the user
     */
     UserInputHandler(String input) {// only for test
        UserInputHandler.scanner = new Scanner(input);
        UserInputHandler.input = input;
    }

    /**
     * This method exists to re-initialise the scanner anew, this to prevent garbage input from the previous query to mess up the method.
     */
    private static void initScanner() {
        if ((input.isEmpty())) {
            UserInputHandler.scanner = new Scanner(System.in);
        }
    }

    /**
     *
     * @param errorMessage this will be printed if the user does a inputMisMatch
     * @param userQuery this is the Query that is asked to the user.
     * @return {@link Integer}
     */
     static int fetchIntFromUser(String errorMessage, String userQuery) {
        while(true) {
            initScanner();
            try {
                System.out.println(userQuery);
                int i = scanner.nextInt();
                closeScannerMethod();
                return i;
            } catch (InputMismatchException e) {
                System.err.println(errorMessage);
            }
        }
    }

    /**
     *
     * @param errorMessage this will be printed if the user does a inputMisMatch
     * @param userQuery this is the Query that is asked to the user.
     * @return {@link Double}
     */
    static double fetchDoubleFromUser(String errorMessage, String userQuery) {
        while(true) {
            initScanner();
            try {
                System.out.println(userQuery);
                double i = scanner.nextDouble();
                closeScannerMethod();
                return i;
            } catch (InputMismatchException e) {
                System.err.println(errorMessage);
            }
        }
    }

    /**
     *
     * @param errorMessage this will be printed if the user does a inputMisMatch
     * @param userQuery this is the Query that is asked to the user.
     * @return {@link Double}
     */
    static long fetchLongFromUser(String errorMessage, String userQuery) {
        while(true) {
            initScanner();
            try {
                System.out.println(userQuery);
                long i = scanner.nextLong();
                closeScannerMethod();
                return i;
            } catch (InputMismatchException e) {
                System.err.println(errorMessage);
            }
        }
    }

    /**
     *
     * @param errorMessage this will be printed if the user does not enter anything.
     * @param userQuery this is the Query that is asked to the user.
     * @return {@link String}
     */
     static String fetchStringFromUser(String errorMessage, String userQuery) {
         while(true) {
             initScanner();
             System.out.print(userQuery);
             String input = scanner.nextLine();
             if (input.isEmpty()) {
                 System.err.println(errorMessage);
             } else {
                 closeScannerMethod();
                 return input;
             }
         }
    }

    /**
     * This method will be called to get a yes or no from the User
     * @param errorMessage this will be printed if the user does a inputMisMatch
     * @param userQuery this is the Query that is asked to the user.
     * @return {@link Boolean}
     */
     static Boolean fetchBooleanFromUser(String errorMessage, String userQuery) {
        while(true) {
            String s = fetchStringFromUser(errorMessage, userQuery);
            if (s.toUpperCase().contains("YES") || s.toUpperCase().contains("Y") || s.toUpperCase().contains("1")) {
                return true;
            } else if (s.toUpperCase().contains("NO") || s.toUpperCase().contains("N") || s.toUpperCase().contains("0")) {
                return false;
            } else {
                System.err.println(errorMessage);
            }
        }

    }

    /**
     * this method closes the scanner object used by the input handler, this method exists here to properly finish the program.
     */
     static void closeScannerMethod() {
        if (scanner != null)
        {
            scanner.reset();
            scanner = null;
        }
    }

}
