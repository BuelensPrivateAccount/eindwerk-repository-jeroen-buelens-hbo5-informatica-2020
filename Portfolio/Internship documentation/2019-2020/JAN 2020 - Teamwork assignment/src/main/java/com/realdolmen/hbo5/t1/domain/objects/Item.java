package com.realdolmen.hbo5.t1.domain.objects;

import com.realdolmen.hbo5.t1.utilities.StringUtils;

import java.text.NumberFormat;
import java.time.LocalDateTime;

public class Item implements Cloneable{
    long artikelNr;
    String eanCode;
    String merk;
    String naam;
    String omschrijvingNL;
    String omschrijvingEN;
    String categorieNL;
    String categorieEN;
    String groep1;
    String groep2;
    String groep3;
    double prijs;
    int aantal;
    LocalDateTime creatieDatum;

    public Item(long artikelNr, String eanCode, String merk, String naam, String omschrijvingNL, String omschrijvingEN, String categorieNL, String categorieEN, String groep1, String groep2, String groep3, double prijs, int aantal, LocalDateTime creatieDatum) {
        this.artikelNr = artikelNr;
        this.eanCode = eanCode;
        this.merk = merk;
        this.naam = naam;
        this.omschrijvingNL = omschrijvingNL;
        this.omschrijvingEN = omschrijvingEN;
        this.categorieNL = categorieNL;
        this.categorieEN = categorieEN;
        this.groep1 = groep1;
        this.groep2 = groep2;
        this.groep3 = groep3;
        this.prijs = prijs;
        this.aantal = aantal;
        this.creatieDatum = creatieDatum;
    }

    public Item(){
        this(0l, null, null, null, null, null, null, null, null, null, null, 0.0, 0, LocalDateTime.now());
    }

    /**
     * Formateert gegevens van het item in een String zodat deze mooi getoond kunnen worden in kolommen.
     * Het ziet er uiteindelijk zo uit:
     * Artikelnummer  |  Naam  |  Prijs  |  Categorie(NL)  |  Merk
     *
     * @return een String met de geformateerde gegevens van dit item.
     */
    public String formatSimple() {
        NumberFormat f = NumberFormat.getCurrencyInstance();

        StringBuilder sb = new StringBuilder();
        sb.append(String.format("|%-12s", Long.toString(this.artikelNr)));
        sb.append(String.format("|%-50s", StringUtils.shorten(this.naam, 50, "...")));
        sb.append(String.format("|%-10s", f.format(this.prijs).replace('.', ',') ));
        sb.append(String.format("|%-50s", StringUtils.shorten(this.categorieNL, 50, "...")));
        sb.append(String.format("|%-20s|", StringUtils.shorten(this.merk, 50, "...")));

        return sb.toString();
    }

    public Item create(long artikelNr, String eanCode, String merk, String naam, String omschrijvingNL, String omschrijvingEN, String categorieNL, String categorieEN, String groep1, String groep2, String groep3, double prijs, int aantal){
        setArtikelNr(artikelNr);

        if(!setEanCode(eanCode)){
            throw new IllegalArgumentException("De opgegeven EAN-code is geen geldige EAN-code");
        }

        setMerk(merk);
        setNaam(naam);
        setOmschrijvingNL(omschrijvingNL);
        setOmschrijvingEN(omschrijvingEN);
        setCategorieNL(categorieNL);
        setCategorieEN(categorieEN);
        setGroep1(groep1);
        setGroep2(groep2);
        setGroep3(groep3);
        setPrijs(prijs);
        setAantal(aantal);
        setCreatieDatum(LocalDateTime.now());

        return this;
    }

    public long getArtikelNr() {
        return artikelNr;
    }

    public boolean setArtikelNr(long artikelNr) {
        this.artikelNr = artikelNr;
        return true;
    }

    public String getEanCode() {return eanCode;}

    public boolean setEanCode(String eanCode) {
        if(eanCode.matches("^[0-9]*$")){
            this.eanCode = eanCode;
            return true;
        }
        return false;
    }

    public String getMerk() {
        return merk;
    }

    public boolean setMerk(String merk) {
        this.merk = merk;
        return true;
    }

    public String getNaam() {
        return naam;
    }

    public boolean setNaam(String naam) {
        this.naam = naam;
        return true;
    }

    public String getOmschrijvingNL() {
        return omschrijvingNL;
    }

    public boolean setOmschrijvingNL(String omschrijvingNL) {
        this.omschrijvingNL = omschrijvingNL;
        return true;
    }

    public String getOmschrijvingEN() {
        return omschrijvingEN;
    }

    public boolean setOmschrijvingEN(String omschrijvingEN) {
        this.omschrijvingEN = omschrijvingEN;
        return true;
    }

    public String getCategorieNL() {
        return categorieNL;
    }

    public boolean setCategorieNL(String categorieNL) {
        this.categorieNL = categorieNL;
        return true;
    }

    public String getCategorieEN() {
        return categorieEN;
    }

    public boolean setCategorieEN(String categorieEN) {
        this.categorieEN = categorieEN;
        return true;
    }

    public String getGroep1() {
        return groep1;
    }

    public boolean setGroep1(String groep1) {
        this.groep1 = groep1;
        return true;
    }

    public String getGroep2() {
        return groep2;
    }

    public boolean setGroep2(String groep2) {
        this.groep2 = groep2;
        return true;
    }

    public String getGroep3() {
        return groep3;
    }

    public boolean setGroep3(String groep3) {
        this.groep3 = groep3;
        return true;
    }

    public double getPrijs() {
        return prijs;
    }

    public boolean setPrijs(double prijs) {
        this.prijs = prijs;
        return true;
    }

    public int getAantal() {
        return aantal;
    }

    public boolean setAantal(int aantal) {
        this.aantal = aantal;
        return true;
    }

    public LocalDateTime getCreatieDatum() {
        return creatieDatum;
    }

    public boolean setCreatieDatum(LocalDateTime creatieDatum) {
        this.creatieDatum = creatieDatum;
        return true;
    }

    @Override
    public String toString() {
        return "Item{" +
                "artikelNr=" + artikelNr +
                ", eanCode='" + eanCode + '\'' +
                ", merk='" + merk + '\'' +
                ", naam='" + naam + '\'' +
                ", omschrijvingNL='" + omschrijvingNL + '\'' +
                ", omschrijvingEN='" + omschrijvingEN + '\'' +
                ", categorieNL='" + categorieNL + '\'' +
                ", categorieEN='" + categorieEN + '\'' +
                ", groep1='" + groep1 + '\'' +
                ", groep2='" + groep2 + '\'' +
                ", groep3='" + groep3 + '\'' +
                ", prijs=" + prijs +
                ", aantal=" + aantal +
                ", creatieDatum=" + creatieDatum +
                '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
         return super.clone();
    }
}

