package com.realdolmen.hbo5.t1.interfaces;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.exceptions.ColumnNameNotOkException;
import com.realdolmen.hbo5.t1.exceptions.FileToBigException;
import com.realdolmen.hbo5.t1.exceptions.FileValidationException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface ICSVReader {
    List<Item> read() throws IOException, ParseException, FileValidationException, ColumnNameNotOkException, FileValidationException, ColumnNameNotOkException, FileToBigException;
    File getFile() throws FileNotFoundException;
    int getAddedItems() throws FileNotFoundException;
    String getImportExceptions() throws FileNotFoundException;
}
