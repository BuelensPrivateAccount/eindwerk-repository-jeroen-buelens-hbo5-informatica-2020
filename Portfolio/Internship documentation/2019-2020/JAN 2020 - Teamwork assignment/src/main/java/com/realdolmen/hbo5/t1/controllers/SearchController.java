package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.interfaces.ISearch;
import com.realdolmen.hbo5.t1.services.searchService.SearchService;
import com.realdolmen.hbo5.t1.services.sortingService.SortingService;

import java.security.InvalidParameterException;
import java.util.List;

public class SearchController implements ISearch {

    private SearchService searchService;
    private List<Item> inProgressDB;

    public SearchController(List<Item> inProgressDB){
        this.inProgressDB = inProgressDB;
    }


    @Override
    public List<Item> searchByName(String name) {
        if(searchService == null){
            setSearchService(new SearchService(inProgressDB));
        }

        if(!name.isEmpty()){
            List<Item> results = searchService.searchByName(name);
            SortingService.sortAlphabeticallyOnName(results);
            return results;
        } else {
            throw new InvalidParameterException("De opgegeven zoekopdracht is leeg.");
        }
    }

    @Override
    public List<Item> searchByCategoryNL(String category) {
        if(searchService == null){
            setSearchService(new SearchService(inProgressDB));
        }

        if(!category.isEmpty()){
            List<Item> results = searchService.searchByCategoryNL(category);
            SortingService.sortAlphabeticallyOnCategoryNL(results);
            return results;
        } else {
            throw new InvalidParameterException("De opgegeven zoekopdracht is leeg.");
        }
    }

    @Override
    public List<Item> searchByID(long id) {
        if(searchService == null){
            setSearchService(new SearchService(inProgressDB));
        }

        if(id >= 0){
            List<Item> results = searchService.searchByID(id);
            SortingService.sortNumericallyOnID(results);
            return results;
        } else {
            throw new InvalidParameterException("De opgegeven is zoekopdracht is leeg.");
        }
    }

    @Override
    public List<Item> searchByBrand(String brand) {
        if(searchService == null){
            setSearchService(new SearchService(inProgressDB));
        }

        if(!brand.isEmpty()){
            List<Item> results = searchService.searchByBrand(brand);
            SortingService.sortAlphabeticallyOnBrand(results);
            return results;
        } else {
            throw new InvalidParameterException("De opgegeven zoekopdracht is leeg.");
        }
    }

    @Override
    public List<Item> searchByEAN(String eanCode) {
        return null;
    }

    protected void setSearchService(SearchService searchService){
        this.searchService = searchService;
    }

    public void setInProgressDB(List<Item> inProgressDB) {
        this.inProgressDB = inProgressDB;
    }
}
