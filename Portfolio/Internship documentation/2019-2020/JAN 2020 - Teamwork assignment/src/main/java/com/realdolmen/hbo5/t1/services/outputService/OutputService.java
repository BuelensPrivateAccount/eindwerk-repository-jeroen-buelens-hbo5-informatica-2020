package com.realdolmen.hbo5.t1.services.outputService;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.interfaces.ICSVWriter;
import com.realdolmen.hbo5.t1.utilities.ConsoleRequestUserInput;
import com.realdolmen.hbo5.t1.utilities.UserInputInterface;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class OutputService implements ICSVWriter {

    private static final String CSV_SEPARATOR = ";";
    private UserInputInterface userInputInterface;
    private List<Item> items;

    public OutputService(List<Item> items) {
        userInputInterface = ConsoleRequestUserInput.getInstance();
        this.items = items;
    }

    public String getFileName() {
        String filename = userInputInterface.requestAString("what name do you want to give to the file for the output");
        return filename.replaceAll("\\s+", "");
    }

    public void setItems(List<Item> items){
        this.items = items;
    }

    public void outputCSV(String path, String filename) throws IOException {
        ArrayList<String> csvLines = new ArrayList<>();
        StringBuilder header = new StringBuilder();
        header.append("artikelnr");
        header.append(CSV_SEPARATOR);
        header.append("eancode");
        header.append(CSV_SEPARATOR);
        header.append("merk");
        header.append(CSV_SEPARATOR);
        header.append("naam");
        header.append(CSV_SEPARATOR);
        header.append("omschrijving_nl");
        header.append(CSV_SEPARATOR);
        header.append("omschrijving_e");
        header.append(CSV_SEPARATOR);
        header.append("categorie_nl");
        header.append(CSV_SEPARATOR);
        header.append("categorie_e");
        header.append(CSV_SEPARATOR);
        header.append("groep1");
        header.append(CSV_SEPARATOR);
        header.append("groep2");
        header.append(CSV_SEPARATOR);
        header.append("groep3");
        header.append(CSV_SEPARATOR);
        header.append("prijs");
        header.append(CSV_SEPARATOR);
        header.append("aantal");
        header.append(CSV_SEPARATOR);
        header.append("creatiedatum");
        csvLines.add(header.toString());
        for (Item item : items) {
            StringBuilder oneLine = new StringBuilder();
            oneLine.append(item.getArtikelNr());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(item.getEanCode());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(item.getMerk());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(item.getNaam());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(item.getOmschrijvingNL());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(item.getOmschrijvingEN());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(item.getCategorieNL());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(item.getCategorieEN());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(item.getGroep1());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(item.getGroep2());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(item.getGroep3());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(item.getPrijs());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(item.getAantal());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(item.getCreatieDatum().atZone(ZoneId.systemDefault()).toEpochSecond());
            csvLines.add(oneLine.toString());
        }
        Files.write(Paths.get(path + filename + ".csv"), csvLines, StandardCharsets.UTF_8,
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
    }

}
