package com.realdolmen.hbo5.t1.interfaces;

import com.realdolmen.hbo5.t1.domain.objects.Item;

import java.util.List;

public interface ISearch {
    List<Item> searchByName(String name);
    List<Item> searchByCategoryNL(String category);
    List<Item> searchByID(long id);
    List<Item> searchByBrand(String brand);

    List<Item> searchByEAN(String eanCode);
}
