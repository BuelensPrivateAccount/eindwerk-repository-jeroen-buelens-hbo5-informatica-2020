package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.interfaces.ICSVWriter;
import com.realdolmen.hbo5.t1.services.outputService.OutputService;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class OutputController implements ICSVWriter {

    private OutputService outputService;
    private List<Item> items;

    public OutputController(List<Item> items){
        this.items = items;
    }

    @Override
    public void outputCSV(String path, String filename) throws IOException {
        if(outputService == null){
            outputService = new OutputService(items);
        }

        outputService.outputCSV(path,filename);
    }

    public File fetchFileInCaseOfDupe(String exportFileName, String default_dir) {
        return new File(default_dir + "Export\\", exportFileName + ".csv");
    }

    public void setItems(List<Item> items) {
        if(outputService == null){
            outputService = new OutputService(items);
        }

        this.items = items;
        outputService.setItems(items);
    }


}
