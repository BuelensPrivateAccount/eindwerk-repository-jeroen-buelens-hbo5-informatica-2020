package com.realdolmen.hbo5.t1.interfaces;

import java.io.File;
import java.io.IOException;

public interface ICSVWriter {
    void outputCSV(String path, String filename) throws IOException;

}
