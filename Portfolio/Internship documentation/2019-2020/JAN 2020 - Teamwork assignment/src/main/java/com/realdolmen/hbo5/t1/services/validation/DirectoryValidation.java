package com.realdolmen.hbo5.t1.services.validation;

import javax.swing.filechooser.FileSystemView;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DirectoryValidation {

    public static boolean checkIfInputOutPutDirectoryExists() {
        String defaultDirectory = FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "\\StockProgram\\";
        Path pathVariable = Paths.get(defaultDirectory);

        return (Files.exists(pathVariable)
                && Files.exists(Paths.get(pathVariable + "Import"))
                && Files.exists(Paths.get(pathVariable + "Export"))
                && Files.exists(Paths.get(pathVariable + "Resources")));
    }
}
