package com.realdolmen.hbo5.t1.exceptions;

public class ColumnNameNotOkException extends Exception{
    public ColumnNameNotOkException(String message) {
        super(message);
    }
}
