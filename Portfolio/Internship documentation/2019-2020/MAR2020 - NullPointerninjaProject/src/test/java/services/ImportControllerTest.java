package com.realdolmen.hbo5.t1.services;

import com.realdolmen.hbo5.t1.dao.ItemDao;
import com.realdolmen.hbo5.t1.exceptions.ColumnNameNotOkException;
import com.realdolmen.hbo5.t1.exceptions.FileToBigException;
import com.realdolmen.hbo5.t1.exceptions.FileValidationException;
import com.realdolmen.hbo5.t1.services.ImportController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;

import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.initMocks;

@ExtendWith(MockitoExtension.class)
class ImportControllerTest {
    Path resourceDirectory = Paths.get("src", "test", "resources");
    String defaultDirectory = resourceDirectory.toFile().getAbsolutePath() + "\\";

    @Mock
    ItemDao itemDao = mock(ItemDao.class);
    ImportController reader;

    @BeforeEach
    public void init() {
       reader = new ImportController(itemDao);
    }

    @Test
    public void testTrowAssertNoCSV() {
        Assertions.assertThrows(FileNotFoundException.class, () -> {
            reader.importCSVToDB(defaultDirectory + "expected.sv");
        });
    }

    @Test
    public void testTrowAssertNoHead() {
        Assertions.assertThrows(ColumnNameNotOkException.class, () -> {
            reader.importCSVToDB(defaultDirectory + "nohead.csv");
        });
    }


    @Test
    public void testTrowAssertNumberFormat() {
        Assertions.assertThrows(NumberFormatException.class, () -> {
            reader.importCSVToDB(defaultDirectory + "importNumberFormat.csv");
        });
    }

    @Test
    public void testNoAddedItems() {
        int addeditems = reader.getAddedItems();
        Assertions.assertEquals(0, addeditems);
    }

    @Test
    public void testnormal() {
        Assertions.assertDoesNotThrow(() -> {
            reader.importCSVToDB(defaultDirectory + "expected.csv");
        });
    }


}