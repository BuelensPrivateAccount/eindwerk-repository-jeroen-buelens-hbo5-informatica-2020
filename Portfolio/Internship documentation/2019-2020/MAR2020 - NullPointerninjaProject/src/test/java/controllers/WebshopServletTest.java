package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.dao.ItemDao;
import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.exceptions.NoEntryFoundException;
import com.realdolmen.hbo5.t1.helper.ServletTestHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class WebshopServletTest extends ServletTestHelper {

    @Mock
    ItemDao itemDao;
    @InjectMocks
    WebshopServlet servlet;

    @Test
    public void testDoGetReturnsFirst20Items() throws NoEntryFoundException {
        List<Item> items = new ArrayList<>();
        Item item = new Item();
        item.setNaam("NullPointerNinja");

        for (int i = 0; i < 20; i++) {
            items.add(item);
        }

        when(request.getParameter("page")).thenReturn("1");
        when(itemDao.findAllItems(anyInt(), anyInt())).thenReturn(items);
        when(itemDao.getItemCount()).thenReturn(20L);
        assertDoesNotThrow(() -> {
            servlet.doGet(request, response);
        });

        List<Item> itemsFromDao = (List<Item>) request.getAttribute("items");

        assertArrayEquals(items.toArray(), itemsFromDao.toArray());
        assertEquals(1, request.getAttribute("page"));
        assertEquals(1L, request.getAttribute("totalPages"));
    }

    @Test
    public void testDoGetPageOneWhenParamPageIsNull() throws NoEntryFoundException {
        List<Item> items = new ArrayList<>();
        Item item = new Item();
        item.setNaam("NullPointerNinja");

        for (int i = 0; i < 20; i++) {
            items.add(item);
        }

        when(request.getParameter("page")).thenReturn(null);
        when(itemDao.findAllItems(anyInt(), anyInt())).thenReturn(items);
        when(itemDao.getItemCount()).thenReturn(20L);

        assertDoesNotThrow(() -> {
            servlet.doGet(request, response);
        });

        assertEquals(1, request.getAttribute("page"));
    }
}