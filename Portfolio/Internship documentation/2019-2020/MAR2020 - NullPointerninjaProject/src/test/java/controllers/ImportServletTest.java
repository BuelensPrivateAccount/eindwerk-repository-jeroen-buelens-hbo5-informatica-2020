package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.exceptions.ColumnNameNotOkException;
import com.realdolmen.hbo5.t1.exceptions.FileToBigException;
import com.realdolmen.hbo5.t1.exceptions.FileValidationException;
import com.realdolmen.hbo5.t1.helper.ServletTestHelper;
import com.realdolmen.hbo5.t1.services.ImportController;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.servlet.ServletException;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ImportServletTest extends ServletTestHelper {

    Path resourceDirectory = Paths.get("src", "test", "resources");
    String defaultDirectory = resourceDirectory.toFile().getAbsolutePath();
    @Mock
    ImportController importController;
    @InjectMocks
    ImportServlet servlet;


    @Test
    public void testUploadCSV() throws IOException, ServletException, FileValidationException, FileToBigException, ParseException, ColumnNameNotOkException {
        when(request.getParameter("upload")).thenReturn("yesbaby");
        when(importController.importCSVToDB(any(File.class))).thenReturn(true);
        when(request.getPart(anyString())).thenReturn(new Part() {
            @Override
            public InputStream getInputStream() throws IOException {
                return IOUtils.toInputStream("artikelnr;eancode;merk;naam;omschrijving_nl;omschrijving_e;categorie_nl;categorie_e;groep1;groep2;groep3;prijs;aantal;creatiedatum\n" +
                        "0;123456;NullPointerNinja;NullPointerNinja;NullPointerNinja;NullPointerNinja;NullPointerNinja;NullPointerNinja;NullPointerNinja;NullPointerNinja;NullPointerNinja;1.0;1;20140502", "UTF-8");
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public String getSubmittedFileName() {
                return "test.csv";
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public void write(String fileName) throws IOException {

            }

            @Override
            public void delete() throws IOException {

            }

            @Override
            public String getHeader(String name) {
                return null;
            }

            @Override
            public Collection<String> getHeaders(String name) {
                return null;
            }

            @Override
            public Collection<String> getHeaderNames() {
                return null;
            }
        });
        assertDoesNotThrow(() -> {
            servlet.processRequest(request, response);
        });

    }
}