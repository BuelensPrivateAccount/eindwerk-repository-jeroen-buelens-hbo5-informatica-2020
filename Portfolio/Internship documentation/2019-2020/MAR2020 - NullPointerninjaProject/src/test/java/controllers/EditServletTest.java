package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.dao.ItemDao;
import com.realdolmen.hbo5.t1.helper.ServletTestHelper;
import com.realdolmen.hbo5.t1.services.ImportController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.servlet.ServletException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class EditServletTest extends ServletTestHelper {
    Path resourceDirectory = Paths.get("src", "test", "resources");
    String defaultDirectory = resourceDirectory.toFile().getAbsolutePath();
    @Mock
    ItemDao itemDao;
    @InjectMocks
    EditServlet servlet;

    @Test
    public void testDelete() {
        when(request.getParameter("action")).thenReturn("delete");
        when(request.getParameter("id")).thenReturn("1");
        when(itemDao.removeItem(anyInt())).thenReturn(true);
        assertDoesNotThrow(() -> {
            servlet.doGet(request, response);
        });
        verify(itemDao, times(1)).removeItem(anyInt());
        assertTrue((boolean) request.getAttribute("success"));
    }

    @Test
    public void testEdit() {
        when(request.getParameter("action")).thenReturn("edit");
        when(request.getParameter("id")).thenReturn("1");
        when(request.getParameter("editedArtikelNr")).thenReturn("1");
        when(request.getParameter("editedPrijs")).thenReturn("1.0");
        when(request.getParameter("editedAantal")).thenReturn("1");
        when(request.getParameter("editedCreatieDatum")).thenReturn(LocalDate.now().toString());
        when(request.getParameter("editedGroep1")).thenReturn("1");
        when(request.getParameter("editedGroep2")).thenReturn("1");
        when(request.getParameter("editedGroep3")).thenReturn("1");
        when(request.getParameter("editedEancode")).thenReturn("1");
        when(request.getParameter("editedBrand")).thenReturn("1");
        when(request.getParameter("editedName")).thenReturn("1");
        when(request.getParameter("editedOmschrijvingNl")).thenReturn("1");
        when(request.getParameter("editedOmschrijvingEn")).thenReturn("1");
        when(request.getParameter("editedCategorieNl")).thenReturn("1");
        when(request.getParameter("editedCategorieEn")).thenReturn("1");
        when(itemDao.editItem(any())).thenReturn(true);
        assertDoesNotThrow(() -> {
            servlet.doPost(request, response);
        });
        verify(itemDao, times(1)).editItem(any());
        assertTrue((boolean) request.getAttribute("success"));

    }
}