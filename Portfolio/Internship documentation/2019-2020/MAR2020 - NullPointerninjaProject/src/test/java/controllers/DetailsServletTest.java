package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.dao.ItemDao;
import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.helper.ServletTestHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class DetailsServletTest extends ServletTestHelper{

    @Mock
    ItemDao itemDao;
    @InjectMocks
    DetailsServlet servlet;

    @Test
    public void testInitEdit() {
        when(request.getParameter(anyString())).thenReturn(null);
        when(request.getParameter("action")).thenReturn("edit");
        when(request.getParameter("id")).thenReturn("1");
        Item item = new Item();
        item.setNaam("nullpointerninja");
        when(itemDao.findItemById(1)).thenReturn(item);
        assertDoesNotThrow(()->{
            servlet.doGet(request, response);
        });
        assertEquals(item, request.getAttribute("item"));
    }

    @Test
    public void testDetails(){
        when(request.getParameter(anyString())).thenReturn(null);
        when(request.getParameter("action")).thenReturn("detail");
        when(request.getParameter("id")).thenReturn("1");
        Item item = new Item();
        item.setNaam("nullpointerninja");
        when(itemDao.findItemById(1)).thenReturn(item);
        assertDoesNotThrow(()->{
            servlet.doGet(request, response);
        });
        assertEquals(item, request.getAttribute("item"));
    }
}