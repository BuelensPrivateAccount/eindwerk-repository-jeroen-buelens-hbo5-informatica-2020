package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.helper.ServletTestHelper;
import com.realdolmen.hbo5.t1.services.ImportController;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.servlet.ServletException;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ErrorPageGeneratorTest extends ServletTestHelper {
    Path resourceDirectory = Paths.get("src", "test", "resources");
    String defaultDirectory = resourceDirectory.toFile().getAbsolutePath();
    ErrorPageGenerator servlet;



    @Test
    public void testErrorPage() throws IOException {
        Exception e = new Exception("testmessage");
        servlet = new ErrorPageGenerator();
        assertDoesNotThrow(()->{
            servlet.gotoErrorPage(request,response,e);
        });
        assertEquals(e.getMessage(),request.getAttribute("errormessage"));
    }
}