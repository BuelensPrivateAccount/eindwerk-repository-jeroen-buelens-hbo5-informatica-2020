<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <title class="lp" data-message="startpage_employees">Employee Panel</title>
    <link rel="stylesheet" href="files/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="files/css/stockprogram.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
<c:import url="/WEB-INF/imports/nav.jsp"/>
<a href="${pageContext.request.contextPath}/import" name="import">Importeer hier uw CSV-bestand</a>
<div class="container">
    <form method="get" class="item-search" action="async">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                        <select id="orderSelect" onchange="loadAll()" name="orderOption">
                            <option class="lp" data-message="chooseOption" disabled selected value=""></option>
                            <option class="lp" data-message="sortPriceAsc" value="orderPriceAsc"></option>
                            <option class="lp" data-message="sortPriceDesc" value="orderPriceDesc"></option>
                            <option class="lp" data-message="sortAmountAsc" value="orderAmountAsc"></option>
                            <option class="lp" data-message="sortAmountDesc" value="orderAmountDesc"></option>
                            <option class="lp" data-message="sortNameAsc" value="orderNameAsc"></option>
                            <option class="lp" data-message="sortNameDesc" value="orderNameDesc"></option>
                        </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <input class="form-control lp" type="search" id="q" name="q"
                           lp-custom-attribute="placeholder"
                           data-message="startpage_type_to_search"
                           placeholder=""/>
                </div>
            </div>
            <div class="col-sm-2">
                <button class="btn btn-primary" type="button" onclick="searchResults(1)">
                    <span class="lp" data-message="search"></span>
                </button>
            </div>
        </div>
    </form>
    <c:if test="${requestScope.success eq true}">
        <h1><span class="lp" data-message="succesMessage"></span></h1>
    </c:if>
    <div class="pagination-container noselect">
        <div class="pagination lp" data-message="previous_page" lp-custom-attribute="title" onclick="previousPage()">
            <svg id="i-chevron-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32"
                 fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                <path d="M20 30 L8 16 20 2"/>
            </svg>
        </div>
        <div class="pagination page-number">
            <input type="number" id="page-number" min="1"/>
            &nbsp;<span class="lp" data-message="out_of"></span>&nbsp;
            <span id="totalPages"></span>
        </div>
        <div class="pagination lp" data-message="next_page" lp-custom-attribute="title" onclick="nextPage()">
            <svg id="i-chevron-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32"
                 fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                <path d="M12 30 L24 16 12 2"/>
            </svg>
        </div>
    </div>

    <div class="row" id="results">

    </div>

</div>

<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<script src="files/js/bootstrap.min.js"></script>
<script src="files/js/StockProgram.js"></script>
<script src="files/js/LangParser.js"></script>
<script>
    let selectElement = document.getElementById('orderSelect');
    let resultsContainer = document.getElementById('results');
    let langParser;

    function loadAll(){
        let order = selectElement.options[selectElement.selectedIndex].value;
        loadAllResults(resultsContainer, 1, 20, order, function () {
            langParser.update("lp");
        });
    }

    $(document).ready(function () {
        let order = selectElement.options[selectElement.selectedIndex].value;
        langParser = setupLangParser("files/lang/");
        loadAllResults(resultsContainer, 1, 20, order, function () {
            langParser.update("lp");
        });
    });

    function searchResults(page) {
        let query = document.getElementById('q').value;
        loadSearchResults(resultsContainer, page, query, function () {
            langParser.update("lp");
        });
    }

    document.getElementById('q').addEventListener("keypress", function (e) {
        if (e.key == "Enter") {
            e.preventDefault();
            searchResults(1);
        }
    });

    document.getElementById('page-number').addEventListener("keypress", function(e){
        if(e.key == "Enter"){
            e.preventDefault();
            if(this.value > 0){
                goToPage(this.value);
            }
        }
    });
</script>
</body>
</html>