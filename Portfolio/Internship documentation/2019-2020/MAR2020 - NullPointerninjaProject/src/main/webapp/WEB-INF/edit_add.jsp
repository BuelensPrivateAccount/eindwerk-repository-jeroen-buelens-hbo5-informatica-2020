<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>
        <c:out value="${item.merk}"/> <c:out value="${item.naam}"/> | Bewerken
    </title>
    <link rel="stylesheet" href="files/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="files/css/stockprogram.css"/>
</head>
<body>
<c:import url="/WEB-INF/imports/nav.jsp"/>
<div class="container">
<h1>Edit or add product: <c:out value="${item.naam}"/> </h1>
<form name="editAdd" method="post" action="edit">
    <input hidden type="text" name="action" value="edit"/>
    <c:if test="${not empty item.pk}">
    <input hidden type="text" name="id" value="<c:out value="${item.pk}"/>"  />
    </c:if>
    <div class="form-group">
        <label for="editedArtikelNr">
            <span class="lp" data-message="article_nr"></span>
        </label>
        <input class="lp form-control" data-message="article_nr" lp-custom-attribute="placeholder" value="<c:out value="${item.artikelNr}"/>" type="number" id="editedArtikelNr" name="editedArtikelNr" required>
    </div>
    <div class="form-group">
        <label for="editedEancode">
            <span class="lp" data-message="eancode"></span>
        </label>
        <input class="lp form-control" data-message="eancode" lp-custom-attribute="placeholder" type="number" value="<c:out value="${item.eanCode}"/>" id="editedEancode" name="editedEancode" required>
    </div>
    <div class="form-group">
        <label for="editedBrand">
            <span class="lp" data-message="brand"></span>
        </label>
        <input class="lp form-control" data-message="brand" lp-custom-attribute="placeholder" type="text" value="<c:out value="${item.merk}"/>"  id="editedBrand" name="editedBrand">
    </div>
    <div class="form-group">
        <label for="editedName">
            <span class="lp" data-message="name"></span>
        </label>
        <input class="lp form-control" data-message="name"  lp-custom-attribute="placeholder" type="text" id="editedName" value="<c:out value="${item.naam}"/>" name="editedName" required>
    </div>
    <div class="form-group">
        <label for="editedOmschrijvingNL">
            <span class="lp" data-message="descriptionNL"></span>
        </label>
         <input class="lp form-control" data-message="descriptionNL" lp-custom-attribute="placeholder" type="text" id="editedOmschrijvingNL" value="<c:out value="${item.omschrijvingNL}"/>" name="editedOmschrijvingNl">
    </div>
    <div class="form-group">
        <label for="editedOmschrijvingEN">
            <span class="lp" data-message="descriptionEN"></span>
        </label>
        <input class="lp form-control" data-message="descriptionEN" lp-custom-attribute="placeholder" type="text" id="editedOmschrijvingEN" value="<c:out value="${item.omschrijvingEN}"/>"name="editedOmschrijvingEn">
    </div>
    <div class="form-group">
        <label for="editedCategorieNL">
            <span class="lp" data-message="categoryNL"></span>
        </label>
        <input class="lp form-control" data-message="categoryNL" lp-custom-attribute="placeholder" type="text" id="editedCategorieNL" value="<c:out value="${item.categorieNL}"/>"name="editedCategorieNl">
    </div>
    <div class="form-group">
        <label for="editedCategorieEN">
            <span class="lp" data-message="CategoryEN"></span>
        </label>
        <input class="lp form-control" data-message="CategoryEN" lp-custom-attribute="placeholder" type="text" id="editedCategorieEN" value="<c:out value="${item.categorieEN}"/>" name="editedCategorieEn">
    </div>
    <div class="form-group">
        <label for="editedGroep1">
            <span class="lp" data-message="group"></span> 1
        </label>
        <input class="lp form-control" data-message="group" lp-custom-attribute="placeholder" type="number" id="editedGroep1" value="<c:out value="${item.groep1}"/>" name="editedGroep1">
    </div>
    <div class="form-group">
        <label for="editedGroep2">
            <span class="lp" data-message="group"></span> 2
        </label>
        <input class="lp form-control" data-message="group" lp-custom-attribute="placeholder" type="number" id="editedGroep2" value="<c:out value="${item.groep2}"/>" name="editedGroep2">
    </div>
    <div class="form-group">
        <label for="editedGroep3">
            <span class="lp" data-message="group"></span> 3
        </label>
        <input class="lp form-control" data-message="group" lp-custom-attribute="placeholder" type="number" id="editedGroep3" value="<c:out value="${item.groep3}"/>" name="editedGroep3">
    </div>
    <div class="form-group">
        <label for="editedPrijs">
            <span class="lp" data-message="price"></span>
        </label>
        <input class="lp form-control" data-message="price" lp-custom-attribute="placeholder" type="text" id="editedPrijs" value="<c:out value="${item.prijs}"/>" name="editedPrijs" required>
    </div>
    <div class="form-group">
        <label for="editedAantal">
            <span class="lp" data-message="amount"></span>
        </label>
        <input class="lp form-control" data-message="amount" lp-custom-attribute="placeholder" type="number" id="editedAantal" value="<c:out value="${item.aantal}"/>" name="editedAantal" required>
    </div>
    <div class="form-group">
        <label for="editedDate">
            <span class="lp" data-message="creation_date"></span>
        </label>
        <input class="lp form-control" data-message="creation_date" lp-custom-attribute="placeholder" type="date" id="editedDate" value="<c:out value="${item.creatieDatum}"/>" name="editedCreatieDatum">
    </div>
    <button class="btn-primary" onclick="" type="submit">
    Save
    </button>
</form>
</div>
<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
<script src="files/js/bootstrap.min.js"></script>
<script src="files/js/StockProgram.js"></script>
<script src="files/js/LangParser.js"></script>
<script>
    $(document).ready(function () {
        setupLangParser("files/lang/");
    });
</script>
</body>
</html>
