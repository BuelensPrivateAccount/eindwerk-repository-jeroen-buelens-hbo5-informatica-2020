<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>
        <c:out value="${item.merk}"/> <c:out value="${item.naam}"/> | Details
    </title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="files/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="files/css/stockprogram.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
<c:import url="/WEB-INF/imports/nav.jsp"/>


<header>
    <div class="container">
        <h1>
            <c:out value="${item.merk}"/> <c:out value="${item.naam}"/>
        </h1>
        <small>
            <span class="lp" data-message="article_nr"></span>: <c:out value="${item.artikelNr}"/> |
            <span class="lp" data-message="eancode"></span>: <c:out value="${item.eanCode}"/>
        </small>
    </div>
</header>
<div class="container">
    <div class="row details">
        <div class="col-md-6 col-sm-12">
            <p class="price">
                € <c:out value="${fn:replace(item.prijs,'.',',')}"/>
            </p>
            <p class="description">
                <c:out value="${item.omschrijvingNL}"/>
            </p>
        </div>
        <div class="col-md-6 col-sm-12">
            <p class="stock">
                <c:choose>
                    <c:when test="${item.aantal lt 0}">
                        <span class="lp" data-message="never_available"></span>
                    </c:when>
                    <c:otherwise>
                        <c:out value="${item.aantal}"/> <span class="lp" data-message="remaining"></span>
                    </c:otherwise>
                </c:choose>
            </p>
            <p class="category">
                <span class="lp" data-message="category"></span>: <c:out value="${item.categorieNL}"/>
            </p>
            <p>
                <b><span class="lp" data-message="group"></span> 1: </b> <c:out value="${item.groep1}"/><br/>
                <b><span class="lp" data-message="group"></span> 2: </b> <c:out value="${item.groep2}"/><br/>
                <b><span class="lp" data-message="group"></span> 3: </b> <c:out value="${item.groep3}"/>
            </p>

            <p>
                <b>
                    <span class="lp" data-message="created_on"></span>
                </b>
                <c:out value="${item.creatieDatum}"/>
            </p>
            <div class="links">
                <a class="item-button" href="details?action=edit&id=<c:out value="${item.pk}"/>">
                    <svg id="i-edit" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="15" height="15"
                         fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round"
                         stroke-width="2">
                        <path d="M30 7 L25 2 5 22 3 29 10 27 Z M21 6 L26 11 Z M5 22 L10 27 Z"/>
                    </svg>
                    <span class="lp" data-message="edit"></span>
                </a>
            </div>

        </div>
    </div>

</div>
<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<script src="files/js/bootstrap.min.js"></script>
<script src="files/js/StockProgram.js"></script>
<script src="files/js/LangParser.js"></script>
<script>
    let langParser ;

    $(document).ready(function () {
        langParser = setupLangParser("files/lang/");
    });
</script>
</body>
</html>
