<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty items}">
    <c:forEach var="item" items="${items}">
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="item card">
                <div class="card-body">
                    <h5 class="card-title">
                        <c:out value="${item.naam}"/>
                    </h5>
                    <h6 class="card-subtitle mb-2 text-muted">
                        <c:out value="${item.merk}"/> - € <c:out value="${fn:replace(item.prijs,'.',',')}"/>
                    </h6>
                    <p class="card-text">
                        <c:out value="${item.omschrijvingNL}"/>
                    </p>

                    <a class="item-button" href="details?action=edit&id=<c:out value="${item.pk}"/>">
                        <svg id="i-edit" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="15" height="15" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                            <path d="M30 7 L25 2 5 22 3 29 10 27 Z M21 6 L26 11 Z M5 22 L10 27 Z" />
                        </svg>
                        <span class="lp" data-message="edit"></span>
                    </a>

                    <a class="item-button" href="details?action=detail&id=<c:out value="${item.pk}"/>">
                        <svg id="i-eye" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="15" height="15" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                            <circle cx="17" cy="15" r="1" />
                            <circle cx="16" cy="16" r="6" />
                            <path d="M2 16 C2 16 7 6 16 6 25 6 30 16 30 16 30 16 25 26 16 26 7 26 2 16 2 16 Z" />
                        </svg>
                        <span class="lp" data-message="details"></span>
                    </a>
                    <div onclick="confirmDelete(<c:out value="${item.pk}"/>)">
                        <svg id="i-close" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32"
                             fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round"
                             stroke-width="2">
                            <path d="M2 30 L30 2 M30 30 L2 2"/>
                        </svg>
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </c:forEach>
    <div id="results-metadata" class="lp" data-message="warning_delete" lp-custom-attribute="deletion-message"
         data-current-page="<c:out value="${page}"/>"
         data-method-used="<c:out value="${method}"/>"
         data-total-pages="<c:out value="${totalPages}"/>"></div>
</c:if>

<script>

</script>