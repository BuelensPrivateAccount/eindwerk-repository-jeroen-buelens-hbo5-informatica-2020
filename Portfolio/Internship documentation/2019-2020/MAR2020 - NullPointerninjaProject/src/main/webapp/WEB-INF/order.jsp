<%--
  Created by IntelliJ IDEA.
  User: MEOBL79
  Date: 19/03/2020
  Time: 10:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><span class="lp" data-message="order">Order</span> | Webshop</title>
    <link rel="stylesheet" href="files/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="files/css/stockprogram.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
<c:import url="/WEB-INF/imports/nav_webshop.jsp"/>

<div class="container">
<h1 class="lp" data-message="order"></h1>
</div>

<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<script src="files/js/bootstrap.min.js"></script>
<script src="files/js/StockProgram.js"></script>
<script src="files/js/LangParser.js"></script>

<script>
    let langParser = setupLangParser("files/lang/");
</script>
</body>
</html>
