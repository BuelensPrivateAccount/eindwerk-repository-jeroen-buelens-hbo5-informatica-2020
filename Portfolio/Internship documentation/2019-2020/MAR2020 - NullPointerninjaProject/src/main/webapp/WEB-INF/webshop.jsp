<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Webshop</title>
    <link rel="stylesheet" href="files/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="files/css/stockprogram.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
<c:import url="/WEB-INF/imports/nav_webshop.jsp"/>

<div class="container">
    <div class="pagination-container noselect">
        <c:if test="${page gt 1}">
            <a href="webshop?page=<c:out value="${page - 1}"/>">
                <div class="pagination lp" data-message="previous_page" lp-custom-attribute="title">
                    <svg id="i-chevron-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32"
                         height="32"
                         fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round"
                         stroke-width="2">
                        <path d="M20 30 L8 16 20 2"/>
                    </svg>
                </div>
            </a>
        </c:if>

        <div class="pagination page-number">
            <input type="number" id="page-number" min="1" value="<c:out value="${page}"/>"/>
            &nbsp;<span class="lp" data-message="out_of"></span>&nbsp;
            <c:out value="${totalPages}"/>
        </div>

        <c:if test="${page lt totalPages+1}">
            <a href="webshop?page=<c:out value="${page + 1}"/>">
                <div class="pagination lp" data-message="next_page" lp-custom-attribute="title">
                    <svg id="i-chevron-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32"
                         height="32"
                         fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round"
                         stroke-width="2">
                        <path d="M12 30 L24 16 12 2"/>
                    </svg>
                </div>
            </a>
        </c:if>

    </div>
    <c:forEach items="${items}" var="item">
        <div class="row products">
            <div class="col-3 image">

            </div>
            <div class="col-6">
                <h3><c:out value="${item.merk} ${item.naam}"/></h3>
                <p><c:out value="${item.omschrijvingNL}"/></p>
                <small><c:out value="${item.categorieNL}"/></small>
            </div>
            <div class="col-3">
                <p class="price">
                    € <c:out value="${fn:replace(item.prijs,'.',',')}"/>
                </p>
                <a style="display:none;" href="order?id=<c:out value="${item.pk}"/>" class="btn btn-primary">
                    <span class="lp" data-message="add_to_order"></span>
                </a>
            </div>
        </div>
    </c:forEach>
</div>

<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<script src="files/js/bootstrap.min.js"></script>
<script src="files/js/StockProgram.js"></script>
<script src="files/js/LangParser.js"></script>

<script>
    let langParser = setupLangParser("files/lang/");

    document.getElementById('page-number').addEventListener('keyup', function(e){
       if(e.key == "Enter"){
           window.location = "webshop?page=" + this.value;
       }
    });

</script>
</body>
</html>
