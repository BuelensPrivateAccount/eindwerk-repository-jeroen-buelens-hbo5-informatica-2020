<%--
  Created by IntelliJ IDEA.
  User: JBLBL75
  Date: 09/03/2020
  Time: 09:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>StockProgram</title>
    <link rel="stylesheet" href="files/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="files/css/stockprogram.css"/>
</head>
<body class="full-flex">

<div class="container">
    <div class="row center">
        <div class="col option">
            <a href="webshop">
                <svg id="i-tag" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="64" height="64"
                     fill="none"
                     stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                    <circle cx="24" cy="8" r="2"/>
                    <path d="M2 18 L18 2 30 2 30 14 14 30 Z"/>
                </svg>
                <h2>
                    <span class="lp" data-message="portal_clients"></span>
                </h2>
            </a>
        </div>
        <div class="col option">
            <a href="employee">
                <svg id="i-work" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="64" height="64"
                     fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                    <path d="M30 8 L2 8 2 26 30 26 Z M20 8 C20 8 20 4 16 4 12 4 12 8 12 8 M8 26 L8 8 M24 26 L24 8"/>
                </svg>
                <h2>
                    <span class="lp" data-message="portal_employees"></span>
                </h2>
            </a>
        </div>
    </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<script src="files/js/bootstrap.min.js"></script>
<script src="files/js/LangParser.js"></script>
<script src="files/js/StockProgram.js"></script>
<script>
    $(document).ready(function () {
        setupLangParser("files/lang/");
    });
</script>
</body>
</html>
