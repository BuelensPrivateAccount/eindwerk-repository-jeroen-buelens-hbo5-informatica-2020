package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.dao.ItemDao;
import com.realdolmen.hbo5.t1.domain.objects.Item;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DetailsServlet", urlPatterns = "/details")
public class DetailsServlet extends HttpServlet {

    private static ItemDao itemDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            if (req.getParameter("action") != null) {
                String action = req.getParameter("action");
                RequestDispatcher rd = null;
                switch (action) {
                    case "edit":
                        initEdit(req, resp);
                        rd = req.getRequestDispatcher("/WEB-INF/edit_add.jsp");
                        break;
                    case "add":
                        rd = req.getRequestDispatcher("/WEB-INF/edit_add.jsp");
                        break;
                    case "detail":
                        if (req.getParameter("id") != null) {
                            try {
                                int id = Integer.parseInt(req.getParameter("id"));
                                Item item = itemDAO.findItemById(id);
                                req.setAttribute("item", item);
                                rd = req.getRequestDispatcher("/WEB-INF/employee_details.jsp");
                            } catch (NumberFormatException e) {
                                throw new Exception("Invalid ID. This ID is not a number.");
                            }
                        } else {
                            throw new Exception("No ID was provided.");
                        }
                }
                if (rd != null) {
                    rd.forward(req, resp);
                } else {
                    throw new Exception("you tried to do a method that has not been implemented yet.");
                }
            }
        } catch (Exception e) {
            ErrorPageGenerator errorPageGenerator = new ErrorPageGenerator();
            errorPageGenerator.gotoErrorPage(req, resp, e);
        }
    }

    private RequestDispatcher initEdit(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        try {
            int id = Integer.parseInt(req.getParameter("id"));
            Item item = itemDAO.findItemById(id);
            req.setAttribute("item", item);
           return req.getRequestDispatcher("/WEB-INF/employee_details.jsp");
        } catch (NumberFormatException e) {
            throw new Exception("Invalid ID. This ID is not a number.");
        }
    }

    @Override
    public void init() throws ServletException {
        itemDAO = new ItemDao();
    }
}
