package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.dao.ItemDao;
import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.exceptions.NoEntryFoundException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "WebshopServlet", urlPatterns = "/webshop")
public class WebshopServlet extends HttpServlet {

    private static ItemDao itemDao;
    private static ErrorPageGenerator epg;

    @Override
    public void init() throws ServletException {
        itemDao = new ItemDao();
        try {
            epg = new ErrorPageGenerator();
        } catch (IOException e) {

        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int page = 0;
        if (req.getParameter("page") != null) {
            try {
                page = Integer.parseInt(req.getParameter("page"));
            } catch (NumberFormatException e) {
                epg.gotoErrorPage(req, resp, new Exception("Something went wrong when trying to load our products."));
            }
        } else {
            page = 1;
        }

        try {
            List<Item> items = itemDao.findAllItems(page, 20);
            long totalItems = itemDao.getItemCount();
            long rounded = (totalItems % 20 == 0) ? 0 : 1;
            long totalPages = (totalItems / 20) + rounded;

            req.setAttribute("totalPages", totalPages);
            req.setAttribute("page", page);
            req.setAttribute("items", items);
        } catch (NoEntryFoundException e) {
            epg.gotoErrorPage(req, resp, new Exception("We could not find any products matching that."));
        }

        req.getRequestDispatcher("/WEB-INF/webshop.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
