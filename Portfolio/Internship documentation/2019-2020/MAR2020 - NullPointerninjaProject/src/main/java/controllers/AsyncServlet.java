package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.dao.ItemDao;
import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.exceptions.NoEntryFoundException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "AsyncServlet", urlPatterns = "/async")
@MultipartConfig
public class AsyncServlet extends HttpServlet {
   private static ItemDao itemDao;

    @Override
    public void init() {
        itemDao = new ItemDao();
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("method") != null) {
            switch (req.getParameter("method")) {
                case "showAll":
                    getOrderedItemsAndForward(req, resp);
                    break;
                case "q":
                    searchItemsByArticleIDOrName(req, resp);
                    break;
            }
        }
    }

    private void getOrderedItemsAndForward(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{
        if(req.getParameter("order") != null){
            switch(req.getParameter("order")){
                case "orderPriceAsc":
                    orderItemsPriceAsc(req, resp);
                    break;
                case "orderPriceDesc":
                    orderItemsPriceDesc(req,resp);
                    break;
                case "orderAmountAsc":
                    orderItemsAmountAsc(req,resp);
                     break;
                case "orderAmountDesc":
                    orderItemsAmountDesc(req,resp);
                    break;
                case "orderNameAsc":
                    orderItemsNameAsc(req,resp);
                    break;
                case "orderNameDesc":
                    orderItemsNameDesc(req,resp);
                    break;
                default:
                    getAllItemsAndForward(req, resp);
                    break;
            }
        } else {
            getAllItemsAndForward(req, resp);
        }
    }

    private void orderItemsNameAsc(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("page") != null && req.getParameter("amount") != null) {

            try {
                int page = Integer.parseInt(req.getParameter("page"));
                int amount = Integer.parseInt(req.getParameter("amount"));

                long totalResults = itemDao.getItemCount();
                long add = (totalResults % amount == 0 ? 0 : 1);
                long totalPages = totalResults / amount + add;

                if (amount >= 2 && amount <= 50 && page >= 1) {
                    List<Item> items = itemDao.allItemsOrderByNameAsc(page, amount);
                    req.setAttribute("method", "all");
                    req.setAttribute("totalPages", totalPages);

                    forwardRequest(req, resp, items, page);
                } else {
                    this.getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(req, resp);
                }
            } catch (NumberFormatException | ServletException | IOException | NoEntryFoundException e) {
                ErrorPageGenerator errorPageGenerator = new ErrorPageGenerator();
                errorPageGenerator.gotoErrorPage(req, resp, e);
            }
        }
    }
    private void orderItemsNameDesc(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("page") != null && req.getParameter("amount") != null) {

            try {
                int page = Integer.parseInt(req.getParameter("page"));
                int amount = Integer.parseInt(req.getParameter("amount"));

                long totalResults = itemDao.getItemCount();
                long add = (totalResults % amount == 0 ? 0 : 1);
                long totalPages = totalResults / amount + add;

                if (amount >= 2 && amount <= 50 && page >= 1) {
                    List<Item> items = itemDao.allItemsOrderByNameDesc(page, amount);
                    req.setAttribute("method", "all");
                    req.setAttribute("totalPages", totalPages);

                    forwardRequest(req, resp, items, page);
                } else {
                    this.getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(req, resp);
                }
            } catch (NumberFormatException | ServletException | IOException | NoEntryFoundException e) {
                ErrorPageGenerator errorPageGenerator = new ErrorPageGenerator();
                errorPageGenerator.gotoErrorPage(req, resp, e);
            }
        }

    }

    private void orderItemsAmountAsc(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        if (req.getParameter("page") != null && req.getParameter("amount") != null) {

            try {
                int page = Integer.parseInt(req.getParameter("page"));
                int amount = Integer.parseInt(req.getParameter("amount"));

                long totalResults = itemDao.getItemCount();
                long add = (totalResults % amount == 0 ? 0 : 1);
                long totalPages = totalResults / amount + add;

                if (amount >= 2 && amount <= 50 && page >= 1) {
                    List<Item> items = itemDao.allItemsOrderByAmountAsc(page, amount);
                    req.setAttribute("method", "all");
                    req.setAttribute("totalPages", totalPages);

                    forwardRequest(req, resp, items, page);
                } else {
                    this.getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(req, resp);
                }
            } catch (NumberFormatException | ServletException | IOException | NoEntryFoundException e) {
                ErrorPageGenerator errorPageGenerator = new ErrorPageGenerator();
                errorPageGenerator.gotoErrorPage(req, resp, e);
            }
        }
    }


    private void orderItemsAmountDesc(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("page") != null && req.getParameter("amount") != null) {

            try {
                int page = Integer.parseInt(req.getParameter("page"));
                int amount = Integer.parseInt(req.getParameter("amount"));

                long totalResults = itemDao.getItemCount();
                long add = (totalResults % amount == 0 ? 0 : 1);
                long totalPages = totalResults / amount + add;

                if (amount >= 2 && amount <= 50 && page >= 1) {
                    List<Item> items = itemDao.allItemsOrderByAmountDesc(page, amount);
                    req.setAttribute("method", "all");
                    req.setAttribute("totalPages", totalPages);

                    forwardRequest(req, resp, items, page);
                } else {
                    this.getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(req, resp);
                }
            } catch (NumberFormatException | ServletException | IOException | NoEntryFoundException e) {
                ErrorPageGenerator errorPageGenerator = new ErrorPageGenerator();
                errorPageGenerator.gotoErrorPage(req, resp, e);
            }
        }
    }


    private void orderItemsPriceDesc(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        if (req.getParameter("page") != null && req.getParameter("amount") != null) {

            try {
                int page = Integer.parseInt(req.getParameter("page"));
                int amount = Integer.parseInt(req.getParameter("amount"));

                long totalResults = itemDao.getItemCount();
                long add = (totalResults % amount == 0 ? 0 : 1);
                long totalPages = totalResults / amount + add;

                if (amount >= 2 && amount <= 50 && page >= 1) {
                    List<Item> items = itemDao.allItemsOrderByPriceDesc(page, amount);
                    req.setAttribute("method", "all");
                    req.setAttribute("totalPages", totalPages);

                    forwardRequest(req, resp, items, page);
                } else {
                    this.getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(req, resp);
                }
            } catch (NumberFormatException | ServletException | IOException | NoEntryFoundException e) {
                ErrorPageGenerator errorPageGenerator = new ErrorPageGenerator();
                errorPageGenerator.gotoErrorPage(req, resp, e);
            }
        }
    }



    private void orderItemsPriceAsc(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        if (req.getParameter("page") != null && req.getParameter("amount") != null) {

            try {
                int page = Integer.parseInt(req.getParameter("page"));
                int amount = Integer.parseInt(req.getParameter("amount"));

                long totalResults = itemDao.getItemCount();
                long add = (totalResults % amount == 0 ? 0 : 1);
                long totalPages = totalResults / amount + add;

                if (amount >= 2 && amount <= 50 && page >= 1) {
                    List<Item> items = itemDao.allItemsOrderByPriceAsc(page, amount);
                    req.setAttribute("method", "all");
                    req.setAttribute("totalPages", totalPages);

                    forwardRequest(req, resp, items, page);
                } else {
                    this.getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(req, resp);
                }
            } catch (NumberFormatException | ServletException | IOException | NoEntryFoundException e) {
                ErrorPageGenerator errorPageGenerator = new ErrorPageGenerator();
                errorPageGenerator.gotoErrorPage(req, resp, e);
            }
        }
    }


    private void searchItemsByArticleIDOrName(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            if(req.getParameter("page") != null){
                Integer page = Integer.parseInt(req.getParameter("page"));
                List<Item> itemResults;

                if (req.getParameter("q") != null && req.getParameter("q").matches("[0-9]+")) {
                    itemResults = itemDao.findItemByArtikelNR(Long.parseLong(req.getParameter("q")), page);
                    req.setAttribute("totalPages", 1);
                } else {
                    itemResults = itemDao.findItemsByName(req.getParameter("q"), page);
                    long totalResults = itemDao.getItemCountFindByName(req.getParameter("q"));
                    long add = (totalResults % 25 == 0 ? 0 : 1);
                    long totalPages = totalResults / 25 + add;
                    req.setAttribute("totalPages", totalPages);
                }

                req.setAttribute("query", req.getParameter("q"));
                req.setAttribute("method", "search");

                forwardRequest(req, resp, itemResults, page);
            }
        } catch (NoEntryFoundException e) {
            this.getServletContext().getRequestDispatcher("/WEB-INF/async/error_no_entry_ajax.jsp").forward(req, resp);
        }
    }

     private void getAllItemsAndForward(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("page") != null && req.getParameter("amount") != null) {

            try {
                int page = Integer.parseInt(req.getParameter("page"));
                int amount = Integer.parseInt(req.getParameter("amount"));

                long totalResults = itemDao.getItemCount();
                long add = (totalResults % amount == 0 ? 0 : 1);
                long totalPages = totalResults / amount + add;

                if (amount >= 2 && amount <= 50 && page >= 1) {
                    List<Item> items = itemDao.findAllItems(page, amount);
                    req.setAttribute("method", "all");
                    req.setAttribute("totalPages", totalPages);

                    forwardRequest(req, resp, items, page);
                } else {
                    this.getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(req, resp);
                }
            } catch (NumberFormatException | ServletException | IOException e) {
                ErrorPageGenerator errorPageGenerator = new ErrorPageGenerator();
                errorPageGenerator.gotoErrorPage(req,resp,e);
            } catch (NoEntryFoundException e){
                this.getServletContext().getRequestDispatcher("/WEB-INF/async/error_no_entry_ajax.jsp").forward(req, resp);
            }
        }
    }

    private void forwardRequest(HttpServletRequest req, HttpServletResponse resp, List<Item> itemResults, Integer page) throws ServletException, IOException {
        req.setAttribute("items", itemResults);
        req.setAttribute("page", page);
        try {
            req.getRequestDispatcher("/WEB-INF/async/resultsByPage.jsp").forward(req, resp);
        } catch (Exception e) {
            ErrorPageGenerator errorPageGenerator = new ErrorPageGenerator();
            errorPageGenerator.gotoErrorPage(req, resp, e);
        }
    }
}
