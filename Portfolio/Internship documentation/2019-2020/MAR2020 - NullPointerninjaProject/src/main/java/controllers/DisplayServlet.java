package com.realdolmen.hbo5.t1.controllers;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DisplayServlet", urlPatterns = "/employee")
public class DisplayServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    public void init() throws ServletException {
        super.init();
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        loadDefaultEmployeePage(request, response);
    }

    private void loadDefaultEmployeePage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
           req.getRequestDispatcher("/WEB-INF/employee_index.jsp").forward(req, resp);
        } catch (IOException e) {
           ErrorPageGenerator errorPageGenerator = new ErrorPageGenerator();
           errorPageGenerator.gotoErrorPage(req,resp,e);
        }
    }
}
