package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.dao.ItemDao;
import com.realdolmen.hbo5.t1.domain.objects.Item;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet(name = "EditServlet", urlPatterns = "/edit")
public class EditServlet extends HttpServlet {
    private static ItemDao itemDao;

    @Override
    public void init() {
        itemDao = new ItemDao();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     if(request.getParameter("action") != null) {
         String action = request.getParameter("action");
         if (action.equals("delete")) {
             int id = Integer.parseInt(request.getParameter("id"));
             try{
                 boolean success = deleteProductByID(request, response, id);
                 request.setAttribute("success", success);
             } catch (Exception e){

             }
             RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/employee_index.jsp");
             rd.forward(request, response);
         }
     }
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        try {
            if (req.getParameter("action") != null) {
                String action = req.getParameter("action");
                RequestDispatcher rd = null;
                switch (action) {
                    case "edit":
                        if (req.getParameter("id") != null) {
                            int id = Integer.parseInt(req.getParameter("id"));
                            editProductById(id, req, resp);
                        } else {
                            addproduct(req, resp);
                        }
                        rd = req.getRequestDispatcher("/WEB-INF/employee_index.jsp");
                        break;
                }
                if (rd != null) {
                    rd.forward(req, resp);
                } else {
                    throw new Exception("you tried to do a method that has not been implemented yet.");
                }
            }
        } catch (Exception e) {
            ErrorPageGenerator errorPageGenerator = new ErrorPageGenerator();
            errorPageGenerator.gotoErrorPage(req, resp, e);
        }

    }

    private void addproduct(HttpServletRequest req, HttpServletResponse resp) {
        long articleNr = Long.parseLong(req.getParameter("editedArtikelNr"));
        String eancode = req.getParameter("editedEancode");
        String brand = req.getParameter("editedBrand");
        String naam = req.getParameter("editedName");
        String omschrijvingNL = req.getParameter("editedOmschrijvingNl");
        String omschrijvingEN = req.getParameter("editedOmschrijvingEn");
        String categoryNL = req.getParameter("editedCategorieNl");
        String categoryEN = req.getParameter("editedCategorieEn");
        String group1 = req.getParameter("editedGroep1");
        String group2 = req.getParameter("editedGroep2");
        String group3 = req.getParameter("editedGroep3");
        double prijs = Double.parseDouble(req.getParameter("editedPrijs"));
        int aantal = Integer.parseInt(req.getParameter("editedAantal"));
        LocalDate creatieDatum = LocalDate.parse(req.getParameter("editedCreatieDatum"));
        Item newItem = new Item(articleNr, eancode, brand, naam, omschrijvingNL, omschrijvingEN, categoryNL, categoryEN, group1, group2, group3, prijs, aantal);
        newItem.setCreatieDatum(creatieDatum);
        boolean success = itemDao.saveToDB(newItem);
        req.setAttribute("success", success);
    }

    private boolean deleteProductByID(HttpServletRequest req, HttpServletResponse resp, int id) {
        return itemDao.removeItem(id);
    }

    private void editProductById(int id, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        long articleNr = Long.parseLong(req.getParameter("editedArtikelNr"));
        String eancode = req.getParameter("editedEancode");
        String brand = req.getParameter("editedBrand");
        String naam = req.getParameter("editedName");
        String omschrijvingNL = req.getParameter("editedOmschrijvingNl");
        String omschrijvingEN = req.getParameter("editedOmschrijvingEn");
        String categoryNL = req.getParameter("editedCategorieNl");
        String categoryEN = req.getParameter("editedCategorieEn");
        String group1 = req.getParameter("editedGroep1");
        String group2 = req.getParameter("editedGroep2");
        String group3 = req.getParameter("editedGroep3");
        double prijs = Double.parseDouble(req.getParameter("editedPrijs"));
        int aantal = Integer.parseInt(req.getParameter("editedAantal"));
        Item updateditem = new Item(id, articleNr, eancode, brand, naam, omschrijvingNL, omschrijvingEN, categoryNL, categoryEN, group1, group2, group3, prijs, aantal);
        if (req.getParameter("editedCreatieDatum") != null && !req.getParameter("editedCreatieDatum").isEmpty()) {
            LocalDate creatieDatum = LocalDate.parse(req.getParameter("editedCreatieDatum"));
            updateditem.setCreatieDatum(creatieDatum);
        }
        boolean success = itemDao.editItem(updateditem);
        req.setAttribute("success", success);


    }

}

