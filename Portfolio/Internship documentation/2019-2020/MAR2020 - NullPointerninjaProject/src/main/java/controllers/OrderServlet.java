package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.dao.ItemDao;
import com.realdolmen.hbo5.t1.domain.objects.Item;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "OrderServlet", urlPatterns = "/order")
public class OrderServlet extends HttpServlet {

    private static ItemDao itemDao;
    private static ErrorPageGenerator epg;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("id") != null){
            try{
                int id = Integer.parseInt(req.getParameter("id"));
                Item item = itemDao.findItemById(id);
                req.setAttribute("item", item);
                req.getRequestDispatcher("/WEB-INF/order.jsp").forward(req, resp);
            } catch (NumberFormatException e){
                epg.gotoErrorPage(req, resp, new Exception("Oops, something went wrong. :/"));
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    @Override
    public void init() throws ServletException {
        itemDao = new ItemDao();
        try {
            epg = new ErrorPageGenerator();
        } catch (IOException e) {

        }
    }
}
