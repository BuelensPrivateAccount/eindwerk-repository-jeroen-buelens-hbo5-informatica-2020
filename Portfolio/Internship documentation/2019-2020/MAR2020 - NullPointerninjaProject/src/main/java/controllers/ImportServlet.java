package com.realdolmen.hbo5.t1.controllers;

import com.realdolmen.hbo5.t1.services.ImportController;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

@WebServlet(name = "ImportServlet", urlPatterns = "/import")
@MultipartConfig
public class ImportServlet extends HttpServlet {
    private static ImportController importController;

    @Override
    public void init() throws ServletException {
        importController = new ImportController();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       req.getRequestDispatcher("/WEB-INF/import.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("upload") != null) {
            uploadCSVFile(request, response);
        }
    }

    private void uploadCSVFile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
        InputStream fileContent = filePart.getInputStream();
        File tempFile = File.createTempFile(fileName, ".csv");
        tempFile.deleteOnExit();
        FileOutputStream out = new FileOutputStream(tempFile);
        IOUtils.copy(fileContent, out);
        try {
           boolean success =  importController.importCSVToDB(tempFile);
            if(success){
                response.sendRedirect("/project1_war_exploded/employee");
            } else{
                throw new Exception("there was a issue with saving the contents of "+filePart.getSubmittedFileName()+" to the database.");
            }
        } catch (Exception e) {
            ErrorPageGenerator errorPageGenerator = new ErrorPageGenerator();
            errorPageGenerator.gotoErrorPage(request,response,e);
        }

    }
}
