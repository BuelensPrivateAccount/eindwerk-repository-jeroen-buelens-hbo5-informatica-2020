package com.realdolmen.hbo5.t1.exceptions;

public class NullPageException extends Exception {
    public NullPageException(String message){
        super(message);
    }
}
