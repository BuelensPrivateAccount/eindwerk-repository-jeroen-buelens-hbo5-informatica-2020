package com.realdolmen.hbo5.t1.exceptions;

public class InvalidPageException extends Exception {
    public InvalidPageException(String message){
        super(message);
    }
}
