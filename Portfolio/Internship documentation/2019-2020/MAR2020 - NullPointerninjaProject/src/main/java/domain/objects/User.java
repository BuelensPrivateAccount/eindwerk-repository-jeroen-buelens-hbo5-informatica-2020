package com.realdolmen.hbo5.t1.domain.objects;

import com.realdolmen.hbo5.t1.domain.objects.database.aid.classes.AbstractDatabaseObject;
import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.time.LocalDate;

@Entity
@Table(name = "user", schema = "stockprogram")
@NamedQueries({
        @NamedQuery(name = "User.findall", query = "SELECT  i  from User i"),
        @NamedQuery(name = "User.findbyid", query = "select i from  User i where i.pk = :id"),
        @NamedQuery(name = "User.findbyUserName", query = "SELECT i from User i where i.username like :username")
})
public class User extends AbstractDatabaseObject {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "username", unique = true)
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "password")
    @Access(AccessType.FIELD)
    private String password;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "surname")
    private String surName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "firstname")
    private String firstName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "birthdate")
    @Access(AccessType.FIELD)
    private java.sql.Date birthDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Column(name = "street")
    private String street;
    @Basic(optional = false)
    @NotNull
    @Column(name = "postalcode")
    private int postalcode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "housenumber")
    private String housenumber;


    public User() {
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = Date.valueOf(birthDate);
    }

    public void setBirthDate(String date) {
        setBirthDate(LocalDate.parse(date));
    }

    public LocalDate getBirthdate() {
        if (birthDate != null) {
            return birthDate.toLocalDate();
        } else {
            return null;
        }
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public void setHashedPassword(String hashedPassword) {
        this.password = hashedPassword;
    }

    public String getPassword() {
        return password;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public boolean checkPassword(String password) {
        return BCrypt.checkpw(password, this.password);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
