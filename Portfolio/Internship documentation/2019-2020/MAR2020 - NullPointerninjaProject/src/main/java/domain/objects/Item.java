package com.realdolmen.hbo5.t1.domain.objects;

import com.realdolmen.hbo5.t1.domain.objects.database.aid.classes.AbstractDatabaseObject;
import com.realdolmen.hbo5.t1.utilities.StringUtils;

import javax.persistence.*;
import java.sql.Date;
import java.text.NumberFormat;
import java.time.LocalDate;

/**
 * this is the database entity from table products
 */
@Entity
@Table(name = "products", schema = "stockprogram")
@NamedQueries({
        @NamedQuery(name = "Item.findall", query = "SELECT  i  from Item i"),
        @NamedQuery(name = "Item.orderAllByPriceAsc", query = "select i from Item i order by i.prijs asc "),
        @NamedQuery(name = "Item.orderAllByPriceDesc", query = "select i from Item i order by i.prijs desc "),
        @NamedQuery(name = "Item.orderAllByAmountAsc", query = "select i from Item i order by i.aantal asc "),
        @NamedQuery(name = "Item.orderAllByAmountDesc", query = "select i from Item i order by i.aantal desc "),
        @NamedQuery(name = "Item.orderAllByNameAsc", query = "select i from Item i order by i.naam asc"),
        @NamedQuery(name = "Item.orderAllByNameDesc", query = "select i from Item i order by i.naam desc"),
        @NamedQuery(name = "Item.findbyid", query = "select i from  Item i where i.pk = :id"),
        @NamedQuery(name= "Item.orderByPriceAsc", query= "select i from Item i order by i.prijs asc"),
        @NamedQuery(name = "Item.findbyName", query = "SELECT i from Item i where i.naam like :name"),
        @NamedQuery(name = "Item.findbyArtikelNR", query = "select i from Item i where i.artikelNr = :artikelnr"),
        @NamedQuery(name = "Item.findbyEANCode", query = "select i from Item i where i.eanCode = :eancode")
})
public class Item extends AbstractDatabaseObject {
    @Basic
    @Access(AccessType.FIELD)
    @Column(unique = true, name = "artikelNr")
    long artikelNr;
    @Basic
    @Access(AccessType.FIELD)
    @Column(unique = true, name = "eancode")
    String eanCode;
    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "merk")
    String merk;
    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "naam")
    String naam;
    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "omschrijvingNL")
    String omschrijvingNL;
    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "omschrijvingEN")
    String omschrijvingEN;
    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "categorieNL")
    String categorieNL;
    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "categorieEn")
    String categorieEN;
    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "groep1")
    String groep1;
    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "groep2")
    String groep2;
    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "groep3")
    String groep3;
    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "prijs")
    double prijs;
    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "aantal")
    int aantal;
    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "creatiedatum")
    Date creatieDatum;

    public Item() {

    }

    public Item(int id, long artikelNr, String eanCode, String brand, String naam, String omschrijvingNL, String omschrijvingEN, String categorieNL, String categorieEN, String groep1, String groep2, String groep3, double prijs, int aantal, LocalDate creatieDatum) {
       this(id,artikelNr,eanCode,brand,naam,omschrijvingNL,omschrijvingEN,categorieNL,categorieEN,groep1,groep2,groep3,prijs,aantal);
       this.setCreatieDatum(creatieDatum);
    }

    public Item(long artikelNr, String eanCode, String brand, String naam, String omschrijvingNL, String omschrijvingEN, String categorieNL, String categorieEN, String groep1, String groep2, String groep3, double prijs, int aantal) {
        this.artikelNr = artikelNr;
        this.eanCode = eanCode;
        this.merk = brand;
        this.naam = naam;
        this.omschrijvingNL = omschrijvingNL;
        this.omschrijvingEN = omschrijvingEN;
        this.categorieNL = categorieNL;
        this.categorieEN = categorieEN;
        this.groep1 = groep1;
        this.groep2 = groep2;
        this.groep3 = groep3;
        this.prijs = prijs;
        this.aantal = aantal;
    }

    public Item(int id, long articleNr, String eancode, String brand, String naam, String omschrijvingNL, String omschrijvingEN, String categoryNL, String categoryEN, String group1, String group2, String group3, double prijs, int aantal) {
        this(articleNr, eancode, brand, naam, omschrijvingNL, omschrijvingEN, categoryNL, categoryEN, group1, group2, group3, prijs, aantal);
        this.pk = id;
    }

    public long getArtikelNr() {
        return artikelNr;
    }

    public void setArtikelNr(long artikelNr) {
        this.artikelNr = artikelNr;
    }

    public String getEanCode() {
        return eanCode;
    }

    public void setEanCode(String eanCode) {
        this.eanCode = eanCode;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getOmschrijvingNL() {
        return omschrijvingNL;
    }

    public void setOmschrijvingNL(String omschrijvingNL) {
        this.omschrijvingNL = omschrijvingNL;
    }

    public String getOmschrijvingEN() {
        return omschrijvingEN;
    }

    public void setOmschrijvingEN(String omschrijvingEN) {
        this.omschrijvingEN = omschrijvingEN;
    }

    public String getCategorieNL() {
        return categorieNL;
    }

    public void setCategorieNL(String categorieNL) {
        this.categorieNL = categorieNL;
    }

    public String getCategorieEN() {
        return categorieEN;
    }

    public void setCategorieEN(String categorieEN) {
        this.categorieEN = categorieEN;
    }

    public String getGroep1() {
        return groep1;
    }

    public void setGroep1(String groep1) {
        this.groep1 = groep1;
    }

    public String getGroep2() {
        return groep2;
    }

    public void setGroep2(String groep2) {
        this.groep2 = groep2;
    }

    public String getGroep3() {
        return groep3;
    }

    public void setGroep3(String groep3) {
        this.groep3 = groep3;
    }

    public double getPrijs() {
        return prijs;
    }

    public void setPrijs(double prijs) {
        this.prijs = prijs;
    }

    public int getAantal() {
        return aantal;
    }

    public void setAantal(int aantal) {
        this.aantal = aantal;
    }

    public void setCreatieDatum(LocalDate creatieDatum) {
        this.creatieDatum = Date.valueOf(creatieDatum);
    }

    public LocalDate getCreatieDatum() {
        if (creatieDatum != null) {
            return creatieDatum.toLocalDate();
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "Item{" +
                "artikelNr=" + artikelNr +
                ", eanCode='" + eanCode + '\'' +
                ", merk='" + merk + '\'' +
                ", naam='" + naam + '\'' +
                ", omschrijvingNL='" + omschrijvingNL + '\'' +
                ", omschrijvingEN='" + omschrijvingEN + '\'' +
                ", categorieNL='" + categorieNL + '\'' +
                ", categorieEN='" + categorieEN + '\'' +
                ", groep1='" + groep1 + '\'' +
                ", groep2='" + groep2 + '\'' +
                ", groep3='" + groep3 + '\'' +
                ", prijs=" + prijs +
                ", aantal=" + aantal +
                ", creatieDatum=" + creatieDatum +
                ", pk=" + pk +
                '}';
    }
}


