package com.realdolmen.hbo5.t1.utilities;

public class StringUtils {

    /**
     * Maakt de opgegeven String korter als die langer is dan de opgegeven lengte. Dan wordt aan het einde
     * van die String de postfix toegevoegd.
     * @param s De string die gecontroleerd wordt
     * @param length De maximale lengte van de String
     * @param postfix Wat er achter de String moet toegevoegd worden in het geval dat die langer is dan 'length'.
     * @return De afgekorte String in het geval dit nodig was.
     */
    public static String shorten(String s, int length, String postfix){
        String shortened;
        if(s.length() >= length){
            shortened = s.substring(0, length-postfix.length()).concat(postfix);
        } else {
            shortened = s;
        }
        return shortened;
    }
}