package com.realdolmen.hbo5.t1.dao;

import com.realdolmen.hbo5.t1.domain.objects.Item;
import com.realdolmen.hbo5.t1.exceptions.NoEntryFoundException;
import com.realdolmen.hbo5.t1.utilities.JPAUtility;

import javax.persistence.*;
import java.util.List;

/**
 * this is JPA aid, this class is designed to do the implementation of raw JPA in your code, so you dont have to fiddle a lot  with it anymore
 */
public class ItemDao {
    JPAUtility jpaUtility ;
    EntityManager em;

    public ItemDao(){
       jpaUtility = JPAUtility.getInstance();
    }

    public boolean saveToDB(Item item) {
        em = jpaUtility.createEntityManager();
        EntityTransaction tr = em.getTransaction();
        try {
            tr.begin();
            em.persist(item);
            tr.commit();
            return true;
        } catch (RollbackException e) {
            return false;
        } finally {
            em.close();
        }
    }

    public boolean saveToDB(List<Item> items) {
        em = jpaUtility.createEntityManager();
        if (items.size() == 1) {
            return saveToDB(items.get(0));
        } else {
            EntityTransaction tr = em.getTransaction();
            try {
                tr.begin();
                for (Item i : items) {
                    em.persist(i);
                }
                tr.commit();
                return true;
            } catch (RollbackException e) {
                return false;
            } finally {
                em.close();
            }
        }
    }

    public Item findItemById(int id) {
        em = jpaUtility.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.findbyid", Item.class);
        query.setParameter("id", id);
        Item item = query.getSingleResult();
        em.close();
        return item;
    }

    public long getItemCount(){
        em = jpaUtility.createEntityManager();
        Query q = em.createNativeQuery("SELECT COUNT(*) FROM products");
        long result = (long)q.getSingleResult();
        em.close();
        return result;
    }

    public long getItemCountFindByName(String name){
        em = jpaUtility.createEntityManager();
        Query q = em.createNativeQuery("SELECT COUNT(*) FROM products WHERE naam LIKE ?");
        q.setParameter(1, "%"+ name +"%");
        long result = (long)q.getSingleResult();
        em.close();
        return result;
    }

    public List<Item> allItemsOrderByPriceAsc(int page, int amount) throws NoEntryFoundException {
        em = jpaUtility.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.orderAllByPriceAsc", Item.class);
        query.setFirstResult((page - 1) * amount);
        query.setMaxResults(amount);
        List<Item> allItems = query.getResultList();
        em.close();
        if (allItems.isEmpty()) {
            throw new NoEntryFoundException();
        }
        return allItems;
    }

    public List<Item> findAllItems(int page, int amount) throws NoEntryFoundException {
        em = jpaUtility.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.findall", Item.class);
        query.setFirstResult((page - 1) * amount);
        query.setMaxResults(amount);
        List<Item> allItems = query.getResultList();
        em.close();
        if (allItems.isEmpty()) {
            throw new NoEntryFoundException();
        }
        return allItems;
    }

    public Item findItemByName(String name, int page) throws NoEntryFoundException {
        return findItemsByName(name, page).get(0);
    }

    public List<Item> findItemsByName(String name, int page) throws NoEntryFoundException {
        em = jpaUtility.createEntityManager();
        TypedQuery<Item> items = em.createNamedQuery("Item.findbyName", Item.class).setParameter("name", "%" + name + "%");
        items.setFirstResult((page - 1));
        items.setMaxResults(25);
        List<Item> itemResults = items.getResultList();
        em.close();
        if (itemResults.isEmpty()) {
            throw new NoEntryFoundException();
        }
        return itemResults;
    }

    public List<Item> findItemByArtikelNR(long artikelNR, int page) throws NoEntryFoundException {
        em = jpaUtility.createEntityManager();
        TypedQuery<Item> items = em.createNamedQuery("Item.findbyArtikelNR", Item.class).setParameter("artikelnr", artikelNR);
        items.setFirstResult((page - 1));
        items.setMaxResults(25);
        List<Item> itemResults = items.getResultList();
        em.close();
        if (itemResults == null) {
            throw new NoEntryFoundException();
        }
        return itemResults;
    }

    public Item findItemByEANCode(String eanCode) throws NoEntryFoundException {
        em = jpaUtility.createEntityManager();
        Item item = em.createNamedQuery("Item.findbyEANCode", Item.class).setParameter("eancode", eanCode).getSingleResult();
        em.close();
        if (item == null) {
            throw new NoEntryFoundException();
        }
        return item;
    }

    public boolean editItem(Item updatedItem) {
        Item item = this.findItemById(updatedItem.getPk());
        em = jpaUtility.createEntityManager();
        try {
            EntityTransaction tr = em.getTransaction();
            tr.begin();
            item.setArtikelNr(updatedItem.getArtikelNr());
            item.setCategorieEN(updatedItem.getCategorieEN());
            item.setCategorieNL(updatedItem.getCategorieNL());
            item.setEanCode(updatedItem.getEanCode());
            item.setMerk(updatedItem.getMerk());
            item.setNaam(updatedItem.getNaam());
            item.setAantal(updatedItem.getAantal());
            item.setCreatieDatum(updatedItem.getCreatieDatum());
            item.setGroep1(updatedItem.getGroep1());
            item.setGroep2(updatedItem.getGroep2());
            item.setGroep3(updatedItem.getGroep3());
            item.setOmschrijvingEN(updatedItem.getOmschrijvingEN());
            item.setOmschrijvingNL(updatedItem.getOmschrijvingNL());
            item.setPrijs(updatedItem.getPrijs());
            em.merge(item);
            tr.commit();
            em.close();
            return true;
        } catch (RollbackException e) {
            em.close();
            return false;
        }
    }

    public boolean removeItem(int id) {
        Item item = this.findItemById(id);
        em = jpaUtility.createEntityManager();
        try {
            em.getTransaction().begin();
            Item itemToBeRemoved = em.merge(item);
            em.remove(itemToBeRemoved);
            em.getTransaction().commit();
            em.close();
            return true;
        } catch (RollbackException e) {
            em.close();
            return false;
        }

    }

    public List<Item> allItemsOrderByPriceDesc(int page, int amount) throws NoEntryFoundException {
        em = jpaUtility.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.orderAllByPriceDesc", Item.class);
        query.setFirstResult((page - 1) * amount);
        query.setMaxResults(amount);
        List<Item> allItems = query.getResultList();
        em.close();
        if (allItems.isEmpty()) {
            throw new NoEntryFoundException();
        }
        return allItems;
    }

    public List<Item> allItemsOrderByAmountAsc(int page, int amount) throws NoEntryFoundException {
        em = jpaUtility.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.orderAllByAmountAsc", Item.class);
        query.setFirstResult((page - 1) * amount);
        query.setMaxResults(amount);
        List<Item> allItems = query.getResultList();
        em.close();
        if (allItems.isEmpty()) {
            throw new NoEntryFoundException();
        }
        return allItems;
    }

    public List<Item> allItemsOrderByAmountDesc(int page, int amount) throws NoEntryFoundException {
        em = jpaUtility.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.orderAllByAmountDesc", Item.class);
        query.setFirstResult((page - 1) * amount);
        query.setMaxResults(amount);
        List<Item> allItems = query.getResultList();
        em.close();
        if (allItems.isEmpty()) {
            throw new NoEntryFoundException();
        }
        return allItems;
    }

    public List<Item> allItemsOrderByNameAsc(int page, int amount) throws NoEntryFoundException {
        em = jpaUtility.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.orderAllByNameAsc", Item.class);
        query.setFirstResult((page - 1) * amount);
        query.setMaxResults(amount);
        List<Item> allItems = query.getResultList();
        em.close();
        if (allItems.isEmpty()) {
            throw new NoEntryFoundException();
        }
        return allItems;
    }

    public List<Item> allItemsOrderByNameDesc(int page, int amount) throws NoEntryFoundException {
        em = jpaUtility.createEntityManager();
        TypedQuery<Item> query = em.createNamedQuery("Item.orderAllByNameDesc", Item.class);
        query.setFirstResult((page - 1) * amount);
        query.setMaxResults(amount);
        List<Item> allItems = query.getResultList();
        em.close();
        if (allItems.isEmpty()) {
            throw new NoEntryFoundException();
        }
        return allItems;
    }
}

