package java_basic;

import java.util.concurrent.ThreadLocalRandom;

public class Exercise1 {

	/*
	 * Don't touch this!
	 */
	static int evenNumbers;
	static int unEvenNumbers;

	
	public static void main(String[] args) {
		int[]anArray = createArray();
		print(anArray);
		findEvenAndUnEvenNumbers(anArray);
		System.out.println("Number of even numbers : "+evenNumbers);
		System.out.println("Number of uneven numbers : "+unEvenNumbers);
	}

	//Random number tussen 0 en 50
	private static int getRandomNumber() {
		return ThreadLocalRandom.current().nextInt(0,50);
	}
	/*
	 * Start here
	 */
	
	//1.Maak een array van integers, gebruik getRandomNumber om de array te vullen
	public static int[] createArray() {

		//wijzig null
		return null;
	}
	
	//2.	Maak een print methode die de array uitprint, gebruik de voorbeeld output als leidraad hoe die er moet uitzien. 
	//Hou rekening met tabs, enters en haakjes op de juiste plaats. 
	public static void print(int[]anArray) {

		
	}
	
	//3.	Zorg in de methode findEvenAndUnEvenNumbers dat het 
	//aantal even nummers uit de array worden geteld en tegelijk ook 
	//alle oneven nummers. Gebruik variabelen evenNumbers en 
	//unEvenNumbers die jou al gegeven zijn in de klasse
	public static void findEvenAndUnEvenNumbers(int[]anArray) {

	}
	

	
	

}
