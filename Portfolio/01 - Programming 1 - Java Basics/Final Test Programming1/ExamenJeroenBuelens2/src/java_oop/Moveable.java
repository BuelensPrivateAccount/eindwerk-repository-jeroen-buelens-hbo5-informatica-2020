package java_oop;

public interface Moveable {
	public void MoveUp();
	public void Movedown();
	public void MoveLeft();
	public void MoveRight();

}
