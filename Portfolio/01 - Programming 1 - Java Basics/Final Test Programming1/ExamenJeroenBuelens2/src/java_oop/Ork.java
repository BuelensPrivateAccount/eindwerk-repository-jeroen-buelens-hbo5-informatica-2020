package java_oop;

public class Ork extends GameObject {
private String wapen;

public Ork() {
	super();
	this.wapen = "default";
	// TODO Auto-generated constructor stub
}

public Ork(int iD, String name, int str, String wapen) {
	super(iD, name, str);
	this.wapen = wapen;
	// TODO Auto-generated constructor stub
}

public String getWapen() {
	return wapen;
}

public void setWapen(String wapen) {
	this.wapen = wapen;
}

@Override
public String toString() {
	return "Ork [wapen=" + wapen + "] is also a " + super.toString();
}

}
