package java_oop;

public class Wizard extends GameObject implements Moveable{
	private int level;
	private Point point; 

	public Wizard() {
		super();
		this.level = 0;
		this.point = new Point();
		// TODO Auto-generated constructor stub
	}

	public Wizard(int iD, String name, int str, int level) {
		super(iD, name, str);
		this.level = level;
		this.point = new Point();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void MoveUp() {
		point.setY(point.getY()+1);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Movedown() {
		point.setY(point.getY()-1);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void MoveLeft() {
		point.setX(point.getX()-1);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void MoveRight() {
		point.setX(point.getX()+1);
		// TODO Auto-generated method stub
		
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	@Override
	public String toString() {
		return "Wizard [level=" + level + ", point=" + point + "] is also a " + super.toString();
	}

}
