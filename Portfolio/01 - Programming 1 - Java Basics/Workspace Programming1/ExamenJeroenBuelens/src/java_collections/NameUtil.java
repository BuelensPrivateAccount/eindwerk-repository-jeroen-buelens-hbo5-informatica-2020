package java_collections;

import java.util.concurrent.ThreadLocalRandom;

public class NameUtil {

	private static String personFirstNames[] = new String[] {"Steven","Maxime","Stijn","Yorick","Jeroen","Yoni","Seda","Moushine","Youssef","Adel","Klaartje","Katrien","Lisa"};
	private static String personLastNames[] = new String[] {"Esnol",
			"Ahriga",
			"Buelens",
			"Coeckelbergh",
			"El Amrani",
			"Lemrabet",
			"Lens",
			"Sunaert",
			"Tehraoui",
			"Van Caekenbergh",
			"Vindelinckx",
			"Wauters"};
	
	
	private static int getRandomNumber(int bound) {
		return ThreadLocalRandom.current().nextInt(0,bound);
	}
	
	public static String generateRandomName() {
		String first = personFirstNames[getRandomNumber(personFirstNames.length)];
		String last = personLastNames[getRandomNumber(personLastNames.length)];
		return first+" "+last;
	}
}
