package java_oop;

public class Wizard extends GameObject implements Movable {
	private int level;
	private Point aPoint;

	public Wizard() {
		this(0, "default", 0, 0);
	}

	public Wizard(int id, String name, int strength, int level) {
		super(id, name, strength);
		this.level = level;
		this.aPoint = new Point();

	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Point getaPoint() {
		return aPoint;
	}

	public void setaPoint(Point aPoint) {
		this.aPoint = aPoint;
	}

	@Override
	public String toString() {
		return "this is a " + this.level + " wizard that's also a gameObject " + super.toString();
	}

	@Override
	public void moveUp() {
		this.aPoint.setY(this.aPoint.getY() + 1);

	}

	@Override
	public void moveDown() {
		this.aPoint.setY(this.aPoint.getY() - 1);

	}

	@Override
	public void moveLeft() {
		this.aPoint.setX(this.aPoint.getX() - 1);

	}

	@Override
	public void moveRight() {
		this.aPoint.setX(this.aPoint.getX() + 1);

	}

}
