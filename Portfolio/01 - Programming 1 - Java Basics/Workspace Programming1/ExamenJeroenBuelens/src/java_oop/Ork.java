package java_oop;

public class Ork extends GameObject{
	private String wapen;

	public Ork() {
		this(0,"default",0,"default");
		
	}

	public Ork(int id, String name, int strength,String wapen) {
		super(id, name, strength);
		this.wapen = wapen;
	
	}

	

	public String getWapen() {
		return wapen;
	}

	public void setWapen(String wapen) {
		this.wapen = wapen;
	}
	
	@Override
	public String toString() {
		return "this is a ork with " + this.wapen+" and is also a gameObject " + super.toString();
	}
}
