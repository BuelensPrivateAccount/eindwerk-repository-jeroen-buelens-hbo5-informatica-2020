package java_oop;

public abstract class GameObject {
	private int id;
	private String name;
	private int strength;
	
	public GameObject(int id, String name, int strength) {
		this.id = id;
		this.name = name;
		this.strength = strength;
	}
	public GameObject() {
		this(0,"default",0);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}
	@Override
	public String toString() {
		return "GameObject [id=" + id + ", name=" + name + ", strength=" + strength + "]";
	}
	

}
