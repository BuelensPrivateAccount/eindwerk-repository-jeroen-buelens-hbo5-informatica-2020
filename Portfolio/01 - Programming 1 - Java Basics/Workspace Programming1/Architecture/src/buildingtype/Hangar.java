package buildingtype;

import abstractArchitecture.Building;

public class Hangar extends Building{
	private String function;

	public Hangar(String buildingnr,String function) {
		super(buildingnr);
		this.function = function;
		// TODO Auto-generated constructor stub
	}

	public Hangar(String function) {
		this("A",function);
	}
		

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

}
