package BuildingApp;

import abstractArchitecture.Building;
import abstractArchitecture.Level;
import abstractArchitecture.RangeException;
import abstractArchitecture.Room;
import buildingtype.Home;

public class Buildingapp {
public static void main(String[] args) {
	Building huis = new Home();
	try {
		huis.addLevel(new Level(0, huis.getBuildingnr()));
		huis.getLevels().get(0).addRoom(new Room("Garage", 1, huis.getBuildingnr(),huis.getLevels().get(0).getLevelnr()));
		
	} catch (RangeException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
}
