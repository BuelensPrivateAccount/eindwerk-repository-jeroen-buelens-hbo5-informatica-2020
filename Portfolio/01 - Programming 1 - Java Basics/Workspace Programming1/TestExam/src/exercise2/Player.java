package exercise2;

public class Player extends Person implements PlaySoccer {
	private String positionOnField;

	public Player() {
		super();
		this.positionOnField = "benched";
		// TODO Auto-generated constructor stub
	}

	public Player(int id, String firstName, String lastName, String positionOnField) {
		super(id, firstName, lastName);
		this.positionOnField = positionOnField;
		// TODO Auto-generated constructor stub
	}

	@Override
	public String play() {
		return "i am playing soccer";
	}

	public String getPositionOnField() {
		return positionOnField;
	}

	public void setPositionOnField(String positionOnField) {
		this.positionOnField = positionOnField;
	}

	@Override
	public String toString() {
		return "Player is a Person: " + super.toString() + "with extra field positionOnField" + this.positionOnField;
	}

}
