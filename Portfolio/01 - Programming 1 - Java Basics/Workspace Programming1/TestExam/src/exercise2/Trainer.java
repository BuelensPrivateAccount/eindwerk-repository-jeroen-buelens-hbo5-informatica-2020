package exercise2;

public class Trainer extends Person {
	private boolean fired;

	public Trainer() {
		super();
		this.fired = false;
		// TODO Auto-generated constructor stub
	}

	public Trainer(int id, String firstName, String lastName,boolean fired) {
		super(id, firstName, lastName);
		this.fired = fired;
		// TODO Auto-generated constructor stub
	}

	public boolean isFired() {
		return fired;
	}

	public void setFired(boolean fired) {
		this.fired = fired;
	}

	@Override
	public String toString() {
		return "Trainer is a Person: "+ super.toString() + "with extra field fired" + this.fired;
	}
	

}
