package exercise2;

import java.util.ArrayList;

public class Team {
	private int id;
	private String teamName;
	private ArrayList<Person> teammembers; 
	
	public Team(int id, String teamName) {
		this.id = id;
		this.teamName = teamName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	


	public void setTeammembers(ArrayList<Person> teammembers) {
		this.teammembers = teammembers;
	}

	public ArrayList<Person> getTeammembers() {
		return teammembers;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	
	

}
