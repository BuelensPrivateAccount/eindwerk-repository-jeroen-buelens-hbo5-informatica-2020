package exercise1;

import java.util.concurrent.ThreadLocalRandom;

public class Exercise1 {

	/*
	 * 1.a Write a program where a integer array (not list) gets 16 random numbers
	 * between 0 and 100. (Use the method given to you) 1.b create a method that
	 * calculates the average of the numbers in that list 1.c create a method that
	 * finds the highest number 1.d create a method that finds the lowest number 1.e
	 * create a print method for the array, and make it look like the example
	 * example print :
	 */

	public static void main(String[] args) {
		int[] test = generateArray();
		print(test);
		System.out.println("Average is: " + getAverage(test));
		System.out.println("lowest number: " + getMin(test));
		System.out.println("highest number: " + getMax(test));

	}

	// given to you for free
	public static int randomInteger() {
		return ThreadLocalRandom.current().nextInt(0, 100);
	}

	public static int getMin(int[] anArray) {
		int min = anArray[0];
		for (int i = 1; i < anArray.length; i++) {
			min = Math.min(min, anArray[i]);
		}
		return min;
	}

	public static int getMax(int[] anArray) {
		int max = anArray[0];
		for (int i = 1; i < anArray.length; i++) {
			max = Math.max(max, anArray[i]);
		}
		return max;
	}

	public static double getAverage(int[] anArray) {
		int average = 0;
		for (int i = 0; i < anArray.length; i++) {
			average += anArray[i];
		}
		return (double) average / anArray.length;
	}

	public static void print(int[] anArray) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i = 0; i < anArray.length; i++) {
			sb.append(anArray[i]);
			if (i < anArray.length - 1) {
				sb.append("     ");
			}
		}
		sb.append("]");
		System.out.println(sb.toString());

	}

	private static int[] generateArray() {
		int[] array = new int[16];
		for (int i = 0; i < array.length; i++) {
			array[i] = randomInteger();
		}
		return array;

	}

}
