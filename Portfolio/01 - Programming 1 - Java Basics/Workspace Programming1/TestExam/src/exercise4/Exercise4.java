package exercise4;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.Map.Entry;

import exercise2.Person;
import exercise2.Player;
import exercise2.Team;
import exercise2.Trainer;
import exercise4.StringGenerator;



public class Exercise4 {
	/*
	 * 	a)	In 2.java oop, you had to create a few classes. Now build a Team with a 11 players and a coach. Make sure that the id is incremental. 
	 * 		So for each player/coach created there is a unique id, Give them also a name, use the given class to create random first and last name. 
	 * 		Tip : use a loop for the players and add the coach as first element in the list. 
		b)	Use the method print to print out all the objects in the list. 
		c)	Now set the coach, fired on false and remove the coach from the team.
		d) 	Make a Map<String,ArrayList<Player>> where the key is the position of the player on the field and 
			find all the players for the same position and pass it as value.
 * 		e) Print the Map in printMap with first the position and than the values, use the tostring of player
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//create new team
		Team funtimeFreddys = new Team(0, "Funtime Freddies");
		//use team.setPersons() to set the arraylist of persons
		funtimeFreddys.setTeammembers(createPersonsForTeam());
		
		print(funtimeFreddys.getTeammembers());
		((Trainer) funtimeFreddys.getTeammembers().get(0)).setFired(false);
		removeCoachFromTeam(funtimeFreddys);
		printMap(findAllPlayersForPosition(funtimeFreddys));
		
		//
	}
	
	public static void removeCoachFromTeam(Team team) {
		team.getTeammembers().remove(0);
		
	}
	
	public static ArrayList<Person> createPersonsForTeam(){
		ArrayList<Person> team = new ArrayList<>();
		team.add(new Trainer(0,StringGenerator.getRandomFirstName(),StringGenerator.getRandomLastName(),false));
		for(int i = 1;i<12;i++) {
			team.add(createPlayer(i));
		}
		return team;
	}
	
	public static Player createPlayer(int id) {
		
		//use StringGenerator.getRandomFirstName, getRandomFirstName, getRandomPositionOnField
		return new Player(id,StringGenerator.getRandomFirstName(),StringGenerator.getRandomLastName(),StringGenerator.getRandomFieldPositions());
	}
	
	public static void print(ArrayList<Person>persons) {
		for(Person p:persons) {
			System.out.println(p.toString());
		}
		
	}
	
	public static HashMap<String,ArrayList<Player>> findAllPlayersForPosition(Team team){
		HashMap<String,ArrayList<Player>> map =  new HashMap<>();
		@SuppressWarnings("unchecked")
		ArrayList<Player> playerarray =  (ArrayList<Player>) ((ArrayList<?>) team.getTeammembers());
		
		for(Player p :playerarray) {
			if(map.containsKey(p.getPositionOnField())) {
				ArrayList<Player> temp = map.get(p.getPositionOnField());
				temp.add(p);
				map.put(p.getPositionOnField(), temp);
			}
			else {
				ArrayList<Player> temp = new ArrayList<Player>();
				temp.add(p);
				map.put(p.getPositionOnField(), temp);
			}
		}
		return map;
	}
	
	public static void printMap(HashMap<String,ArrayList<Player>> positionMap) {
			for (Entry<String, ArrayList<Player>> e : positionMap.entrySet()) {
				System.out.println("the member with postion " + e.getKey());
				for(Player p : e.getValue()) {
					System.out.println(p.getFirstName() + " " + p.getLastName());
				}
			}

		
		
	}

}
