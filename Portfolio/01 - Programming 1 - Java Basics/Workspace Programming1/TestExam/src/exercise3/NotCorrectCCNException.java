package exercise3;

public class NotCorrectCCNException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2386334620460452576L;
	
	public NotCorrectCCNException() {
		super("not a credit card number");
	}

}
