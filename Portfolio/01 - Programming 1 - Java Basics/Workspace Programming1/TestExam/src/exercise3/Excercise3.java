package exercise3;

import java.util.Scanner;

public class Excercise3 {

	/*
	 * Don't change the main class, use start application to perform the logic. a)
	 * Create the class NotCorrectCCNException, and make sure that this is a custom
	 * exception. Create a constructor where you call the super constructor with
	 * message "Not a credit card number" b) Ask the user for a credit card number,
	 * use the scanner and the method receiveACreditCardNumber(). the length of the
	 * ccn is 12 digits (use isCorrectNumber) , make sure this is correct, else
	 * throw a NotCorrectCCNException.
	 * 
	 */

	public static void main(String[] args) {
		startApplication();
	}

	public static boolean isCorrectNumber(String ccn) throws NotCorrectCCNException {
		if (ccn.length() == 12) {
			return true;
		} 
			throw new NotCorrectCCNException();
		
	}

	public static void startApplication() {
		// receive a ccn
		System.out.println("enter number please.");
		Scanner scan = new Scanner(System.in);
		String ccn = scan.nextLine();
		try {
			isCorrectNumber(ccn);
		} catch (NotCorrectCCNException e) {
			System.err.println(e.getMessage());
		}
		
		scan.close();

		// check if it is correct

		// if not, ask for a new one (keep doing this until it is correct)
	}
}
