package ex_1;

import java.util.ArrayList;

import java.util.Collections;

import java.util.Scanner;

public class StringToList {

	private static Scanner scanner;

	public static void main(String[] args) {
		String s = userRandomStringInput();
		ArrayList<String> stringList = createListFromString(s);
		print(stringList);
		System.out.println();
		print(sortAlphabetically(stringList));
		print(randomizeAllWords(stringList));
	}

	// TODO 1.1 Create a string by asking the user for a sentence (more than one
	// word)
	private static String userRandomStringInput() {
		System.out.println("input plz");
		scanner = new Scanner(System.in);
		String s = scanner.nextLine();
		return s;
	}

	// TODO 1.2 Create a list for the string s, by separating the words using a
	// space
	private static ArrayList<String> createListFromString(String s) {
		String[] slist = s.split(" ");
		ArrayList<String> listFromString = new ArrayList<String>();
		for (int i = 0; i < slist.length; i++) {
			slist[i] = slist[i].trim();
			listFromString.add(slist[i]);
		}
		return listFromString;
	}

	// TODO 1.3 Sort the string list alphabetically
	// https://docs.oracle.com/javase/8/docs/api/?java/util/Collections.html
	// See if you can use something that exists in the api
	private static ArrayList<String> sortAlphabetically(ArrayList<String> stringList) {
		Collections.sort(stringList);
		return stringList;
	}

	// TODO 1.4 a.For every word in the string list you need to scramble each word,
	// letter by letter
	private static ArrayList<String> randomizeAllWords(ArrayList<String> stringList) {
		ArrayList<String> result = new ArrayList<>();
		for(String s: stringList) {
			getScrambledWord(s);
		}

		return result;
	}
	public static String getScrambledWord(String str) {
	    char[] character = str.toCharArray();
	    String result = new String();

	    ArrayList<Character> chars = new ArrayList<Character>(); //an arraylist is an array wich dynamically changes its size depending on the amount of its elements
	    for (int i = 0; i < character.length; i++) {// first we put all characters of the word into that arraylist
	        chars.add(character[i]);
	    }

	    while(chars.size()>0){//then we iterate over the arraylist as long as it has more than 0 elements
	        int index = (int)(Math.random() * chars.size());//we create a random index in the range of 0 and the arraylists size
	        result += chars.get(index);// we add the letter at the index we generated to the scrambled word variable
	        chars.remove(index);// then we remove the character we just added to the scrambled word, from the arraylist, so it cant be in there twice
	    }// thus the size decreases by 1 each iteration until every element of the arrraylist is somewhere in the scrambled word

	    return result;
	}

	// I will give this for free
	private static void print(ArrayList<String> stringList) {
		stringList.forEach(s -> System.out.print(s + " "));
	}

}
