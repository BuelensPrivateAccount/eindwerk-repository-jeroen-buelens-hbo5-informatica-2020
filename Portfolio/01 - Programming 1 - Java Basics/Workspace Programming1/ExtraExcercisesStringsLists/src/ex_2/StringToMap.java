package ex_2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import util.RandomGenerator;

public class StringToMap {
	
	

	public static void main(String[] args) {
		ArrayList<String> randomStringList = RandomGenerator.generateRandomStringList(10);
		printList(randomStringList);
		System.out.println(" ");
		printMap(calculateNumberOfCharacters(randomStringList));
		System.out.println(" ");
		printMap(calculateNumberOfCharacters(generateAlpabetMap(),randomStringList));
	}

	// TODO 2.2 create the printMap method, loop over the hashMap (tip : use
	// EntrySet)
	// https://docs.oracle.com/javase/9/docs/api/java/util/HashMap.html
	private static void printMap(HashMap<Character, Integer> characterMap) {
		for (Map.Entry<Character, Integer> e : characterMap.entrySet()) {
			System.out.println("[" + e.getKey() + " + " + e.getValue() + "]");
		}

	}

	// Free of charge, thank you
	private static void printList(ArrayList<String> stringList) {
		stringList.forEach(s -> System.out.print(s + " "));
	}

	// TODO 2.1 Create for each character that occurs in the list a Entry<key,value>
	// where the key is the character
	// and the value is the number of occurence of that character
	// for example : 'a',10
	// 'b',20
	// ...
	private static HashMap<Character, Integer> calculateNumberOfCharacters(ArrayList<String> stringList){
		return calculateNumberOfCharacters(new HashMap<Character, Integer>(),stringList);
	}
	
	private static HashMap<Character, Integer> calculateNumberOfCharacters(HashMap<Character, Integer> map,ArrayList<String> stringList) {
		for (String s : stringList) {
			for (char a : s.toLowerCase().toCharArray()) {
				if (map.containsKey(a)) {
					map.put(a, (map.get(a) + 1));
				} else {
					map.put(a, 1);
				}
			}
		}

		return map;
	}

	public static HashMap<Character,Integer>generateAlpabetMap(){
		HashMap<Character, Integer> alphabetMap = new HashMap<>();
		for (char c = 'a'; c <= 'z'; c++) {
			alphabetMap.put(c, 0);
		}
		return alphabetMap;
		
	}
}
