package util;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class RandomGenerator {

	public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static final String lower = upper.toLowerCase();

	public static String generateRandomWord() {
		int rand = generateRandomInt(0, upper.length());
		String s = "";
		for (int i = 0; i < rand; i++) {
			if (i == 0) {
				s += upper.charAt(rand);
			} else {
				s += lower.charAt(rand);
			}
			rand = generateRandomInt(0, upper.length());
		}
		return s;
	}

	public static int generateRandomInt(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max);
	}

	public static ArrayList<String> generateRandomStringList(int max) {
		ArrayList<String> randomStringList = new ArrayList<>();
		for (int i = 0; i < max; i++) {
			randomStringList.add(generateRandomWord());
		}
		return randomStringList;
	}

}
