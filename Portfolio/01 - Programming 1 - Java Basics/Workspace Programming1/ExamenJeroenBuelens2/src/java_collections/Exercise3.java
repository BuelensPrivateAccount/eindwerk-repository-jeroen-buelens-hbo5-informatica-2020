package java_collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class Exercise3 {

	/*
	 * Don't touch
	 */
	public static void main(String[] args) {
		ArrayList<Person> persons = personList(10);
		HashMap<Gender, ArrayList<String>> map = getMapByGender(persons);
		printMap(map);

	}

	/*
	 * Start here 1. Vul de arrayList met het aantal gegeven personen die in de
	 * parameter van de methode worden meegegeven. Elke even index zal een Man
	 * worden, en elke oneven index zal een vrouw worden. Voor de naam gebruik je de
	 * NameUtil.generateRandomName methode die jou al is gegeven.
	 */
	public static ArrayList<Person> personList(int length) {
		ArrayList<Person> persons = new ArrayList<Person>();
		for (int i = 0; i < persons.size(); i++) {
			if (i % 2 == 0) {
				persons.add(new Person(NameUtil.generateRandomName(), Gender.M));

			} else {
				persons.add(new Person(NameUtil.generateRandomName(), Gender.V));
			}

		}

		return persons;
	}

	/*
	 * 2.Verdeel de arraylist in een HashMap per geslacht. Mannen bij mannen en
	 * vrouwen bij vrouwen. Als key zal Gender worden opgeslagen en als value zal
	 * een arraylist van String worden bijgehouden.
	 */
	public static HashMap<Gender, ArrayList<String>> getMapByGender(ArrayList<Person> persons) {
		HashMap<Gender, ArrayList<String>> mapByGender = new HashMap<Gender, ArrayList<String>>();
		for(Person p : persons) {
			if(mapByGender.containsKey(p.getGender())) {
				ArrayList<String> temp = mapByGender.get(p.getGender());
				temp.add(p.getName());
				mapByGender.put(p.getGender(), temp);
			}
			else {
				ArrayList<String> temp = new ArrayList<String>();
				temp.add(p.getName());
				mapByGender.put(p.getGender(), temp);
			}
		}
		return mapByGender;
	}

	public static void printMap(HashMap<Gender, ArrayList<String>> mapByGender) {
		for (Entry<Gender, ArrayList<String>> entry : mapByGender.entrySet()) {
			System.out.println(entry.getKey().getGen());
			for (String s : entry.getValue()) {
				System.out.println("\t" + s);
			}
		}
	}

}
