package java_oop;

public abstract class GameObject {
	private int iD;
	private String name;
	private int str;
	public GameObject(int iD, String name, int str) {
		this.iD = iD;
		this.name = name;
		this.str = str;
	}
	public GameObject() {
		this(0,"default",0);
	}
	public int getiD() {
		return iD;
	}
	public void setiD(int iD) {
		this.iD = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getStr() {
		return str;
	}
	public void setStr(int str) {
		this.str = str;
	}
	@Override
	public String toString() {
		return "GameObject [iD=" + iD + ", name=" + name + ", str=" + str + "]";
	}
	

}
