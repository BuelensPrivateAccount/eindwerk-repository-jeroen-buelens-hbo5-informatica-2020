package exercise_2;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ShoppingBasket {

	// TODO 2.2 a shopping basket has a arrayList of products and a client
	String client;
	ArrayList<Product> products = new ArrayList<Product>();
	// instantiate the arraylist of products

	// Constructor with arg string name for property client
	public ShoppingBasket(String client) {
		this.client = client;
	}

	// method to add a new product
	public void addProduct(Product product) {
		this.products.add(product);
	}

	// method print bill
	public void printBill() {
		DecimalFormat f = new DecimalFormat("##.00");
		double total = 0;
		System.out.print("Total bill for " + this.client + "\n");
		// Now loop and show each product

//		for(int i = 0;i<products.size();i++) {
//			System.out.println(products.get(i).getName() + "\nPrice: "+products.get(i).getPrice());
//			//calculate the total while looping
//			total += products.get(i).getPrice();
//			
//		}

		// alternate solution
		for (Product i : products) {
			System.out.println(i.getName() + "\nPrice: " + f.format(i.getPrice()));
			// calculate the total while looping
			total += i.getPrice();
		}

		System.out.println("Total to pay " + f.format(total));
	}

	// add getters and setters only for client
	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

}
