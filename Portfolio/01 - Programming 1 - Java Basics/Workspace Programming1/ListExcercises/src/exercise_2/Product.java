package exercise_2;

public class Product {

	// TODO 2.1 create class Product
	// a Product has a name (String) and a price (double)
	private String name;
	private Double price;

	// constructor with all properties

	public Product(String name, Double price) {

		this.name = name;
		this.price = price;
	}

	// Constructor with no args
	public Product() {
		this("default", 0.0);

	}

	// Getters and Setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "This Product is " + this.name + " with the price of " + this.price + " euro.";
	}

	// override the toString, make it pretty

}
