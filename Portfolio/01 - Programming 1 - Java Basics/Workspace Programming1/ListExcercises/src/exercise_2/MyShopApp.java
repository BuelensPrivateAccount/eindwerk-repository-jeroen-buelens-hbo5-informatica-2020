package exercise_2;

import java.util.concurrent.ThreadLocalRandom;

public class MyShopApp {

	//TODO 2.3
	public static void main(String[] args) {
		ShoppingBasket shoppingBasket = new ShoppingBasket("Jeroen");
		//Create a new ShoppingBasket 
		addRandomProductsToBasket(shoppingBasket);
		//add products
		shoppingBasket.printBill();
		//print the bill
	}
	
	
	private static void addRandomProductsToBasket(ShoppingBasket shoppingBasket){
		String[] productNames = {"Mouse","Keyboard","Screen","Tablet","iPhone"};
		//For each item in productNames there should be a new Product with the item as name and random double as price
//		for(int i=0;i<productNames.length;i++) {
//		 shoppingBasket.addProduct(new Product(productNames[i],randomDouble()));
//		}
		//alternate solution
		for(String i: productNames) {
			shoppingBasket.addProduct(new Product(i,randomDouble()));
		}
		//use shoppingBasket.addProduct() to add the products
	}
	
	private static double randomDouble() {
		return ThreadLocalRandom.current().nextDouble(50, 1000);
	}
	
}
