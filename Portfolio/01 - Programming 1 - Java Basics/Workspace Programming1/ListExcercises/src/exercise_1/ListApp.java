package exercise_1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class ListApp {
	
	//Follow the steps, and run the main method, the result should be something like this
	/*
	 * There are 10 random numbers in the list
10	12	18	51	57	55	95	57	15	21	
The number 5 occurs 3 times in my list
The number 2 occurs 2 times in my list

	 */
	
	//TODO
	public static void main(String[] args) {
		//change the listToTest with the random List
		ArrayList<Integer> listToTest = new ArrayList<>(Arrays.asList(5,5,5,3,2,1,2));
		//if you did not manage to solve 1.1, try the other excercises with the listToTest
		listToTest = getRandomIntegerList(20);
		printList(listToTest);
		seeOccurenceOf(5, listToTest);
		seeOccurenceOf(2, listToTest);

	}

	
	//TODO 1.1 create a list with x random integers, between 0 and 100
	private static ArrayList<Integer> getRandomIntegerList(int x){
		ArrayList<Integer> randomIntList = new ArrayList<>();
		//step 1, create a new ArrayList called randomIntList
		for(int i = 0;i<x;i++) {
		
		//step 2, loop from 0 till x
			randomIntList.add(randomInt(0, 100));
		}
		//step 3, add a random number, use randomInt method
		
		//step 4, replace the return with your arrayList
		return randomIntList;
	}
	
	private static Integer randomInt(int min, int max) {
		//this returns a random number between min and max (I use +1 to include the number of max)
		return ThreadLocalRandom.current().nextInt(min, max+1);
	}
	
	//TODO 1.2 create a print method that the list
	private static void printList(ArrayList<Integer>aList) {
		//step 1, replace size with the actual size of the list
		System.out.println("There are "+aList.size()+" random numbers in the list");
		//step 2, use a loop to print all the numbers in the list
		for(int i=0;i<aList.size();i++) {
			System.out.println(aList.get(i));
		}
		
	}
	
	//TODO 1.3 print the number of times a specific int occurs in the list
	//for example {5,5,5,5,3,2,1} => 5 will occur 4 times
	private static void seeOccurenceOf(int numToCheck, ArrayList<Integer>listToCheck) {
		int counter = 0;
		//step 1, check if the list has the number, if not print 'not in list'
		if(!listToCheck.contains(numToCheck)) {
			System.out.println("not in list");
		}
		//step 2, if the number is in the list, loop to search for the number and add 1 to the counter
		else {
			for(int i=0;i<listToCheck.size();i++) {
				if(listToCheck.get(i) == numToCheck){
					counter++;
				}
			}
		}
		//step 3, print 
		System.out.println("The number "+numToCheck+" occurs "+counter+" times in my list");
	}
}
