package theorem;

import constants_and_basics.RequestInputFromUser;

public class ArrayForWhileloop {
	/*
	 * logical conditions XOR ( ( x || y ) && ! ( x && y ) ) XOR2 (x ^ y) AND x && y
	 * OR x || y
	 * 
	 * switch (x){ case "a": blah break; case "b": blah break; }
	 * 
	 */
	public static void workingdays() {
		DAYS workingday = DAYS.MONDAY;
		switch (workingday) {
		case SATURDAY:
		case SUNDAY:
			System.out.println("Not a workday");
			break;
		default:
			System.out.println("Workday bitch");

		}
	}
	//enum is a enumeration of constant Value
	private enum DAYS{
		MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY;
	} 
	

	public static void doBasicSwitchWithPrimitives() {
		//allowed: byte,enum, short, int, wrapper class byte, short, int, String.
		//not allowed: boolean, long, double float, Boolean or Long,...
		int number = new RequestInputFromUser("number please", "number").getInt();
		switch (number) {
		case 0:
			System.out.println("your number is 0");
			break;
		case 1:
			System.out.println("your number is 1");
			break;
		default:
			System.out.println("this is default");
			System.out.println("your number is " + number);
			break;
		}
	}

	public static void counterFor() {
		int[] inputUserNumber = new int[3];
		for (int i = 0; i < inputUserNumber.length; i++) {
			inputUserNumber[i] = new RequestInputFromUser("number please", "number").getInt();
			if (inputUserNumber[i] != 0) {
				System.out.println(inputUserNumber[i]);
			}
		}
	}

	public static void counterWhile() {
		int[] inputUserNumber = new int[3];
		int counter = 0;
		while (counter < inputUserNumber.length) {
			inputUserNumber[counter] = new RequestInputFromUser("number please", "number").getInt();
			if (inputUserNumber[counter] != 0) {
				System.out.println(inputUserNumber[counter]);
			}
			counter++;
		}
	}
	public static void fibbonachi() {
		System.out.println("The first 20 numbers of Fibonacci are: ");
	
		StringBuilder sb = new StringBuilder();
		int[] fibonacci = new int[20];
		fibonacci[0] = 1;
		fibonacci[1] = 1;
		int number1 = fibonacci[0] + fibonacci[1];
		sb.append(fibonacci[0] + " " + fibonacci[1] + " ");
		for(int i=2;i<fibonacci.length;i++) {
			fibonacci[i] = fibonacci[i-1] + fibonacci [i-2];
			sb.append(fibonacci[i] + " ");
			number1 = number1 + fibonacci[i];
		}
		String result = sb.toString();
		System.out.println(result);
		double number3 = (double) number1/ fibonacci.length;
		System.out.println("The average is " + number3);
	}


	public static void main(String[] args) {
		fibbonachi();
		RequestInputFromUser.closeScanner();
	}
}
