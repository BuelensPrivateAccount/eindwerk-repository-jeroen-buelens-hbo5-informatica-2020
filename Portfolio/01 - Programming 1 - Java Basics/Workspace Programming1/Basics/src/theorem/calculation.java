package theorem;

public class calculation {

	int myValue; //no initialisation (needed) default value = 0
	public /* toegankelijk van buitenaf */ static /*deze methode kan opgeroepen worden zonder instantie van classe te maken*/ void main(String[] args) {
		// declaration
		byte a = 1, b = 2, c;
		int value01 = 14, value02 = 16, sum = value01+value02;
		//running code
		c = (byte) (a + b); // (byte) is necessary to re-convert a and b to byte, else you get a compile
							// error. As Java defaults all smaller numbers to integers.
							// thus we casted it as a byte
		System.out.println("Result equals to " + c);
		System.out.println("uitkomst "+sum);
		calculate2Values(); //calling on the private void below
		System.out.println("\n");
		memeprint.JediSecrets.jediSecrets();//we called upon package MyFirstProgram, class JediSecrets, method main
		System.out.println("\n");
		memeprint.HighIq.highIq();

	}
	private static void calculate2Values() {
		//need be called upon from main
		int	value01=1,value02=2,sum;
		sum=value01+value02;
		System.out.println("hello");
		System.out.println("the sum from the private method "+sum);
	}
}