package theorem;

import constants_and_basics.*;
import memeprint.*;

/* Sequential execution -> gwn doen
   Conditional execution -> if X
   Repetitive execution -> while X
   input op console: scanner [verwekt input in console]
*/
public class CalculatorSimple {
	// variables
	private static double firstOperand;
	private static double secondOperand;

	private static void CalculatingValues() {
		double result;
		
		firstOperand = new RequestInputFromUser("first operand please", "number").getDouble();
		
		String savedOperator = new RequestInputFromUser("operator please", "text").getInput();
		if (savedOperator.equalsIgnoreCase("wortel") || savedOperator.equalsIgnoreCase("square root")
				|| savedOperator.equalsIgnoreCase("v")) {
			result = Math.sqrt(firstOperand);
			System.out.println("The Square Root of " + firstOperand + " = " + result);
			return;
		}
		if (savedOperator.equalsIgnoreCase("Rick") || savedOperator.equalsIgnoreCase("morty")
				|| savedOperator.equalsIgnoreCase("IQ") || savedOperator.equalsIgnoreCase("Pickle")) {
			HighIq.highIq();
			return;
		}
		if (savedOperator.equalsIgnoreCase("allStar")) {
			SmashMouth.smashMouth();
			return;
		}
		if (savedOperator.equalsIgnoreCase("jedi")) {
			JediSecrets.jediSecrets();
			return;
		}
		if (savedOperator.equalsIgnoreCase("bees")) {
			Bees.bees();
			return;
		}
	
		secondOperand = new RequestInputFromUser("second operand please", "number").getDouble();
		while(true) {
		if (savedOperator.equals("+")) {
			result = firstOperand + secondOperand;
			System.out.println(firstOperand + " + " + secondOperand + " = " + result);
			break;
		} else if (savedOperator.equals("-")) {
			result = firstOperand - secondOperand;
			System.out.println(firstOperand + " - " + secondOperand + " = " + result);
			break;
		} else if (savedOperator.equalsIgnoreCase("x") || savedOperator.equals("*")) {
			result = firstOperand * secondOperand;
			System.out.println(firstOperand + " x " + secondOperand + " = " + result);
			break;
		} else if (savedOperator.equals("/")) {
			result = firstOperand / secondOperand;
			System.out.println(firstOperand + " / " + secondOperand + " = " + result);
			break;
		} else if (savedOperator.equals("^")) {
			result = Math.pow(firstOperand, secondOperand);
			System.out.println(firstOperand + " ^ " + secondOperand + " = " + result);
			break;
		} else {
			savedOperator = new RequestInputFromUser("operator please", "text").getInput();
		}}
	}

	public static void main(String[] args) {
		CalculatingValues();
		RequestInputFromUser.closeScanner();

	}

}
