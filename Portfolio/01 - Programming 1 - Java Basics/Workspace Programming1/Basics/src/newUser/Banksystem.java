package newUser;

import java.util.Scanner;

public class Banksystem {
	private static Scanner scanner;
	private static User user;
	private static Account account;
	private static UserManagement userManagement;
	private static int bankrupcy;

	public static void main(String[] args) {
		// start app
		scanner = new Scanner(System.in);
		System.out.println("Welcome, create your account please.");
		// registreren van user
		registerUser();
		// bankrekening aanmaken
		createBankAccount();
		// saldo raadplegen
		checkSaldo();
		//scaner sluiten, om leaks te voorkomen.
		scanner.close();
	}

	public static void checkSaldo() {
		String code = "";
		System.out.println(
				"Welcome with us, your valued and trusted bank.\nTo get your saldo,\nMay we have a pin code for the new account please.");
		code = scanner.next();
		if(!code.equals(account.getCode())) {
			if(bankrupcy > 2) { 
				System.out.println("you have entered your pin wrong too much");
				return;
			}else {
				System.out.println("you have entered your pin wrong, please try again.\nYou have " + (2 - bankrupcy) + " tries left\n");
				bankrupcy++;
				checkSaldo();
			}
			
		}else {
		System.out.println("your account has: " + account.getSaldo() + " in your checking account\n");
		System.out.println("do you want to make a deposit or retrieve money?\n");
		while (true) {
			String answer1 = scanner.next();
			if (answer1.equalsIgnoreCase("Yes")) {
				System.out.println("Do you want to deposit or retrieve?");
				while(true) {
					String answer2 = scanner.next();
					if(answer2.equalsIgnoreCase("deposit")) {
						System.out.println("how much do you want to deposit");
						double saldo = addsaldo() + account.getSaldo();
						account.setSaldo(saldo);
						System.out.println("deposit successful, you now have " + account.getSaldo() + " in your account.");
						break;
					}
					else if(answer2.equalsIgnoreCase("retrieve")) {
						double saldo = account.getSaldo()- addsaldo();
						if (saldo>0) {
						account.setSaldo(saldo);
						System.out.println("retrieval successful, you now have " + account.getSaldo() + " in your account.");
						}else {
							System.out.println("you do not have eneugh funds.");
						}
						break;
					}
					else {
						System.out.println("please enter \"Deposit\" or \"Retrieve\"");
					}
					
				}
				return;
			} else if (answer1.equalsIgnoreCase("No")) {
				return;
			} else {
				System.out.println("please enter \"Yes\" or \"No\"");
			}
		}
		}
	}

	private static void createBankAccount() {
		scanner = new Scanner(System.in);
		// TODO Auto-generated method stub
		// create account
		// code must be entered by user
		// check of die 4 lang is
		String code = "";
		System.out.println(
				"\nWelcome with us, your new bank.\nPlease create a checking account.\n\nMay we have a pin code for the new account please.");
		code = scanner.next();
		if (code.length() != 4) {
			System.out.println("Your pin code was not up to form,\nPlease enter a 4 digit pin code.");
			createBankAccount();
			// melding and rego
		}
		System.out.println("do you want to make a starting deposit");
		while (true) {

			String answer = scanner.next();
			if (answer.equalsIgnoreCase("Yes")) {
				double saldo = addsaldo();
				account = new Account(code, saldo);
				user.setAccount(account);
				return;
			} else if (answer.equalsIgnoreCase("No")) {
				account = new Account(code);
				user.setAccount(account);
				return;
			} else {
				System.out.println("please enter \"Yes\" or \"No\"");
			}
		}
		// ask user to deposit money
		// if yes

		// if no
		// add account to user.}

	}

	private static double addsaldo() {
		System.out.println("How much do you want to deposit");
		if(scanner.hasNextDouble()) {
			return scanner.nextDouble();
		}
		System.out.println("you did not deposit a valid sum, we added 0.0 to your account.");
		return 0.0;
	}

	private static void registerUser() {
		scanner = new Scanner(System.in);
		// TODO Auto-generated method stub
		String surName;
		String firstName;
		String email;
		userManagement = new UserManagement();
		System.out.println("Please enter your Surname: ");
		surName = scanner.nextLine();
		System.out.println("Please enter your first name: ");
		firstName = scanner.nextLine();
		System.out.println("Please enter your email adress: ");
		email = scanner.next();
		if (email.contains("@") && email.substring(email.indexOf("@")).contains(".")
				&& !email.substring(email.indexOf("@") + 1).contains("@") && !surName.equals("")
				&& !firstName.equals("")) {
			user = new User(surName, firstName, email);
			userManagement.saveUsers(user);
			System.out.println("Thanks " + firstName + " for creating a user account\n");
		} else {
			System.out.println("one or more inputs were empty.\nOr the email wasn't correct");
			registerUser();
		}

	}

}
