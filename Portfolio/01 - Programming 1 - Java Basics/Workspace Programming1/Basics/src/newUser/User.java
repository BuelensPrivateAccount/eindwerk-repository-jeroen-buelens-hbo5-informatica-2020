package newUser;

public class User {
       private String name, firstName, email;
       private Account account;
       
       public User(String name, String firstName, String email, Account account) {
             if(validateData(firstName, name, email)) {
                    this.firstName = firstName;
                    this.name = name;
                    this.email = email;
                    this.account = account;
             }
       }
       
       public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public User(String name, String firstName, String email) {
             this(name, firstName, email, null);
       }
       
       private boolean validateEmail(String email) {
             if(email.indexOf('@') != -1 
             && email.indexOf('.') != -1) {
                    return true;
             } else {
                    return false;
             }
       }
       private boolean validateName(String name) {
             if(name != "") {
                    return true;
             } else {
                    return false;
             }
       }
       private boolean validateFirstName(String firstName) {
             if(firstName != "") {
                    return true;
             } else {
                    return false;
             }
       }
       private boolean validateData(String firstName, String name, String email) {
             if(this.validateEmail(email)) {
                    if(this.validateFirstName(firstName) && this.validateName(name)) {
                           return true;
                    } else {
                           return false;
                    }
             } else {
                    return false;
             }
       }
       
       public String getName() {
             return name;
       }

       public void setName(String name) {
             if(validateName(name)) {
                    this.name = name;
             }
       }

       public String getFirstName() {
             return firstName;
       }

       public void setFirstName(String firstName) {
             if(validateFirstName(firstName)) {
                    this.firstName = firstName;
             }
       }

       public String getEmail() {
             return email;
       }

       public void setEmail(String email) {
             if(validateEmail(email)) {
                    this.email = email;
             }
       }
}
