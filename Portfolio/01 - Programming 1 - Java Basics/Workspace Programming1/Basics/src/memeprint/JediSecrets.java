package sept19.jeroen.work;
public class JediSecrets{
	public static void jediSecrets(){
		System.out.println("Did you ever hear the tragedy of Darth Plagueis the Wise?\nI thought not. It's not a story the Jedi would tell you." + "\n" + "It's a Sith legend. Darth Plagueis was a Dark Lord of the Sith, so powerful and so wise he could use the Force to influence the midichlorians to create life..." + "\n" + "He had such a knowledge of the dark side that he could even keep the ones he cared about from dying." + "\n" + "The dark side of the Force is a pathway to many abilities some consider to be unnatural." + "\n" + "He became so powerful... the only thing he was afraid of was losing his power, which eventually, of course, he did." + "\n" + "Unfortunately, he taught his apprentice everything he knew, then his apprentice killed him in his sleep." + "\n" + "It's ironic he could save others from death, but not himself.");
}
	
}
