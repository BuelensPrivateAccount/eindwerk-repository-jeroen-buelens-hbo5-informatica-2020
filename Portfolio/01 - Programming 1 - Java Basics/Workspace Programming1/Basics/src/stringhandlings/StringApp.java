package stringhandlings;


import java.util.Arrays;
import java.util.Scanner;

public class StringApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sn = new Scanner(System.in);
		System.out.println("Geef een woord.");
		String input = sn.nextLine();
		if (stringIsPalindrome(input)) {
			System.out.println(input + " is een Palindrome");
		} else {
			System.out.println(input + " is not een Palindrome");
		}
		System.out.println("geef een zin");
		input = sn.nextLine();
		System.out.println("geef een woord om te checken dat het in de vorige zin zat.");
		String input2 = sn.nextLine();
		if (CheckcontentsString(input, input2)) {
			System.out.println("\"" + input + "\" contains \"" + input2 + "\"");
		} else {
			System.out.println("Error 404 " + input2 + " was not found in " + input);
		}
		System.out.println("geef een aantal woorden in, gescheiden door comma's");
		input = sn.nextLine();
		String[] slist = splittedString(input);
		for(int i=0;i<slist.length;i++) {
			System.out.println(slist[i]);
		}
		System.out.println("geef een zin.");
		input = sn.nextLine();
		System.out.println("geef een stuk van de vorige zin dat vervangen moet worden.");
		input2 = sn.nextLine();
		System.out.println("geef me de vervanging.");
		String input3 = sn.nextLine();
		System.out.println(replaceAPart(input, input2, input3));
		sn.close();

	}

	public static boolean stringIsPalindromewithStringbuilder(String s) {
		StringBuilder sb = new StringBuilder();
		sb.append(s);
		sb.reverse();
		return s.toLowerCase().equalsIgnoreCase(sb.toString().toLowerCase());
	}

	public static boolean stringIsPalindrome(String s) {
		char[] chars = s.toLowerCase().toCharArray();
		char[] charsreversed = new char[chars.length];
		int i2 = 0;
		for (int i1 = chars.length - 1; i1 >= 0; i1--) {
			charsreversed[i2] = chars[i1];
			i2++;
		}
		String reversed = new String(charsreversed);
		return s.equalsIgnoreCase(reversed);
	}

	public static boolean CheckcontentsString(String s, String v) {
		return s.contains(v);
	}
	
	public static String[] returnArrayOfStringsByBreakingCommas (String s){
		int occuranceAmmount = 0;
		
		for(int i = 0;i<s.length();i++) {
			if(s.charAt(i)==',')
				occuranceAmmount++;
		}
		
		String[] stringList = new String[occuranceAmmount+2];
		stringList[0] = s.substring(0, s.indexOf(',')-1);
		
		for(int i=1;i<occuranceAmmount+1;i++) {
			if(i>1 && i < occuranceAmmount -2) {
			stringList[i] = s.substring(s.indexOf(',',s.indexOf(',' + i))+1,s.indexOf(',',s.indexOf(','+ i+1))-1);
			} else if(i==1){
			 stringList[i] =s.substring(s.indexOf(',')+1,s.indexOf(',',s.indexOf(',' + i))-1);
			}
			else {
				stringList[i]=s.substring(s.indexOf(',',s.indexOf(',' + i))+1,s.length()-1);
			}
		}
		return stringList;
	
	}
	public static String[] splittedString(String s) {
		String[] slist = s.split(",");
		for(int i = 0; i < slist.length;i++) {
			slist[i] = slist[i].trim();
		}
		Arrays.sort(slist);
		return slist;
	}
	public static String replaceAPart (String s, String target,String replacement) {
		String a = s.toLowerCase().replace(target.toLowerCase(), replacement.toLowerCase());
		return a;
		
	}

}
