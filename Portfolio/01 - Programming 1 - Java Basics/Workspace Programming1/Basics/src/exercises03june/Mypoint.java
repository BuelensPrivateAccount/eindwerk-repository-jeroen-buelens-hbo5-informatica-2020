package exercises03june;

public class Mypoint {
	private int x;
	private int y;

	public Mypoint() {
		x = 0;
		y = 0;
	}

	public Mypoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int[] getXY() {
		int[] spitout = new int[2];
		spitout[0] = this.x;
		spitout[1] = this.y;
		return spitout;
	}

	public double distance(Mypoint p2) {
		return distance(p2.x, p2.y);
	}

	public double distance(int x, int y) {
		return Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2));
	}

	@Override
	public String toString() {
		return ("[ " + this.x + " , " + this.y + " ]");
	}

}
