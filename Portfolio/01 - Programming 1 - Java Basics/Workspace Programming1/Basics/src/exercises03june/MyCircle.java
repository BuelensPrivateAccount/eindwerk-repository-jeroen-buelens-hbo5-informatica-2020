package exercises03june;

public class MyCircle {
	private Mypoint center = new Mypoint();
	private int radius;

	public MyCircle(Mypoint point, int radius) {
		this(point.getX(), point.getY(), radius);
	}

	public MyCircle(int x, int y, int radius) {
		this.center = new Mypoint(x, y);
		this.setRadius(radius);
	}

	public double getArea() {
		return Math.pow(this.radius, 2) * Math.PI;
	}

	public Mypoint getCenter() {
		return center;
	}

	public void setCenter(int x, int y) {
		this.center = new Mypoint(x, y);

	}

	public void setCenter(Mypoint point) {
		this.setCenter(point.getX(), point.getY());
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = Math.abs(radius);
	}

	public double getAreaOverLap2Circles(MyCircle circle) {

		double hypotamus = Math.hypot(circle.center.getX() - this.center.getX(), circle.center.getX() - this.center.getY());

		if (hypotamus < this.radius + circle.radius) {

			double x = (this.radius * this.radius - circle.radius * circle.radius + hypotamus * hypotamus)
					/ (2 * hypotamus);
			double y = Math.sqrt(this.radius * this.radius - x * x);

			if (hypotamus < Math.abs(circle.radius - this.radius)) {
				return Math.PI * Math.min(this.radius * this.radius, circle.radius * circle.radius);
			}
			return this.radius * this.radius * Math.asin(y / this.radius)
					+ circle.radius * circle.radius * Math.asin(y / circle.radius)
					- y * (x + Math.sqrt(x * x + circle.radius * circle.radius - this.radius * this.radius));
		}
		return 0.0;

	}

	@Override
	public String toString() {
		return "Circle @ ( " + center.getX() + " , " + center.getY() + " ) radius= " + radius;
	}
}































