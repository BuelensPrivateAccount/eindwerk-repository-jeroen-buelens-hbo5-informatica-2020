package constants_and_basics;

public class PrintConstants {
public static final String LINE = getLine(30, "-");
public static final String DOUBLE_LINE = getLine(30, "=");
public static final String NEW_LINE = "\n";
public static final String COMMA = ",";
public static final String DOT = ".";
public static final String TAB = getLine(8, " ");
public static final String ANDSYMBOL = " & ";
public static final String EQUALSYMBOL = " = ";
public static final String ARROW = "-->";
public static final String DOTDOTDOT = getLine(3, DOT);
public static final String SPACE = " ";
public static final double KILOMETERS_PER_MILE = 1.609;
public static final double POUNDS_PER_KILO = 0.45359237;
public static final double INCH_PER_METER =  0.0254;


public static String getLine(int number,String string) {
	StringBuilder sb = new StringBuilder();
	for(int i=1;i<=number;i++) {
		sb.append(string);
	}
	String result = sb.toString();
	return result;
}



}
