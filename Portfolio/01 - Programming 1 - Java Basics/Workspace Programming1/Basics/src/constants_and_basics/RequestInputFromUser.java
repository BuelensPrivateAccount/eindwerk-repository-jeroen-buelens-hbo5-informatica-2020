package constants_and_basics;

import java.util.Scanner;

public class RequestInputFromUser {
	private static Scanner requestStringFromUser = new Scanner(System.in).useDelimiter("\n");
	private static Scanner requestNumberFromUser = new Scanner(System.in);
	private static String errorMessage;
	private double inputteddouble;
	private String inputtedString;

	public RequestInputFromUser(String message, String type) {
		if (type.equalsIgnoreCase("number")) {
			inputteddouble = inputDouble(message);
			inputtedString = null;
		}
		if (type.equalsIgnoreCase("string") || type.equalsIgnoreCase("text")) {
			inputtedString = inputString(message);
		}
	}

	private static Double inputDouble(String message) {
		while (true) {
			System.out.println(message);
			boolean isDouble = requestNumberFromUser.hasNextDouble();
			if (!isDouble) {
				errorMessage = ("Please enter a Valid input, \"" + requestNumberFromUser.next()
						+ "\" is not a valid input...");
				System.out.println(errorMessage);
			} else {
				return requestNumberFromUser.nextDouble();
			}
		}
	}

	private static String inputString(String message) {
		System.out.println(message);
		return requestStringFromUser.next();
	}

	public static void closeScanner() {
		if (requestStringFromUser != null) {
			requestStringFromUser.close();
		}
		if (requestNumberFromUser != null) {
			requestNumberFromUser.close();
		}
	}
	public String getInput() {
		if(inputtedString == null) {
		return Double.toString(inputteddouble);
		}
		else {
			return inputtedString;
		}
	}
	public double getDouble() {
		try {
		return Double.parseDouble(getInput());
		}catch(Exception e) {
			System.out.println("your string can't be transformed to a double");
			return 0.0;
		}
	}
	
	public int getInt() {
		try {
			return (int) Double.parseDouble(getInput());
			}catch(Exception e) {
				System.out.println("your string can't be transformed to a integer");
				return 0;
			}
		
	}
}
