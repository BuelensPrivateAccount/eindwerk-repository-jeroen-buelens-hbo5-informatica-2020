package basicobjectoriented;

public class Pet {
	private String name;
	private String type;
	private int age;
	private boolean hasAldreadyParsedGrewup = false;

	public Pet(String name, String type) {
		this.name = name;
		this.type = type;

	}

	public String makeNoise() {
		if(type.equals("dog")) {
			return "Woef";	
		}
		else if(type.equals("cat")) {
			return "miaow";
		}
		else { 
			return"";
		}
		
	}
	public void dogGrewUp() {
		if(!hasAldreadyParsedGrewup) {
		if (age>2) {
			name = name.substring(0, (name.length() - 4)); //TJE = 3 letters & the extra one as length goes form 1-x and index goes from 0-x
			hasAldreadyParsedGrewup = true;
		}
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
