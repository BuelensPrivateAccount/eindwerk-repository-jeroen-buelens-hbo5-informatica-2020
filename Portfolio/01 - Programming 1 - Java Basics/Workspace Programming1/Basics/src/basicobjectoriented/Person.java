package basicobjectoriented;

/*
 * Order of a class:
 * variablen
 * methoden
 * setters/setters
 * tostring	
 */

public class Person {
	private String name;
	private int age;
	private Pet pet;
	private Adress adress;

	public Person() {
		this.name = "John Doe";
		this.adress = new Adress();
		this.age = 0;
		
	}

	public Person(String name, Adress adress, int age) { // constructor, returns itself & has the same name than the class
		this.name = name; // this slaagt op de classe, de name/adress/age van this classe
		this.adress = adress;
		this.age = age;

	}
	public Person(String name, Adress adress, int age, Pet pet) {
		this(name,adress,age); //constructor overloading, expanding another contstructor with string string int to also have pet
		this.pet = pet;
	}

	public void talk() {
		System.out.println("talking");
		if (this.pet!=null) {
			System.out.println(this.pet.makeNoise());
		}
	}

	public void sleep() {

	}

	public void rave() {

	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getAge() {
		return this.age;
	}

	public Pet getPet() {
		return pet;
	}

	public void setPet(Pet pet) {
		this.pet = pet;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", adress=" + adress + ", age=" + age + "]";
	}
	

}
