package basics;

import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;

public class Basics1 {
	static int counter;

	public static void main(String[] args) {
		YearMonth month = YearMonth.of(2037, Month.OCTOBER);
		printCalender(month);
		String s = "HeLlO wOrLd";
		System.out.println(countlowercase(s));
	}

	public static void printCalender(YearMonth month) {
		LocalDate date = LocalDate.of(month.getYear(), month.getMonth(), 1);
		StringBuilder sb = new StringBuilder();
		int counter = 0;
		for (int index1 = 0; index1 < (date.getDayOfWeek().getValue() - 7) * -1; index1++) {
			sb.append("\t");
			counter++;
		}
		for (int index2 = 0; index2 < month.lengthOfMonth(); index2++) {
			sb.append(index2 + 1);
			counter++;
			if (counter % 7 == 0) {
				sb.append("\n");
			} else {
				sb.append("\t");
			}
		}
		System.out.println("Sun\tMon\tTue\t Wed\tThu\tFri\tSat");
		System.out.println(sb.toString());
	}

	public static int countlowercase(String s) {
		s.chars().forEach(c -> {
			if(Character.isLowerCase((char)c)) {
				counter++;
			};
		});
		return counter;
	}

}
