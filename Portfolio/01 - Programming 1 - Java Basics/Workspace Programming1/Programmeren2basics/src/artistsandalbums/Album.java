package artistsandalbums;

import java.util.ArrayList;

public class Album {
	private String name;
	private ArrayList<Track> tracks;
	private ArrayList<Artist> artists;
	
	public Album(String name, ArrayList<Track> tracks, ArrayList<Artist> artists) {
		this.name = name;
		this.tracks = tracks;
		this.artists = artists;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Track> getTracks() {
		return tracks;
	}

	public void setTracks(ArrayList<Track> tracks) {
		this.tracks = tracks;
	}

	public ArrayList<Artist> getArtists() {
		return artists;
	}

	public void setArtists(ArrayList<Artist> artists) {
		this.artists = artists;
	}
	

}
