package artistsandalbums;

import java.util.ArrayList;

public class Artist {
	private String name;
	private ArrayList<String> members = new ArrayList<String>();
	private String location;
	
	public Artist(String name, String location) {
		this.name = name;
		this.location = location;
	}

	public Artist(String name, ArrayList<String> members, String location) {
		this.name = name;
		this.members = members;
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<String> getMembers() {
		return members;
	}

	public void setMembers(ArrayList<String> members) {
		this.members = members;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	

}
