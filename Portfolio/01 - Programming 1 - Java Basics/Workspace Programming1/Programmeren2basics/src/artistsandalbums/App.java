package artistsandalbums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class App {
	static ArrayList<Album> listOfAlbums = new ArrayList<Album>();
	static ArrayList<Artist> listOfArtists = new ArrayList<Artist>();

	public static void main(String[] args) {
		createData();
//		ArrayList<String> listToUse = new ArrayList<String>();
//		listOfArtists.forEach(a -> {
//			StringBuilder sb = new StringBuilder();
//			sb.append(a.getLocation() + " ");
//			a.getMembers().forEach(s -> {
//				sb.append(s + " ");
//
//			});
//			listToUse.add(sb.toString());
//		});
//		listToUse.forEach(m -> {
//			System.out.println(m);
//		});
		
	System.out.println(listOfArtists.stream().map((artist)->{
		return artist.getMembers() + artist.getLocation();
	}).collect(Collectors.toList()));
	}

	public static void createData() {
		Artist beatles = new Artist(
				"The Beatles", new ArrayList<String>(Arrays.asList("Jhon Lennon", "Paul McCartney", "Ringo starr",
						"George Harrison", "Pete Best", "Tommy Moore", "Jimmy Nicol", "Normal Chapman", "Chas Newby")),
				"Liverpool");
		listOfArtists.add(beatles);
		ArrayList<Track> tracklist = new ArrayList<Track>();
		for (int i = 0; i < 14; i++) {
			tracklist.add(new Track("track" + i));
		}

		listOfAlbums.add(new Album("Please, Please, Me", tracklist, new ArrayList<Artist>(Arrays.asList(beatles))));
		for (int i = 0; i < 10; i++) {
			listOfAlbums.add(new Album("Album " + (i + 1), tracklist, new ArrayList<Artist>(Arrays.asList(beatles))));
		}
		for (int i = 0; i < 56; i++) {
			listOfArtists.add(new Artist("Artist " + (i + 1), "Hell"));
		}
	}

}
