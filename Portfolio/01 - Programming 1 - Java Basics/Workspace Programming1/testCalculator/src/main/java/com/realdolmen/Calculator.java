package com.realdolmen;


public class Calculator {
    private int getal1;
    private int getal2;

    public Calculator(int getal1, int getal2) {
        this.getal1 = getal1;
        this.getal2 = getal2;
    }

    public int addition() throws Exception {
        long sum = (long)getal1 + (long)getal2;
        if (sum < (long)Integer.MAX_VALUE) {
            return getal1 + getal2;
        } else {
            throw new Exception("Input out of range");
        }


    }

    public double division() throws ArithmeticException {
        if(getal2 ==0){
            throw new ArithmeticException();
        }else{
        return getal1 / getal2;}
    }



    public double power() throws Exception{
        if (Math.pow(getal1,getal2)>Integer.MAX_VALUE){
            throw new Exception("number is too damn high");
        } else{
            return Math.pow(getal1,getal2);
        }

    }
}
