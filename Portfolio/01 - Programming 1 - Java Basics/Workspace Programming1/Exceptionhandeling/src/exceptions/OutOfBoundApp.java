package exceptions;

public class OutOfBoundApp {
	// This is stupid, but it can happen when you don't pay attention

	public static void main(String[] args) {
		// catch me if you can
		String[] randomArray = { "Ssmmm", "mskjdfmkl", "smkdjf", "skmljf" };
		printArray(randomArray);
	}

	private static void printArray(String[] stringArray) {

		for (int i = 0; i < (stringArray.length + 1); i++) {
			try {
				System.out.println(stringArray[i]);
			} catch (ArrayIndexOutOfBoundsException e) {
				System.err.println("Index out of bound the current itteration: " + i
						+ " is bigger the last valid index: " + (stringArray.length - 1));
			}
		}
	}

}
