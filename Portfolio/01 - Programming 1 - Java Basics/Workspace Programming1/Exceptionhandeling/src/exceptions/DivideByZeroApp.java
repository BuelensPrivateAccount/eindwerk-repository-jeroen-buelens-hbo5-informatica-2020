package exceptions;

public class DivideByZeroApp {
	// Can you divide by zero??

	public static void main(String[] args) {
		// catch me if you can

		System.out.println(divide(15, 0));
	}

	public static double divide(int x, int y) {
		try {
			return x / y;
		} catch (Exception e) {
			System.err.println("Thou can't divide by zero, don't try to be chuck norris");
			return 0;
		}
	}
}
