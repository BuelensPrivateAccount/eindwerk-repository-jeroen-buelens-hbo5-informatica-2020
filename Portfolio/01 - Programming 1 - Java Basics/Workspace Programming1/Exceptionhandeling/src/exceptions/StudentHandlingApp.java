package exceptions;

public class StudentHandlingApp {

	
	public static void main(String[] args) {
		try{
			jeroen();
		} catch(BallException e) {
			System.err.println(e);
		}
	}
	
	
	private static void yorick() throws BallException  {
		steven();
	}
	
	private static void tim() throws BallException  {
		adel();
	}
	
	private static void alexander() throws BallException  {
		seda();
	}
	
	private static void jeroen() throws BallException {
		youssef();
	}
	
	private static void yoni() throws BallException  {
		arneWauters();
	}
	
	private static void arneLens() throws BallException  {
		yorick();
	}
	
	private static void maxime() throws BallException {
		alexander();
	}
	
	private static void stijn() throws BallException {
		arneLens();
	}
	
	private static void devin()throws BallException  {
		illyas();
	}
	
	private static void arneWauters() throws BallException  {
		maxime();
	}
	
	private static void seda() throws BallException  {
		stijn();
	}
	
	private static void illyas() throws BallException  {
		moushine();
	}
	
	private static void moushine() throws BallException {
		tim();
	}
	
	private static void adel() throws BallException  {
		yoni();
	}
	
	private static void youssef()throws BallException  {
		devin();
	}
	
	private static void steven() throws BallException {
		throw new BallException ("Ball thrown");
	}
}
