package realworld_example;

public class QueryHelper {
	
	public static String createQueryWithError() {
		return "select * from personss";

	}

	public static String createQueryWhereFirstNameLike(String firstName) {
		return "select * from person where firstname like '" +firstName+"%'";
	}
	
	public static String createQueryWhereLastNameLike(String lastName) {
		return "select * from person where lastname like '" +lastName+"%'";
	}
	
	public static String createQueryAll() {
		return "select * from person";
	}
	
	public static String createQueryFindById(int id) {
		return "select * from person where id = "+id;
	}
}
