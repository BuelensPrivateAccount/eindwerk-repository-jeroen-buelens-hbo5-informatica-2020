package realworld_example;

public class Person {
	private Integer id;
	private String name;
	private String firstName;
	
	public Person(Integer id, String name, String firstName) {
		this.id = id;
		this.name = name;
		this.firstName = firstName;
	}

	public Person() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", firstName=" + firstName + "]";
	}
	
	
}
