package realworld_example;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class PersonRepository {
	
	public List<Person> findAllPersonsWhere(String query) throws MySqlException{
		List<Person> persons = new ArrayList<>();
		Connection con = ConnectionsManager.getConnection();
		Statement stmnt = null;
		try {
			stmnt = con.createStatement();
			ResultSet result = stmnt.executeQuery(query);
			while(result.next()) {
				persons.add(new Person(result.getInt("id"),result.getString("firstname"),result.getString("lastname")));
			}
		} catch (SQLException e) {
			//If something goes wrong, this method will print the full stack trace 
			//So when you write a full stack java program, the stack trace with the error will
			//go throughout every method in every layer of your application that is used to get this data
			//the error stack trace will go back to the top level of your application, and that is usually 
			//the client side of front end of your application and a lot of information about your database will be
			//shown  
			e.printStackTrace();
			throw new MySqlException();
		}
		
		return persons;
	}
	
	public int insertPerson(Person p) throws MySqlException {
		Connection con = ConnectionsManager.getConnection();
		Statement stmnt = null;
		try {
			stmnt = con.createStatement();
			String pQuery = "INSERT INTO person (firstname, lastname) VALUES ('" + p.getFirstName() + "', '"
					+ p.getName() + "')";
			System.out.println(pQuery);
			stmnt.execute(pQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet rsPerson = stmnt.getGeneratedKeys();
			while (rsPerson.next()) {
				p.setId(rsPerson.getInt(1));
			}
			return p.getId();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new MySqlException();
		}
	}
	
}
