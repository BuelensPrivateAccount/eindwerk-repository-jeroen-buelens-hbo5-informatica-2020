package exercises;

public class NumberUserInputException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4234030364383089204L;

	public NumberUserInputException(String message) {
		super(message);

	}

	public NumberUserInputException() {
	}

	// TODO Ex.1b geef een gepaste message (string) mee aan de constructor, gebruik
	// de correcte
	// super constructor om de message door te geven

}
