package exercises;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserInputMismatchEx {

	private static Scanner scan;

	public static void main(String[] args) {
		// TODO 2.c vang de nodige exceptions of die door de methode startApp
		// aangeduid worden als checked Exception
		try {
			startApp();
		}
		catch(NumberUserInputException e){
			System.err.println(e.getMessage());
		}
	}

	// TODO ex.1 Wanneer de gebruiker geen getal ingeeft zal er iets gebeuren
	// probeer de exception te behandelen en geef de gebruiker de
	// kans om opnieuw een getal in te geven.
	private static void startApp() throws NumberUserInputException {
		scan = new Scanner(System.in);
		System.out.println("Geef een getal tussen 0 en 10 ");
		// TODO ex.2a Maak een class NumberUserInputException (reeds gemaakt) aan die
		// extends van Exception,
		// zorg dan dat wanneer het getal niet tussen 0 en 10 ligt er een new
		// NumberUserInputException gegooid
		// wordt.
		try {
			int i = scan.nextInt();
			if(i <0 || i>10) {
				throw new NumberUserInputException("User did not enter a number bewteen 0 and 10");
			}
			System.out.println("het getal is " + i);
		} catch (InputMismatchException e) {
			System.err.println("user did not enter a number\n");
			startApp();
	}

}
}
