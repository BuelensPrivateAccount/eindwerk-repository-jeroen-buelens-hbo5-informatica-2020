package exercises;

public class NullPointerApp {

	public static void main(String[] args) {
		//pas dit niet aan
		doSomethingCool(null);
	}
	
	//TODO Ex.3 Run de app, er gebeurd iets niet zo cool, zorg dat alles een goed verloop krijgt 
	//Maak dat je zowieso, de lijn 'bedankt voor het gebruiken van de app'. Tip : gebruik finally
	private static void doSomethingCool(String s) {
		try {
		System.out.println("Hello "+s.toUpperCase());
		}catch(NullPointerException e) {
			System.err.println("You entered a null, please don't");
		}finally {
			System.out.println("\nThanks for using the app.");
		}
		
	}

}
