package abstractentities;
/*
 * This game was made and developped by Buelens Jeroen.
 */

public abstract class Npc extends Entity {
	protected String npcTag = "Enemy";
	
	public int dealDamage() {
		return rng(damagemin, damagemax);

	}
	public void takeDamage(int damage) {
		this.hp -= damage;
		if (this.hp > 0) {
			System.out.println("the "+ npcTag +" took " + damage + " damage, he has " + this.hp + " left");
		}
	

}
}
