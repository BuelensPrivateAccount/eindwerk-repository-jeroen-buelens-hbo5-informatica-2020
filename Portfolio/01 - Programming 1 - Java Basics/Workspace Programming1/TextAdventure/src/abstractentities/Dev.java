package abstractentities;

/*
 * this is developper, intended to test encounters.
 */
public class Dev extends PlayerCharacter {
	public Dev() {
		super();
		super.name = "GOD";
		super.hp = 999999;
		super.maxHp = super.hp;
		super.damagemin = 999999;
		super.damagemax = 999999;
		}


	@Override
	public int dealDamage() {
		return 999999;
	}
	@Override
	public void takeDamage(int damage) {
		System.out.println("you are God and are immune to damage.");
	}
	@Override
	public void levelUp() {
	}
		

}
