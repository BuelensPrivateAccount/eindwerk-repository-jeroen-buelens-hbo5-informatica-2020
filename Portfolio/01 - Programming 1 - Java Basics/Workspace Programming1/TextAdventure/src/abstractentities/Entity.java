package abstractentities;
/*
 * This game was made and developped by Buelens Jeroen.
 */

import engine.Rng;
import interfaces.Action;

public abstract class Entity extends Rng implements Action{
	protected String name;
	protected int damagemin;
	protected int damagemax;
	protected int hp;
	
	public int rng(int damagemin, int damagemax) {
		return Rng.RandomInteger(damagemin, damagemax);
		
	}
	


	public int getHp() {
		return hp;
	}

	public int getDamagemin() {
		return damagemin;
	}

	public int getDamagemax() {
		return damagemax;
	}
	public String getName() {
		return name;
	}



	public void setHp(int hp) {
		this.hp = hp;
	}
}
