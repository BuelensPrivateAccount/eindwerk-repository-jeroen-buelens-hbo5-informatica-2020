package abstractentities;
/*
 * This game was made and developped by Buelens Jeroen.
 */

public abstract class PlayerCharacter extends Entity {
	protected int potions;
	protected int maxHp;
	
	public String playerTag = "You";

	public PlayerCharacter() {
		this.potions = 0;
	}

	public void levelUp() {
		super.damagemax += rng(1, 3);
		super.damagemin += rng(0, 1);
		if(damagemin > damagemax) {
			int tempStorage = damagemin;
			damagemin = damagemax;
			damagemax = tempStorage;
		}
		int hpbonus = rng(1, 4);
		this.maxHp += hpbonus;
		super.hp += hpbonus;

	}

	public void heal() {
		if (potions > 0) {
			System.out.println("you drink a potion");
			int rng = rng(5,15);
			super.hp += rng;
			if (super.hp > maxHp) {
				super.hp = maxHp;
				System.out.println("the potion healed you to full health, you got "+this.potions+ " left.");
			} else {
				System.out.println("the potion was revigorating, you healed for " +rng+" Health, you got "+this.potions+ " left.");
			}
		} else {
			System.out.println("you don't have any potions left.");
		}

	}

	public void loot() {
		if (rng(0, 1) == 1) {
			int potionfound = rng(1, 2);
			System.out.println("you find " + potionfound + " potions");
			this.potions += potionfound;
		}

	}

	public void takeDamage(int damage) {
		this.hp -= damage;
		if (this.hp > 0) {
			System.out.println(playerTag + " took " + damage + " damage, " + playerTag + " have " + this.hp + " left");
		}

	}

	public int getPotions() {
		return potions;
	}

	public int getStartHp() {
		return maxHp;
	}


}
