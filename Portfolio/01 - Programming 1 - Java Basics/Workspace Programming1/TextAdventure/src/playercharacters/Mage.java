package playercharacters;
/*
 * This game was made and developped by Buelens Jeroen.
 */

import abstractentities.PlayerCharacter;

public class Mage extends PlayerCharacter {
	private int burndamage;
	
	public Mage() {
		super();
		super.name = "Mage";
		super.damagemin = 1;
		super.damagemax = 6;
		super.hp = 20;
		super.maxHp = super.hp;
	}

	@Override
	public int dealDamage() {
		
			int damage = rng(damagemin, damagemax);
			damage += burndamage(damage);
			return damage;
			
	}
	
	public int burndamage(int damage) {
		int damagedealt = burndamage;
		burndamage = (int) Math.round(damage/3);
		return damagedealt;
	}
	

}
