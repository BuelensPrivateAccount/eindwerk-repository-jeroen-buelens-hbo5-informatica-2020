package playercharacters;

import abstractentities.PlayerCharacter;

public class Knight extends PlayerCharacter {

	
	public Knight() {
		super();
		super.name = "Knight";
		super.damagemin = 1;
		super.damagemax = 8;
		super.hp = 24;
		super.maxHp = super.hp;
	}

	@Override
	public int dealDamage() {
		return rng(damagemin, damagemax);
	}
	

}
