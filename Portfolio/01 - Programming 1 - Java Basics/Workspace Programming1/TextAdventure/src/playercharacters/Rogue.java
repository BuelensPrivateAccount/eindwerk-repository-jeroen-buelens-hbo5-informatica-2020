package playercharacters;

/*
 * This game was made and developped by Buelens Jeroen.
 */
import abstractentities.PlayerCharacter;

public class Rogue extends PlayerCharacter {
	
	public Rogue() {
		super();
		super.name = "Rogue";
		super.damagemin = 1;
		super.damagemax = 4;
		super.hp = 22;
		super.maxHp = super.hp;
	}

	@Override
	public int dealDamage() {
		if (rng(0, 5) ==5) {
			System.out.println("you sneakly doublestabbed!");
			int damage = rng(damagemin, damagemax);
			damage += rng(damagemin, damagemax);
			return damage;
			
		}
		return rng(damagemin, damagemax);
	}
	

}
