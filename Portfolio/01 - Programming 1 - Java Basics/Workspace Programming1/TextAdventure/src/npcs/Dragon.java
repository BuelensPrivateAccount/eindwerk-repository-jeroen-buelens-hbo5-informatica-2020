package npcs;
/*
 * This game was made and developped by Buelens Jeroen.
 */

import abstractentities.Npc;

public class Dragon extends Npc {

	public Dragon() {
		super.name = "Dragon";
		super.hp = 50;
		super.damagemin = 4;
		super.damagemax = 14;
	}

	
}
