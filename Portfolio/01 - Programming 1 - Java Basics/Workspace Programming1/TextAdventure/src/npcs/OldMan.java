package npcs;

import abstractentities.Npc;
import abstractentities.PlayerCharacter;

public class OldMan extends Npc{
	

	public OldMan() {
		super.name = "Old Man";
	}
	
	public void oldManEncounter(PlayerCharacter player) {
		System.out.println("you encountered a old, wise man who trains and heals you.");
		player.setHp(player.getStartHp());
		player.levelUp();
	}

}
