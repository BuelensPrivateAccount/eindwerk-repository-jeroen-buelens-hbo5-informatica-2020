package npcs;
/*
 * This game was made and developped by Buelens Jeroen.
 */

import abstractentities.Npc;

public class BlackKnight extends Npc {

	public BlackKnight() {
		super.name = "Black Knight";
		super.hp = 12;
		super.damagemin = 2;
		super.damagemax = 6;
	}

}
