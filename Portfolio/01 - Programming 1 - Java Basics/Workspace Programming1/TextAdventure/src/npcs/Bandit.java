package npcs;
/*
 * This game was made and developped by Buelens Jeroen.
 */

import abstractentities.Npc;

public class Bandit extends Npc {

	public Bandit() {
		super.name = "Bandit";
		super.hp = 8;
		super.damagemin = 1;
		super.damagemax = 3;
	}

	
}
