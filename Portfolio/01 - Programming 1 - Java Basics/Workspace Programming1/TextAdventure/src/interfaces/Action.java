package interfaces;

public interface Action {
	public int dealDamage();
	public void takeDamage(int damage);
	public int rng(int min, int max);
	
	

}
