package engine;
/*
 * This game was made and developped by Buelens Jeroen.
 */

import java.util.Scanner;
import abstractentities.Dev;
import abstractentities.Npc;
import abstractentities.PlayerCharacter;
import npcs.*;
import playercharacters.*;

public class Engine {
	public static int counter = 1;

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("What character would you like to play?\n Rogue/Mage/Knight.");
		String input = scan.next();
		PlayerCharacter pc = new Knight();
		switch (input.toLowerCase()) {
		case "mage":
			pc = new Mage();
			break;
		case "rogue":
			pc = new Rogue();
			break;
		case "dev":
			pc = new Dev();
			break;
		default:
			pc = new Knight();
		}
		System.out.println(
				"You sit around in the Tavern at the adventurers guild,\na new request came up, the princess was kidnapped & the guild sends out adventurers to save her!\nYou embark on the epic Journey.\n");
		while (true) {
			if (counter > 9 && pc.getName() != "GOD") {
				System.out.println("Congratulations, you saved the princess!");
				scan.close();
				return;
			}
			if (pc.getHp() < 1) {
				System.out.println("GAME OVER");
				scan.close();
				return;
			}
			System.out.println("\nyou are a " + pc.getName());
			System.out.println("your current hp = " + pc.getHp());
			System.out.println("you can deal damage between: " + pc.getDamagemin() + " & " + pc.getDamagemax());
			if (pc.getPotions() != 0) {
				System.out.println("you have " + pc.getPotions() + " in your inventory, use them wisely.");
			}
			System.out.println("\n");

			Npc enemy = new Bandit();
			int randomSeed = Rng.RandomInteger(0, 10);
			if (counter%9 == 0 && counter != 0) {
				enemy = new Dragon();
			} else {
				switch (randomSeed) {
				case 0:
				case 2:
					enemy = new BlackKnight();
					break;
				case 1:
				case 3:
				case 5:
					enemy = new OldMan();
					break;
				default:
					enemy = new Bandit();
				}
			}
			if (enemy.getName().equalsIgnoreCase("Old Man")) {
				((OldMan) enemy).oldManEncounter(pc);
				continue;
			}
			System.out.println("enemy # " + counter);
			if(enemy.getName().equalsIgnoreCase("Dragon")) {
			System.out.println("You're at the kidnappers hideout, you enter their keep and shudder,");
			}
			System.out.println("A " + enemy.getName() + " blocks your progress.");
			
			while (true) {
				System.out.println("What do you want to do?\nAttack/Heal");
				input = scan.next();
				if (input.equalsIgnoreCase("Heal")) {
					pc.heal();
					pc.takeDamage(enemy.dealDamage());
				} else {
					enemy.takeDamage(pc.dealDamage());
					if (enemy.getHp() < 1) {
						System.out.println("Enemy defeated, you can progress!");
						pc.loot();
						randomSeed = Rng.RandomInteger(0, 5);
						if (randomSeed < 2) {
							System.out.println("You leveled up, your stats went up!");
							pc.levelUp();
						}
						break;
					}
					pc.takeDamage(enemy.dealDamage());
				}
			}
			counter++;
		}

	}

}
