package engine;

public abstract class Rng {
	public static int RandomInteger(int min, int max) {
		double randomDouble = Math.random();
		randomDouble = randomDouble * (max * 100) + (min * 100);
		int randomInt = (int) Math.round(randomDouble/100);
		return randomInt;
	}
}
