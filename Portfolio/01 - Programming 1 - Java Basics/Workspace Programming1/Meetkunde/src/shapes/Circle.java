package shapes;

public class Circle extends Shape {
	private double radius;

	public Circle() {
		super();
		this.radius = 1.0;
	}

	public Circle(double radius, String color, boolean filled) {
		super(color, filled);
		this.radius = radius;
	}

	public Circle(double radius) {
		super();
		this.radius = radius;
	}

	@Override
	public double getArea() {
		return Math.pow(this.radius, 2) * Math.PI;
	}

	@Override
	public double getPerimeter() {
		return 2 * Math.PI * this.radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + "] which is a subclass of:\n" + super.toString();
	}

}
