package shapes;

public class Square extends Rectangle {

	public Square() {
		super();
	}

	public Square(double size, boolean isFilled, String color) {
		super(size, size, color, isFilled);
	}

	public Square(double size) {
		super(size, size);
	}

	@Override
	public String toString() {
		return "Square with side= " + super.getLength() + " , which is a subclass of:\n" + super.toString();
	}

	@Override
	public void setLength(double length) {
		setSide(length);
	}

	@Override
	public void setWidth(double width) {
		setSide(width);
	}

	public void setSide(double size) {
		super.setLength(size);
		super.setWidth(size);
	}

	public double getSide() {
		return getLength();

	}

}
