package shapes;

public class ShapeClient {
	
public static void main(String[] args) {
	Shape[] shapes = new Shape[3];
	shapes[0] = new Circle(5);
	shapes[1] = new Square(8);
	shapes[2] = new Rectangle(2, 3);
	for(int i =0;i<shapes.length;i++) {
		System.out.println("the area of "+ shapes[i].toString() +"\n= "+shapes[i].getArea()+"\n");
		System.out.println("the perimeter of "+ shapes[i].toString() +"\n= "+shapes[i].getPerimeter()+"\n");
	}
	
}
}
