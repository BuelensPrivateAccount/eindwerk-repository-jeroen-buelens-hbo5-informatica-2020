package shapes;

import interfaces.Refactor;

public class ResizableSquare extends Square implements Refactor {

	public ResizableSquare() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ResizableSquare(double size, boolean isFilled, String color) {
		super(size, isFilled, color);
		// TODO Auto-generated constructor stub
	}

	public ResizableSquare(double size) {
		super(size);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void resize(int percent) {
		setSide(Math.abs(getSide() * percent * 0.01));
		System.out.println("resized to " + percent + "% of the original size");
	}

}
