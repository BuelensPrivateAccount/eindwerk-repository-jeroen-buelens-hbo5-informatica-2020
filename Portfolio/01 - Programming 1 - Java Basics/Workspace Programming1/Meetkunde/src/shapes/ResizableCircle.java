package shapes;

import interfaces.Refactor;

public class ResizableCircle extends Circle implements Refactor {

	public ResizableCircle() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ResizableCircle(double radius, String color, boolean filled) {
		super(radius, color, filled);
		// TODO Auto-generated constructor stub
	}

	public ResizableCircle(double radius) {
		super(radius);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void resize(int percent) {
		setRadius(Math.abs(getRadius() * (percent / 100)));
		System.out.println("resized to " + percent + "% of the original size");

	}

}
