package Animals;

public class Cat extends Animal {
	private String hairball;

	public Cat(String hairball) {
		this.hairball = hairball;
	}

	@Override
	public void makeNoise() {
		System.out.println("purrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");

	}

	public String getHairball() {
		return hairball;
	}

	public void setHairball(String hairball) {
		this.hairball = hairball;
	}

}
