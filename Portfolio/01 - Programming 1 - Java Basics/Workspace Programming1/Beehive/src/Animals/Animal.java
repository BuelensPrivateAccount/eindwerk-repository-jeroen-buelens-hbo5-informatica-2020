package Animals;

import interfaces.Noisy;

public abstract class Animal implements Noisy{
	public void live() {
		System.out.println("i am a living creature.");
	}

}
