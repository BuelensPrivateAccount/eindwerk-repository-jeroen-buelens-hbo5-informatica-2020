package transport;

import interfaces.Flying;

public class Balloon extends Transport implements Flying{
	private String warmthofAirInBalloon;

	public Balloon(int speed, String warmthofAirInBalloon) {
		super(speed);
		this.warmthofAirInBalloon = warmthofAirInBalloon;
	}
	public Balloon(int speed) {
		super(speed);
		this.warmthofAirInBalloon = "Hot";
	}
	public Balloon() {
		super();
		this.warmthofAirInBalloon = "hot";
	}
	
	public String getWarmthofAirInBalloon() {
		return warmthofAirInBalloon;
	}
	public void setWarmthofAirInBalloon(String warmthofAirInBalloon) {
		this.warmthofAirInBalloon = warmthofAirInBalloon;
	}
	@Override
	public void flying() {
		System.out.println("I'm a balloon and i'm floating");
		
	}
	

}
