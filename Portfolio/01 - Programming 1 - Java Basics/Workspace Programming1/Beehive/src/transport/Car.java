package transport;

import interfaces.Noisy;

public class Car extends Transport implements Noisy{
	private int amountOfWheels;

	public Car(int speed, int amountOfWheels) {
		super(speed);
		this.amountOfWheels = amountOfWheels;
	}
	public Car(int speed) {
		super(speed);
		this.amountOfWheels = 4;
	}
	public Car() {
		super();
		this.amountOfWheels = 4;
	}
	public void makeNoise() {
		System.out.println("Vroem");
	}
	public int getAmountOfWheels() {
		return amountOfWheels;
	}
	public void setAmountOfWheels(int amountOfWheels) {
		this.amountOfWheels = amountOfWheels;
	}
	

}
