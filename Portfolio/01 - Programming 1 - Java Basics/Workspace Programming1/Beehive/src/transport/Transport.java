package transport;

public abstract class Transport {
	private int speed;

	public Transport(int speed) {
		this.speed = speed;
	}

	public Transport() {
		this.speed = 5;
	}

	public void move() {
		System.out.println("i am moving");
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

}
