package transport;

import interfaces.Flying;
import interfaces.Noisy;

public class Plane extends Transport implements Flying,Noisy{
	private int amountOfWheels;

	public Plane(int speed, int amountOfWheels) {
		super(speed);
		this.amountOfWheels = amountOfWheels;
	}
	public Plane(int speed) {
		super(speed);
		this.amountOfWheels = 8;
	}
	public Plane() {
		super();
		this.amountOfWheels = 8;
	}
	
	public void makeNoise() {
		System.out.println("Woosh");
	}
	public int getAmountOfWheels() {
		return amountOfWheels;
	}
	public void setAmountOfWheels(int amountOfWheels) {
		this.amountOfWheels = amountOfWheels;
	}
	@Override
	public void flying() {
		System.out.println("I'm a plane and i'm flying.");
		
	}
	

}
