package bees;

import Animals.Animal;
import interfaces.Flying;

public abstract class Bee extends Animal implements Flying {
	private String color;
	private double size;
	private double wingspan;

	public Bee(String color, double size, double wingspan) {
		this.color = color;
		this.size = size;
		this.wingspan = wingspan;
	}

	public Bee() {
		this.color = "black&yellow";
		this.size = 1.0;
		this.wingspan = 1 / 0;
	}

	public void makeNoise() {
		System.out.println("i am a bee and i go zoom zoom.");
	}
	public void flying() {
		System.out.println("bee is flying");
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public double getWingspan() {
		return wingspan;
	}

	public void setWingspan(double wingspan) {
		this.wingspan = wingspan;
	}
}
