package bees;

public class Warrior extends Bee{
	private double painSting;
	private double speed;
	private int strength;
	
	public Warrior() {
		super();
		this.painSting = 1.0;
		this.speed = 1.0;
		this.strength = 1;
	}
	public Warrior(String color, double size, double wingspan,double painSting,double speed, int strength) {
		super(color, size, wingspan);
		this.painSting = painSting;
		this.speed = speed;
		this.strength = strength;
	}
	public Warrior(double painSting,double speed, int strength) {
		super();
		this.painSting = painSting;
		this.speed = speed;
		this.strength = strength;
	}
	public void attack() {
		System.out.println("attacking enemey");
	}
	public double getPainSting() {
		return painSting;
	}
	public void setPainSting(double painSting) {
		this.painSting = painSting;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}

}
