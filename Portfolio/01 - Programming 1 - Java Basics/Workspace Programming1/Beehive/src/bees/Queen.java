package bees;

public class Queen extends Bee{
	private int eggsperhour;
	private int strength;
	
	public Queen() {
		super();
		this.eggsperhour = 10;
		this.strength = 1;
	}
	public Queen(String color, double size, double wingspan,int capacity, int strength) {
		super(color, size, wingspan);
		this.eggsperhour = capacity;
		this.strength = strength;
	}
	public Queen(int capacity, int strength) {
		super();
		this.eggsperhour = capacity;
		this.strength = strength;
	}
	public void layegg() {
		System.out.println("gathering nectar");
	}
	public int getEggsperhour() {
		return eggsperhour;
	}
	public void setEggsperhour(int eggsperhour) {
		this.eggsperhour = eggsperhour;
	}
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}


}
