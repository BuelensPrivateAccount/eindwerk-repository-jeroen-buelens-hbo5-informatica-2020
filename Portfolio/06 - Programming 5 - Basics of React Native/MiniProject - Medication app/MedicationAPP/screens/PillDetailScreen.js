import React from 'react';
import {Text, View} from 'react-native';

import DefaultStyle from "../constants/default-style";
import Card from "../components/Card";
import color from "../constants/colors"

const PillDetailScreen = props => {
    props.navigation.setOptions({
        title: props.route.params.category.title,
        headerStyle: {
            backgroundColor: colors.background
        }
    })
    ;
/*
basic showing of more data
 */
    return (
        <View styles={DefaultStyle.screen}>
            {props.navigation.setParams({
                headerStyle: {
                    backgroundColor: colors.card
                },
            })}
            <Card>
                <Text>
                    Name: {props.route.params.PILL.pillname}
                </Text>
                <Text>
                    Pill color: {props.route.params.PILL.pillcolor}
                </Text>
                <Text>
                    Active component: {props.route.params.PILL.active_component}
                </Text>
                <Text>
                    Intake
                    Time: {props.route.params.PILL.intake_time}
                </Text>
                <Text>
                    price: {props.route.params.PILL.price}
                </Text>
            </Card>
        </View>

    );

};

export default PillDetailScreen;
