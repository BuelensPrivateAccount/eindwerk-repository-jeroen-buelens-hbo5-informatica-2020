import React, {useEffect, useState} from 'react';
import {FlatList, Text} from 'react-native';

import PillTile from "../components/PillTile";
import color from "../constants/colors";
import {getDB, findAllPills} from "../func/Database";


const HomeScreen = props => {
    const [pills, setPills] = useState([]);
    const DB = getDB();

    /*
     * bootup of page, get all pills
     */

    useEffect(() => {
        (async () => {
            if (pills.length === 0) {
                setPills(await findAllPills(DB));
            }
        })();
    });

/*
render a specific item
 */
    const renderGridItem = (itemData) => {
        return <PillTile
            color={color.background}
            ONPRESS={
                () => props.navigation.navigate(
                    'PILL', {
                        PILL: itemData.item
                    }
                )
            }
        ><Text>
            {itemData.item.name}
        </Text>
            <Text>
                {itemData.item.intake_time}
            </Text>
        </PillTile>;
    };
/*
flatlist usage
 */
    return (
        <FlatList data={pills} renderItem={renderGridItem} numColumns={2}/>
    );

};

export default HomeScreen;
