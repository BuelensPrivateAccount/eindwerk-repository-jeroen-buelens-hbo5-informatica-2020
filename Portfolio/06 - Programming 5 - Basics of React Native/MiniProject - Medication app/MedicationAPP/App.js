import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import * as Font from 'expo-font';
import {AppLoading} from 'expo';

import Colors from './constants/colors';
import AppNavigator from "./navigation/ApplicationNavigator"
import {addPillToMedication, getDB, initDB} from "./func/Database";
import Pill from "./models/Pill";


/*
 * This loads the fonts to be used in the application
 *
 */
const fetchFonts = () => {
    return Font.loadAsync({
        /* here be fonts in the future*/
    });
};

/*
this starts the database, adds in my dummydata, because i coudn't find a better place to do so.
 */
const startDB = async () => {
    let db = getDB();
    await initDB(db);
    let pill1 = new Pill(1,"dafalgan forte","white","paracetamol","9:00","3.44EUR");
    await addPillToMedication(db,pill1);
    let pill2 = new Pill(1,"Seretide Diskus 50ɥg/250ɥg","powder","Salmeterol/Fluticasonpropinaat","9:00 + 19:00","?");
    await addPillToMedication(db,pill2);
};


/*
public static void main
 */

export default function App() {
    /*
    load the database
     */
    const [DB, setDB] = useState(false);
    if (!DB) {
        return (
            <AppLoading
                startAsync={startDB}
                onFinish={() => setDB(true)}
                onError={(err) => console.log(err)}
            />
        );
    }
    return (
        <AppNavigator/>
    );
}
/*
css
 */
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.primary,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
