/**
 * this is the model of pill, what is should contain and what it is, used throughout the application
 */

class Pill {
    constructor(id, pillname, pillcolor, active_component, intake_time, price) {
        this.id = id;
        this.pillname = pillname;
        this.pillcolor = pillcolor;
        this.active_component = active_component;
        this.intake_time = intake_time;
        this.price = price;
    }
}

export default Pill;
