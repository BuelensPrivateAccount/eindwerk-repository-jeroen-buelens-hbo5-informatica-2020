export default {
    primary: "#9BE5AA",
    secondary:"#627264",
    accent:"#B5DFCA",
    background:"#C5E7E2",
    text:"#AD9BAA",
};