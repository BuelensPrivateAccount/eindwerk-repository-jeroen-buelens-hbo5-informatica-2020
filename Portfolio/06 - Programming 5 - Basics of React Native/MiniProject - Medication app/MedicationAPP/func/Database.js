import React from 'react';
import * as SQLite from 'expo-sqlite';
import Pill from "../models/Pill";

/**
 * finds a specific pill
 * @param db
 * @param id
 * @returns {Promise<Pill>}
 */
export const findPillById = async (db, id) => {
    return await runQuery(
        db,
        "SELECT * from pills where id = ?",
        [id]
    )
};
/**
 *finds all pills
 * @param db
 * @returns {Promise<Array Pill>}
 */
export const findAllPills = async (db) => {
    return await runQuery(
        db,
        "select * from pills"
    )
};


export const getDB = () => {
    return SQLite.openDatabase("Medication-db");
};
/**
 * initialising the database
 * from https://docs.expo.io/versions/latest/sdk/sqlite/
 * @param db
 * @returns {Promise<void>}
 */
export const initDB = async (db) => {
    let result = await runQuery(db,
        "create table if not exists pills (id integer primary key autoincrement not null, pillname VARCHAR(255) not null, pillcolor VARCHAR(255),active_component VARCHAR(255) not null,intake_time VARCHAR(255),price VARCHAR(255)); "
    );
};
/**
 adds a pill to the database.
 */
export const addPillToMedication = async (db, pill) => {
    let result = await runQuery(
        db,
        "INSERT INTO pills (pillname,pillcolor,active_component,intake_time,price) VALUES (?,?,?,?,?)",
        [pill.pillname, pill.pillcolor, pill.active_component, pill.intake_time, pill.price]
    );
    console.log("added");
};

/**
 from https://docs.expo.io/versions/latest/sdk/sqlite/, how to run a basic query
 */
const runQuery = async (db, query, params = []) => {
    return new Promise((resolve, reject) => {
        db.transaction(
            transaction => {
                transaction.executeSql(
                    query,
                    params,
                    (_, {rows: {_array}}) => {
                        resolve(_array);
                    }
                )
            },
            error => {
                reject(error);
            }
        );
    });
};

