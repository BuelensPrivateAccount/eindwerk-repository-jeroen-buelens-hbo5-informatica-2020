import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from "../screens/HomeScreen";
import PillDetailScreen from "../screens/PillDetailScreen";

const PillNavigator = createStackNavigator();

function MyMealsNavigator(){
    return(
        <PillNavigator.Navigator>
            <PillNavigator.Screen name="Home" component={HomeScreen}/>
            <PillNavigator.Screen name="PillDetail" component={PillDetailScreen}/>
        </PillNavigator.Navigator>
    )
}
export default MyMealsNavigator;
