import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Ionicons} from '@expo/vector-icons';
import PillNavigator from '../navigation/PillNavigator';
import color from "../constants/colors";
import HomeScreen from "../screens/HomeScreen";

const Tab = createBottomTabNavigator();

const AppNavigator = props => {
    return (
        <NavigationContainer>
            <PillNavigator/>
        </NavigationContainer>
    )
};

/*
this ends up unused, but i don't have the heart to remove it.
 */
const TabNavigator = props => {
    return (
        <Tab.Navigator
            tabBarOptions={{
                activeTintColor: color.accent,
                style: {
                    backgroundColor: color.background,
                }
            }}
            screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                    let iconName
                    if (route.name === 'Home') {
                        /*if statement, if icon is focussed*/
                        iconName = focused ? 'ios-home' : 'md-home'
                    }
                    return <Ionicons name={iconName} color={color} size={size}/>
                }
            })}>
            <Tab.Sceen name='Home' component={HomeScreen}/>
        </Tab.Navigator>

    )
};

export default AppNavigator;