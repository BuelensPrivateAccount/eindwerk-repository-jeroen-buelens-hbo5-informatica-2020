import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';

import DefaultStyle from "../constants/default-style";

const PillTile = props => {
    return (
        <TouchableOpacity
            style={{...DefaultStyle.gridItem, ...{backgroundColor: props.color}}} onPress={props.ONPRESS}>
            <View>
                <Text
                    style={DefaultStyle.title}>
                    {props.children}
                </Text>
            </View>
        </TouchableOpacity>
    );
};

export default PillTile;
