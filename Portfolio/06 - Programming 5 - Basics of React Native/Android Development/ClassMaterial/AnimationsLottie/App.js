import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import LottieView from "lottie-react-native";
export default class index extends Component {
    render() {
        return (
            <View style={styles.container}>
                <LottieView
                    source={require("./assets/1712-bms-rocket.json")}
                    loop
                    autoPlay
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
