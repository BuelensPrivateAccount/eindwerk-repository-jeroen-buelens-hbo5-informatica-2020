import {StyleSheet} from 'react-native'
import colors from '../constants/colors'

export default StyleSheet.create({
    defaultText :{
        fontFamily: 'Xanthousy',
        color: colors.primary,
        fontSize : 18,
    }

})