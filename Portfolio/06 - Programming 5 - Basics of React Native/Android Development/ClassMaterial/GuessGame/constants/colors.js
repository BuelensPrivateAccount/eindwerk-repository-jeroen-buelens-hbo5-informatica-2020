export default {
    primary: "#B0E0E6",
    secondary: "#FFC0CB",
    accent:"#4682b4",
    white: "#ffffff",
};
