import React, {useState} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import Colors from '../constants/colors';
import Card from '../components/Card';
import Input from "../components/Input";

const Home = props => {
    const [enteredValue, setEnteredValue] = useState('');

    const letterInputHandler = inputText => {
        setEnteredValue(inputText.replace(/[^a-zA-Z -]/g, ''));
    };

    return(
        <View style={styles.container}>
            <Card style={styles.inputContainer}>
            <Text style={styles.title}>Welcome to the guessing game</Text>
                <Input style={styles.input}
                       keyboardType='default'
                       maxLength={255}
                       onChangeText={letterInputHandler}
                       value={enteredValue}
                       autoCorrect={false}
                />
            </Card>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 10,
    },
        inputContainer: {
            width: 300,
            /*responsive design*/
            maxWidth: '80%',
        },
    input: {
        borderWidth: 2,
        width: 270,
        textAlign: 'center',
        marginVertical: 10,
    },
    title: {
        fontSize: 20,
        color: Colors.white,
    },
    buttonContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        paddingHorizontal: 15,

    },
    button: {
        width: 120,

    }


});

export default Home;
