import React, {useState} from 'react';
import {View, Text, TextInput, StyleSheet, Button, Image} from 'react-native';
import Colors from '../constants/colors';
import Card from '../components/Card';
import Input from '../components/Input';
import GlobalButton from "../components/GlobalButton";
import {AntDesign} from '@expo/vector-icons';


const StartGameScreen = props => {
    const [enteredValue, setEnteredValue] = useState('');

    const numberInputHandler = inputText => {
        setEnteredValue(inputText.replace(/[^0-9]/g, ''));
    };

    const doResetText = props => {
        setEnteredValue("");
    };


    return (
        <View style={styles.container}>
            <Text style={styles.title}>The Game Screen </Text>

            <Card style={styles.inputContainer}>
                <Input style={styles.input}
                       keyboardType='number-pad'
                       maxLength={2}
                       onChangeText={numberInputHandler}
                       value={enteredValue}
                />
                <View style={styles.buttonContainer}>
                    <GlobalButton title="Reset"
                                  buttonStyle={
                                      {
                                          width: 120,
                                          height: 40,
                                          backgroundColor: Colors.secondary,
                                      }
                                  }
                                  textStyle={
                                      {
                                          fontSize: 16,
                                      }
                                  }
                                  changeTo={doResetText}
                    />
                    <GlobalButton title="Confirm"
                                  buttonStyle={
                                      {
                                          width: 120,
                                          height: 40,
                                          backgroundColor: Colors.secondary,
                                      }
                                  }
                                  textStyle={
                                      {
                                          fontSize: 16,
                                      }
                                  }
                    />
                </View>
            </Card>
            <Card>
                <Text>{enteredValue}</Text>
                <View style={styles.buttonContainer}>
                    <GlobalButton
                        buttonStyle={
                            {
                                width: 120,
                                height: 40,
                                backgroundColor: Colors.secondary,
                            }
                        }
                        textStyle={
                            {
                                fontSize: 16,
                            }
                        }
                        toChange={
                            () => props.onStartGame(enteredValue)
                        }
                    ><AntDesign name={"smileo"}/>
                        <Text>Start Guessing</Text>
                    </GlobalButton>
                </View>
            </Card>
            <Image source={require('../assets/bby.png')} style={{
                width: '80%',
                height: 300,
                borderRadius: 150,
                borderColor: Colors.accent,
                borderWidth: 2,
                overflow: 'hidden'
            }}/>
        </View>

    )
};
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 10,
    },
    inputContainer: {
        width: 300,
        /*responsive design*/
        maxWidth: '80%',
    },
    input: {
        borderWidth: 2,
        width: 50,
        textAlign: 'center',
        marginVertical: 10,
    },
    title: {
        fontSize: 20,
        color: Colors.white
    },
    buttonContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        paddingHorizontal: 15,

    },
    button: {
        width: 120,

    }


});

export default StartGameScreen;
