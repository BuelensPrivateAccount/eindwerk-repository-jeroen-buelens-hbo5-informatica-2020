import React, {useState} from 'react';
import {View, Text, TextInput, StyleSheet, Button,ScrollView} from 'react-native';
import Colors from '../constants/colors';
import Card from '../components/Card';
import GlobalButton from "../components/GlobalButton";


function RNG(ceiling, floor, exclude) {
    const rng = Math.floor(Math.random() * (ceiling - floor)) + floor;
    if (rng === exclude) {
        return RNG(ceiling, floor, exclude);
    } else {
        return rng;
    }
}

const [currentGuess, setCurrentGuess] = useState(RNG(1, 100, props.userChoice));
const [guesses, setGuesses] = useState([]);

const addItem = (item) => {
    setGuesses([...guesses, item]);
};

function onGameOver(rounds){
    //TODO implement onGameover
}

const GuessingGame = props => {
    this.state = {
        NumberHolder: 1,
        ceiling: 100,
        floor: 1,
    };

    function ceilingandgo(ceiling) {
        if (props.userChoice > ceiling) {
            //allert user hes a lier
            floorandgo(ceiling);
        }
        else {
            state.ceiling = ceiling;
            let newguess = RNG(ceiling, state.floor, currentGuess);
            setCurrentGuess(newguess);
            addItem(newguess)
            if(newguess === props.userChoice){
                onGameOver(guesses.length);
            }
        }
    }



    function floorandgo(floor) {
        if (props.userChoice < floor) {
            //allert user hes a lier
            ceilingandgo(floor);
        } else {
            state.floor = floor;
            let newguess = RNG(ceiling, state.floor, currentGuess);
            setCurrentGuess(newguess);
            addItem(newguess)
            if(newguess === props.userChoice){
                onGameOver(guesses.length);
            }
        }
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>The Game Screen </Text>

            <Card style={styles.inputContainer}>
                <Text>Guess: {currentGuess} </Text>
                <View style={styles.buttonContainer}>
                    <GlobalButton title="Lower"
                                  buttonStyle={
                                      {
                                          width: 120,
                                          height: 40,
                                          backgroundColor: Colors.secondary,
                                      }
                                  }
                                  textStyle={
                                      {
                                          fontSize: 16,
                                      }
                                  }
                                  changeTo={
                                      ceilingandgo(currentGuess)
                                  }
                    />
                    <GlobalButton title="Higher"
                                  buttonStyle={
                                      {
                                          width: 120,
                                          height: 40,
                                          backgroundColor: Colors.secondary,
                                      }
                                  }
                                  textStyle={
                                      {
                                          fontSize: 16,
                                      }
                                  }
                                  changeTo={floorandgo(currentGuess)}
                    />
                </View>
            </Card>
            <Card>
                <ScrollView>
                    {guesses.map(guess => <View><Text>{guess}</Text></View>)}
                </ScrollView>
            </Card>
        </View>

    )
};
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 10,
    },
    inputContainer: {
        width: 300,
        /*responsive design*/
        maxWidth: '80%',
    },
    input: {
        borderWidth: 2,
        width: 50,
        textAlign: 'center',
        marginVertical: 10,
    },
    title: {
        fontSize: 20,
        color: Colors.white
    },
    buttonContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        paddingHorizontal: 15,

    },
    button: {
        width: 120,

    }


});

export default GuessingGame;
