import React from 'react';
import {StyleSheet, Stylesheet, Text, View} from 'react-native';
import Colors from '../constants/colors';

const Header = props =>{
    return(
        <View style={styles.header}>
                <Text style={styles.headerTitle}>Header</Text>
        </View>
    )
};
const styles = StyleSheet.create({
    header:{
        width: '100%',
        height: 90,
        backgroundColor: Colors.white,
        paddingTop: 30,
        alignItems:'center',
        justifyContent:'center',

    },
    headerTitle:{
        fontSize:25,
        color: Colors.secondary,
    }
})
export default Header;
