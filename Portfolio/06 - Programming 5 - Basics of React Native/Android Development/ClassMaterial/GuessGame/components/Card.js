import React from 'react';
import { StyleSheet, View } from 'react-native';
import Colors from '../constants/colors';

const Card = props =>{
    return (
        <View style={{...styles.card, ...props.style}}>
            {props.children}
        </View>
    );
};
const styles = StyleSheet.create({
    card: {
        backgroundColor:Colors.primary,
        borderRadius: 20,
        shadowColor: 'gray',
        shadowOffset: {width:2, height:2},
        shadowRadius: 6,
        shadowOpacity: 0.90,
        elevation: 8,
        alignItems:'center',
        paddingVertical:20
    }
});
export default Card;
