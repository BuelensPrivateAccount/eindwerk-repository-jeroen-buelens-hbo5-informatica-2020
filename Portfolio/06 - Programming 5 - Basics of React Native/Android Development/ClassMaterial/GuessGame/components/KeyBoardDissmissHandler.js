import React from 'react';
import {TouchableWithoutFeedback,View,Keyboard} from 'react-native';

const KeyboardDissmissHandler = props =>{
    return(
        <TouchableWithoutFeedback onPress={() =>{Keyboard.dismiss();}}>
            <View>
                {props.children}
            </View>
        </TouchableWithoutFeedback>
    );
};


export default KeyboardDissmissHandler;

