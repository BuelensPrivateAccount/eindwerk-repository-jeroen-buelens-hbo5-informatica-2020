import React ,{useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import StartGameScreen from "./screens/StartGameScreen";
import Header from "./components/Header";
import Home from "./screens/Home";
import Colors from './constants/colors';
import KeyboardDissmissHandler from "./components/KeyBoardDissmissHandler";
import Guess from "./screens/GuessingGame";
import {AppLoading} from 'expo';
import defaultstyle from './constants/DefaultStyling';


const [dataloaded,setdataloaded] = useState(false);
const [userChoice, setUserNumber] = useState('');
const startGameHandler = (selectedNumber) => {
    setUserNumber(selectedNumber);
};

let startGameScreen =  <StartGameScreen onStartGame={startGameHandler}/>;
let GuessingGame = <Guess userChoice={userChoice}/>


const fetchFonts = () => {
    return Font.loadAsync({
        'xanthousy' : require('./Fonts/Xanthousy - Personal Use.otf'),
    })
}


export default function App() {
    this.state = {
        screenNumber: 0,
    };
    if(!dataloaded){
        return <AppLoading startAsync={fetchFonts} onFinish={() => setdataloaded(true)}/>
    }

    return (
        <View style={styles.container}>
            <KeyboardDissmissHandler>
                <Header/>
                <Home/>
                {(this.state.screenNumber === 0) ? startGameScreen : GuessingGame}
            </KeyboardDissmissHandler>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.secondary,
    },
});
