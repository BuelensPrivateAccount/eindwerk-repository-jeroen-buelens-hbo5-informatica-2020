import React from 'react';
import {Text, View, ImageBackground, ScrollView, Platform, Image} from 'react-native';

import axios from 'axios';
import {Header, Item, Icon, Input, Button, ListItem, List} from 'native-base';

const searching = require('../assets/search.jpg');


class SearchBody extends React.Component {
state={
    abilitydata: {},
    ability: ""
}

    findAbility = (url) => {
        let self = this;
        let userInput = this.state.ability.toLowerCase();
        axios.get(url)
            .then(function (response) {
                self.setState({abilitydata: response.data});
            })
            .catch(function (error) {
                console.log(error)
            });
    }

    render() {

        let pkmn = this.props.data;
        return (
            <View>
                <ImageBackground
                    source={{uri: "http://pokemongolive.com/img/posts/raids_loading.png"}}
                    style={{width: '100%', height: '100%'}}>
                    <ScrollView style={{flex: 1}}>

                        <Text style={styles.header}># {pkmn.id} - {pkmn.name.toUpperCase()}</Text>
                        <View style={styles.viewStyle}>
                            <Image
                                source={{uri: pkmn.sprites.front_default}}
                                style={styles.img}/>

                        </View>
                        <View style={styles.info}>
                            <ListItem itemDivider>
                                <Text style={{fontWeight: 'bold'}}>Size: </Text>

                            </ListItem>
                            <ListItem>
                                <Text>Weight: {pkmn.weight / 10} kg</Text>

                            </ListItem>
                            <ListItem>
                                <Text>Height: {pkmn.height / 10} m</Text>

                            </ListItem>
                            <ListItem itemDivider>
                                <Text style={{fontWeight: 'bold'}}>Abilities: </Text>
                            </ListItem>
                            <List dataArray={pkmn.abilities}
                                  renderRow={(item) =>
                                      <ListItem>
                                          <Text>{item.ability.name}</Text>
                                          {this.findAbility(item.ability.url)}
                                          <Text>{this.state.abilitydata.effect_entries.short_effect}</Text>
                                      </ListItem>}>
                            </List>


                        </View>
                    </ScrollView>
                </ImageBackground>
            </View>

        )
    }
}

const styles = {
    header: {
        fontSize: 30,
        color: 'red',
        textAlign: 'center'
    },
    viewStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    img: {
        height: 200,
        width: 200,
        justifyContent: 'center',
        alignItems: 'center'
    },
    info: {
        flex: 1,
        backgroundColor: 'white',
        opacity: 0.6
    }
}

export default SearchBody;
