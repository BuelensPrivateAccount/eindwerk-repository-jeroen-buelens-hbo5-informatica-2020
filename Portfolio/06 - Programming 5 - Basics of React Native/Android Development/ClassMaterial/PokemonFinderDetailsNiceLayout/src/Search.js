import React from 'react';
import {Text, View, ImageBackground, Platform} from 'react-native';
import {Header, Item, Icon, Input, Button} from 'native-base';
import Loading from './Loading';
import SearchBody from './SearchBody';
import axios from 'axios';


class Search extends React.Component {

    state = {
        data: {},
        userInput: "1",
        onCall: false
    }


    findPKM = () => {
        let self = this;
        self.setState({onCall: true});
        let userInput = this.state.userInput.toLowerCase();
        axios.get("https://pokeapi.co/api/v2/pokemon/"+userInput)
            .then(function (response) {
                self.setState({data: response.data});
                self.setState({onCall: false});
            })
            .catch(function (error) {
                console.log(error)
            });
    }

    renderbody = () => {
        if (this.state.onCall) {
            return (<Loading/>)
        } else {
            return (<SearchBody data={this.state.data}/>)

        }
    }


    render() {
        return (
            <View>
                <Header
                    searchBar
                    rounded>
                    <Item>
                        <Icon name="ios-search"
                              onPress={this.findPKM}/>
                        < Input
                            value={this.state.userInput}
                            placeholder="Find"
                            onChangeText={(userInput) => this.setState({userInput})}
                        />
                    </Item>

                </Header>
                {this.renderbody()}

            </View>

        )
    }
}

const styles = {

    viewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 25,
        color: 'blue',
        alignItems: 'center'
    },
    buttonStyle: {
        margin: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    }
}


export default Search;
