import React from 'react';
import {Text, View, Platform, Image, ImageBackground} from 'react-native';
import {Button} from 'native-base';
import Landing from './src/Landing';
import Search from './src/Search';
import Loading from './src/Loading';

export default class App extends React.Component {
    state = {
        currentScreen: "Landing"
    }
    switchScreen = (currentScreen) => {
        this.setState({currentScreen});
    }

    renderScreen = () => {
        if (this.state.currentScreen === "Landing") {
            return (
                <Landing
                    switchScreen={this.switchScreen}
                />
            )
        } else if (this.state.currentScreen === "Search") {
            return (
                <Search/>
            )
        }else if (this.state.currentScreen === "Loading") {
            return (
                <Loading/>
            )
        }
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderScreen()}
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        marginTop: Platform.OS === "android" ? 24 : 0
    }
}
