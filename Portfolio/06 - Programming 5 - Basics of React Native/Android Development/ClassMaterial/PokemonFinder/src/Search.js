import React from 'react';
import {Text, View, ImageBackground, Platform} from 'react-native';
import {Button, Header, Container, Content,Item,Icon,Input} from 'native-base';
import Information from "./Information";

class Search extends React.Component {
    render() {
        return (
            <View>
                <Header
                    searchBar
                    rounded
                >
                    <Item>
                        <Icon name={'ios-search'}
                        />
                    <Input placeholder="Find Pokemon"/>
                    </Item>
                </Header>
                   <Information/>
            </View>

        )
    }
}

const styles = {

    viewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 25,
        color: 'blue',
        alignItems: 'center'
    },
    buttonStyle: {
        margin: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    }
}


export default Search;
