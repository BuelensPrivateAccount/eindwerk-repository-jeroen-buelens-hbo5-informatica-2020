import React from 'react';
import { View, Image} from 'react-native';
import {Container} from "native-base";

export default class Loading extends React.Component{
    render() {
        return (
            <Container>
                <Image source={{uri: "https://media.tenor.com/images/39d6060576a516f1dd437eafccafbdb1/tenor.gif"}}
                style={styles.img}
                />
            </Container>
        )
    }
}

const styles = {
    img:{
        height: 400,
        width: 400,
        justifyContent: 'center',
        alignItems: 'center'
    }
}