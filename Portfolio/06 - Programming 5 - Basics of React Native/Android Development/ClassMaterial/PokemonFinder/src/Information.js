import React from 'react';
import {Container, ListItem,Content} from "native-base";
import {ImageBackground, View, Text, TextInput, ScrollView} from "react-native";

const background  = require("../assets/raids_loading.png");

class Information extends React.Component {
    render() {
        return (
            <View>
                <ImageBackground
                    source={background}
                    style={{width: "100%", height: "100%"}}>
                    <ScrollView>

                        <Text style={styles.header}># number - Name</Text>
                        <Content style={styles.info}>
                        <ListItem>
                            <Text>Size</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Height</Text>

                        </ListItem>
                        <ListItem>
                            <Text>Weight</Text>

                        </ListItem>
                        <ListItem itemDivider>
                            <Text>Abilities</Text>

                        </ListItem>
                        <ListItem>
                            <Text>1</Text>

                        </ListItem>
                        <ListItem>
                            <Text>1</Text>

                        </ListItem>
                        </Content>
                    </ScrollView>
                </ImageBackground>
            </View>
        )
    }
}

export default Information;

const styles = {
    header: {
        color: 'white',
        fontSize: 30,
        textAlign: "center"
    },
    info: {
        backgroundColor: 'white',
        opacity: 0.55
    }
}