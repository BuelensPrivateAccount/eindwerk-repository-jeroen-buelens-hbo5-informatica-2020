import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';

import DefaultStyle from "../constants/default-style";

const CategoryGridTile = props => {
    return (
        <TouchableOpacity
            style={{...DefaultStyle.gridItem,...{backgroundColor: props.color}}}>
            <View >
                <Text
                    style={DefaultStyle.title}>{props.title}</Text>
            </View>
        </TouchableOpacity>
    );
};

export default CategoryGridTile;
