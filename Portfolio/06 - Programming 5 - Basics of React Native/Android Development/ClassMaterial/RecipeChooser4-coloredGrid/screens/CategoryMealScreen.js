import React from 'react';
import { Text, View } from 'react-native';

import DefaultStyle from "../constants/default-style";

const CategoryMealScreen = props =>{
    return(
        <View  styles={DefaultStyle.screen}>
            <Text style={DefaultStyle.title}>
                The CategoryMeal Screen
            </Text>
        </View>
    );
};

export default CategoryMealScreen;
