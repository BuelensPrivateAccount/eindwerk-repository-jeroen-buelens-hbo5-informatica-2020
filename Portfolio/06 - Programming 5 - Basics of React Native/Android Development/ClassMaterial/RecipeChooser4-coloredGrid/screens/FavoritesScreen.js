import React from 'react';
import { Text, View } from 'react-native';

import DefaultStyle from "../constants/default-style";

const FavoritesScreen = props =>{
    return(
        <View  styles={DefaultStyle.screen}>
            <Text style={DefaultStyle.title}>
                The Favorites Screen
            </Text>
        </View>
    );
};

export default FavoritesScreen;
