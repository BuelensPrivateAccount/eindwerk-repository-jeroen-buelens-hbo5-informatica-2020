import React from 'react';
import { Text, View } from 'react-native';

import DefaultStyle from "../constants/default-style";


const FilterScreen = props =>{
    return(
        <View  styles={DefaultStyle.screen}>
            <Text style={DefaultStyle.title}>
                The Filter Screen
            </Text>
        </View>
    );
};

export default FilterScreen;
