import React from 'react';
import { Text, View } from 'react-native';

import DefaultStyle from "../constants/default-style";

const MealDetailScreen = props =>{
    return(
        <View  styles={DefaultStyle.screen}>
            <Text style={DefaultStyle.title}>
                The MealDetail Screen
            </Text>
        </View>
    );
};

export default MealDetailScreen;
