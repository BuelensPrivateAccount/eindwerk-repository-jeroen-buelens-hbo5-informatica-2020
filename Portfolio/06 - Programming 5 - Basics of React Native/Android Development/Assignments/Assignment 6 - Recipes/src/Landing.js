import React from 'react';
import {Text, View, ImageBackground, Platform} from 'react-native';
import {Button} from 'native-base';

const gunmetal = "#2B303A";
const lightCornflowerBlue = "#92DCE5";
const platinum = "#EEE5E9";
const shadow = "#8C705F";
const vermilion = "#D64933";

var myBackground = require('../assets/icons/foodsplash.jpg');

export default class Landing extends React.Component {
    render() {
        return (
            <View>
                <ImageBackground source={myBackground} style={{width: '100%', height: '100%'}}>
                    <View style={styles.viewStyle}>
                        <Text style={styles.titleStyle}>What shall we eat today?</Text>
                        <Button rounded
                            block={true}
                            style={styles.buttonStyle}
                            onPress={() => this.props.switchScreen("Search")}
                        >
                            <Text style={styles.buttonText}>give me something!</Text>

                        </Button>
                    </View>
                </ImageBackground>
            </View>

        )
    }
}

const styles = {

    viewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 25,
        color: gunmetal,
        alignItems: 'center',
        backgroundColor: platinum+"80"
    },
    buttonStyle: {
        margin: 10,
        backgroundColor: vermilion,
    },
    buttonText: {
        color: platinum,
        fontSize: 20
    }
}


//export default Landing;
