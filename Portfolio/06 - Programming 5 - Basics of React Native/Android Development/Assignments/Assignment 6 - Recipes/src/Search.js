import React from 'react';
import {Text, View, ImageBackground, Platform} from 'react-native';
import {Header, Item, Icon, Input, Button} from 'native-base';
import SearchBody from './SearchBody';
import axios from 'axios';

const gunmetal = "#2B303A";
const lightCornflowerBlue = "#92DCE5";
const platinum = "#EEE5E9";
const shadow = "#8C705F";
const vermilion = "#D64933";


class Search extends React.Component {


    state = {
        APiData: {},
        userInput: "cheese",
        onCall: false
    }


    findFood = () => {
        let self = this;
        let userInput = this.state.userInput.toLowerCase();
        let url = "https://api.spoonacular.com/recipes/search?query=" + userInput + "&number=1&apiKey="+apiKey;
        axios.get(url)
            .then(function (response) {
                console.log(response.data);  //get's data
                self.setState({APIdata: response.data});
            })
            .catch(function (error) {
                console.log(error)
            });
    }

    renderbody = () => {
        console.log(this.state.APIdata) //this thing, is undefined
           // return (<SearchBody data={this.state.APIdata} key={apiKey}/>)
    }


    render() {
        return (
            <View>
                <Header
                    searchBar
                    rounded
                style={styles.headerStyle}>
                    <Item>
                        <Icon name="ios-search"
                              onPress={this.findFood}/>
                        < Input
                            value={this.state.userInput}
                            placeholder="Find"
                            onChangeText={(userInput) => this.setState({userInput})}
                        />
                    </Item>

                </Header>
                {this.renderbody()}

            </View>

        )
    }
}

const apiKey = "f4c8e8ae6d3b440d8d1e5c78d6342a87";

const styles = {
    viewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 25,
        color: gunmetal,
        alignItems: 'center'
    },
    buttonStyle: {
        margin: 10
    },
    buttonText: {
        color: platinum,
        fontSize: 20
    },
    headerStyle: {
        backgroundColor: lightCornflowerBlue,
        color: gunmetal
    }
}


export default Search;
