import React from 'react';
import {Text, View, ImageBackground, ScrollView, Platform, Image} from 'react-native';

import axios from 'axios';
import {Header, Item, Icon, Input, Button, ListItem, List} from 'native-base';

const searching = require('../assets/search.jpg');
const gunmetal = "#2B303A";
const lightCornflowerBlue = "#92DCE5";
const platinum = "#EEE5E9";
const shadow = "#8C705F";
const vermilion = "#D64933";


class SearchBody extends React.Component {
    state = {
        ingredientsdata: {}
    }

    findIngredients = (id) => {
        let self = this;
        let userInput = this.state.userInput.toLowerCase();
        axios.get("https://api.spoonacular.com/recipes/" + id + "/information?apiKey="+this.props.key)
            .then(function (response) {
                self.setState({ingredientsdata: response.data.extendedIngredients});
            })
            .catch(function (error) {
                console.log(error)
            });
    }

    render() {

        let recipe = this.props.data;
        console.log(recipe)
        return (
            <View>
                <ImageBackground
                    source={{uri: "https://image.freepik.com/free-vector/hand-drawn-healthy-food-background_23-2148241421.jpg"}}
                    style={{width: '100%', height: '100%'}}>
                    <ScrollView style={{flex: 1}}>
                        <Text style={styles.header}>{recipe.title.toUpperCase()}</Text>
                        <View style={styles.viewStyle}>
                            <Image
                                source={{uri: recipe.image}}
                                style={styles.img}/>

                        </View>
                        <View style={styles.info}>
                            <ListItem>
                                <Text>Servings: {recipe.servings} kg</Text>

                            </ListItem>
                            <ListItem>
                                <Text>Ready in: {recipe.readyInMinutes} minutes</Text>

                            </ListItem>
                            <ListItem itemDivider>
                                <Text style={{fontWeight: 'bold'}}>Ingredients: </Text>
                            </ListItem>
                            {this.findIngredients(recipe.id)}
                            <List dataArray={this.state.ingredientsdata}
                                  renderRow={(item) =>
                                      <ListItem>
                                          <Text>{item.id}</Text>
                                          {/*todo: change id into item name by looking up the specific ingredient through the api*/}
                                          <Text>{item.measures.metric.amount}</Text>
                                      </ListItem>}>
                            </List>
                        </View>
                    </ScrollView>
                </ImageBackground>
            </View>

        )
    }
}

const styles = {
    header: {
        fontSize: 30,
        color: vermilion,
        textAlign: 'center'
    },
    viewStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    img: {
        height: 200,
        width: 200,
        justifyContent: 'center',
        alignItems: 'center'
    },
    info: {
        flex: 1,
        color: gunmetal,
        backgroundColor: platinum,
        opacity: 0.6
    }
}

export default SearchBody;
