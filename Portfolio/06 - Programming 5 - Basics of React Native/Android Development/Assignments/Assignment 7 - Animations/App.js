import React, {Component} from "react";
import {View, StyleSheet, Text,Image,TouchableOpacity} from "react-native";
import LottieView from "lottie-react-native";
import * as Animatable from "react-native-animatable";
import {Shake} from "react-native-motion";


export default class index extends Component {
    state = {
        value: 0
    };

    _startAnimation = () => {
        this.setState({value: this.state.value + 1});
    };

    render() {
        return (
            <View style={styles.container}>
                <LottieView
                    source={require("./assets/17896-wash-your-hands.json")}
                    loop
                    autoPlay
                />
                <Text>Wash your hands!</Text>
                <Text>Or else!</Text>
                <LottieView
                    source={require("https://assets8.lottiefiles.com/private_files/lf30_1KyL2Q.json")}
                    loop
                    autoPlay
                />
                <Animatable.View
                    imatable.View style={styles.card}
                    animation="slideInDown"
                    iterationCount={5}
                    direction="alternate">
                    <Image source={require('./assets/icon.png')}/>
                    <Text style={styles.whiteText}>slideInDown Animation</Text>
                </Animatable.View>
                <view>
                    <TouchableOpacity onPress={this._startAnimation}>
                        <Text >Start animation</Text>
                    </TouchableOpacity>
                    <View
                        style={{alignItems: "center"}}>
                        <Shake value={this.state.value} type="timing">
                            <Text>{this.state.value}</Text>
                        </Shake>
                    </View>
                </view>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
