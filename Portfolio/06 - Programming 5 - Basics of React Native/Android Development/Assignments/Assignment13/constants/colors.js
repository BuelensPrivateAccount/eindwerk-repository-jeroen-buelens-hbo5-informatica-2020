export default {
    primary: "#355c7d",
    secondary: "#98c1d9",
    tertiary:"#e0fbfc",
    accent: "#ee6c4d",
    darkaccent: "#293241",
    white: "#ffffff",

};