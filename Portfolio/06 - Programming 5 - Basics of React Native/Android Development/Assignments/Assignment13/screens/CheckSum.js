import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Colors from '../constants/colors';
import Card from '../components/Card';

let userInput = this.props.userinput;

const CheckSum = props => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>your input </Text>
            <Card style={styles.inputContainer}>
                <Text>{userInput.get('name')}</Text>
                <Text>{userInput.get('surname')}</Text>
                <Text>{userInput.get('birthday')}</Text>
                <Text>{userInput.get('bloodtype')}</Text>
                <Text>{userInput.get('email')}</Text>
                <Text>{userInput.get('adress')}</Text>
                <Text>{userInput.get('facebook')}</Text>
                <Text>{userInput.get('twitter')}</Text>
                <Text>{userInput.get('phone')}</Text>
            </Card>
        </View>

    )
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 10,
    },
    inputContainer: {
        width: 500,
        /*responsive design*/
        maxWidth: '80%',
    },
    input: {
        borderWidth: 2,
        width: 180,
        textAlign: 'center',
        marginVertical: 10,
    },
    title: {
        fontSize: 20,
        color: Colors.tertiary,
    },
    buttonContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        paddingHorizontal: 15,

    },
    button: {
        width: 120,

    },
    inputLineContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    basicText: {
        fontSize: 12,
        color: Colors.secondary,
        marginVertical: 10,
    },

});

export default CheckSum;
