import React, {useState} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Colors from '../constants/colors';
import Card from "../components/Card";
import Input from "../components/Input";
import CheckBOX from "../components/CheckBox";
import GlobalButton from "../components/GlobalButton";


const Home = props => {
    const [firstName, setFirstName] = useState('');
    const [Surname, setSurname] = useState('');
    const [birthdate, setBirthdate] = useState(new Date(1598051730000));
    const [BloodType, setBloodType] = useState('');
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [email, setEmail] = useState('');
    const [Adress, setAdress] = useState('');
    const [facebook, setFacebook] = useState('')
    const [twitter, setTwitter] = useState('');
    const [phone, setPhone] = useState('');

    let userinput = new Map([
        ['name', firstName],
        ['surname', Surname],
        ['birthday', birthdate],
        ['bloodtype', BloodType],
        ['email', email],
        ['adress', Adress],
        ['facebook', facebook],
        ['twitter', twitter],
        ['phone', phone],
    ]);

    const letterInputHandler = inputText => {
        setFirstName(inputText.replace(/[^a-zA-Z \-]/g, ''));
    };
    const letterNumberInputHandler = inputText => {
        setFirstName(inputText.replace(/[^a-zA-Z0-9\. \-]/g, ''));
    };
    const NumberInputHandler = inputText => {
        setFirstName(inputText.replace(/[^0-9\. ]/g, ''));
    };

    const BloodTypeInputHandler = inputText => {
        setFirstName(inputText.replace(/[^aAbBOo+\-]/g, ''));
    };

    const EmailRegexHandler = InputText => {
        const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        if (expression.test(String(InputText).toLowerCase())) {
            setEmail('');
        }
    };

    const RunDateTimePickerBirthday = (event, selectedDate) => {
        const currentDate = selectedDate || birthdate;
        setShow(Platform.OS === 'ios');
        setBirthdate(currentDate);
    };

    return (
        <View style={styles.container}>
            <Card style={styles.inputContainer}>
                <Text style={styles.title}>Identification</Text>
                <View style={styles.inputLineContainer}>
                    <Text style={styles.basicText}>First name: </Text>
                    <Input style={styles.input}
                           keyboardType='default'
                           maxLength={255}
                           onChangeText={letterInputHandler}
                           value={firstName}
                           autoCorrect={false}
                    /></View>
                <View style={styles.inputLineContainer}>
                    <Text style={styles.basicText}>Surname: </Text>
                    <Input style={styles.input}
                           keyboardType='default'
                           maxLength={255}
                           onChangeText={letterInputHandler}
                           value={Surname}
                           autoCorrect={false}
                    /></View>
                <View style={styles.inputLineContainer}>
                    <Text style={styles.basicText}>Birthday: </Text>
                    <DateTimePicker
                        testID="dateTimePicker"
                        timeZoneOffsetInMinutes={0}
                        value={birthdate}
                        mode={mode}
                        is24Hour={true}
                        display="default"
                        onChange={RunDateTimePickerBirthday}
                    />
                </View>
                <View style={styles.inputLineContainer}>
                    <Text style={styles.basicText}>BloodType: </Text>
                    <Input style={styles.input}
                           keyboardType='default'
                           maxLength={3}
                           onChangeText={BloodTypeInputHandler}
                           value={BloodType}
                           autoCorrect={false}
                    /></View>
            </Card>
            <Card style={styles.inputContainer}>
                <Text style={styles.title}>Contact info</Text>
                <View style={styles.inputLineContainer}>
                    <Text style={styles.basicText}>Adress: </Text>
                    <Input style={styles.input}
                           keyboardType='default'
                           maxLength={255}
                           onChangeText={letterNumberInputHandler}
                           value={Adress}
                           autoCorrect={false}
                           multiline={true}
                    /></View>
                <View style={styles.inputLineContainer}>
                    <Text style={styles.basicText}>Email: </Text>
                    <Input style={styles.input}
                           keyboardType='default'
                           maxLength={255}
                        /*todo implement email regex*/
                           value={email}
                           autoCorrect={false}
                    /></View>
                <View style={styles.inputLineContainer}>
                    <Text style={styles.basicText}>Phone: </Text>
                    <Input style={styles.input}
                           keyboardType='default'
                           maxLength={255}
                           onChangeText={NumberInputHandler}
                           value={phone}
                           autoCorrect={false}
                    /></View>
            </Card>
            <Card style={styles.inputContainer}>
                <Text style={styles.title}>Social info</Text>
                <View style={styles.inputLineContainer}>
                    <Image source={require('../assets/Socialmedia/Facebook.png')} style={{width: 30, height: 30}}/>
                    <Input style={styles.input}
                           keyboardType='default'
                           maxLength={255}
                           onChangeText={letterInputHandler}
                           value={facebook}
                           autoCorrect={false}
                    /></View>
                <View style={styles.inputLineContainer}>
                    <Image source={require('../assets/Socialmedia/Twitter.png')} style={{width: 30, height: 30}}/>
                    <Input style={styles.input}
                           keyboardType='default'
                           maxLength={255}
                           onChangeText={letterInputHandler}
                           value={twitter}
                           autoCorrect={false}
                    /></View>
            </Card>
            <Card style={styles.inputContainer}>
                <Text style={styles.title}>Allergies</Text>
                <CheckBOX checkboxTitle={"Glutten"}/>
                <CheckBOX checkboxTitle={"SeaFood"}/>
                <CheckBOX checkboxTitle={"Nuts"}/>
                <CheckBOX checkboxTitle={"Grass"}/>
                <CheckBOX checkboxTitle={"Lactose"}/>
                <CheckBOX checkboxTitle={"eggs"}/>
                <CheckBOX checkboxTitle={"tomatoes"}/>
                <CheckBOX checkboxTitle={"Sesame Seed"}/>
            </Card>
            <GlobalButton title={"Confirm"}
                          toChange={
                              () => props.userinput(userinput)
                          }/>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 10,
    },
    inputContainer: {
        width: 500,
        /*responsive design*/
        maxWidth: '80%',
    },
    input: {
        borderWidth: 2,
        width: 180,
        textAlign: 'center',
        marginVertical: 10,
    },
    title: {
        fontSize: 20,
        color: Colors.tertiary,
    },
    buttonContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        paddingHorizontal: 15,

    },
    button: {
        width: 120,

    },
    inputLineContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    basicText: {
        fontSize: 12,
        color: Colors.secondary,
        marginVertical: 10,
    },

});

export default Home;