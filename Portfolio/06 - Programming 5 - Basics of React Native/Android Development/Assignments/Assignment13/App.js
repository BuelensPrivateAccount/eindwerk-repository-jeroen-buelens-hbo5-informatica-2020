import React ,{useState} from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';

import Header from "./components/Header";
import Home from "./screens/Home";
import Colors from './constants/colors';
import KeyboardDissmissHandler from "./components/KeyBoardDissmissHandler";
import checkSum from "./screens/CheckSum";

const [userInput, setUserInput] = useState('');

const startGameHandler = (userInput) => {
    setUserInput(userInput);
};

let checkSum = <checkSum userinput = {userInput}/>
let home = <Home userinput = {startGameHandler}/>

export default function App() {

    this.state={
        screenNumber : 0,
    }

    return (
        <View style={styles.container}>
            <ScrollView>
                <KeyboardDissmissHandler>
                    <Header
                        title={"Contacts"}
                        textStyle={{color: Colors.darkaccent}}/>
                    {(this.state.screenNumber === 0) ? checkSum : home}
                </KeyboardDissmissHandler>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.secondary,
    },
});
