import React, {useState} from 'react';
import {CheckBox, StyleSheet} from 'react-native';
import Colors from '../constants/colors';

const CheckBOX = props => {
    const [checked, SetChecked] = useState(false);

    return (
        <CheckBox
            {...props}
            style={{...styles.CheckBOX, ...props.style}}
            checked={checked}
            title={props.checkboxTitle}
        />
    );
};

const styles = StyleSheet.create({
    CheckBOX: {
        height: 30,
        borderColor: Colors.accent,
        borderWidth: 2,
        marginVertical: 10,
        alignItems: 'flex-start',
    }

});
export default CheckBOX;
