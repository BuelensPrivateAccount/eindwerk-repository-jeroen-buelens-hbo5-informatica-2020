import React from 'react';
import {TouchableWithoutFeedback, View, Keyboard, ScrollView} from 'react-native';

const KeyboardDissmissHandler = props => {
    return (
        <ScrollView>
            <View>
                {props.children}
            </View>
        </ScrollView>
    );
};


export default KeyboardDissmissHandler;

