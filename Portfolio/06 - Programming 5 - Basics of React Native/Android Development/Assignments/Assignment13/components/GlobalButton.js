import React from 'react';
import {StyleSheet, View, Button, Text, Platform, TouchableOpacity} from 'react-native';
import Colors from "../constants/colors";

const GlobalButton = props => {
    return (
        <TouchableOpacity onPress={props.changeTo} style={{...styles.btn, ...props.buttonStyle}}>
            <Text style={{...styles.btnText, ...props.textStyle}}>
                {(props.title !== undefined) ? props.title: props.children}
            </Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    btn: {
        width: 40,
        height: 40,
        maxWidth: "80%",
        backgroundColor: Colors.primary,
        alignItems: 'center',
        justifyContent: 'center',

    },
    btnText: {
        fontSize: 8,
        color: Colors.white,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default GlobalButton;