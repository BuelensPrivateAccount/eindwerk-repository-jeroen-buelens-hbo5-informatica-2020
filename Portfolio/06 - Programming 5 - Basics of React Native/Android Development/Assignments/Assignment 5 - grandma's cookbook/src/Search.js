import React from 'react';
import {Text, View, ImageBackground, Platform} from 'react-native';
import {Button, Header, Container, Content,Item,Icon,Input} from 'native-base';
import CremeBrulee from "./CremeBrulee";

class Search extends React.Component {
    render() {
        return (
            <View>
                <Header
                    searchBar
                    rounded
                >
                    <Item>
                        <Icon name={'ios-search'}
                        />
                    <Input placeholder="Find Food Recipes, this does not work yet, will transfer to diffrent pages"/>
                    </Item>
                </Header>
                   <CremeBrulee/>
            </View>

        )
    }
}

const styles = {

    viewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 25,
        color: 'blue',
        alignItems: 'center'
    },
    buttonStyle: {
        margin: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    }
}


export default Search;
