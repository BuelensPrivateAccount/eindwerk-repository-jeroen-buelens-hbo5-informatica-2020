import React from 'react';
import {Container, ListItem, Content} from "native-base";
import {ImageBackground, View, Text, TextInput, ScrollView, Image} from "react-native";


const background = require("../assets/foodbackground.png");
const grandma = require("../assets/GrandmaBrullee.jpg");

class CremeBrulee extends React.Component {
    render() {
        return (
            <View>
                <ImageBackground
                    source={grandma}
                    style={{width: "100%", height: "100%"}}>
                    <ScrollView>
                        <Text style={styles.header}>Recipe #1 - Crème brûlée</Text>
                        <Content style={styles.info}>
                            <ListItem itemDivider>
                                <Text>Ingredients</Text>
                            </ListItem>
                            <ListItem>
                                <Text>1x zakje 125g mix voor "Crème brûlée" van de Aveve</Text>
                            </ListItem>
                            <ListItem>
                                <Text>250ml melk</Text>
                            </ListItem>
                            <ListItem>
                                <Text>400ml goeie room, diene van 40%</Text>
                            </ListItem>
                            <ListItem itemDivider>
                                <Text>Cooking Steps</Text>
                            </ListItem>
                            <ListItem>
                                <Text>1. Breng de melk en de goeie room samen in een koem.</Text>
                            </ListItem>
                            <ListItem>
                                <Text>2. zet dà op het vuur tot et bijna kookt.</Text>
                            </ListItem>
                            <ListItem>
                                <Text>3. pakt nè klopper en voeg da pakje daar aan toe, en klopt tot dà der voledig in zit.</Text>
                            </ListItem>
                            <ListItem>
                                <Text>4. giet á "Crème brûlée" in klèine koemmekes en zet ze op 't teras.</Text>
                            </ListItem>
                            <ListItem>
                                <Text>5. vlak voó dat ge 't opdiend, doe daar broìne suiker op en met deine brander, ga dér over, dat ge zòen laag ebt.</Text>
                            </ListItem>
                        </Content>
                    </ScrollView>
                </ImageBackground>
            </View>
        )
    }
}

export default CremeBrulee;

const styles = {
    header: {
        color: 'white',
        fontSize: 30,
        textAlign: "center"
    },
    info: {
        backgroundColor: 'white',
        opacity: 0.55
    }
}