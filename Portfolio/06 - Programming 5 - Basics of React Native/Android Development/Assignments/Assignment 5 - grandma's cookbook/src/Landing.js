import React from 'react';
import {Text, View, ImageBackground, Platform} from 'react-native';
import {Button} from 'native-base';


const background = require('../assets/GrandmaSplash.jpg');

export default class Landing extends React.Component {
    render() {
        return (
            <View>
                <ImageBackground source={background} style={{width: "100%", height: "100%"}}>
                    <View style={styles.viewStyle}>
                        <Text style={styles.titleStyle}>Grandma's Secret Stash of Recipes</Text>
                        <Button
                            block={true}
                            style={styles.buttonStyle}
                            onPress={() => this.props.switchScreen("Search")}
                        >
                            <Text style={styles.buttonText}>Start</Text>

                        </Button>
                    </View>
                </ImageBackground>
            </View>

        )
    }
}

const styles = {

    viewStyle:{
        flex: 1,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center'
    },
    titleStyle: {
        fontSize:25,
        color: 'blue',
        alignItems:'center'
    },
    buttonStyle:{
        margin:10
    },
    buttonText:{
        color:'white',
        fontSize: 20
    }
}



//export default Landing;
