import React from 'react';
import {Container, ListItem, Content} from "native-base";
import {ImageBackground, View, Text, TextInput, ScrollView, Image} from "react-native";


const background = require("../assets/scampioma.jpg");

class Scampis extends React.Component {
    render() {
        return (
            <View>
                <ImageBackground
                    source={background}
                    style={{width: "100%", height: "100%"}}>
                    <ScrollView>
                        <Text style={styles.header}>Recipe #2 - Scampi's à la Rozà</Text>
                        <Content style={styles.info}>
                            <ListItem itemDivider>
                                <Text>Ingredients (4pers)</Text>
                            </ListItem>
                            <ListItem>
                                <Text>40 ongepelde scampi's diepvries</Text>
                            </ListItem>
                            <ListItem>
                                <Text>8 tenen look</Text>
                            </ListItem>
                            <ListItem>
                                <Text>2 sjalotjes look</Text>
                            </ListItem>
                            <ListItem>
                                <Text>200ml light room</Text>
                            </ListItem>
                            <ListItem>
                                <Text>20ml tomaten ketchup</Text>
                            </ListItem>
                            <ListItem>
                                <Text>2 teaspoons milde currypoeder</Text>
                            </ListItem>
                            <ListItem>
                                <Text>verse peterselie</Text>
                            </ListItem>
                            <ListItem>
                                <Text>olijf olie</Text>
                            </ListItem>
                            <ListItem>
                                <Text>peper en zout</Text>
                            </ListItem>
                            <ListItem itemDivider>
                                <Text>Cooking Steps</Text>
                            </ListItem>
                            <ListItem>
                                <Text>ontdooi en pel de scampi's, hou de pellen bij</Text>
                            </ListItem>
                            <ListItem>
                                <Text>haal het darmkanaal van de scampi er uit met een patatten mes</Text>
                            </ListItem>
                            <ListItem>
                                <Text>sni de sjalot en de look fijn</Text>
                            </ListItem>
                            <ListItem>
                                <Text>bak de sjalot en de look op heet vuur tot de ui glanzig is.</Text>
                            </ListItem>
                            <ListItem>
                                <Text>voeg de scampi en scampi pellen toe en bak die licht aan</Text>
                            </ListItem>
                            <ListItem>
                                <Text>haal de scampi's uit de pan en zet ze aan kant.</Text>
                            </ListItem>
                            <ListItem>
                                <Text>voeg de ketchup, room en currypoeder toe en laat 15-20 minutejes inkoken</Text>
                            </ListItem>
                            <ListItem>
                                <Text>verwijder de scampi pellen en voeg de scampi's terug toe</Text>
                            </ListItem>
                            <ListItem>
                                <Text>voeg peterselie toe als afwerking, serveer met pasta, brood of frietjes.</Text>
                            </ListItem>
                        </Content>
                    </ScrollView>
                </ImageBackground>
            </View>
        )
    }
}

export default Scampis;

const styles = {
    header: {
        color: 'white',
        fontSize: 30,
        textAlign: "center"
    },
    info: {
        backgroundColor: 'white',
        opacity: 0.55
    }
}