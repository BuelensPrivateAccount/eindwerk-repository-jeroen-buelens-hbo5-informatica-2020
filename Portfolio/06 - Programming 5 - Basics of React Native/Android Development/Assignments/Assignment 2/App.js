import React, {Component} from 'react';
import {StyleSheet, Image, TextInput} from 'react-native';
import {Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text} from 'native-base';

const background = require("./assets/BagonLine.png");

class Runner extends React.Component {
    state = {
        inputMessage: "input something here",
        list: [],
        text: "fill in here",
    }

    deleteAListElement = (e) => {
        var array = this.state.list;
        let index = array.indexOf(e);
        array.splice(index, 1);
        this.setState({list: array});
    }

    initialiseList = () => {
        return this.state.list.map(
            t => {
                return (
                    <Text key={t}
                          onPress={() => {
                              this.deleteAListElement(t)
                          }}
                          style={styles.normaltext}
                    >{t}</Text>
                )
            }
        )
    }

    addAElement = () => {
        let newElement = this.state.inputmessage;
        var fullList = this.state.list;
        fullList.push(newElement);
        this.setState({list: fullList, inputmessage: "input something here"});
    }

    render() {
        return (
            <Container>
                <header>
                    <left>
                        <Button transparent>
                            <Icon name='menu'/>
                        </Button>
                    </left>
                    <Body>
                        <Title>A List of Names Pokemon have</Title>
                    </Body>
                </header>
                <Content>
                    <Image
                        source={background}
                        style={{width: '100%', height: '100%'}}
                    />
                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={(text) => this.setState({text})}
                        value={this.state.text}
                    />
                    <Button
                        title="Pokemon Listed"
                        onPress={this.addAElement}
                    />
                    {this.initialiseList()}

                </Content>
                <Footer>
                    //footer here
                </Footer>
            </Container>

            //
            //
         //   < View
            //     style = {styles.main} >
            //         < ImageBackground
            //     source = {background}
            //     style = {styles.backgroundStyle} >
            //         < view
            //     style = {
            //     {
            //         flex: 1
            //     }
            // }>
            // <
            //     view
            //     style = {
            //     {
            //         flex: 2
            //     }
            // }>
            //
            // <
            //     TextInput
            //     style = {styles.inputStyle}
            //     onChangeText = {(text)
            // =>
            //     this.setState({text})
            // }
            //     value = {this.state.text}
            //     />
            //     <Button
            //         title="Pokemon Listed"
            //         onPress={this.addAElement}/>
            //     < /view>
            //     <view style={{flex: 3}}>
            //         {this.initialiseList()}
            //     </view>
            //     < /view>
            // </ImageBackground>
            // </View>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        flexDirection: 'column',
    },
    backgroundStyle: {
        width: '100%',
        height: '100%',
        resizeMode: "cover",
    },
    normaltext: {
        textAlign: "center",
        color: "#ffffff",
    },
    inputStyle: {
        height: 40,
        borderColor: "white",
        borderWidth: 2,
        color: "white"
    }

});

export default Runner;
