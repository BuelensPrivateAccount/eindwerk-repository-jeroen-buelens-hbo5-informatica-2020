import {Body, Button, Container, Content, Footer, FooterTab, Header, Left, Right, Text, Title} from "native-base";
import {ImageBackground} from "react-native";

const twitterLogo = require('../assets/twitter_Logo.jfif');

export default class Tweet extends React.Component {
    render() {
        return (
            <Container>
                <Header>
                    <Left>
                    </Left>
                    <Body>
                        <Title>Did you Ever Hear</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    <ImageBackground source={twitterLogo} style={{width: '100%', height: '100%'}}>
                        <Text>Did you ever hear the tragedy of Darth Plagueis The Wise? I thought not. It’s not a story the Jedi would tell you. It’s a Sith legend. Darth Plagueis was a Dark Lord of the Sith, so powerful and so wise he could use the Force to influence the midichlorians to create life… He had such a knowledge of the dark side that he could even keep the ones he cared about from dying. The dark side of the Force is a pathway to many abilities some consider to be unnatural. He became so powerful… the only thing he was afraid of was losing his power, which eventually, of course, he did. Unfortunately, he taught his apprentice everything he knew, then his apprentice killed him in his sleep. Ironic. He could save others from death, but not himself.</Text>
                        <Button onPress={() => this.props.switchScreen("switchScreen")} title={'Home'}/>
                    </ImageBackground>
                </Content>
                <Footer>
                    <FooterTab>
                    </FooterTab>
                </Footer>
            </Container>

        )
    }


}

const styles = {

    viewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 25,
        color: 'blue',
        alignItems: 'center'
    },
    buttonStyle: {
        margin: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    }
}