import {Body, Button, Container, Content, Footer, FooterTab, Header, Left, Right, Text, Title} from "native-base";
import {ImageBackground} from "react-native";

const redditLogo = require('../assets/reddit-2.svg');

export default class Reddit extends React.Component {
    render() {
        return (
            <Container>
                <Header>
                    <Left>
                    </Left>
                    <Body>
                        <Title>Reddit</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    <ImageBackground source={redditLogo} style={{width: '100%', height: '100%'}}>
                        <Text>Choose something</Text>
                        <Button onPress={() => this.props.switchScreen("switchScreen")} title={'SubReddit'}/>
                        <Button onPress={() => this.props.switchScreen("switchScreen")} title={'Home'}/>
                    </ImageBackground>
                </Content>
                <Footer>
                    <FooterTab>
                    </FooterTab>
                </Footer>
            </Container>

        )
    }


}

const styles = {

    viewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 25,
        color: 'blue',
        alignItems: 'center'
    },
    buttonStyle: {
        margin: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    }
}