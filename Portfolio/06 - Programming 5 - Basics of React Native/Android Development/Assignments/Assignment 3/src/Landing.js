import React from 'react';
import {View, ImageBackground, Platform} from 'react-native';
import {Text, Container, Button, Header, Left, Icon, Body, Title, Right, Content, Footer, FooterTab} from 'native-base';

const Background = require('../assets/background.jpg');

export default class Landing extends React.Component {
    render() {
        return (
            <Container>
                <Header>
                    <Left>
                    </Left>
                    <Body>
                        <Title>MyApplication</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    <ImageBackground source={Background} style={{width: '100%', height: '100%'}}>
                        <Text>Choose something</Text>
                        <Button onPress={()=> this.props.switchScreen("switchScreen")} title={'Reddit'}/>
                        <Button onPress={()=> this.props.switchScreen("switchScreen")} title={'Twitter'}/>
                    </ImageBackground>
                </Content>
                <Footer>
                    <FooterTab>
                    </FooterTab>
                </Footer>
            </Container>

        )
    }


}

const styles = {

    viewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 25,
        color: 'blue',
        alignItems: 'center'
    },
    buttonStyle: {
        margin: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    }
}