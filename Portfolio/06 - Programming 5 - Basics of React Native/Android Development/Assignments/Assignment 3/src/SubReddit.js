import {Body, Button, Container, Content, Footer, FooterTab, Header, Left, Right, Text, Title} from "native-base";
import {ImageBackground} from "react-native";

const redditLogo = require('../assets/reddit-2.svg');

export default class SubReddit extends React.Component {
    render() {
        return (
            <Container>
                <Header>
                    <Left>
                    </Left>
                    <Body>
                        <Title>/r/iamverydumb</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    <ImageBackground source={redditLogo} style={{width: '100%', height: '100%'}}>
                        <Text>I had no clue what to make this about, i struggled with everything of my dependancies breaking at one point, so starting from 15:30 i wasn't able to actually verify anything on my phone.</Text>
                        <Text>This makes sense as long as we stay loooooow key, but from half the Sh1t around me in this mess of a code I haven't realised WHY something needed to be like that, nor did we get why it worked.</Text>
                        <Text>We saw "this.props.switchScreen("string)" but why does this string say Swichscreen, what is props and how does a button that acesses it do something.</Text>
                        <Text>every second of every day i'm feeling dumber and dumber because to me it feels like i've been thrown in a game without having a propper platform to jump off from, this is frustrating as hell.</Text>
                        <Text>It's like being told that you'll learn later why this works, but because of that and how my brain reacts i've been in a mental block and i still want to chuck out this pc, this code, these dependancies i dont know what they do,....</Text>
                        <Text>im frustrated and this is depressing me.</Text>
                        <Button onPress={() => this.props.switchScreen("switchScreen")} title={'Home'}/>
                    </ImageBackground>
                </Content>
                <Footer>
                    <FooterTab>
                    </FooterTab>
                </Footer>
            </Container>

        )
    }


}

const styles = {

    viewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 25,
        color: 'blue',
        alignItems: 'center'
    },
    buttonStyle: {
        margin: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    }
}