import React from 'react';
import {StyleSheet, View, ImageBackground, Platform} from 'react-native';
import {Text, Container} from 'native-base';
import Landing from './src/Landing';
import Reddit from './src/Reddit';
import Tweet from './src/Tweet';
import Twitter from './src/Twitter';
import SubReddit from './src/SubReddit';

const Background = require('./assets/background.jpg');


export default class App extends React.Component {
    state = {
        currentScreen: "Landing"

    }
    switchScreen = (currentScreen) => {
        this.setState({currentScreen})
    }

    render() {
        if (this.state.currentScreen === "Landing") {
            return (
                <Landing switchScreen={this.switchScreen}/>
            )
        } else if (this.state.currentScreen === "Reddit") {
            return (
                <Reddit switchScreen={this.switchScreen}/>
            )
        } else if (this.state.currentScreen === "SubReddit") {
            return (
                <SubReddit switchScreen={this.switchScreen}/>
            )
        } else if (this.state.currentScreen === "Twitter") {
            return (
                <Twitter switchScreen={this.switchScreen}/>
            )
        } else if (this.state.currentScreen === "Reddit") {
            return (
                <Tweet switchScreen={this.switchScreen}/>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: Platform.OS === "android" ? 24 : 0,
    },
});
