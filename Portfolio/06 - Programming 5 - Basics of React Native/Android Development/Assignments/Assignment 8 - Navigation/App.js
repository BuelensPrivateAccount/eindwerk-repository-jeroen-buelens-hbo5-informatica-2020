import * as React from 'react';
import { Button, View,Text } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import {ImageBackground} from "react-native";
import SomethingFancy from "./src/somethingFancy"

function HomeScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' ,backgroundColor: "#ff000f"}}>

            <Text>I'm using drawer navigation for this :)</Text>
            <Button
                onPress={() => navigation.navigate('Notifications')}
                title="Hello, i can be clicked."
            />
            <Button
                onPress={() => navigation.navigate('Screen 2')}
                title="go to blue"
            />
            <Button
                onPress={() => navigation.navigate('Screen 3')}
                title="Go to pink"
            />
        </View>
    );
}

function screen2 ({navigation}){
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center',backgroundColor: "#0000ff" }}>
            <Button onPress={() => navigation.goBack()} title="Go back home" />
        </View>
    );
}
function screen3 ({navigation}){
    return (
        <SomethingFancy navigation = {navigation}/>
    );
}

function NotificationsScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <ImageBackground source={require('./assets/freddy.png')} style={{width: "100%",height: "100%"}}>
            <Button onPress={() => navigation.goBack()} title="Go back home" />
            </ImageBackground>
        </View>
    );
}

const Drawer = createDrawerNavigator();

export default function App() {
    return (
        <NavigationContainer>
            <Drawer.Navigator initialRouteName="Home">
                <Drawer.Screen name="Home" component={HomeScreen} />
                <Drawer.Screen name="Notifications" component={NotificationsScreen} />
                <Drawer.Screen name="Screen 2" component={screen2} />
                <Drawer.Screen name="Screen 3" component={screen3} />

            </Drawer.Navigator>
        </NavigationContainer>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});