import React from 'react';
import {StyleSheet, Text, View, ImageBackground, TextInput, Button} from 'react-native';

var myBackground = require('./assets/background.png')

class App extends React.Component {
    state = {
        todomessage: "placeholder",
        todo: ["handen wassen", "papieren zakdoek", "social distancing"]
    }
    devareToDo = (t) => {
        var array = this.state.todo;
        var toDoToDevare = array.indexOf(t);
        array.splice(toDoToDevare, 1);
        this.setState({todo: array})
    }
    addToDo = () => {
        var newTodo = this.state.todomessage;
        var toDoArray = this.state.todo;
        toDoArray.push(newTodo);
        this.setState({todo: toDoArray, todomessage: ""})
    }
    createToDoList = () => {
        return this.state.todo.map(
            t => {
                return (
                    <Text key={t} onPress={() => {
                        this.devareToDo(t)
                    }} style={styles.paragraph}>{t}</Text>
                )
            }
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={myBackground} style={{width: '100%', height: '100%'}}>
                    <Text style={styles.paragraph}>To Do List</Text>
                    <TextInput style={styles.inputStyle} onChangeText={(todomessage) => this.setState({todomessage})}
                               value={this.state.todomessage}/>
                    <Button
                        title="Add To Do"
                        onPress={this.addToDo}/>
                    {this.createToDoList()}
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    paragraph: {
        color: '#ffffff',
        todomessageAlign: 'center'
    },
    inputStyle: {
        height: 40,
        borderColor: "white",
        borderWidth: 2,
        color: "white"
    }
})

export default App;



