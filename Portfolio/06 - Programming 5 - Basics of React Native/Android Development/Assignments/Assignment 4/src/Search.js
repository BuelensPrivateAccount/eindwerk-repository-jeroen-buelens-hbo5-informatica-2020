import React from 'react';
import {StyleSheet, View, Platform} from 'react-native';
import {Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text} from 'native-base';
import {ImageBackground} from "react-native-web";

const myBackground = require('../assets/BagonLine.png');

export default class Search extends React.Component {

    render() {
        return (<Container>
                <ImageBackground source={myBackground} style={{width: "100%", height: "100%"}}>
                    <Text>FoundThem</Text>
                </ImageBackground>
            </Container>
        )
    }

}

const styles = {

    viewStyle:{
        flex: 1,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center'
    },
    titleStyle: {
        fontSize:25,
        color: 'blue',
        alignItems:'center'
    },
    buttonStyle:{
        margin:10
    },
    buttonText:{
        color:'white',
        fontSize: 20
    }
}

