import React from 'react';
import {StyleSheet, View, Platform, ImageBackground} from 'react-native';
import {Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text} from 'native-base';


const myBackground = require('../assets/BagonLine.png');

export default class Landing extends React.Component {
    render() {
        return (
            <View>
                <ImageBackground source={myBackground} style={{width: '100%', height: '100%'}}>
                    <View style={styles.viewStyle}>
                        <Text style={styles.titleStyle}>Let's find Pokemons</Text>
                        <Button
                            block={true}
                            style={styles.buttonStyle}
                            onPress={() => this.props.switchScreen("switchScreen")}
                        >
                            <Text style={styles.buttonText}>Find'em</Text>

                        </Button>
                    </View>
                </ImageBackground>
            </View>

        )
    }
}

const styles = {

    viewStyle:{
        flex: 1,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center'
    },
    titleStyle: {
        fontSize:25,
        color: 'blue',
        alignItems:'center'
    },
    buttonStyle:{
        margin:10
    },
    buttonText:{
        color:'white',
        fontSize: 20
    }
}