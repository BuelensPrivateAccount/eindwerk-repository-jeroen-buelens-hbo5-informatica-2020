import React from 'react';
import {StyleSheet, View, Platform} from 'react-native';
import {Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text} from 'native-base';
import Landing from "./src/Landing";
import Search from "./src/Search";


class runner extends React.Component {
    state = {
        currentScreen: "Landing",
    }

    switchScreen = (currentScreen) => {
        this.setState({currentScreen})

    }


    renderScreen = () => {
        if (this.state.currentScreen === "Landing") {
            return (
                <Landing switchScreen={this.switchScreen}/>
            )
        } else if (this.state.currentScreen === "Search") {
            <Search/>
        }
    }

    render() {
        return (
            <Container>
                {this.renderScreen()}
            </Container>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: Platform.OS === 'android' ? 24 : 0,
    },
});

export default runner;
