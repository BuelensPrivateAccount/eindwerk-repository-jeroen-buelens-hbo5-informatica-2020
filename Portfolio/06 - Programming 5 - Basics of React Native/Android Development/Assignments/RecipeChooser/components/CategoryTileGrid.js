import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList } from 'react-native';
import DefaultStyle from "../constants/default-style";

const CategoryTileGrid = props => {
    return(
        <TouchableOpacity style={[styles.categoryItem, {backgroundColor: props.color}]} onPress={props.ONPRESS} >
            <Text style={styles.categoryTitle}>{props.title}</Text>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    categoryItem: {
        backgroundColor: "#ffffff",
        borderRadius: 5,
        width: "45%",
        height: 100,
        margin: "2.5%",
        alignItems: "flex-end",
        justifyContent: "flex-end",
        padding: 20
    },
    categoryTitle: {
        fontSize: 18,
        fontWeight: "700"
    }
});

export default CategoryTileGrid;
