import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, FlatList} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import CategoryGridTile from "../components/CategoryTileGrid";

import DefaultStyle from "../constants/default-style";
import Colors from '../constants/colors';
import CategoryMealScreen from "./CategoryMealScreen";

import {CATEGORIES} from "../data/dummy-data";

const CategoriesScreen = props => {

    const renderGridItem = (itemData) => {
        return (
            <CategoryGridTile title={itemData.item.title} color={itemData.item.color} ONPRESS={
            () => props.navigation.navigate(
                'CategoryMeal', {
                    CATEGORY: itemData.item
                }
            )
        }
        />
        )
    };

    return (
        <FlatList data={CATEGORIES} renderItem={renderGridItem} numColumns={2}/>
    );

};

const styles = StyleSheet.create({


});

export default CategoriesScreen;
