import React, {useLayoutEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import DefaultStyle from "../constants/default-style";
import Colors from '../constants/colors';


const CategoryMealScreen = props => {
        props.navigation.setOptions({
            title: props.route.params.category.title,
            headerStyle: {
                backgroundColor: props.route.params.CATEGORY.color,
            }
        });


    return (
        <View styles={DefaultStyle.screen}>
            <Text>
                {props.navigation.setParams({
                    headerStyle: {
                        backgroundColor: props.route.params.CATEGORY.color
                    },
                })}
                #{props.route.params.CATEGORY.id} {props.route.params.CATEGORY.title}
            </Text>
        </View>

    );

};

const styles = StyleSheet.create({});

export default CategoryMealScreen;
