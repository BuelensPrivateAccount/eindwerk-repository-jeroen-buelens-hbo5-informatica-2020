// stack of pages or screens
import React from 'react';
import { createStackNavigator,createAppContainer } from '@react-navigation/stack';


import CategoriesScreen from "../screens/CategoriesScreen";
import CategoryMealScreen from "../screens/CategoryMealScreen";
import MealDetailScreen from "../screens/MealDetailScreen";




const MealsNavigator = createStackNavigator();

function MyMealsNavigator(){

    return(
        <MealsNavigator.Navigator>
            <MealsNavigator.Screen name="Jeroen Buelens" component={CategoriesScreen}/>
            <MealsNavigator.Screen name="CategoryMeal" component={CategoryMealScreen}/>
            <MealsNavigator.Screen name="MealDetail" component={MealDetailScreen}/>
        </MealsNavigator.Navigator>
    )
}


export default MyMealsNavigator;
