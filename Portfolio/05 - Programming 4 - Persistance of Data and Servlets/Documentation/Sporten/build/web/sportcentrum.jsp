<%-- 
    Document   : sportcentrum
    Created on : Feb 4, 2020, 2:15:09 PM
    Author     : NDMBC65
--%>

<%@page import="com.realdolmen.sporten.beans.Sportcentrum" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Sportcentrum details</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body>
        <%Sportcentrum sportcentrum = (Sportcentrum) request.getAttribute("sportcentrum");%>
        <div id="headercontainer">
            <div id="header">
                <h1>Sport je jong!</h1>
            </div>
        </div>
        <div id="content">
            <h1>Details sportcentrum <%=sportcentrum.getCentrumnaam()%></h1>                   
                   <p>
                        <label for="voornaamid">Naam:</label>
                        <input type="text" name="naam" value="<%=sportcentrum.getCentrumnaam()%>" id="voornaamid"/>
                    </p>
                    <p>
                        <label for="straatid">Straat en nummer:</label>
                        <input type="text" name="straat" value="<%=sportcentrum.getStraat()%> <%=sportcentrum.getHuisnummer()%>" id="straatid" />
                    </p>
                    <p>
                        <label for="postcodeid">Postcode:</label>
                        <input type="text" name="postcode" value="<%=sportcentrum.getPostcode()%>" id="postcodeid" />
                    </p>
                    <p>
                        <label for="woonplaatsid">Woonplaats</label>
                        <input type="text" name="woonplaats" value="<%=sportcentrum.getWoonplaats()%>" id="woonplaatsid" />
                    </p>
                     <p>
                        <label for="telefoonid">Telefoon</label>
                        <input type="text" name="telefoon" value="<%=sportcentrum.getTelefoon() %>" id="telefoonid" />
                    </p>
                
            
            <p>  <a href="index.jsp">Terug naar beginpagina</a></p>
        </div>
    </body>
</html>
