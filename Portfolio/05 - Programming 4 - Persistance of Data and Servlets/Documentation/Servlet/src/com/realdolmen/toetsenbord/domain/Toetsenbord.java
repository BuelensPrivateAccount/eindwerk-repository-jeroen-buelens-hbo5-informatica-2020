package com.realdolmen.toetsenbord.domain;

public class Toetsenbord {

	private int serienummer;
	private String merk;
	private boolean draadloos;
	private double prijs;

	public Toetsenbord() {
		serienummer = 987450;
		merk = "?";
		draadloos = true;
	}

	public int getSerienummer() {
		return serienummer;
	}

	public void setSerienummer(int serienummer) {
		this.serienummer = serienummer;
	}

	public String getMerk() {
		return merk;
	}

	public void setMerk(String merk) {
		this.merk = merk;
	}

	public boolean isDraadloos() {
		return draadloos;
	}

	public void setDraadloos(boolean draadloos) {
		this.draadloos = draadloos;
	}

	public double getPrijs() {
		return prijs;
	}

	public void setPrijs(double prijs) {
		this.prijs = prijs;
	}

	public Toetsenbord(int serienummer, String merk, boolean draadloos, double prijs) {
		super();
		this.serienummer = serienummer;
		this.merk = merk;
		this.draadloos = draadloos;
		this.prijs = prijs;
	}

    public double getPrijsDollar(double omreken) {
        return prijs * omreken;
    }


    public String toString() {
        return "Het toetsenbord met serienummer " + serienummer + " kost � " + prijs;
    }

	
	

}
