package com.realdolmen.sports.domain;

public class Camp {
	private int id;
	private String campname;
	private Sportscenter center_id;
	private Sportsbranch sportsbranch_id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCampname() {
		return campname;
	}
	public void setCampname(String campname) {
		this.campname = campname;
	}
	public Sportscenter getCenter_id() {
		return center_id;
	}
	public void setCenter_id(Sportscenter center_id) {
		this.center_id = center_id;
	}
	public Sportsbranch getSportsbranch_id() {
		return sportsbranch_id;
	}
	public void setSportsbranch_id(Sportsbranch sportsbranch_id) {
		this.sportsbranch_id = sportsbranch_id;
	}
	public Camp(int id, String campname, Sportscenter center_id, Sportsbranch sportsbranch_id) {
		super();
		this.id = id;
		this.campname = campname;
		this.center_id = center_id;
		this.sportsbranch_id = sportsbranch_id;
	}
	public Camp() {
		super();
	}
	@Override
	public String toString() {
		return "Camp [id=" + id + ", campname=" + campname + ", center_id=" + center_id + ", sportsbranch_id="
				+ sportsbranch_id + "]";
	}
	
	
}
