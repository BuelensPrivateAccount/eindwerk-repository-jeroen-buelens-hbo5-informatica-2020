package com.realdolmen.sports.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySqlConnection {
    private static final String URL = "jdbc:mysql://localhost:3306/sports";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    private MySqlConnection() {
    }
    public static Connection createConnection(Connection connection) throws SQLException {
        if (connection == null || connection.isClosed()) {
            return DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } else {
            return connection;
        }
    }

    public static void closeConnections(ResultSet set, Connection con, PreparedStatement preparedStatement) {
        try {
            if (set != null) {
                set.close();
            }
            preparedStatement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

  
}
