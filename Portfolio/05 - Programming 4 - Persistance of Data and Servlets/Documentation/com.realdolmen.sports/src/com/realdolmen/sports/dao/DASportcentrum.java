package com.realdolmen.sports.dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import com.realdolmen.sports.domain.Sportscenter;

public class DASportcentrum {
    private final String url, login, password;
    public DASportcentrum(String url, String login, String password) 
    		throws ClassNotFoundException {
   
        this.url = url;
        this.login = login;
        this.password = password;
    }

  

	public Sportscenter getSportcentrum() {
        Sportscenter sportscenter = null;

        try (
             Connection connection = DriverManager.getConnection(url, login, password);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM sportscenter where id = 1");) 
            {
            if (resultSet.next()) {
                sportscenter = new Sportscenter();
                sportscenter.setId(resultSet.getInt("id"));
               
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sportscenter;
    }
}

