package com.realdolmen.sports.domain;

public class Sportsbranch {
	private String omschrijving;
	private int id;
	public String getOmschrijving() {
		return omschrijving;
	}
	public void setOmschrijving(String omschrijving) {
		this.omschrijving = omschrijving;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Sportsbranch(String omschrijving, int id) {
		super();
		this.omschrijving = omschrijving;
		this.id = id;
	}
	public Sportsbranch() {
		super();
	}
	@Override
	public String toString() {
		return "Sportsbranch [omschrijving=" + omschrijving + ", id=" + id + "]";
	}
	
	
}
