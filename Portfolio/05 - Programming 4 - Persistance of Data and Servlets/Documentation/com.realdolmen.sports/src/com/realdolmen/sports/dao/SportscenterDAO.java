package com.realdolmen.sports.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.realdolmen.sports.domain.Sportscenter;
import com.realdolmen.sports.utilities.MySqlConnection;

public class SportscenterDAO {
	private Connection con;
	private ResultSet set;
	private PreparedStatement statement;

	private Sportscenter setToData(ResultSet set) throws SQLException {
		Sportscenter sc = new Sportscenter();

		sc.setId(set.getInt("id"));

		return sc;
	}

	public Sportscenter findByID() {

		Sportscenter sc = new Sportscenter();

		try {
			con = MySqlConnection.createConnection(con);
			statement = con.prepareStatement("select * from sportscenter where id = 1;");
			set = statement.executeQuery();
			if (set.next()) {
				sc = setToData(set);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return sc;

	}

	public ArrayList<Sportscenter> getSportscenters() {
		ArrayList<Sportscenter> sportscenters = new ArrayList<>();
		try (Connection connection = MySqlConnection.createConnection(con);
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM sportscenter");) {
			while (resultSet.next()) {
				Sportscenter sportscenter = new Sportscenter();
				sportscenter.setId(resultSet.getInt("id"));
				sportscenter.setCentername(resultSet.getString("centername"));
				sportscenters.add(sportscenter);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sportscenters;
	}

	public Sportscenter getSportscenterBN(String sportscentername) {
		Sportscenter sportscenter = null;
		try (Connection connection = MySqlConnection.createConnection(con);
				PreparedStatement statement = connection.prepareStatement("SELECT * FROM sportscenter WHERE centername like '%Netepark%'");) {
			//statement.setString(1,sportscentername);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				sportscenter = new Sportscenter();
				sportscenter.setId(resultSet.getInt("id"));
				sportscenter.setCentername(resultSet.getString("centername"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sportscenter;
	}
	
	public Sportscenter getSportscenterByName(String sportscentername) {
		Sportscenter sportscenter = null;
		try (Connection connection = MySqlConnection.createConnection(con);
				PreparedStatement statement = connection.prepareStatement("SELECT * FROM sportscenter WHERE centername like ?");) {
			statement.setString(1,"%"+sportscentername+"%");
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				sportscenter = new Sportscenter();
				sportscenter.setId(resultSet.getInt("id"));
				sportscenter.setCentername(resultSet.getString("centername"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sportscenter;
	}


}
