package com.realdolmen.sports.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.realdolmen.sports.domain.Camp;
import com.realdolmen.sports.domain.Sportsbranch;
import com.realdolmen.sports.domain.Sportscenter;

public class SportscampDAO {
	String url = "jdbc:mysql://localhost:3306/sports";
	String login = "root";
	String password = "";
	Camp camp = null;
	ArrayList<Camp> camps = new ArrayList<>();

	public ArrayList<Camp> getCamps() {

		try (Connection connection = DriverManager.getConnection(url, login, password);
				PreparedStatement statement = connection
						.prepareStatement("select camp.id, center_id, sportscenter.id, centername, "
								+ "campname, sportsbranch_id, sportsbranch.id, omschrijving "
								+ "from camp, sportscenter, sportsbranch "
								+ "where center_id = sportscenter.id and sportsbranch_id = sportsbranch.id order by camp.id");) {
			ResultSet set = statement.executeQuery();

			while (set.next()) {
				camp = new Camp();
				Sportsbranch sb = new Sportsbranch();
				Sportscenter sc = new Sportscenter();
				camp.setCampname(set.getString("campname"));

				sb.setOmschrijving(set.getString("omschrijving"));
				camp.setSportsbranch_id(sb);
				camp.setCenter_id(sc);

				camps.add(camp);

			}
		} catch (SQLException e) {
			e.printStackTrace();

		}

		return camps;
	}
}
