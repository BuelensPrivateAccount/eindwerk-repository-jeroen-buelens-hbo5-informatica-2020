package com.realdolmen.sports.application;

import java.util.ArrayList;

import com.realdolmen.sports.dao.SportscampDAO;
import com.realdolmen.sports.dao.SportscenterDAO;
import com.realdolmen.sports.domain.Camp;
import com.realdolmen.sports.domain.Sportscenter;

public class SportsRunner {

	public static void main(String[] args) {
		

//		DASportcentrum scdao = new DASportcentrum("jdbc:mysql://localhost:3306/sports", "root","");
//		Sportscenter sc = scdao.getSportcentrum();
//		System.out.println(sc.toString());
		
		SportscenterDAO scdao = new SportscenterDAO();
		Sportscenter sc = scdao.findByID();
		System.out.println(sc.toString());
		
		ArrayList<Sportscenter> sportscenters = scdao.getSportscenters();
		for (Sportscenter sportscenter : sportscenters) {
		System.out.println(sportscenter.toString());
		}
		
		Sportscenter sportscenter = scdao.getSportscenterByName("Netepark");
		System.out.println(sportscenter.toString());
		
		SportscampDAO camp = new SportscampDAO();
		
		ArrayList<Camp> camps = camp.getCamps();
		for(Camp c : camps) {
			System.out.println(c.toString());
		}
		
		
		

	}

}
