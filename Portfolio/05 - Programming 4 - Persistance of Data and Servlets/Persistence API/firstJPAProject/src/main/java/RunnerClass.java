import beans.Point;

import javax.persistence.*;
import javax.persistence.Persistence;
import java.util.List;


public class RunnerClass {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("MyFirstPU");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        //put it in
        transaction.begin();
        for (int i = 0; i < 1000; i++) {
            Point point = new Point();
            point.setMessage("Happy birthday!");
            point.setName("maxime");
            entityManager.persist(point);
        }
        transaction.commit();

        // Retrieve all the Point objects from the database:
        TypedQuery<Point> query =  entityManager.createQuery("SELECT point FROM Point point", Point.class);
        List<Point> results = query.getResultList();
        for (Point p : results) {
            System.out.println(p);
        }

        //finishing up
        entityManager.close();
        entityManagerFactory.close();
    }
}
