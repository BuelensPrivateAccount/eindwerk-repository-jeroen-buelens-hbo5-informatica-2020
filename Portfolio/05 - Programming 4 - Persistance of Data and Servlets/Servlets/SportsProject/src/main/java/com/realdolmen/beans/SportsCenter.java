package com.realdolmen.beans;

public class SportsCenter extends AbstractBean {
    private String centername;
    private String street;
    private String housenumber;
    private int postalcode;
    private String city;
    private String phone;

    public SportsCenter(int id, String centername, String street, String housenumber, int postalcode, String city, String phone) {
        super(id);
        this.centername = centername;
        this.street = street;
        this.housenumber = housenumber;
        this.postalcode = postalcode;
        this.city = city;
        this.phone = phone;
    }

    public SportsCenter() {
    }

    public String getCentername() {
        return centername;
    }

    public void setCentername(String centername) {
        this.centername = centername;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(String housenumber) {
        this.housenumber = housenumber;
    }

    public int getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(int postalcode) {
        this.postalcode = postalcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "SportsCenter{" +
                "centername='" + centername + '\'' +
                ", street='" + street + '\'' +
                ", housenumber='" + housenumber + '\'' +
                ", postalcode=" + postalcode +
                ", city='" + city + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
