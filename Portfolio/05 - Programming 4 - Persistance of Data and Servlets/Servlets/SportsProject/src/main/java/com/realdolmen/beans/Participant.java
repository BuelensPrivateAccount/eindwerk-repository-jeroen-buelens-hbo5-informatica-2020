package com.realdolmen.beans;

import java.sql.Date;

public class Participant extends AbstractBean {
    private String participantname;
    private String firstname;
    private Date birthdate;
    private char sex;
    private String street;
    private String housenumber;
    private int bus;
    private int postalcode;
    private String city;
    private String phone;
    private String mobile;
    private String email;

    public Participant(int id, String participantname, String firstname, Date birthdate, char sex, String street, String housenumber, int bus, int postalcode, String city, String phone, String mobile, String email) {
        super(id);
        this.participantname = participantname;
        this.firstname = firstname;
        this.birthdate = birthdate;
        this.sex = sex;
        this.street = street;
        this.housenumber = housenumber;
        this.bus = bus;
        this.postalcode = postalcode;
        this.city = city;
        this.phone = phone;
        this.mobile = mobile;
        this.email = email;
    }

    public Participant() {
    }

    public String getParticipantname() {
        return participantname;
    }

    public void setParticipantname(String participantname) {
        this.participantname = participantname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(String housenumber) {
        this.housenumber = housenumber;
    }

    public int getBus() {
        return bus;
    }

    public void setBus(int bus) {
        this.bus = bus;
    }

    public int getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(int postalcode) {
        this.postalcode = postalcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Participant{" +
                "participantname='" + participantname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", birthdate=" + birthdate +
                ", sex=" + sex +
                ", street='" + street + '\'' +
                ", housenumber='" + housenumber + '\'' +
                ", bus=" + bus +
                ", postalcode=" + postalcode +
                ", city='" + city + '\'' +
                ", phone='" + phone + '\'' +
                ", mobile='" + mobile + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
