package com.realdolmen.controller;

import com.realdolmen.beans.SportsCenter;
import com.realdolmen.dao.SportCenterDAO;
import com.realdolmen.utilities.ConnectionManagerInterface;
import com.realdolmen.utilities.OracleConnectionManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "ManageServlet", urlPatterns = "/ManageServlet")
public class ManageServlet extends HttpServlet {

    private SportCenterDAO sportCenterDAO = null;

    @Override
    public void init() throws ServletException {
        ConnectionManagerInterface connectionManager = new OracleConnectionManager("admin", "sql");
        sportCenterDAO = new SportCenterDAO(connectionManager);
    }


    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            RequestDispatcher rd = request.getRequestDispatcher("sportcentrum.jsp");
            SportsCenter sportsCenter;
            int id = Integer.parseInt(request.getParameter("sportcentrumid"));
            sportsCenter = sportCenterDAO.findSportsCenterByID(id);
            request.setAttribute("sportscenter", sportsCenter);
            rd.forward(request, response);
        } catch (SQLException e) {
            RequestDispatcher rd = request.getRequestDispatcher("errorpage.jsp");
            if (e.getMessage().contains("no records")) {
                request.setAttribute("errormessage", e.getMessage());
            } else {
                throw new ServletException(e);
            }
            rd.forward(request,response);
        }


    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
}
