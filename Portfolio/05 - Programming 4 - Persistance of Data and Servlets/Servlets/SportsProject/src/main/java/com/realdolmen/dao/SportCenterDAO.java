package com.realdolmen.dao;

import com.realdolmen.beans.SportsCenter;
import com.realdolmen.utilities.ConnectionManagerInterface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SportCenterDAO {
    private ConnectionManagerInterface connectionManager;


    public SportCenterDAO(ConnectionManagerInterface connectionManagerInterface) {
        this.connectionManager = connectionManagerInterface;
    }

    public SportsCenter findSportsCenterByID(int id) throws SQLException {
        SportsCenter sportsCenter;
        Connection connection = this.connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("Select * from sportcentrum where id= ?");
        statement.setInt(1, id);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            sportsCenter = setToData(set);
        } else {
            throw new SQLException("no records were found while looking for a Sportcenter.");
        }
        set.close();
        statement.close();
        this.connectionManager.closeConnection();
        return sportsCenter;
    }

    public List<SportsCenter> findSportCenters() throws SQLException {
        List<SportsCenter> sportsCenters = new ArrayList<>();
        Connection connection = this.connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * from sportcentrum");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                sportsCenters.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("no records were found in the database, there are no Sportcenters.");
        }
        statement.close();
        set.close();
        this.connectionManager.closeConnection();
        return sportsCenters;
    }

    public List<SportsCenter> findSportCentersOnName(String name) throws SQLException {
        List<SportsCenter> sportsCenters = new ArrayList<>();
        Connection connection = this.connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * from sportcentrum where centrumnaam like ?;");
        statement.setString(1,"%" + name + "%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                sportsCenters.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("no records were found when looking for the name: "+name);
        }
        statement.close();
        set.close();
        this.connectionManager.closeConnection();
        return sportsCenters;
    }


    private SportsCenter setToData(ResultSet set) throws SQLException {
        return new SportsCenter(set.getInt("id"), set.getString("centrumnaam"), set.getString("straat"), set.getString("huisnummer"), set.getInt("postcode"), set.getString("woonplaats"), set.getString("telefoon"));
    }

}
