package com.realdolmen.beans;

public abstract class AbstractBean {
    private int id;

    public AbstractBean(){}

    public AbstractBean(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
