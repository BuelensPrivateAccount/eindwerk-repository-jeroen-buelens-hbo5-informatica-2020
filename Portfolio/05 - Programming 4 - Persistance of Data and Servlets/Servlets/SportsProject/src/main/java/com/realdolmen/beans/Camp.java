package com.realdolmen.beans;

import java.sql.Date;

public class Camp extends AbstractBean {
    private SportsCenter center;
    private String campname;
    private Date startdate;
    private Date enddate;
    private SportsBranch sportsbranch;
    private int min_birthyear;
    private int max_birthyear;
    private double price;
    private int available_spots;

    public Camp(int id, SportsCenter center, String campname, Date startdate, Date enddate, SportsBranch sportsbranch, int min_birthyear, int max_birthyear, double price, int available_spots) {
        super(id);
        this.center = center;
        this.campname = campname;
        this.startdate = startdate;
        this.enddate = enddate;
        this.sportsbranch = sportsbranch;
        this.min_birthyear = min_birthyear;
        this.max_birthyear = max_birthyear;
        this.price = price;
        this.available_spots = available_spots;
    }

    public Camp(){}

    public SportsCenter getCenter() {
        return center;
    }

    public void setCenter(SportsCenter center) {
        this.center = center;
    }

    public String getCampname() {
        return campname;
    }

    public void setCampname(String campname) {
        this.campname = campname;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public SportsBranch getSportsbranch() {
        return sportsbranch;
    }

    public void setSportsbranch(SportsBranch sportsbranch) {
        this.sportsbranch = sportsbranch;
    }

    public int getMin_birthyear() {
        return min_birthyear;
    }

    public void setMin_birthyear(int min_birthyear) {
        this.min_birthyear = min_birthyear;
    }

    public int getMax_birthyear() {
        return max_birthyear;
    }

    public void setMax_birthyear(int max_birthyear) {
        this.max_birthyear = max_birthyear;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAvailable_spots() {
        return available_spots;
    }

    public void setAvailable_spots(int available_spots) {
        this.available_spots = available_spots;
    }

    @Override
    public String toString() {
        return "Camp{" +
                "center=" + center +
                ", campname='" + campname + '\'' +
                ", startdate=" + startdate +
                ", enddate=" + enddate +
                ", sportsbranch=" + sportsbranch +
                ", min_birthyear=" + min_birthyear +
                ", max_birthyear=" + max_birthyear +
                ", price=" + price +
                ", available_spots=" + available_spots +
                '}';
    }
}
