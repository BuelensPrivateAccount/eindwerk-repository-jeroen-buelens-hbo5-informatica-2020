package com.realdolmen.utilities;

import java.sql.SQLException;

public interface ConnectionManagerInterface {
    java.sql.Connection getConnection() throws SQLException;

    void closeConnection();
}
