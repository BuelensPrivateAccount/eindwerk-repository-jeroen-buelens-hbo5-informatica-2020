package com.realdolmen.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class OracleConnectionManager implements ConnectionManagerInterface {
    private static final String URL = "jdbc:oracle:thin:@localhost:1521:XE";
    private String username;
    private String password;
    private java.sql.Connection connection;

    public OracleConnectionManager(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
           DriverManager.registerDriver( new oracle.jdbc.driver.OracleDriver());
            connection = DriverManager.getConnection(URL, this.username, this.password);
        }
        return connection;
    }

    @Override
    public void closeConnection() {
        try {
            if (!(this.connection == null || this.connection.isClosed())) {
                this.connection.close();
            }
        } catch (SQLException ignored) {
        }
    }
}
