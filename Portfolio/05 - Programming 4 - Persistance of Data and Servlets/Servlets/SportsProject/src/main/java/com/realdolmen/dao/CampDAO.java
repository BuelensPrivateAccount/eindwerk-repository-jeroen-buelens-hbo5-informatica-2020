package com.realdolmen.dao;



import com.realdolmen.beans.Camp;
import com.realdolmen.beans.SportsBranch;
import com.realdolmen.beans.SportsCenter;
import com.realdolmen.utilities.ConnectionManagerInterface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CampDAO {
    private ConnectionManagerInterface connectionManager;

    public CampDAO(ConnectionManagerInterface connectionManagerInterface) {
        this.connectionManager = connectionManagerInterface;
    }

    public Camp findCampByID(int campId) throws SQLException {
        Camp camp;
        Connection connection = this.connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * from kamp ca inner join sporttak br on ca.sporttak_id = br.id inner join sportcentrum sc on ca.centrum_id = sc.id where ca.id = ?;");
        statement.setInt(1, campId);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            camp = setToData(set);
        } else {
            throw new SQLException("no records were found.");
        }
        set.close();
        statement.close();
        this.connectionManager.closeConnection();
        return camp;
    }

    public List<Camp> findCampByName(String name) throws SQLException {
        List<Camp> camps = new ArrayList<>();
        Connection connection = this.connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * from kamp ca inner join sporttak br on ca.sporttak_id = br.id inner join sportcentrum sc on ca.centrum_id = sc.id where ca.KAMPNAAM like ?;");
        statement.setString(1, "%" + name + "%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                Camp camp = setToData(set);
                camps.add(camp);
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        set.close();
        statement.close();
        this.connectionManager.closeConnection();
        return camps;
    }

    private Camp setToData(ResultSet set) throws SQLException {
        return new Camp(set.getInt("ca.id"),
                new SportsCenter(
                        set.getInt("sc.id"),
                        set.getString("sc.centrumnaam"),
                        set.getString("sc.straat"),
                        set.getString("sc.huisnummer"),
                        set.getInt("sc.postcode"),
                        set.getString("sc.woonplaats"),
                        set.getString("sc.telefoon")),
                set.getString("ca.kampnaam"),
                set.getDate("ca.begindatum"),
                set.getDate("ca.einddatum"),
                new SportsBranch(set.getInt("br.id"),
                        set.getString("br.omschrijving")),
                set.getInt("ca.min_gebjaar"),
                set.getInt("ca.max_gebjaar"),
                set.getDouble("ca.prijs"),
                set.getInt("ca.aantal_plaatsen"));
    }

}
