package com.realdolmen.beans;

import java.sql.Date;

public class Registration extends AbstractBean {
    Date registrationDate;
    Camp camp;
    Participant participant;

    public Registration(int id, Date registrationDate, Camp camp, Participant participant) {
        super(id);
        this.registrationDate = registrationDate;
        this.camp = camp;
        this.participant = participant;
    }

    public Registration() {
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Camp getCamp() {
        return camp;
    }

    public void setCamp(Camp camp) {
        this.camp = camp;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    @Override
    public String toString() {
        return "Registration{" +
                "registrationDate=" + registrationDate +
                ", camp=" + camp +
                ", participant=" + participant +
                '}';
    }
}
