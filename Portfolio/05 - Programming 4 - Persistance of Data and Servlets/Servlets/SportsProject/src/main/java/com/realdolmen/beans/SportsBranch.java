package com.realdolmen.beans;

public class SportsBranch extends AbstractBean {
    private String omschrijving;

    public SportsBranch(int id, String omschrijving) {
        super(id);
        this.omschrijving = omschrijving;
    }

    public SportsBranch(){}

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    @Override
    public String toString() {
        return "SportsBranch{" +
                "omschrijving='" + omschrijving + '\'' +
                '}';
    }
}
