<%--
  Created by IntelliJ IDEA.
  User: JBLBL75
  Date: 03/02/2020
  Time: 11:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="com.realdolmen.beans.*" %>
<html>
<head>
    <title>SportCentrum</title>
    <link href="${pageContext.request.contextPath}/CSS/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<% SportsCenter sportsCenter = (SportsCenter) request.getAttribute("sportscenter");%>
<div id="headercontainer">
    <div id="header">
        <h1>Sport je Jong</h1>
    </div>
    <div id="content">
        <h1>Details sportcentrum <%=sportsCenter.getCentername()%></h1>
        <p>
            <label for="voornaamid">Naam:</label>
            <input type="text" name="naam" value="<%=sportsCenter.getCentername()%>" id="voornaamid"/>
        </p>
        <p>
            <label for="straatid">Straat en nummer:</label>
            <input type="text" name="straat" value="<%=sportsCenter.getStreet()%> <%=sportsCenter.getHousenumber()%>" id="straatid"/>
        </p>
        <p>
            <label for="postcodeid">Postcode:</label>
            <input type="text" name="postcode" value="<%=sportsCenter.getPostalcode()%>" id="postcodeid"/>
        </p>
        <p>
            <label for="woonplaatsid">Woonplaats:</label>
            <input type="text" name="woonplaats" id="woonplaatsid" value="<%=sportsCenter.getCity()%>"/>
        </p>
        <p>
            <label for="telefoonid">Telefoon:</label>
            <input type="text" name="telefoon" value="<%=sportsCenter.getPhone()%>" id="telefoonid">
        </p>
        <p>
            <a href="index.jsp">Terug naar beginpagina</a>
        </p>
    </div>
</div>
</body>
</html>
