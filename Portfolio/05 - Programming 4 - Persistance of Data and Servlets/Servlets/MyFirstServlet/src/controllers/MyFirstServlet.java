package controllers;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Servlet implementation class MyFirstServlet
 */
public class MyFirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String message;

	public void init() throws ServletException{
		message = "hello world";
	}

	public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<h1>"+message+"</h1>");
	}

	public void destroy(){}
}
