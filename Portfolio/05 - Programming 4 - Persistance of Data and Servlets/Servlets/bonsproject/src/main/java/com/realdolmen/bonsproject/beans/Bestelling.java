/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.bonsproject.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JBLBL75
 */
@Entity
@Table(name = "BESTELLING")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bestelling.findAll", query = "SELECT b FROM Bestelling b"),
    @NamedQuery(name = "Bestelling.findById", query = "SELECT b FROM Bestelling b WHERE b.id = :id"),
    @NamedQuery(name = "Bestelling.findByAantal", query = "SELECT b FROM Bestelling b WHERE b.aantal = :aantal"),
    @NamedQuery(name = "Bestelling.findByDatum", query = "SELECT b FROM Bestelling b WHERE b.datum = :datum")})
public class Bestelling implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AANTAL")
    private BigInteger aantal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datum;
    @JoinColumn(name = "BONID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Bon bonid;
    @JoinColumn(name = "KLANTID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Klant klantid;

    public Bestelling() {
    }

    public Bestelling(BigDecimal id) {
        this.id = id;
    }

    public Bestelling(BigDecimal id, BigInteger aantal, Date datum) {
        this.id = id;
        this.aantal = aantal;
        this.datum = datum;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigInteger getAantal() {
        return aantal;
    }

    public void setAantal(BigInteger aantal) {
        this.aantal = aantal;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Bon getBonid() {
        return bonid;
    }

    public void setBonid(Bon bonid) {
        this.bonid = bonid;
    }

    public Klant getKlantid() {
        return klantid;
    }

    public void setKlantid(Klant klantid) {
        this.klantid = klantid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bestelling)) {
            return false;
        }
        Bestelling other = (Bestelling) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.realdolmen.bonsproject.Bestelling[ id=" + id + " ]";
    }
    
}
