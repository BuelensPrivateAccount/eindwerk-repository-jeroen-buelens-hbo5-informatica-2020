<%@ page import="com.realdomen.beans.Game" %>
<%@ page import="java.util.List" %>
<%@ page import="com.realdomen.beans.Borrower" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Result</title>
</head>
<body>
<h1>Game:</h1>
<% List<Borrower> borrowers = (List<Borrower>) request.getAttribute("borrowers");
if(borrowers.size() ==1){
    response.sendRedirect("ManageServlet?choice=showborrowerbyid&id="+ borrowers.get(0).getpK());
}
for(Borrower borrower : borrowers){%>
<p><a href="ManageServlet?choice=showborrowerbyid&id=<%=borrower.getpK()%>"><%=borrower.getBorrowerName()%></a></p>
<%}%>
</body>
</html>
