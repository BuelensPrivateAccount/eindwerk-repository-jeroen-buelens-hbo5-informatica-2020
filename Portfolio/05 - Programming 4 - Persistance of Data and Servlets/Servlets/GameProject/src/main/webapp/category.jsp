<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="com.realdomen.beans.*" %>
<html>
<head>
    <title>Category</title>
</head>
<body>
<% Category category = (Category) request.getAttribute("category");%>
<h1><%=category.toString()%></h1>
<p><a href="index.jsp">Return to start</a></p>
</body>
</html>
