<%@ page import="com.realdomen.beans.Borrower" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: ikihiyori
  Date: 10/02/2020
  Time: 14:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>create a new loan</title>
</head>
<body>
<%
    int gameID = (Integer) request.getAttribute("GameID");
    List<Borrower> borrowers = (List<Borrower>) request.getAttribute("borrowers");
%>
<form action="/AdminServlet">
    <%if (borrowers != null && !borrowers.isEmpty()) {%>
    <label for="dropdownborrower">Borrowers:</label>
    <select id="dropdownborrower" onchange="showTheSpecifics()"><%
        for (Borrower borrower : borrowers) { %>
        <option value="<%=borrower.getpK()%>"><%=borrower.getBorrowerName()%>
        </option>
        <%}%>
    </select>
    <%}%>
    <p>days on loan: <input type="number" onkeyup="checkTextField()" id="daysOnLoan" name="daysOnLoan"/></p>
    <p>
        <button onclick="submitSave()" type="button" id="submitbutton" disabled>save</button>
        <input type="submit" name="init" value="go back"/>
    </p>
</form>
<script type="text/javascript">
    function checkTextField() {
        if (document.getElementById("daysOnLoan").value.length === 0) {
            document.getElementById("submitbutton").disabled = 'disabled';
        } else {
            document.getElementById("submitbutton").disabled = false;
        }
    }

    function submitSave() {
        let link = "/AdminServlet?action=saveAnewLoan";
        let element = document.getElementById("dropdownborrower");
        let borrowerID = element.options[element.selectedIndex].value;
        link += "&borrowerID=" + borrowerID;
        let daysOnLoan = document.getElementById("daysOnLoan").value;
        link += "&daysOnLoan=" + daysOnLoan;
        link += "&gameID="+ <%=gameID%>;
        window.location.replace(link);
    }
</script>
</body>
</html>
