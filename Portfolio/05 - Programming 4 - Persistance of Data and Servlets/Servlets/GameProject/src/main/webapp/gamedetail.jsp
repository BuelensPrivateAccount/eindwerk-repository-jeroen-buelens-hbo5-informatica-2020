<%@ page import="com.realdomen.beans.Game" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<% Game game = (Game) request.getAttribute("game");%>
<head>
    <title><%=game.getGameName()%></title>
</head>
<body>
<%

    if (game.getGameName() != null) {
%>
<h2>
    name: <%=game.getGameName()%>
</h2>
<%
    }
    if (game.getEditor() != null) {
%>
<p>
    editor: <%=game.getEditor()%>
</p>
<%
    }
    if (game.getAuthor() != null) {
%>
<p>author: <%=game.getAuthor()%>
</p>
<%
    }
    if (game.getYearEdition() != 0) {
%>
<p>year edition: <%=game.getYearEdition()%>
</p>
<%
    }
    if (game.getAge() != null) {
%>
<p>age: <%=game.getAge()%>
</p>
<%
    }
    if (game.getMinPlayers() != 0) {
%>
<p>minimum players: <%=game.getMinPlayers()%>
</p>
<%
    }
    if (game.getMaxPlayers() != 0) {
%>
<p>maximum players: <%=game.getMaxPlayers()%>
</p>
<%
    }
    if (game.getDifficulty().getName() != null) {
%>
<p>difficulty: <%=game.getDifficulty().getName()%>
</p>
<%
    }
    if (game.getCategory().getCategoryName() != null) {
%>
<p>category: <%=game.getCategory().getCategoryName()%>
</p>
<%
    }
    if (game.getPlayDuration() != null) {
%>
<p>play duration: <%=game.getPlayDuration()%>
</p>
<%
    }
    if (game.getPrice() != 0.0) {
%>
<p>price: $<%=game.getPrice()%>
</p>
<%
    }
    if (game.getImage() != null) {
%>
<p><img src="images/<%=game.getImage()%>" alt="<%=game.getImage()%>"></p>
<%
    }
%>
<p><a href="index.jsp">Return to start</a></p>
</body>
</html>
