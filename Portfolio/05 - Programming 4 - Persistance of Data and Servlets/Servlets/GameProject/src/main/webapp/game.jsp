<%@ page import="com.realdomen.beans.Game" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Result</title>
</head>
<body>
<h1>Game:</h1>
<% List<Game> games = (List<Game>) request.getAttribute("game");
if(games.size() ==1){
    response.sendRedirect("ManageServlet?choice=showgamebyid&id="+games.get(0).getpK());
}
for(Game game : games){%>
<p><a href="ManageServlet?choice=showgamebyid&id=<%=game.getpK()%>"><%=game.getGameName()%></a> category: <%=game.getCategory().getCategoryName()%> price: $<%=game.getPrice()%></p>
<%}%>
</body>
</html>
