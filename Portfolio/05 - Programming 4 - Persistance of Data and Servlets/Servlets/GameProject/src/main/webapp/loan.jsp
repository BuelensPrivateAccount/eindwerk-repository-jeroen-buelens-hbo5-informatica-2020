<%@ page import="com.realdomen.beans.Loan" %>
<%@ page import="java.util.List" %>
<%@ page import="com.realdomen.beans.Borrower" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Loans</title>
</head>
<body>
<%List<Loan> loans = (List<Loan>) request.getAttribute("loans");%>
<%List<Borrower> borrowers = (List<Borrower>) request.getAttribute("borrowers");%>
<%if (borrowers != null && !borrowers.isEmpty()) {%>
<label for="dropdownborrower">Borrowers:</label>
<select id="dropdownborrower" onchange="showTheSpecifics()"><%
    for (Borrower borrower : borrowers) { %>
    <option value="<%=borrower.getpK()%>"><%=borrower.getBorrowerName()%>
    </option>
    <%}%>
</select>
<%}%>
<%
    if (loans != null && !loans.isEmpty()) {
        for (Loan loan : loans) {
%>
<p><%=loan.toString()%>
</p>
<%
        }
    }
    if (validateLoansListForButtonReturn(loans)) {
%>
<p>
<form action="ManageServlet" method="post">
    <input type="submit" value="show all loans" name="loan"/>
</form>
</p><%}%>
<script type="text/javascript">
    function showTheSpecifics() {
        let option = document.getElementById("dropdownborrower");
        let value = option.options[option.selectedIndex].value;
        window.location.replace("/ManageServlet?choice=findLoanById&id=" + value)

    }
</script>
<p><a href="index.jsp">Return to index page</a></p>
</body>
</html>
<%!
    public boolean validateLoansListForButtonReturn(List<Loan> loans) {
        Loan previousLoan = null;
        if (loans == null || loans.isEmpty()) {
            return false;
        }
        for (Loan loan : loans) {
            if (previousLoan != null) {
                if (!(previousLoan.getBorrower().getBorrowerName().equals(loan.getBorrower().getBorrowerName()))) {
                    return false;
                }
            }
            previousLoan = loan;
        }
        return true;
    }%>