<%@ page import="com.realdomen.beans.Game" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Adminpage</title>
</head>
<body>
<%
    int booleanInt = 0;
    if (request.getAttribute("success") != null) {
        booleanInt = (Integer) request.getAttribute("success");
    }
    List<Game> games = (List<Game>) request.getAttribute("games");
    if (booleanInt == 1) {
        response.sendRedirect("ManageServlet?choice=showallloans");
    } else {
        if (games != null) {
            for (Game game : games) {%>
<p><a href="AdminServlet?action=opendetails&id=<%=game.getpK()%>"><%=game.getGameName()%>
</a></p>
<%
            }
        }
    }
%>
</body>
</html>