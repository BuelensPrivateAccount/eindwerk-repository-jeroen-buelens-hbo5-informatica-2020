<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>IndexPage</title>
</head>
<body>
<header>
    <h1>GamesMiniProject</h1>
</header>
<main>
    <form action="ManageServlet" method="post">
        <p><input type="submit" value="Show First Category" name="FirstCategory"/></p>
        <p><a href="/ManageServlet?choice=FirstBorrower">Show First Borrower</a></p>
        <p><input type="submit" value="Show 5th Game" name="game5"/></p>
        <p>
            <label>Show a game of choice</label>
            <input name="gameofchoicetext" type="text" placeholder="name of the game" id="gameofchoicetext" onkeyup="checkTextField()"/>
            <input type="submit" value="search" name="gameofchoice" id="gameofchoicebutton">
            <script type="text/javascript">
            function checkTextField() {
                if (document.getElementById("gameofchoicetext").value.length === 0) {
                    document.getElementById("gameofchoicebutton").disabled = 'disabled';
                } else {
                    document.getElementById("gameofchoicebutton").disabled = false;
                }
            }
        </script>
        </p>
        <p>
            <input type="submit" value="all games" name="allgames">
        </p>
        <p>
            <input type="submit" value="show all borrowings" name="loan"/>
        </p>
        <p>
            <input type="submit" value="advanced search games" name="init" formmethod="post" formaction="SearchServlet">
        </p>
        <p><input type="submit" value="advanced search borrowers"  name="initBorrower" formmethod="post"  formaction="SearchServlet"/> </p>
        <p>
            <input type="submit" value="Administrative page" name="init" formaction="AdminServlet" formmethod="post">
        </p>
    </form>
</main>
</body>
</html>