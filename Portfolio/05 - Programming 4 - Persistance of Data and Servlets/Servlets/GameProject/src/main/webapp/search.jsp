<%@ page import="com.realdomen.beans.Category" %>
<%@ page import="java.util.List" %>
<%@ page import="com.realdomen.beans.Difficulty" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Advanced Search page</title>
</head>
<body>
<h1>Advanced search page</h1>
<h2>Search for a game</h2>
<form action="/SearchServlet">
    <%List<Category> categories = (List<Category>) request.getAttribute("allcategories");%>
    <%List<Difficulty> difficultyList = (List<Difficulty>) request.getAttribute("alldifficulties");%>
    <p>
        <label for="dropdowndifficulty">Minimal Difficulty: </label>
        <select id="dropdowndifficulty" name="gamedifficulty" onchange="checkTextFieldsGame()">
            <option value="0">all</option>
            <%for (Difficulty difficulty : difficultyList) {%>
            <option value="<%=difficulty.getpK()%>"><%=difficulty.getName()%>
            </option>
            <%}%>
        </select>
    </p>
    <p>
        <label for="dropdowndcategory">Category: </label>
        <select id="dropdowndcategory" name="gamecategory" onchange="checkTextFieldsGame()">
            <option value="0">no Category</option>
            <%for (Category category : categories) {%>
            <option value="<%=category.getpK()%>"><%=category.getCategoryName()%>
            </option>
            <%}%>
        </select>
    </p>
    <p>
        <label for="gamenametext">name:
        </label>
        <input type="text" placeholder="game name" name="gamenamesearch" id="gamenametext"
               onkeyup="checkTextFieldsGame()"/>
    </p>
    <p>
        <label for="gameeditortext">editor:</label>
        <input type="text" placeholder="editor" name="gameeditorsearch" id="gameeditortext"
               onkeyup="checkTextFieldsGame()"/>
    </p>
    <p>
        <label for="gameyeareditionnumber">year edition: </label>
        <input type="number" placeholder="year" name="gameyearedition" id="gameyeareditionnumber" min="1970"
               onkeyup="checkTextFieldsGame()">
    </p>
    <p>
        <button onclick="submitSearch()" type="button" id="submitbutton" disabled>Search</button>
    </p>
</form>
<h2>Search for a borrower</h2>
<form action="/SearchServlet">
    <label></label>
    <input type="text" placeholder="borrower name" name="borrowernamesearch" id="borrowernamesearch"
           onkeyup="checkTextFieldsBorrower()">
    <input type="submit" id="submitsearchborrower" name="submitsearchborrower"value="search" disabled>
</form>
<script type="text/javascript">
    function checkTextFieldsBorrower() {
        let button = document.getElementById("submitsearchborrower");
        let textbox = document.getElementById("borrowernamesearch");
        if (!(textbox.value.length === 0)) {
            button.disabled = false;
        } else {
            button.disabled = 'disabled';
        }
    }

    function checkTextFieldsGame() {
        let button = document.getElementById("submitbutton");
        let gamenametextbox = document.getElementById("gamenametext");
        let gameeditortextbox = document.getElementById("gameeditortext");
        let gameyeartextbox = document.getElementById("gameyeareditionnumber");
        let difficulty = document.getElementById("dropdowndifficulty");
        let category = document.getElementById("dropdowndcategory");
        if (!(gameeditortextbox.value.length === 0) || !(gamenametextbox.value.length === 0) || !(gameyeartextbox.value.length === 0) || !(difficulty.value === "0" && category.value === "0")) {
            button.disabled = false;
        } else {
            button.disabled = 'disabled';
        }
    }

    function submitSearch() {
        let link = "/SearchServlet?action=advancedsearch";
        let element = document.getElementById("dropdowndcategory");
        let categoryID = element.options[element.selectedIndex].value;
        if (categoryID !== "0") {
            link += "&categoryID=" + categoryID;
        }
        element = document.getElementById("dropdowndifficulty");
        let difficultyID = element.options[element.selectedIndex].value;
        if (difficultyID !== "0") {
            link += "&difficultyID=" + difficultyID;
        }

        let yearEditionNumber = document.getElementById("gameyeareditionnumber").value;
        if (yearEditionNumber !== "") {
            link += "&yearEdition=" + yearEditionNumber;
        }
        let gameName = document.getElementById("gamenametext").value;
        if (gameName !== "") {
            link += "&gameName=" + gameName;
        }
        let editor = document.getElementById("gameeditortext").value;
        if (editor !== "") {
            link += "&editor=" + editor;
        }
        window.location.replace(link);
    }
</script>
</body>
</html>
