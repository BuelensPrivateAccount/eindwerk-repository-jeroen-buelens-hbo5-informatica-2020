package com.realdomen.beans;
/**
 * This is the Game Class, this is the java object belonging with the Game table on the database, with it's getters and setters
 */
public class Game extends AbstractBean {
    private String gameName;
    private String editor;
    private String author;
    private int yearEdition;
    private String age;
    private int minPlayers;
    private int maxPlayers;
    private Difficulty difficulty;
    private Category category;
    private String playDuration;
    private double price;
    private String image;

    public Game(){}
    public Game(int id, String gameName, String editor, String author, int yearEdition, String age, int minPlayers, int maxPlayers, Difficulty difficulty, Category category, String playDuration, double price, String image) {
        super(id);
        this.gameName = gameName;
        this.editor = editor;
        this.author = author;
        this.yearEdition = yearEdition;
        this.age = age;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
        this.difficulty = difficulty;
        this.category = category;
        this.playDuration = playDuration;
        this.price = price;
        this.image = image;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYearEdition() {
        return yearEdition;
    }

    public void setYearEdition(int yearEdition) {
        this.yearEdition = yearEdition;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public String getPlayDuration() {
        return playDuration;
    }

    public void setPlayDuration(String playDuration) {
        this.playDuration = playDuration;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Game{" +
                "gameName='" + gameName + '\'' +
                ", editor='" + editor + '\'' +
                ", author='" + author + '\'' +
                ", yearEdition=" + yearEdition +
                ", age='" + age + '\'' +
                ", minPlayers=" + minPlayers +
                ", maxPlayers=" + maxPlayers +
                ", difficulty=" + difficulty +
                ", category=" + category +
                ", playDuration='" + playDuration + '\'' +
                ", price=" + price +
                ", image='" + image + '\'' +
                '}';
    }
}
