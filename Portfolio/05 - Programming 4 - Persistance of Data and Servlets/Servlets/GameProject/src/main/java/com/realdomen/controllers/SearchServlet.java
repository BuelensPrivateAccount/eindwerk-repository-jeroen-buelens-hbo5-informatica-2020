package com.realdomen.controllers;

import com.realdomen.beans.Borrower;
import com.realdomen.beans.Category;
import com.realdomen.beans.Difficulty;
import com.realdomen.beans.Game;
import com.realdomen.repo.BorrowerDAO;
import com.realdomen.repo.CategoryDAO;
import com.realdomen.repo.DifficultyDAO;
import com.realdomen.repo.GameDAO;
import com.realdomen.utilities.ConnectionManager;
import com.realdomen.utilities.MySQLConnectionManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "SearchServlet", urlPatterns = "/SearchServlet")
public class SearchServlet extends HttpServlet {
    CategoryDAO categoryDAO = null;
    DifficultyDAO difficultyDAO = null;
    GameDAO gameDAO = null;
    BorrowerDAO borrowerDAO = null;

    @Override
    public void init() throws ServletException {
        try {
            ConnectionManager connectionManager = new MySQLConnectionManager("root", "");
            categoryDAO = new CategoryDAO(connectionManager);
            difficultyDAO = new DifficultyDAO(connectionManager);
            gameDAO = new GameDAO(connectionManager);
            borrowerDAO = new BorrowerDAO(connectionManager);
        } catch (SQLException e) {
            throw new ServletException(e);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            RequestDispatcher rd = null;
            if (request.getParameter("init") != null) {
                rd = initSearchPage(request, response);
            } else if (request.getParameter("submitsearchborrower") != null) {
                rd = searchBorrower(request, response);
            } else if (request.getParameter("action") != null) { //hyperlink management
                String action = request.getParameter("action");
                if ("advancedsearch".equals(action.toLowerCase())) {
                    rd = advancedSearch(request, response);
                }
            }
            if (rd != null) {
                rd.forward(request, response);
            } else {
                throw new ServletException("you tried to do a a method/option that isn't implemented or available yet, please contact staff.");
            }
        } catch (SQLException e) {
            RequestDispatcher rd = request.getRequestDispatcher("errorPage.jsp");
            if (e.getMessage().contains("no records")) {
                request.setAttribute("errormessage", e.getMessage());
            } else {
                request.setAttribute("errormessage", "if you see this page, that means an error has occurred, we're sorry.");
                request.setAttribute("devstacktrace", e);
            }
            rd.forward(request, response);
        }

    }

    private RequestDispatcher searchBorrower(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("borrowerlist.jsp");
        String name = request.getParameter("borrowernamesearch");
        List<Borrower> borrowers = borrowerDAO.findAllBorrowersWithName(name);
        request.setAttribute("borrowers", borrowers);
        return rd;
    }


    private RequestDispatcher advancedSearch(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("game.jsp");
        List<Game> games;
        int categoryID = 0;
        int difficultyID = 0; //mag 0 zijn in query want > 0
        int year = 0;
        String gameName = "";
        String editor = "";
        String parameter = request.getParameter("categoryID");
        if (parameter != null) {
            categoryID = Integer.parseInt(request.getParameter("categoryID"));
        }
        parameter = request.getParameter("difficultyID");
        if (parameter != null) {
            difficultyID = Integer.parseInt(request.getParameter("difficultyID"));
        }
        parameter = request.getParameter("yearEdition");
        if (parameter != null) {
            year = Integer.parseInt(request.getParameter("yearEdition"));
        }
        parameter = request.getParameter("gameName");
        if (parameter != null) {
            gameName = request.getParameter("gameName").toLowerCase();
        }
        parameter = request.getParameter("editor");
        if (parameter != null) {
            gameName = request.getParameter("editor").toLowerCase();
        }
        if (categoryID == 0) {
            if (year == 0) {
                games = gameDAO.findGameAdvancedSearchNoCategoryAndNoYear(difficultyID, gameName, editor);
            } else {
                games = gameDAO.findGameAdvancedSearchNoCategory(difficultyID, year, gameName, editor);
            }
        } else {
            if (year == 0) {
                games = gameDAO.findGameAdvancedSearchNoYear(categoryID, difficultyID, gameName, editor);
            } else {
                games = gameDAO.findGameAdvancedSearch(categoryID, difficultyID, year, gameName, editor);
            }
        }
        request.setAttribute("game", games);
        return rd;
    }

    private RequestDispatcher initSearchPage(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("search.jsp");
        List<Difficulty> difficulties = difficultyDAO.findAllDifficulies();
        request.setAttribute("alldifficulties", difficulties);
        List<Category> categories = categoryDAO.findAllCategories();
        request.setAttribute("allcategories", categories);
        return rd;
    }
}
