package com.realdomen.controllers;

import com.realdomen.beans.Borrower;
import com.realdomen.beans.Game;
import com.realdomen.beans.Loan;
import com.realdomen.repo.*;
import com.realdomen.utilities.ConnectionManager;
import com.realdomen.utilities.MySQLConnectionManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

@WebServlet(name = "AdminServlet", urlPatterns = "/AdminServlet")
public class AdminServlet extends HttpServlet {
    CategoryDAO categoryDAO = null;
    DifficultyDAO difficultyDAO = null;
    GameDAO gameDAO = null;
    BorrowerDAO borrowerDAO = null;
    LoanDAO loanDAO = null;

    @Override
    public void init() throws ServletException {
        try {
            ConnectionManager connectionManager = new MySQLConnectionManager("root", "");
            categoryDAO = new CategoryDAO(connectionManager);
            difficultyDAO = new DifficultyDAO(connectionManager);
            gameDAO = new GameDAO(connectionManager);
            borrowerDAO = new BorrowerDAO(connectionManager);
            loanDAO = new LoanDAO(connectionManager);
        } catch (SQLException e) {
            throw new ServletException(e);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            RequestDispatcher rd = null;
            if (request.getParameter("init") != null) {
                rd = initAdminHome(request.getRequestDispatcher("adminPage.jsp"), request, response);
            } else if (request.getParameter("action") != null) { //hyperlink management
                String action = request.getParameter("action");
                if (action.equals("saveAnewLoan")) {
                    int daysOnLoan = Integer.parseInt(request.getParameter("daysOnLoan"));
                    int borrowerID = Integer.parseInt(request.getParameter("borrowerID"));
                    int gameID = Integer.parseInt(request.getParameter("gameID"));
                    rd = CreateANewLoan(daysOnLoan, borrowerID, gameID, request, response);
                } else if(action.equals("opendetails")){
                    int id = Integer.parseInt(request.getParameter("id"));
                    rd = initDetails(id,request,response);
                }
            }
            if (rd != null) {
                rd.forward(request, response);
            } else {
                throw new ServletException("you tried to do a a method/option that isn't implemented or available yet, please contact staff.");
            }
        } catch (SQLException e) {
            RequestDispatcher rd = request.getRequestDispatcher("errorPage.jsp");
            if (e.getMessage().contains("no records")) {
                request.setAttribute("errormessage", e.getMessage());
            } else {
                request.setAttribute("errormessage", "if you see this page, that means an error has occurred, we're sorry.");
                request.setAttribute("devstacktrace", e);
            }
            rd.forward(request, response);
        }

    }

    private RequestDispatcher initDetails(int id, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("newLoan.jsp");
        List<Borrower> borrowers = borrowerDAO.findAllBorrowerNameAndPKS();
        request.setAttribute("GameID", id);
        request.setAttribute("borrowers",borrowers);
        return rd;
    }

    private RequestDispatcher CreateANewLoan(int daysOnLoan, int borrowerID, int gameID, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("adminPage.jsp");
        Date now = new java.sql.Date(new java.util.Date().getTime());
        Date future = addDays(now, daysOnLoan);
        boolean success = loanDAO.createNewLoan(borrowerID, gameID, now, future);
        int booleanInt = checkSuccess(success);
        request.setAttribute("success", booleanInt);
        return rd;
    }

    private int checkSuccess(boolean success) {
        if(success){
            return 1;
        }else{
            return 0;
        }
    }

    private RequestDispatcher initAdminHome(RequestDispatcher rd, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        List<Game> gameList = gameDAO.findgames();
        request.setAttribute("games", gameList);
        return rd;
    }

    private static Date addDays(Date date, int days) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, days);
        return new Date(c.getTimeInMillis());
    }

}
