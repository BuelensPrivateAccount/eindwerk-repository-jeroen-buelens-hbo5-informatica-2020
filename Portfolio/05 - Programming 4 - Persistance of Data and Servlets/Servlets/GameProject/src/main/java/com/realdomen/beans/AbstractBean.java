package com.realdomen.beans;

public abstract class AbstractBean {
    int id;

    public AbstractBean() {
    }

    public AbstractBean(int id) {
        this.id = id;
    }

    public int getpK() {
        return id;
    }

    public void setpK(int id) {
        this.id = id;
    }
}
