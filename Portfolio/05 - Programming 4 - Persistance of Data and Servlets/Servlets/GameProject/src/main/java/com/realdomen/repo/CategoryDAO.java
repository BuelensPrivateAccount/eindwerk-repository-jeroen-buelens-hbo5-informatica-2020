package com.realdomen.repo;

import com.realdomen.beans.Category;
import com.realdomen.utilities.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CategoryDAO {
    private ConnectionManager connectionManager;

    public CategoryDAO(ConnectionManager connectionManager) throws SQLException {
        this.connectionManager = connectionManager;
    }

    public Category findCategoryByID(int id) throws SQLException{
        Category category;
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT category.id,category.category_name from category category where id = ?");
        statement.setInt(1,id);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            category = fullSetToData(set);
        } else {
            throw new SQLException("no records were found: no Category was found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return category;
    }

    private Category fullSetToData (ResultSet set) throws SQLException{
        return new Category(set.getInt("category.id"),set.getString("category.category_name"));
    }

    public List<Category> findAllCategories() throws SQLException {
        List<Category> categories = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT category.id,category.category_name from category category;");
        ResultSet set = statement.executeQuery();
        if(set.next()){
            do{
                categories.add(fullSetToData(set));
            }while(set.next());
        }else{
            throw new SQLException("there are no categories on the database");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return categories;
    }
}
