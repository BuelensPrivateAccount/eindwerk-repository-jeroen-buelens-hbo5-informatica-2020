package com.realdomen.controllers;

import com.realdomen.beans.Borrower;
import com.realdomen.beans.Category;
import com.realdomen.beans.Game;
import com.realdomen.beans.Loan;
import com.realdomen.repo.BorrowerDAO;
import com.realdomen.repo.CategoryDAO;
import com.realdomen.repo.GameDAO;
import com.realdomen.repo.LoanDAO;
import com.realdomen.utilities.ConnectionManager;
import com.realdomen.utilities.MySQLConnectionManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


@WebServlet(name = "ManageServlet", urlPatterns = "/ManageServlet")
public class ManageServlet extends HttpServlet {
    private CategoryDAO categoryDAO = null;
    private BorrowerDAO borrowerDAO = null;
    private GameDAO gameDAO = null;
    private LoanDAO loanDAO = null;

    @Override
    public void init() throws ServletException {
        try {
            ConnectionManager connectionManager = new MySQLConnectionManager("root", "");
            categoryDAO = new CategoryDAO(connectionManager);
            borrowerDAO = new BorrowerDAO(connectionManager);
            gameDAO = new GameDAO(connectionManager);
            loanDAO = new LoanDAO(connectionManager);
        } catch (SQLException e) {
            throw new ServletException(e);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            RequestDispatcher rd = null;
            if (request.getParameter("FirstCategory") != null) {
                rd = ShowFirstCategory(request, response);
            } else if (request.getParameter("game5") != null) {
                rd = showAGameByID(5, request, response);
            } else if (request.getParameter("gameofchoice") != null) {
                String s = request.getParameter("gameofchoicetext");
                rd = showAGameByName(s, request, response);
            } else if (request.getParameter("allgames") != null) {
                rd = showAllGames(request, response);
            } else if (request.getParameter("loan") != null) {
                rd = getAllBorrowersAndAllLoans(request, response);
            } else if (request.getParameter("choice") != null) {
                String choice = request.getParameter("choice");
                switch (choice) {
                    case "FirstBorrower":
                        rd = ShowABorrower(1, request, response);
                        break;
                    case "showgamebyid": {
                        int id = Integer.parseInt(request.getParameter("id"));
                        rd = showAGameByID(id, request, response);
                        break;
                    }
                    case "findLoanById": {
                        int id = Integer.parseInt(request.getParameter("id"));
                        rd = getAllBorrowersAndASingleSpecificLoan(id, request, response);
                        break;
                    }
                    case "showborrowerbyid":{
                        int id = Integer.parseInt(request.getParameter("id"));
                        rd = ShowABorrower(id,request,response);
                        break;
                    }
                    case "showallloans":{
                        rd = getAllBorrowersAndAllLoans(request, response);
                        break;
                    }
                }
            }
            if (rd != null) {
                rd.forward(request, response);
            } else {
                throw new ServletException("you tried to do a a method/option that isn't implemented or available yet, please contact staff.");
            }
        } catch (SQLException e) {
            RequestDispatcher rd = request.getRequestDispatcher("errorPage.jsp");
            if (e.getMessage().contains("no records")) {
                request.setAttribute("errormessage", e.getMessage());
            } else {
                request.setAttribute("errormessage", "if you see this page, that means an error has occurred, we're sorry.");
                request.setAttribute("devstacktrace", e);
            }
            rd.forward(request, response);

        }
    }

    private RequestDispatcher getAllBorrowersAndAllLoans(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("loan.jsp");
        List<Borrower> borrowers = borrowerDAO.findAllBorrowerNamesOfBorrowersWhomHaveAtLeastASingleLoan();
        request.setAttribute("borrowers", borrowers);
        List<Loan> loans = loanDAO.findAllLoansContainingGameNameBorrowerNameBorrowDateAndReturnDate();
        request.setAttribute("loans",loans);
        return rd;
    }

    private RequestDispatcher getAllBorrowersAndASingleSpecificLoan(int id, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("loan.jsp");
        List<Borrower> borrowers = borrowerDAO.findAllBorrowerNamesOfBorrowersWhomHaveAtLeastASingleLoan();
        request.setAttribute("borrowers", borrowers);
        List<Loan> loan = loanDAO.findLoansContainingGameNameBorrowerNameBorrowDateAndReturnDateFromASpecificBorrower(id);
        request.setAttribute("loans", loan);
        return requestDispatcher;
    }

    private RequestDispatcher showAllGames(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("game.jsp");
        List<Game> games = gameDAO.findgames();
        request.setAttribute("game", games);
        return rd;
    }

    private RequestDispatcher showAllLoans(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("loan.jsp");
        List<Loan> loans = loanDAO.findAllLoansContainingGameNameBorrowerNameBorrowDateAndReturnDate();
        request.setAttribute("loans", loans);
        return rd;
    }

    private RequestDispatcher showAGameByName(String s, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("game.jsp");
        List<Game> games = gameDAO.findgameNamePriceAndCategoryNamebyGamename(s);
        request.setAttribute("game", games);
        return rd;
    }

    private RequestDispatcher showAGameByID(int id, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("gamedetail.jsp");
        Game game = gameDAO.findGameByID(id);
        request.setAttribute("game", game);
        return rd;
    }

    private RequestDispatcher ShowABorrower(int id, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("borrower.jsp");
        Borrower borrower = borrowerDAO.findBorrowerNameAndCityByID(id);
        request.setAttribute("borrower", borrower);
        return rd;
    }

    private RequestDispatcher ShowFirstCategory(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("category.jsp");
        Category category = categoryDAO.findCategoryByID(1);
        request.setAttribute("category", category);
        return rd;
    }
}
