package com.realdomen.beans;
/**
 * This is the Difficulty Class, this is the java object belonging with the Difficulty table on the database, with it's getters and setters
 */
public class Difficulty extends AbstractBean {
    private String name;

    public Difficulty(){}
    public Difficulty(int id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Difficulty{" +
                "name='" + name + '\'' +
                '}';
    }
}
