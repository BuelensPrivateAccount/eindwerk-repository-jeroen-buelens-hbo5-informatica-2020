package com.realdomen.repo;

import com.realdomen.beans.Borrower;
import com.realdomen.beans.Category;
import com.realdomen.utilities.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BorrowerDAO {
    private ConnectionManager connectionManager;

    public BorrowerDAO(ConnectionManager connectionManager) throws SQLException {
        this.connectionManager = connectionManager;
    }

    public List<Borrower> findAllBorrowerNamesOfBorrowersWhomHaveAtLeastASingleLoan() throws SQLException{
        List<Borrower> borrowers = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select distinct borrower.id,borrower.borrower_name from borrower borrower inner join borrow borrow on borrower.id = borrow.borrower_id;");
        ResultSet set = statement.executeQuery();
        if(set.next()){
            do{
                Borrower borrower = new Borrower();
                borrower.setpK(set.getInt("borrower.id"));
                borrower.setBorrowerName(set.getString("borrower.borrower_name"));
                borrowers.add(borrower);
            }while (set.next());
        } else {
            throw new SQLException("no records were found: There was no Borrower found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return borrowers;
    }

    public Borrower findBorrowerByID(int id) throws SQLException {
        Borrower borrower;
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT borrower.id,borrower.borrower_name,borrower.street,borrower.house_number,borrower.bus_number,borrower.postcode,borrower.city,borrower.telephone,borrower.email from borrower borrower where id = ?");
        statement.setInt(1, id);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            borrower = fullSetToData(set);
        } else {
            throw new SQLException("no records were found: There was no Borrower found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return borrower;
    }

    public Borrower findBorrowerNameAndCityByID(int id) throws SQLException {
        Borrower borrower;
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT borrower.id," +
                "borrower.borrower_name," +
                "borrower.city" +
                " from borrower borrower where id = ?");
        statement.setInt(1, id);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            borrower = new Borrower();
            borrower.setpK(set.getInt("borrower.id"));
            borrower.setBorrowerName(set.getString("borrower.borrower_name"));
            borrower.setCity(set.getString("borrower.city"));
        } else {
            throw new SQLException("no records were found: There was no Borrower found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return borrower;
    }

    private Borrower fullSetToData(ResultSet set) throws SQLException {
        return new Borrower(set.getInt("borrower.id"), set.getString("borrower.borrower_name"), set.getString("borrower.street"), set.getString("borrower.house_number"), set.getString("borrower.bus_number"), set.getInt("borrower.postcode"), set.getString("borrower.city"), set.getString("borrower.telephone"), set.getString("borrower.email"));
    }


    public List<Borrower> findAllBorrowersWithName(String name) throws SQLException {
        List<Borrower> borrowers = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select  borrower.id,borrower.borrower_name from borrower borrower where borrower_name like ?;");
        statement.setString(1,name);
        ResultSet set = statement.executeQuery();
        if(set.next()){
            do{
                Borrower borrower = new Borrower();
                borrower.setpK(set.getInt("borrower.id"));
                borrower.setBorrowerName(set.getString("borrower.borrower_name"));
                borrowers.add(borrower);
            }while (set.next());
        } else {
            throw new SQLException("no records were found: There was no Borrower found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return borrowers;
    }

    public List<Borrower> findAllBorrowerNameAndPKS() throws SQLException {
        List<Borrower> borrowers = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select  borrower.id,borrower.borrower_name from borrower borrower;");
        ResultSet set = statement.executeQuery();
        if(set.next()){
            do{
                Borrower borrower = new Borrower();
                borrower.setpK(set.getInt("borrower.id"));
                borrower.setBorrowerName(set.getString("borrower.borrower_name"));
                borrowers.add(borrower);
            }while (set.next());
        } else {
            throw new SQLException("no records were found: There was no Borrower found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return borrowers;
    }
}
