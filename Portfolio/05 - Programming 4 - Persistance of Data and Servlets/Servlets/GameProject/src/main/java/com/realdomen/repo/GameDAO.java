package com.realdomen.repo;

import com.realdomen.beans.Category;
import com.realdomen.beans.Difficulty;
import com.realdomen.beans.Game;
import com.realdomen.utilities.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GameDAO {
    private ConnectionManager connectionManager;

    public GameDAO(ConnectionManager connectionManager){
        this.connectionManager = connectionManager;
    }

    public Game findGameByID(int id) throws SQLException {
        Game game;
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select game.id," +
                "game.game_name," +
                "game.editor," +
                "game.author," +
                "game.year_edition," +
                "game.age," +
                "game.min_players," +
                "game.max_players," +
                "difficulty.id," +
                "difficulty.difficulty_name," +
                "category.id," +
                "category.category_name," +
                "game.play_duration," +
                "game.price," +
                "game.image " +
                "from game game " +
                "inner join category category " +
                "on game.category_id = category.id " +
                "inner join difficulty difficulty " +
                "on game.difficulty_id = difficulty.id" +
                " where game.id = ?;");
        statement.setInt(1, id);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            game = fullSetToData(set);
        } else {
            throw new SQLException("no records were found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return game;
    }

    public List<Game> findgamesbyname(String s) throws SQLException {
        List<Game> games = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select game.id," +
                "game.game_name," +
                "game.editor," +
                "game.author," +
                "game.year_edition," +
                "game.age," +
                "game.min_players," +
                "game.max_players," +
                "difficulty.id," +
                "difficulty.difficulty_name," +
                "category.id," +
                "category.category_name," +
                "game.play_duration," +
                "game.price," +
                "game.image " +
                "from game game " +
                "inner join category category " +
                "on game.category_id = category.id " +
                "inner join difficulty difficulty " +
                "on game.difficulty_id = difficulty.id" +
                " where game.game_name like ?" +
                " order by game.game_name;");
        statement.setString(1, "%" + s + "%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                games.add(fullSetToData(set));
            }
            while (set.next());
        } else {
            throw new SQLException("no records were found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return games;
    }
    public List<Game> findgameNamePriceAndCategoryNamebyGamename(String s) throws SQLException {
        List<Game> games = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select game.id," +
                "game.game_name," +
                "game.editor," +
                "category.category_name," +
                "game.price " +
                "from game game " +
                "inner join category category " +
                "on game.category_id = category.id " +
                "inner join difficulty difficulty " +
                "on game.difficulty_id = difficulty.id" +
                " where game.game_name like ?" +
                " order by game.game_name;");
        statement.setString(1, "%" + s + "%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                Game game = new Game();
                game.setpK(set.getInt("game.id"));
                game.setGameName(set.getString("game.game_name"));
                game.setEditor(set.getString("game.editor"));
                Category category = new Category();
                category.setCategoryName(set.getString("category.category_name"));
                game.setCategory(category);
                game.setPrice(set.getDouble("game.price"));
                games.add(game);
            }
            while (set.next());
        } else {
            throw new SQLException("no records were found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return games;
    }
    public List<Game> findgames() throws SQLException {
        List<Game> games = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select game.id," +
                "game.game_name," +
                "game.editor," +
                "game.author," +
                "game.year_edition," +
                "game.age," +
                "game.min_players," +
                "game.max_players," +
                "difficulty.id," +
                "difficulty.difficulty_name," +
                "category.id," +
                "category.category_name," +
                "game.play_duration," +
                "game.price," +
                "game.image " +
                "from game game " +
                "inner join category category " +
                "on game.category_id = category.id " +
                "inner join difficulty difficulty " +
                "on game.difficulty_id = difficulty.id" +
                " order by game.game_name;");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                games.add(fullSetToData(set));
            }
            while (set.next());
        } else {
            throw new SQLException("no records were found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return games;
    }

    private Game fullSetToData(ResultSet set) throws SQLException {
        return new Game(set.getInt("game.id"),
                set.getString("game.game_name"),
                set.getString("game.editor"),
                set.getString("game.author"),
                set.getInt("game.year_edition"),
                set.getString("game.age"),
                set.getInt("game.min_players"),
                set.getInt("game.max_players"),
                new Difficulty(
                        set.getInt("difficulty.id"),
                        set.getString("difficulty.difficulty_name")
                ),
                new Category(
                        set.getInt("category.id"),
                        set.getString("category.category_name")
                ),
                set.getString("game.play_duration"),
                set.getDouble("game.price"),
                set.getString("game.image")
        );
    }


    public List<Game> findGameAdvancedSearchNoCategory(int difficultyID, int year, String gameName, String editor) throws SQLException {
        List<Game> games = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select game.id,game.game_name,game.editor,game.author,game.year_edition,game.age,game.min_players,game.max_players,difficulty.id,difficulty.difficulty_name,category.id,category.category_name,game.play_duration,game.price,game.image " +
                "from game game " +
                "inner join category category " +
                "on game.category_id = category.id " +
                "inner join difficulty difficulty " +
                "on game.difficulty_id = difficulty.id " +
                "AND game.difficulty_id > ? " +
                "AND game.year_edition = ? " +
                "AND game.game_name like ?" +
                "AND game.editor like ? " +
                "order by game.game_name;");
        statement.setInt(1,difficultyID);
        statement.setInt(2,year);
        statement.setString(3,"%"+gameName+"%");
        statement.setString(4,"%"+editor+"%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                games.add(fullSetToData(set));
            }
            while (set.next());
        } else {
            throw new SQLException("no records were found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return games;
    }

    public List<Game> findGameAdvancedSearch(int categoryID, int difficultyID, int year, String gameName, String editor) throws SQLException {
        List<Game> games = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select game.id,game.game_name,game.editor,game.author,game.year_edition,game.age,game.min_players,game.max_players,difficulty.id,difficulty.difficulty_name,category.id,category.category_name,game.play_duration,game.price,game.image " +
                "from game game " +
                        "inner join category category " +
                        "on game.category_id = category.id " +
                        "inner join difficulty difficulty " +
                        "on game.difficulty_id = difficulty.id " +
                        "where game.category_id = ? " +
                        "AND game.difficulty_id > ? " +
                        "AND game.year_edition = ? " +
                        "AND game.game_name like ?" +
                        "AND game.editor like ? " +
                        "order by game.game_name;");
        statement.setInt(1,categoryID);
        statement.setInt(2,difficultyID);
        statement.setInt(3,year);
        statement.setString(4,"%"+gameName+"%");
        statement.setString(5,"%"+editor+"%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                games.add(fullSetToData(set));
            }
            while (set.next());
        } else {
            throw new SQLException("no records were found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return games;
    }

    public List<Game> findGameAdvancedSearchNoCategoryAndNoYear(int difficultyID, String gameName, String editor) throws SQLException {
        List<Game> games = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select game.id,game.game_name,game.editor,game.author,game.year_edition,game.age,game.min_players,game.max_players,difficulty.id,difficulty.difficulty_name,category.id,category.category_name,game.play_duration,game.price,game.image " +
                "from game game " +
                "inner join category category " +
                "on game.category_id = category.id " +
                "inner join difficulty difficulty " +
                "on game.difficulty_id = difficulty.id " +
                "AND game.difficulty_id > ? " +
                "AND game.game_name like ?" +
                "AND game.editor like ? " +
                "order by game.game_name;");
        statement.setInt(1,difficultyID);
        statement.setString(2,"%"+gameName+"%");
        statement.setString(3,"%"+editor+"%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                games.add(fullSetToData(set));
            }
            while (set.next());
        } else {
            throw new SQLException("no records were found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return games;
    }

    public List<Game> findGameAdvancedSearchNoYear(int categoryID, int difficultyID, String gameName, String editor) throws SQLException {
        List<Game> games = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select game.id,game.game_name,game.editor,game.author,game.year_edition,game.age,game.min_players,game.max_players,difficulty.id,difficulty.difficulty_name,category.id,category.category_name,game.play_duration,game.price,game.image " +
                "from game game " +
                "inner join category category " +
                "on game.category_id = category.id " +
                "inner join difficulty difficulty " +
                "on game.difficulty_id = difficulty.id " +
                "where game.category_id = ? " +
                "AND game.difficulty_id > ? " +
                "AND game.game_name like ?" +
                "AND game.editor like ? " +
                "order by game.game_name;");
        statement.setInt(1,categoryID);
        statement.setInt(2,difficultyID);
        statement.setString(3,"%"+gameName+"%");
        statement.setString(4,"%"+editor+"%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                games.add(fullSetToData(set));
            }
            while (set.next());
        } else {
            throw new SQLException("no records were found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return games;
    }
}
