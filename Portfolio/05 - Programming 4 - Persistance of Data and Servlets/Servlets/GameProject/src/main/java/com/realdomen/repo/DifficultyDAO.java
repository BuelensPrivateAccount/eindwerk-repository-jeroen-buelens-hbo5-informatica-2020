package com.realdomen.repo;

import com.realdomen.beans.Difficulty;
import com.realdomen.utilities.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DifficultyDAO {
    private ConnectionManager connectionManager;

    public DifficultyDAO(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public List<Difficulty> findAllDifficulies() throws SQLException {
       List<Difficulty> difficultyList = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT difficulty.id,difficulty.difficulty_name from difficulty difficulty;");
        ResultSet set = statement.executeQuery();
        if(set.next()){
            do{
                difficultyList.add(fullSetToData(set));
            }while(set.next());
        }else{
            throw new SQLException("there are no Difficulties on the database");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return difficultyList;
    }

    private Difficulty fullSetToData(ResultSet set) throws SQLException{
        return new Difficulty(set.getInt("difficulty.id"),set.getString("difficulty.difficulty_name"));
    }
}
