package com.realdomen.beans;

/**
 * This is the Category Class, this is the java object belonging with the Category table on the database, with it's getters and setters
 */
public class Category extends AbstractBean {
    private String categoryName;

    public Category(){}
    public Category(int id, String categoryName) {
        super(id);
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryName='" + categoryName + '\'' +
                '}';
    }
}


