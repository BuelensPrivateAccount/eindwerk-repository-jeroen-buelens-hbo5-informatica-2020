package com.realdomen.repo;

import com.realdomen.beans.Borrower;
import com.realdomen.beans.Game;
import com.realdomen.beans.Loan;
import com.realdomen.utilities.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LoanDAO {
    private ConnectionManager connectionManager;

    public LoanDAO(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public List<Loan> findLoansContainingGameNameBorrowerNameBorrowDateAndReturnDateFromASpecificBorrower(int id) throws SQLException {
        List<Loan> loans = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select game.game_name, borrower.borrower_name, loan.borrow_date, loan.return_date from borrow loan inner join game game on game.id = loan.game_id inner join borrower borrower on loan.borrower_id = borrower.id where borrower.id = ? order by game.game_name,loan.borrow_date;");
        statement.setInt(1, id);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                Loan loan = new Loan();
                loan.setBorrowDate(set.getDate("loan.borrow_date"));
                loan.setReturnDate(set.getDate("loan.return_date"));
                Borrower borrower = new Borrower();
                borrower.setBorrowerName(set.getString("borrower.borrower_name"));
                loan.setBorrower(borrower);
                Game game = new Game();
                game.setGameName(set.getString("game.game_name"));
                loan.setGame(game);
                loans.add(loan);
            } while (set.next());
        } else {
            throw new SQLException("no records were found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return loans;
    }


    public List<Loan> findAllLoansContainingGameNameBorrowerNameBorrowDateAndReturnDate() throws SQLException {
        List<Loan> loans = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select game.game_name," +
                "borrower.id," +
                "borrower.borrower_name," +
                "loan.borrow_date," +
                "loan.return_date " +
                "from borrow loan " +
                "inner join game game " +
                "on game.id = loan.game_id " +
                "inner join borrower borrower " +
                "on loan.borrower_id = borrower.id " +
                "order by game.game_name," +
                "loan.borrow_date");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                Loan loan = new Loan();
                loan.setBorrowDate(set.getDate("loan.borrow_date"));
                loan.setReturnDate(set.getDate("loan.return_date"));
                Borrower borrower = new Borrower();
                borrower.setpK(set.getInt("borrower.id"));
                borrower.setBorrowerName(set.getString("borrower.borrower_name"));
                loan.setBorrower(borrower);
                Game game = new Game();
                game.setGameName(set.getString("game.game_name"));
                loan.setGame(game);
                loans.add(loan);
            } while (set.next());
        } else {
            throw new SQLException("no records were found.");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return loans;
    }

    private Loan fullSetToData(ResultSet set) throws SQLException {

        return new Loan();
    }

    public boolean createNewLoan(int borrowerID, int gameID, Date now, Date future) {
        try {
            Connection connection = connectionManager.getConnection();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO games.borrow(game_id,borrower_id,borrow_date,return_date) values(?,?,?,?);");
            statement.setInt(1, gameID);
            statement.setInt(2, borrowerID);
            statement.setDate(3, now);
            statement.setDate(4, future);
            statement.execute();
            statement.close();
            connectionManager.closeConnection();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }
}
