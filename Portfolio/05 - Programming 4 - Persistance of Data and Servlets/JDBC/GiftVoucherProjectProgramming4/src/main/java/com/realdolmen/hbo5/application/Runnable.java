package com.realdolmen.hbo5.application;

import com.realdolmen.hbo5.domain.Client;
import com.realdolmen.hbo5.domain.Orders;
import com.realdolmen.hbo5.domain.Voucher;
import com.realdolmen.hbo5.repo.ClientDAO;
import com.realdolmen.hbo5.repo.OrderDAO;
import com.realdolmen.hbo5.repo.VoucherDAO;
import com.realdolmen.hbo5.utilities.ConnectionManager;
import com.realdolmen.hbo5.utilities.MySQLConnectionManager;

import java.util.List;

public class Runnable {
    public static void main(String[] args) {
        ConnectionManager connectionManager = null;
        try {
            connectionManager = new MySQLConnectionManager("root", "");
            ClientDAO clientDAO = new ClientDAO(connectionManager);
            Client oneClient = clientDAO.findClientById(1);
            System.out.println(oneClient.toString());
            List<Client> clients = clientDAO.findAllClients();
            printAllinList(clients);
            clients = clientDAO.findClientsByName("e");
            printAllinList(clients);
            VoucherDAO voucherDAO = new VoucherDAO(connectionManager);
            Voucher oneVoucher = voucherDAO.findVoucherById(1);
            System.out.println(oneVoucher.toString());
            List<Voucher>vouchers = voucherDAO.findAllVouchers();
            printAllinList(vouchers);
            vouchers = voucherDAO.findVoucherByName("e");
            printAllinList(vouchers);
            OrderDAO orderDAO = new OrderDAO(connectionManager);
            List<Orders> orders = orderDAO.findAllOrders();
            printAllinList(orders);
            orders = orderDAO.findOrdersByClientName("e");
            printAllinList(orders);
            Orders orders1 = orderDAO.findOrdersByID(1);
            System.out.println(orders1.toString());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connectionManager != null) {
                connectionManager.closeConnection();
            }
        }
    }

    private static void printAllinList(List<?> list){
        for(Object o : list){
            System.out.println(o.toString());
        }
    }
}
