package com.realdolmen.hbo5.domain;

import java.sql.Date;

public class Voucher {
    private int id;
    private String vouchernr;
    private Theme theme;
    private String name;
    private String Information;
    private String supplier;
    private double price;
    private Date valid_until;

    public Voucher(int id, String vouchernr, Theme theme, String name, String information, String supplier, double price, Date valid_until) {
        this.id = id;
        this.vouchernr = vouchernr;
        this.theme = theme;
        this.name = name;
        Information = information;
        this.supplier = supplier;
        this.price = price;
        this.valid_until = valid_until;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVouchernr() {
        return vouchernr;
    }

    public void setVouchernr(String vouchernr) {
        this.vouchernr = vouchernr;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInformation() {
        return Information;
    }

    public void setInformation(String information) {
        Information = information;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getValid_until() {
        return valid_until;
    }

    public void setValid_until(Date valid_until) {
        this.valid_until = valid_until;
    }

    @Override
    public String toString() {
        return "Voucher{" +
                "id=" + id +
                ", vouchernr='" + vouchernr + '\'' +
                ", theme=" + theme +
                ", name='" + name + '\'' +
                ", Information='" + Information + '\'' +
                ", supplier='" + supplier + '\'' +
                ", price=" + price +
                ", valid_until=" + valid_until +
                '}';
    }
}
