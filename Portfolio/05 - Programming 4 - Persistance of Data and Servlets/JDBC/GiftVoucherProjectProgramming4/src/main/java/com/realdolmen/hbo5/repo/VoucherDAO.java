package com.realdolmen.hbo5.repo;

import com.realdolmen.hbo5.domain.Theme;
import com.realdolmen.hbo5.domain.Voucher;
import com.realdolmen.hbo5.utilities.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class VoucherDAO {
    private ConnectionManager connectionManager;

    public VoucherDAO(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public Voucher findVoucherById(int id) throws SQLException {
        Voucher voucher;
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select voucher.id," +
                "voucher.vouchernr," +
                "theme.id,theme.theme," +
                "voucher.name,voucher.information," +
                "voucher.supplier,voucher.price,voucher." +
                "valid_until " +
                "from voucher voucher" +
                " inner join theme theme " +
                "on theme.id = voucher.themeID " +
                "where voucher.id = ?;");
        statement.setInt(1, id);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                voucher = setToData(set);
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return voucher;
    }

    public List<Voucher> findVoucherByName(String name) throws SQLException {
        List<Voucher> vouchers = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select voucher.id," +
                "voucher.vouchernr," +
                "theme.id,theme.theme," +
                "voucher.name,voucher.information," +
                "voucher.supplier,voucher.price,voucher." +
                "valid_until " +
                "from voucher voucher" +
                " inner join theme theme " +
                "where voucher.name like ?;");
        statement.setString(1, "%"+name+"%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                vouchers.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return vouchers;
    }

    public List<Voucher> findAllVouchers() throws SQLException {
        List<Voucher> vouchers = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select voucher.id," +
                "voucher.vouchernr," +
                "theme.id,theme.theme," +
                "voucher.name,voucher.information," +
                "voucher.supplier,voucher.price,voucher." +
                "valid_until " +
                "from voucher voucher" +
                " inner join theme theme;");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                vouchers.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return vouchers;
    }

    private Voucher setToData(ResultSet set) throws SQLException {
        return new Voucher(
                set.getInt("voucher.id"),
                set.getString("voucher.vouchernr"),
                new Theme(
                        set.getInt("theme.id"),
                        set.getString("theme.theme")
                ),
                set.getString("voucher.name"),
                set.getString("voucher.information"),
                set.getString("voucher.supplier"),
                set.getDouble("voucher.price"),
                set.getDate("voucher.valid_until")
        );
    }
}
