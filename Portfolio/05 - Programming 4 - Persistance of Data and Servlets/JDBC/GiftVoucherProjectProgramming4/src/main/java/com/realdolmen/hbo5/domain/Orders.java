package com.realdolmen.hbo5.domain;

import java.sql.Date;

public class Orders {
    private int id;
    private Voucher voucher;
    private Client client;
    private int quantity;
    private Date date;

    public Orders(int id, Voucher voucher, Client client, int quantity, Date date) {
        this.id = id;
        this.voucher = voucher;
        this.client = client;
        this.quantity = quantity;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Voucher getVoucher() {
        return voucher;
    }

    public void setVoucher(Voucher voucher) {
        this.voucher = voucher;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "id=" + id +
                ", voucher=" + voucher +
                ", client=" + client +
                ", quantity=" + quantity +
                ", date=" + date +
                '}';
    }
}
