package com.realdolmen.hbo5.repo;

import com.realdolmen.hbo5.domain.Theme;
import com.realdolmen.hbo5.utilities.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ThemeDAO {
   private ConnectionManager connectionManager;

    public ThemeDAO(ConnectionManager connectionManager){
        this.connectionManager = connectionManager;
    }

    public Theme findThemeByID(int id) throws SQLException {
        Theme theme;
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select theme.id,theme.theme from theme theme where theme.id = ?;");
        statement.setInt(1, id);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                theme = setToData(set);
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return theme;
    }
    public List<Theme> findThemeByName(String name) throws SQLException {
        List<Theme> themes = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select theme.id,theme.theme from theme theme where theme.theme like ?;");
        statement.setString(1,"%"+name+"%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                themes.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return themes;
    }
    public List<Theme> findAllThemes() throws SQLException {
        List<Theme> themes = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select theme.id,theme.theme from theme theme;");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                themes.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return themes;
    }

    private Theme setToData(ResultSet set) throws SQLException {
        return new Theme(
                set.getInt("theme.id"),
                set.getString("theme.theme")
        );
    }
}
