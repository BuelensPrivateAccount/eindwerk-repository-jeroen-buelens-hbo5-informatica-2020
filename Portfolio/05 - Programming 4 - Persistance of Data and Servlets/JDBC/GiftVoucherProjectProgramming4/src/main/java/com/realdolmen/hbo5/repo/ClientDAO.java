package com.realdolmen.hbo5.repo;

import com.realdolmen.hbo5.domain.Client;
import com.realdolmen.hbo5.utilities.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientDAO {
    private ConnectionManager connectionManager;

    public ClientDAO(ConnectionManager connectionManager) throws SQLException {
        this.connectionManager = connectionManager;
    }

    public Client findClientById(int id) throws SQLException {
        Client client;
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select client.id,client.name,client.firstname,client.phonenumber,client.email from client client where client.id = ?;");
        statement.setInt(1, id);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                client = setToData(set);
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return client;
    }

    public List<Client> findClientsByName(String name) throws SQLException {
        List<Client> clients = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select client.id,client.name,client.firstname,client.phonenumber,client.email from client client where client.name like ?;");
        statement.setString(1, "%" + name + "%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                clients.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return clients;
    }

    public List<Client> findAllClients() throws SQLException {
        List<Client> clients = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select client.id,client.name,client.firstname,client.phonenumber,client.email from client client;");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                clients.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return clients;
    }

    private Client setToData(ResultSet set) throws SQLException {
        return new Client(
                set.getInt("client.id"),
                set.getString("client.name"),
                set.getString("client.firstname"),
                set.getString("client.phonenumber"),
                set.getString("client.email")
        );
    }
}
