package com.realdolmen.hbo5.repo;

import com.realdolmen.hbo5.domain.Client;
import com.realdolmen.hbo5.domain.Orders;
import com.realdolmen.hbo5.domain.Theme;
import com.realdolmen.hbo5.domain.Voucher;
import com.realdolmen.hbo5.utilities.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class OrderDAO {
    private ConnectionManager connectionManager;

    public OrderDAO(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public List<Orders> findAllOrders() throws SQLException {
        List<Orders> orders = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select " +
                "orders.id,voucher.id," +
                "voucher.vouchernr," +
                "theme.id,theme.theme" +
                ",voucher.name,voucher.information,voucher.supplier,voucher.price,voucher.valid_until," +
                "client.id,client.name,client.firstname,client.phonenumber,client.email," +
                "orders.quantity,orders.date " +
                "from orders orders" +
                " inner join voucher voucher" +
                " on voucher.id = orders.voucherID " +
                "inner join theme theme " +
                "on theme.id = voucher.themeID " +
                "inner join client client " +
                "on orders.clientID = client.id;");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                orders.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        statement.close();
        set.close();
        connectionManager.closeConnection();
        return orders;
    }

    public List<Orders> findOrdersByClientName(String name) throws SQLException {
        List<Orders> orders = new ArrayList<>();
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select " +
                "orders.id,voucher.id," +
                "voucher.vouchernr," +
                "theme.id,theme.theme" +
                ",voucher.name,voucher.information,voucher.supplier,voucher.price,voucher.valid_until," +
                "client.id,client.name,client.firstname,client.phonenumber,client.email," +
                "orders.quantity,orders.date " +
                "from orders orders" +
                " inner join voucher voucher" +
                " on voucher.id = orders.voucherID " +
                "inner join theme theme " +
                "on theme.id = voucher.themeID " +
                "inner join client client " +
                "on orders.clientID = client.id " +
                "where client.name like ? ;");
        statement.setString(1, "%" + name + "%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                orders.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return orders;
    }

    public Orders findOrdersByID(int id) throws SQLException {
        Orders orders;
        Connection connection = connectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("select " +
                "orders.id,voucher.id," +
                "voucher.vouchernr," +
                "theme.id,theme.theme" +
                ",voucher.name,voucher.information,voucher.supplier,voucher.price,voucher.valid_until," +
                "client.id,client.name,client.firstname,client.phonenumber,client.email," +
                "orders.quantity,orders.date " +
                "from orders orders" +
                " inner join voucher voucher" +
                " on voucher.id = orders.voucherID " +
                "inner join theme theme " +
                "on theme.id = voucher.themeID " +
                "inner join client client " +
                "on orders.clientID = client.id " +
                "where orders.id = ?;");
        statement.setInt(1,id);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                orders = setToData(set);
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        set.close();
        statement.close();
        connectionManager.closeConnection();
        return orders;
    }

    private Orders setToData(ResultSet set) throws SQLException {
        return new Orders(
                set.getInt("orders.id"),
                new Voucher(
                        set.getInt("voucher.id"),
                        set.getString("voucher.vouchernr"),
                        new Theme(
                                set.getInt("theme.id"),
                                set.getString("theme.theme")
                        ),
                        set.getString("voucher.name"),
                        set.getString("voucher.information"),
                        set.getString("voucher.supplier"),
                        set.getDouble("voucher.price"),
                        set.getDate("voucher.valid_until")
                ),
                new Client(
                        set.getInt("client.id"),
                        set.getString("client.name"),
                        set.getString("client.firstname"),
                        set.getString("client.phonenumber"),
                        set.getString("client.email")
                ), set.getInt("orders.quantity"),
                set.getDate("orders.date")
        );
    }
}
