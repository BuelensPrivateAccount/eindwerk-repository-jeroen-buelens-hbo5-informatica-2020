package main.domain;

import java.sql.Date;

public class Participant {
    private int id;
    private String participantName;
    private String firstName;
    private Date birthDate;
    private char sex;
    private String street;
    private String houseNumber;
    private int bus;
    private int postalCode;
    private String city;
    private String email;
    private String mobile;
    private String phone;

    public Participant(int id, String participantName, String firstName, Date birthDate, char sex, String street, String houseNumber, int bus, int postalCode, String city, String email, String mobile, String phone) {
        this.id = id;
        this.participantName = participantName;
        this.firstName = firstName;
        this.birthDate = birthDate;
        this.sex = sex;
        this.street = street;
        this.houseNumber = houseNumber;
        this.bus = bus;
        this.postalCode = postalCode;
        this.city = city;
        this.email = email;
        this.mobile = mobile;
        this.phone = phone;
    }

    public Participant() {
        this.id = 0;
        this.participantName = "participantName";
        this.firstName =  "firstName";
        this.birthDate = new Date(0L);
        this.sex = "x".toCharArray()[0];
        this.street = "street";
        this.houseNumber = "houseNumber";
        this.bus = 0;
        this.postalCode = 0;
        this.city = "city";
        this.email = "email";
        this.mobile = "mobile";
        this.phone = "phone";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getParticipantName() {
        return participantName;
    }

    public void setParticipantName(String participantName) {
        this.participantName = participantName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public int getBus() {
        return bus;
    }

    public void setBus(int bus) {
        this.bus = bus;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
