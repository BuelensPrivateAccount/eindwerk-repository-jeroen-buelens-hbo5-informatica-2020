package main.domain;

import java.sql.Date;

public class Registration {
    Date registrationDate;
    Camp camp;
    Participant participant;

    public Registration(Date registrationDate, Camp camp, Participant participant) {
        this.registrationDate = registrationDate;
        this.camp = camp;
        this.participant = participant;
    }

    public Registration() {
        this.registrationDate = new Date(0L);
        this.camp = new Camp();
        this.participant = new Participant();
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Camp getCamp() {
        return camp;
    }

    public void setCamp(Camp camp) {
        this.camp = camp;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }
}
