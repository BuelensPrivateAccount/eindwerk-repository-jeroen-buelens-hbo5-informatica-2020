package main.utilities;

import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnectionManager implements ConnectionManager {
    private static final String URL = "jdbc:mysql://localhost:3306/sports";
    private String username;
    private String password;
    private java.sql.Connection connection;

    public MySQLConnectionManager(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public java.sql.Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(URL, this.username, this.password);
        }
        return connection;

    }

    public void closeConnection() {
        try {
            if (!(this.connection == null || this.connection.isClosed())) {
                this.connection.close();
            }
        } catch (SQLException ignored) {
        }
    }
}
