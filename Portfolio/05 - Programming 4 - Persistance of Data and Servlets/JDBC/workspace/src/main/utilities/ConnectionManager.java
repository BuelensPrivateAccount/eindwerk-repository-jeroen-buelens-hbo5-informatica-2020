package main.utilities;

import java.sql.SQLException;

public interface ConnectionManager {
    java.sql.Connection getConnection() throws SQLException;

    void closeConnection();
}
