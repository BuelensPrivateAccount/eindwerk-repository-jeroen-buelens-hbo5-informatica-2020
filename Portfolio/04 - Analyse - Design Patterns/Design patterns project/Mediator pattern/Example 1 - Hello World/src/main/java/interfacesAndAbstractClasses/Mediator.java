package interfacesAndAbstractClasses;


public interface Mediator {
    public void send(String message, Colleague colleague);
}

