package widget;

import abstractClasses.DialogueDirector;

public class Button extends Widget {

    public Button(DialogueDirector dialogueDirector) {
        super(dialogueDirector);
    }

    public void click() {
        changed();
    }
}
