package abstractClasses;


import widget.Widget;


public interface DialogueDirector {

    public void showDialogue();

    public void widgetChanged(Widget widget);

    public void addWidget(Widget widget);

    public void removeWidget(Widget widget);

}
