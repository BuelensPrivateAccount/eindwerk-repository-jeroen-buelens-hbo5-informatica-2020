package widget;

import abstractClasses.DialogueDirector;

public abstract class Widget {

    DialogueDirector dialogueDirector;

    public Widget(DialogueDirector dialogueDirector) {
        this.dialogueDirector = dialogueDirector;
        dialogueDirector.addWidget(this);
    }
    public void changed(){
        dialogueDirector.widgetChanged(this);
    }
}
