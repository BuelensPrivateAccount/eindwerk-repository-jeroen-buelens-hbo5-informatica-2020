package client;

import director.FontDialogueDirector;
import widget.Button;
import widget.EntryField;

public class Client {
    public static void main(String[] args) {
        FontDialogueDirector fontDialogueDirector = new FontDialogueDirector();
        Button button = new Button(fontDialogueDirector);
        EntryField entryField = new EntryField(fontDialogueDirector);

        System.out.println("in our fontwindow we now enter a string of text");
        entryField.enterText();
        System.out.println("as this is a demo im now simulating a buttonPress:");
        button.click();
    }
}
