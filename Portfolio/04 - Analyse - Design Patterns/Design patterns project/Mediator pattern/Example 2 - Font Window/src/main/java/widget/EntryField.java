package widget;

import abstractClasses.DialogueDirector;

import java.util.Scanner;

public class EntryField extends Widget {
   private String textInTextBox;

    public EntryField(DialogueDirector dialogueDirector) {
        super(dialogueDirector);
    }

    public void enterText(){
        Scanner scanner = new Scanner(System.in);
        textInTextBox = scanner.nextLine();
        changed();
    }

    public String getTextInTextBox() {
        return textInTextBox;
    }

}
