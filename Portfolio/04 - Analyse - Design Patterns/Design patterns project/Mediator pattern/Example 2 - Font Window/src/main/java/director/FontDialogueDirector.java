package director;

import abstractClasses.DialogueDirector;
import widget.Button;
import widget.EntryField;
import widget.Widget;

import java.util.ArrayList;
import java.util.List;

public class FontDialogueDirector implements DialogueDirector {
    List<Widget> widgets = new ArrayList<Widget>();


    public void showDialogue() {

    }

    public void widgetChanged(Widget widget) {
        if (widget instanceof Button) {
            System.out.println("The button was clicked");
        } else if (widget instanceof EntryField) {
            EntryField entryField = (EntryField) widget;
            String s = entryField.getTextInTextBox();
            System.out.println("The entryfield was filled with : " + s);
        }
    }

    public void addWidget(Widget widget) {
        widgets.add(widget);
    }

    public void removeWidget(Widget widget) {
        widgets.remove(widget);
    }
}
