import expansion.CurrentConditionDisplay;
import expansion.ForecastDisplay;
import expansion.HeatIndexDisplay;
import expansion.WeatherData;

import java.util.Random;


public class WeatherStation {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        CurrentConditionDisplay currentConditionDisplay = new CurrentConditionDisplay(weatherData);
        ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);
        HeatIndexDisplay heatIndexDisplay = new HeatIndexDisplay(weatherData);

        System.out.println("this is a model test of the observer pattern, \nthis will use this pattern to get the data to a display to display the temperature and humidity data in the console.");
        for (int i = 0; i < 9; i++) {
            float temperature = generateRandomFloat(0f,30f);
            float humidity = generateRandomFloat(0f,100f);
            float pressure = generateRandomFloat(0f,100f);
            weatherData.setMeasurements(temperature, humidity, pressure);
        }
    }

    /**
     * this method returns a random float to use in the main to stimulate data
     * @param min a float of minimal value
     * @param max a float of maximal value
     * @return a pseudo random float
     */
    private static Float generateRandomFloat(float min, float max) {
        Random random = new Random();
        return random.nextFloat() * (max - min) + min;
    }
}
