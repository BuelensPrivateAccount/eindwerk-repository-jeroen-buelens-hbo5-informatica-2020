package expansion;

import basePackage.interfaces.DisplayElement;
import basePackage.interfaces.Observer;
import basePackage.interfaces.Subject;

public class CurrentConditionDisplay implements Observer, DisplayElement {
    private float temperature;
    private float humidity;
    private Subject data;

    public CurrentConditionDisplay(Subject weatherData) {
        this.data = weatherData;
        weatherData.registerObserver(this);
    }

    public void display() {
        System.out.println("current conditions: " + temperature + "degrees Celsius and " + humidity +  "% humidity");
    }

    public void update(float temp, float humidity, float pressure) {
        this.temperature = temp;
        this.humidity = humidity;
        display();
    }

    public void unSub(){
        data.removeObserver(this);
    }
}
