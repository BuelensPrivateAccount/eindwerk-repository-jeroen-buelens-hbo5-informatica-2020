package basePackage.interfaces;

public interface DisplayElement {
    void display();
}
