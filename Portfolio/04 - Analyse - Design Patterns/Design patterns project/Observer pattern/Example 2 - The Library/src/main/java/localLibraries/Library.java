package localLibraries;

import interfaces.Observer;
import interfaces.Subject;

public abstract class Library implements Observer{
    protected Subject data;

    abstract public void add(Object o);


}
