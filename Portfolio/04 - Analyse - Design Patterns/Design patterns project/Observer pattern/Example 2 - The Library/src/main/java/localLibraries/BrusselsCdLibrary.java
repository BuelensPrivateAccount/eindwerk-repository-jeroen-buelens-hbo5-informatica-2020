package localLibraries;

import interfaces.Subject;
import repo.Book;
import repo.Cdroms;
import repo.Movie;

import java.util.List;

public class BrusselsCdLibrary extends Library {
    private List<Cdroms> cdroms;

    public BrusselsCdLibrary(Subject data) {
      super.data = data;
      super.data.registerObserver(this);
    }

    @Override
    public void add(Object o) {
        Cdroms cdroms = (Cdroms) o;
        this.cdroms.add(cdroms);

    }

    @Override
    public void update(List<Book> books, List<Movie> movies, List<Cdroms> cdroms) {
        this.cdroms = cdroms;
        display();
    }

    private void display() {
        StringBuilder sb = new StringBuilder();
        for(Cdroms c : this.cdroms){
            sb.append(c.getAlbumName());
            sb.append(" by ");
            sb.append(c.getArtist());
            sb.append(" ");
        }
        System.out.println("the cd's  currently in stock are: " + sb.toString());
    }
}
