import localLibraries.BrusselsCdLibrary;
import localLibraries.Librarydata;
import localLibraries.LocalBlockBusters;
import localLibraries.VlezenbeekLibrary;
import repo.Book;
import repo.Cdroms;
import repo.Movie;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LibraryApp {
    public static void main(String[] args) {
        Librarydata librarydata = new Librarydata();
        BrusselsCdLibrary brusselsCdLibrary = new BrusselsCdLibrary(librarydata);
        VlezenbeekLibrary vlezenbeekLibrary = new VlezenbeekLibrary(librarydata);
        LocalBlockBusters localBlockBusters = new LocalBlockBusters(librarydata);
        int number = 15;
        List<Book> books = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            Book book = new Book();
            book.setTitle(generateRandomString(15));
            book.setAuthor(generateRandomString(15));
            books.add(book);
        }

        List<Movie> movies = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            Movie movie = new Movie();
            movie.setMovieName(generateRandomString(15));
            movie.setMovieName(generateRandomString(15));
            movies.add(movie);
        }

        List<Cdroms> cdroms = new ArrayList<>();
        for(int i = 0;i<number;i++){
            Cdroms cdrom = new Cdroms();
            cdrom.setAlbumName(generateRandomString(15));
            cdrom.setArtist(generateRandomString(15));
            cdroms.add(cdrom);
        }
        librarydata.changeDate(movies,cdroms,books);

    }

    private static String generateRandomString(int number) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = number;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();
        return generatedString;
    }
}
