package localLibraries;

import interfaces.Subject;
import repo.Book;
import repo.Cdroms;
import repo.Movie;

import java.util.List;

public class LocalBlockBusters extends Library {
    private List<Movie> movies;

    public LocalBlockBusters(Subject data) {
        super.data = data;
        super.data.registerObserver(this);
    }

    @Override
    public void add(Object o) {
        Movie movie = (Movie) o;
        movies.add(movie);

    }

    @Override
    public void update(List<Book> books, List<Movie> movies, List<Cdroms> cdroms) {
        this.movies = movies;
        display();
    }

    private void display(){
        StringBuilder sb = new StringBuilder();
        for(Movie m : movies){
            sb.append(m.getMovieName());
            sb.append(", ");
        }
        System.out.println("the movies currently in stock are: " + sb.toString());
    }
}
