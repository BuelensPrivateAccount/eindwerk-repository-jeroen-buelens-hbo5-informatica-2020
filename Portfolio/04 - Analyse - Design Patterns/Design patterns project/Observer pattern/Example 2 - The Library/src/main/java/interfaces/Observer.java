package interfaces;


import repo.*;

import java.util.List;

public interface Observer {
    void update(List<Book> books, List<Movie> movies, List<Cdroms> cdroms);
}
