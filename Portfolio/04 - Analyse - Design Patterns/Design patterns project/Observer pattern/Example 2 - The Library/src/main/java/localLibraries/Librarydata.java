package localLibraries;

import interfaces.Observer;
import interfaces.Subject;
import repo.*;


import java.util.ArrayList;
import java.util.List;

public class Librarydata implements Subject {
    private List<Observer> observers;
    private List<Movie> movies;
    private List<Cdroms> cdroms;
    private List<Book> books;

    public Librarydata(){
        observers = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer o) {
       observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        int index = observers.indexOf(o);
        if (!(index < 0)) {
            observers.remove(index);
        }
    }

    @Override
    public void notifyObserver() {
        for (Observer o : observers) {
            o.update(books,movies,cdroms);
        }
    }

    public void changeDate(List<Movie> movies,List<Cdroms> cdroms,List<Book> books){
        this.movies = movies;
        this.cdroms = cdroms;
        this.books = books;
        notifyObserver();
    }
}
