package repo;

public class Book {
     private String title;
     private String author;
     private Boolean onloan;

     public String getTitle() {
          return title;
     }

     public void setTitle(String title) {
          this.title = title;
     }

     public String getAuthor() {
          return author;
     }

     public void setAuthor(String author) {
          this.author = author;
     }

     public Boolean getOnloan() {
          return onloan;
     }

     public void setOnloan(Boolean onloan) {
          this.onloan = onloan;
     }
}
