package localLibraries;


import interfaces.Subject;
import repo.*;


import java.util.List;


public class VlezenbeekLibrary extends Library {
    private List<Book> books;

    public VlezenbeekLibrary(Subject data) {
        super.data = data;
        super.data.registerObserver(this);
    }

    @Override
    public void add(Object o) {
        Book book = (Book) o;
        books.add(book);
    }


    public void display(){
        StringBuilder sb = new StringBuilder();
        for(Book b : books){
            sb.append(b.getTitle());
            sb.append(" by ");
            sb.append(b.getAuthor());
            sb.append(" ");
        }
        System.out.println("the books currently in stock are: " + sb.toString());
    }

    @Override
    public void update(List<Book> books, List<Movie> movies, List<Cdroms> cdroms) {
        this.books = books;
        display();
    }
}
