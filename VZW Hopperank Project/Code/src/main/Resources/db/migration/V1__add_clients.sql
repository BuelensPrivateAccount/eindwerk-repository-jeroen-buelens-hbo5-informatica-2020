CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(255) NOT NULL,
  `firstname` varchar(255) not null,
  `mddlname` varchar(255) DEFAULT NULL,
  `dossierlnk` varchar(255) not null ,
  `birthdate` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `postalcode` int(11) DEFAULT NULL,
  `housenumber` varchar(255) DEFAULT NULL,
  `VAPHNR` varchar(255) not null ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
