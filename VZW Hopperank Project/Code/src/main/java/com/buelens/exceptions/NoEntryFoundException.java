package com.buelens.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoEntryFoundException extends Exception {
    public NoEntryFoundException(){
        super("No entry for your query was found");
    }
    public NoEntryFoundException(String message){
        super(message);
    }
}
