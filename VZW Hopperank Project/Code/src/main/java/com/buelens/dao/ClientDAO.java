package com.buelens.dao;

import com.buelens.domain.objects.Client;
import com.buelens.utilities.JPAUtility;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import java.util.List;


public class ClientDAO {
    JPAUtility jpaUtility;
    EntityManager em;

    public ClientDAO() {
        jpaUtility = JPAUtility.getInstance();
    }

    protected ClientDAO(JPAUtility jpaUtility) {
        this.jpaUtility = jpaUtility;
    }


    public boolean saveToDB(Client client) {
        em = jpaUtility.createEntityManager();
        EntityTransaction tr = em.getTransaction();
        try {
            tr.begin();
            em.persist(client);
            tr.commit();
            return true;
        } catch (RollbackException e) {
            return false;
        } finally {
            em.close();
        }
    }

    public boolean saveToDB(List<Client> clients) {
        em = jpaUtility.createEntityManager();
        if (clients.size() == 1) {
            return saveToDB(clients.get(0));
        } else {
            EntityTransaction tr = em.getTransaction();
            try {
                tr.begin();
                for (Client i : clients) {
                    em.persist(i);
                }
                tr.commit();
                return true;
            } catch (RollbackException e) {
                return false;
            } finally {
                em.close();
            }
        }
    }

    public Client findClientById(int id) {
        em = jpaUtility.createEntityManager();
        TypedQuery<Client> query = em.createNamedQuery("Client.findByID", Client.class);
        query.setParameter("id", id);
        Client client = query.getSingleResult();
        em.close();
        return client;
    }

    public List<Client> findAllClients() {
        em = jpaUtility.createEntityManager();
        TypedQuery<Client> query = em.createNamedQuery("Client.findAll", Client.class);
        List<Client> clients = query.getResultList();
        em.close();
        return clients;
    }

    public List<Client> findBySurname(String surname) {
        em = jpaUtility.createEntityManager();
        TypedQuery<Client> query = em.createNamedQuery("Client.findBySurname", Client.class);
        query.setParameter("surname", "%" + surname + "%");
        List<Client> clients = query.getResultList();
        em.close();
        return clients;
    }

    public Client findOneClientBySurname(String surname) {
        return findBySurname(surname).get(0);
    }

    public List<Client> findByFirstName(String firstName) {
        em = jpaUtility.createEntityManager();
        TypedQuery<Client> query = em.createNamedQuery("Client.findByFirstName", Client.class);
        query.setParameter("firstname", "%" + firstName + "%");
        List<Client> clients = query.getResultList();
        em.close();
        return clients;
    }

    public Client findOneClientByFirstName(String firstName) {
        return findByFirstName(firstName).get(0);
    }

}
