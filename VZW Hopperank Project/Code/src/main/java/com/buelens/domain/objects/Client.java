package com.buelens.domain.objects;

import com.buelens.domain.objects.database.aid.classes.AbstractDatabaseObject;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;

@Entity
@Table(name = "clients", schema = "hopperank")
@NamedQueries({
        @NamedQuery(name = "Client.findByID", query = "select c from Client c where c.pk = :id"),
        @NamedQuery(name = "Client.findAll", query = "select c from Client c"),
        @NamedQuery(name="Client.findBySurname",query = "select c from Client c where c.surname like :surname order by c.surname desc"),
        @NamedQuery(name="Client.findBySurnameASC",query = "select c from Client c where c.surname like :surname order by c.surname asc"),
        @NamedQuery(name= "Client.findByFirstName",query = "select c from Client c where c.firstName like :firstname order by c.firstName desc"),
        @NamedQuery(name= "Client.findByFirstNameASC",query = "select c from Client c where c.firstName like :firstname order by c.firstName asc"),
})
public class Client extends AbstractDatabaseObject {
    @Basic(optional = false)
    @Access(AccessType.FIELD)
    @Column(name = "surname", nullable = false)
    private String surname;
    @Basic(optional = false)
    @Access(AccessType.FIELD)
    @Column(name = "firstname", nullable = false)
    private String firstName;
    @Basic
    @Access(AccessType.FIELD)
    @Column(name = "mddlname")
    private String middleName;
    @Basic(optional = false)
    @Access(AccessType.FIELD)
    @Column(name = "dossierlnk", nullable = false)
    private String dossierLink;
    @Basic(optional = false)
    @Access(AccessType.FIELD)
    @Column(name = "birthdate", nullable = false)
    private java.sql.Date birthDate;
    @Basic(optional = false)
    @Access(AccessType.FIELD)
    @Column(name = "VAPHNR", nullable = false)
    private String vaphNumber;
    @Basic(optional = false)
    @Access(AccessType.FIELD)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Access(AccessType.FIELD)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @Access(AccessType.FIELD)
    @Column(name = "street")
    private String street;
    @Basic(optional = false)
    @Access(AccessType.FIELD)
    @Column(name = "postalcode")
    private int postalcode;
    @Basic(optional = false)
    @Access(AccessType.FIELD)
    @Column(name = "housenumber")
    private String housenumber;


    public Client() {
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getDossierLink() {
        return dossierLink;
    }

    public void setDossierLink(String dossierLink) {
        this.dossierLink = dossierLink;
    }

    public String getVaphNumber() {
        return vaphNumber;
    }

    public void setVaphNumber(String vaphNumber) {
        this.vaphNumber = vaphNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(int postalcode) {
        this.postalcode = postalcode;
    }

    public String getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(String housenumber) {
        this.housenumber = housenumber;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = Date.valueOf(birthDate);
    }

    public void setBirthDate(String date) {
        setBirthDate(LocalDate.parse(date));
    }

    public LocalDate getBirthdate() {
        if (birthDate != null) {
            return birthDate.toLocalDate();
        } else {
            return null;
        }
    }

}
