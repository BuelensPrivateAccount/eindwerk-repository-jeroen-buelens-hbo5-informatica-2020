package com.buelens.domain.objects.database.aid.classes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * This Class is a generic Database entity helper class, this class exists to make sure every class whom extends from this has a field called "ID" in the database that functions like a id
 */
@MappedSuperclass
public abstract class AbstractDatabaseObject implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", unique = true)
    @Access(AccessType.FIELD)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int pk;

    public int getPk() {
        return pk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractDatabaseObject)) return false;
        AbstractDatabaseObject that = (AbstractDatabaseObject) o;
        return getPk() == that.getPk();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPk());
    }

}



