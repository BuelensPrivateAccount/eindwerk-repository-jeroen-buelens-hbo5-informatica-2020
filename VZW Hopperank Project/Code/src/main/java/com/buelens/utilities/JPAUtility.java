package com.buelens.utilities;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * This helper singleton class holds our EntityManagerFactory & is designed to easy call upon our persistance and use entitymanagers with it
 */
public class JPAUtility {
    private final EntityManagerFactory emf;
    private static JPAUtility jpaUtility;


    public void close() {
        emf.close();
    }

    public EntityManager createEntityManager() {
        return emf.createEntityManager();
    }

    private JPAUtility() {
        emf = Persistence.createEntityManagerFactory("com.buelens.HopperankDB");
    }

    public static JPAUtility getInstance() {
        if (jpaUtility == null) {
            jpaUtility = new JPAUtility();
        }
        return jpaUtility;
    }
}
