package com.buelens.controllers;

import com.buelens.dao.ClientDAO;
import com.buelens.domain.objects.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClientSpringController {


    private ClientDAO clientDAO = new ClientDAO();

    @GetMapping("/clientByID")
    public Client clientByID(@RequestParam(name = "id", required = true) int id) {
        return clientDAO.findClientById(id);
    }

    @GetMapping("/allclients")
    public List<Client> allClients() {
        return clientDAO.findAllClients();
    }

    @GetMapping("/saveToDB")
    public boolean saveToDB(
            @RequestParam(name = "surname", required = true) String surname,
            @RequestParam(name = "firstname", required = true) String firstName,
            @RequestParam(name = "middlename", required = false, defaultValue = "") String middleName,
            @RequestParam(name = "dossierlnk", required = true) String dossierLnk,
            @RequestParam(name = "birth", required = true) String birthday,
            @RequestParam(name = "vaphNR", required = true) String vaph,
            @RequestParam(name = "email", required = false, defaultValue = "") String email,
            @RequestParam(name = "city", required = false, defaultValue = "") String city,
            @RequestParam(name = "street", required = false, defaultValue = "") String street,
            @RequestParam(name = "postalcode", required = false, defaultValue = "0000") int postal,
            @RequestParam(name = "housenr", required = false, defaultValue = "") String housenr
    ) {
        Client client = new Client();
        client.setBirthDate(birthday);
        client.setFirstName(firstName);
        client.setSurname(surname);
        client.setMiddleName(middleName);
        client.setDossierLink(dossierLnk);
        client.setEmail(email);
        client.setVaphNumber(vaph);
        client.setCity(city);
        client.setStreet(street);
        client.setPostalcode(postal);
        client.setHousenumber(housenr);
        return clientDAO.saveToDB(client);
    }

    @GetMapping("/clientByFirstName")
    public List<Client> clientByFirstName(@RequestParam(name = "firstname", required = true, defaultValue = "a") String firstName) {
        return clientDAO.findByFirstName(firstName);
    }
    @GetMapping("/clientBySurname")
    public List<Client> clientBySurname(@RequestParam(name = "surname", required = true, defaultValue = "a") String surname) {
        return clientDAO.findBySurname(surname);
    }

}
