package com.buelens.objects.database.aid.classes;

import com.buelens.domain.objects.database.aid.classes.AbstractDatabaseObject;
import com.buelens.helper.ReflectTool;
import com.buelens.domain.objects.database.aid.classes.AbstractDatabaseObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.*;

import static com.buelens.helper.AssertAnnotations.assertField;
import static com.buelens.helper.AssertAnnotations.assertType;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AbstractDatabaseObjectTest {
    @Test
    public void AbstractDBObjectIsMappedSuperClass() {
        assertType(AbstractDatabaseObject.class, MappedSuperclass.class);
    }

    @Test
    public void verifyPkIsAFieldWithCorrectAnnotations() {
        assertField(AbstractDatabaseObject.class, "pk", Id.class, Basic.class, Access.class, Column.class, GeneratedValue.class);
        GeneratedValue generatedValue = ReflectTool.getFieldAnnotation(AbstractDatabaseObject.class, "pk", GeneratedValue.class);
        Access access = ReflectTool.getFieldAnnotation(AbstractDatabaseObject.class, "pk", Access.class);
        Basic basic = ReflectTool.getFieldAnnotation(AbstractDatabaseObject.class, "pk",Basic.class);
        Column column = ReflectTool.getFieldAnnotation(AbstractDatabaseObject.class, "pk",Column.class);
        assertFalse(basic.optional());
        assertEquals("id",column.name());
        assertTrue(column.unique());
        assertEquals(AccessType.FIELD, access.value());
        assertEquals(GenerationType.IDENTITY, generatedValue.strategy());
    }
}