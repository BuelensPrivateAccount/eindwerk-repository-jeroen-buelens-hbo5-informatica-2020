package com.buelens.objects;

import com.buelens.domain.objects.Client;
import com.buelens.helper.ReflectTool;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.*;

import static com.buelens.helper.AssertAnnotations.assertField;
import static com.buelens.helper.AssertAnnotations.assertType;
import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
public class ClientTest {

    @Test
    public void emptyTest() {
    }

    @Test
    public void ClientClassAnnotationsTest() {
        assertType(Client.class, Entity.class, Table.class, NamedQueries.class);
    }

    @Test
    public void surnameFieldVerify() {
        assertField(Client.class, "surname", Basic.class, Access.class, Column.class);
        Access access = ReflectTool.getFieldAnnotation(Client.class, "surname", Access.class);
        Column column = ReflectTool.getFieldAnnotation(Client.class, "surname", Column.class);
        assertEquals("surname", column.name());
        assertFalse(column.nullable());
        assertEquals(AccessType.FIELD, access.value());
    }

    @Test
    public void firstNameFieldVerify() {
        assertField(Client.class, "firstName", Basic.class, Access.class, Column.class);
        Access access = ReflectTool.getFieldAnnotation(Client.class, "firstName", Access.class);
        Column column = ReflectTool.getFieldAnnotation(Client.class, "firstName", Column.class);
        assertEquals("firstname", column.name());
        assertFalse(column.nullable());
        assertEquals(AccessType.FIELD, access.value());
    }

    @Test
    public void middleNameFieldVerify() {
        assertField(Client.class, "middleName", Basic.class, Access.class, Column.class);
        Access access = ReflectTool.getFieldAnnotation(Client.class, "middleName", Access.class);
        Column column = ReflectTool.getFieldAnnotation(Client.class, "middleName", Column.class);
        assertEquals("mddlname", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }

    @Test
    public void dossierLinkFieldVerify() {
        assertField(Client.class, "dossierLink", Basic.class, Access.class, Column.class);
        Access access = ReflectTool.getFieldAnnotation(Client.class, "dossierLink", Access.class);
        Column column = ReflectTool.getFieldAnnotation(Client.class, "dossierLink", Column.class);
        assertEquals("dossierlnk", column.name());
        assertFalse(column.nullable());
        assertEquals(AccessType.FIELD, access.value());
    }

    @Test
    public void birthDateFieldVerify() {
        assertField(Client.class, "birthDate", Basic.class, Access.class, Column.class);
        Access access = ReflectTool.getFieldAnnotation(Client.class, "birthDate", Access.class);
        Column column = ReflectTool.getFieldAnnotation(Client.class, "birthDate", Column.class);
        assertEquals("birthdate", column.name());
        assertFalse(column.nullable());
        assertEquals(AccessType.FIELD, access.value());
    }

    @Test
    public void vaphNumberFieldVerify() {
        assertField(Client.class, "vaphNumber", Basic.class, Access.class, Column.class);
        Access access = ReflectTool.getFieldAnnotation(Client.class, "vaphNumber", Access.class);
        Column column = ReflectTool.getFieldAnnotation(Client.class, "vaphNumber", Column.class);
        assertEquals("VAPHNR", column.name());
        assertFalse(column.nullable());
        assertEquals(AccessType.FIELD, access.value());
    }

    @Test
    public void emailFieldVerify() {
        assertField(Client.class, "email", Basic.class, Access.class, Column.class);
        Access access = ReflectTool.getFieldAnnotation(Client.class, "email", Access.class);
        Column column = ReflectTool.getFieldAnnotation(Client.class, "email", Column.class);
        assertEquals("email", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }

    @Test
    public void cityFieldVerify() {
        assertField(Client.class, "city", Basic.class, Access.class, Column.class);
        Access access = ReflectTool.getFieldAnnotation(Client.class, "city", Access.class);
        Column column = ReflectTool.getFieldAnnotation(Client.class, "city", Column.class);
        assertEquals("city", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }

    @Test
    public void streetFieldVerify() {
        assertField(Client.class, "street", Basic.class, Access.class, Column.class);
        Access access = ReflectTool.getFieldAnnotation(Client.class, "street", Access.class);
        Column column = ReflectTool.getFieldAnnotation(Client.class, "street", Column.class);
        assertEquals("street", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }

    @Test
    public void postalcodeFieldVerify() {
        assertField(Client.class, "postalcode", Basic.class, Access.class, Column.class);
        Access access = ReflectTool.getFieldAnnotation(Client.class, "postalcode", Access.class);
        Column column = ReflectTool.getFieldAnnotation(Client.class, "postalcode", Column.class);
        assertEquals("postalcode", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }

    @Test
    public void housenumberFieldVerify() {
        assertField(Client.class, "housenumber", Basic.class, Access.class, Column.class);
        Access access = ReflectTool.getFieldAnnotation(Client.class, "housenumber", Access.class);
        Column column = ReflectTool.getFieldAnnotation(Client.class, "housenumber", Column.class);
        assertEquals("housenumber", column.name());
        assertEquals(AccessType.FIELD, access.value());
    }
}
