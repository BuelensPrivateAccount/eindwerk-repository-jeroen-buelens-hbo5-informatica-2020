package com.buelens.dao;

import com.buelens.domain.objects.Client;
import com.buelens.utilities.JPAUtility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.metamodel.Metamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ClientDAOTest {

    EntityTransaction tr = new EntityTransaction() {
        @Override
        public void begin() {

        }

        @Override
        public void commit() {

        }

        @Override
        public void rollback() {

        }

        @Override
        public void setRollbackOnly() {

        }

        @Override
        public boolean getRollbackOnly() {
            return false;
        }

        @Override
        public boolean isActive() {
            return false;
        }
    };

    @Mock
    EntityManager em = new EntityManager() {
        @Override
        public void persist(Object entity) {

        }

        @Override
        public <T> T merge(T entity) {
            return null;
        }

        @Override
        public void remove(Object entity) {

        }

        @Override
        public <T> T find(Class<T> entityClass, Object primaryKey) {
            return null;
        }

        @Override
        public <T> T find(Class<T> entityClass, Object primaryKey, Map<String, Object> properties) {
            return null;
        }

        @Override
        public <T> T find(Class<T> entityClass, Object primaryKey, LockModeType lockMode) {
            return null;
        }

        @Override
        public <T> T find(Class<T> entityClass, Object primaryKey, LockModeType lockMode, Map<String, Object> properties) {
            return null;
        }

        @Override
        public <T> T getReference(Class<T> entityClass, Object primaryKey) {
            return null;
        }

        @Override
        public void flush() {

        }

        @Override
        public void setFlushMode(FlushModeType flushMode) {

        }

        @Override
        public FlushModeType getFlushMode() {
            return null;
        }

        @Override
        public void lock(Object entity, LockModeType lockMode) {

        }

        @Override
        public void lock(Object entity, LockModeType lockMode, Map<String, Object> properties) {

        }

        @Override
        public void refresh(Object entity) {

        }

        @Override
        public void refresh(Object entity, Map<String, Object> properties) {

        }

        @Override
        public void refresh(Object entity, LockModeType lockMode) {

        }

        @Override
        public void refresh(Object entity, LockModeType lockMode, Map<String, Object> properties) {

        }

        @Override
        public void clear() {

        }

        @Override
        public void detach(Object entity) {

        }

        @Override
        public boolean contains(Object entity) {
            return false;
        }

        @Override
        public LockModeType getLockMode(Object entity) {
            return null;
        }

        @Override
        public void setProperty(String propertyName, Object value) {

        }

        @Override
        public Map<String, Object> getProperties() {
            return null;
        }

        @Override
        public Query createQuery(String qlString) {
            return null;
        }

        @Override
        public <T> TypedQuery<T> createQuery(CriteriaQuery<T> criteriaQuery) {
            return null;
        }

        @Override
        public Query createQuery(CriteriaUpdate updateQuery) {
            return null;
        }

        @Override
        public Query createQuery(CriteriaDelete deleteQuery) {
            return null;
        }

        @Override
        public <T> TypedQuery<T> createQuery(String qlString, Class<T> resultClass) {
            return null;
        }

        @Override
        public Query createNamedQuery(String name) {
            return null;
        }

        @Override
        public <T> TypedQuery<T> createNamedQuery(String name, Class<T> resultClass) {
            return null;
        }

        @Override
        public Query createNativeQuery(String sqlString) {
            return null;
        }

        @Override
        public Query createNativeQuery(String sqlString, Class resultClass) {
            return null;
        }

        @Override
        public Query createNativeQuery(String sqlString, String resultSetMapping) {
            return null;
        }

        @Override
        public StoredProcedureQuery createNamedStoredProcedureQuery(String name) {
            return null;
        }

        @Override
        public StoredProcedureQuery createStoredProcedureQuery(String procedureName) {
            return null;
        }

        @Override
        public StoredProcedureQuery createStoredProcedureQuery(String procedureName, Class... resultClasses) {
            return null;
        }

        @Override
        public StoredProcedureQuery createStoredProcedureQuery(String procedureName, String... resultSetMappings) {
            return null;
        }

        @Override
        public void joinTransaction() {

        }

        @Override
        public boolean isJoinedToTransaction() {
            return false;
        }

        @Override
        public <T> T unwrap(Class<T> cls) {
            return null;
        }

        @Override
        public Object getDelegate() {
            return null;
        }

        @Override
        public void close() {

        }

        @Override
        public boolean isOpen() {
            return false;
        }

        @Override
        public EntityTransaction getTransaction() {
            return null;
        }

        @Override
        public EntityManagerFactory getEntityManagerFactory() {
            return null;
        }

        @Override
        public CriteriaBuilder getCriteriaBuilder() {
            return null;
        }

        @Override
        public Metamodel getMetamodel() {
            return null;
        }

        @Override
        public <T> EntityGraph<T> createEntityGraph(Class<T> rootType) {
            return null;
        }

        @Override
        public EntityGraph<?> createEntityGraph(String graphName) {
            return null;
        }

        @Override
        public EntityGraph<?> getEntityGraph(String graphName) {
            return null;
        }

        @Override
        public <T> List<EntityGraph<? super T>> getEntityGraphs(Class<T> entityClass) {
            return null;
        }
    };
    @Mock
    JPAUtility jpaUtility;
    @Mock
    TypedQuery<Client> query;

    ClientDAO clientDAO;

    @BeforeEach
    public void setUP() {
        MockitoAnnotations.initMocks(this);
        clientDAO = new ClientDAO(jpaUtility);
        when(jpaUtility.createEntityManager()).thenReturn(em);
        lenient().when(em.getTransaction()).thenReturn(tr);
        lenient().when(em.createNamedQuery(anyString(), eq(Client.class))).thenReturn(query);
    }

    @Test
    void saveToDB() {
        Client client = new Client();
        client.setSurname("Buelens");
        ArgumentCaptor<Client> captor = ArgumentCaptor.forClass(Client.class);
        clientDAO.saveToDB(client);
        verify(em).persist(captor.capture());
        assertEquals(client, captor.getValue());
    }

    @Test
    void testSaveToDB() {
        List<Client> clients = new ArrayList<>();
        Client client = new Client();
        client.setSurname("Buelens");
        for (int i = 0; i < 5; i++) {
            client.setFirstName(i + "");
            clients.add(client);
        }
        clientDAO.saveToDB(clients);
        verify(em, times(5)).persist(any());
    }

    @Test
    void findClientById() {
        Client client = new Client();
        client.setSurname("Buelens");
        when(query.getSingleResult()).thenReturn(client);
        ArgumentCaptor<Integer> captor = ArgumentCaptor.forClass(Integer.class);
        Client result = clientDAO.findClientById(1);
        verify(query, times(1)).setParameter(anyString(), captor.capture());
        assertEquals(client, result);
        assertEquals(1, captor.getValue());
    }

    @Test
    void findAllClients() {
        List<Client> clients = new ArrayList<>();
        Client client = new Client();
        client.setSurname("Buelens");
        for (int i = 0; i < 5; i++) {
            client.setFirstName(i + "");
            clients.add(client);
        }
        when(query.getResultList()).thenReturn(clients);
        List<Client> result = clientDAO.findAllClients();
        assertEquals(clients, result);
    }

    @Test
    void findBySurname() {
        List<Client> clients = new ArrayList<>();
        Client client = new Client();
        client.setSurname("Buelens");
        for (int i = 0; i < 5; i++) {
            client.setFirstName(i + "");
            clients.add(client);
        }
        when(query.getResultList()).thenReturn(clients);
        List<Client> result = clientDAO.findBySurname("Jeroen");
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(query, times(1)).setParameter(anyString(), captor.capture());
        assertEquals(clients, result);
        assertEquals("%Jeroen%", captor.getValue());
    }
    

    @Test
    void findByFirstName() {
        List<Client> clients = new ArrayList<>();
        Client client = new Client();
        client.setFirstName("Buelens");
        for (int i = 0; i < 5; i++) {
            client.setFirstName(i + "");
            clients.add(client);
        }
        when(query.getResultList()).thenReturn(clients);
        List<Client> result = clientDAO.findByFirstName("Jeroen");
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(query, times(1)).setParameter(anyString(), captor.capture());
        assertEquals(clients, result);
        assertEquals("%Jeroen%", captor.getValue());
    }

   
}