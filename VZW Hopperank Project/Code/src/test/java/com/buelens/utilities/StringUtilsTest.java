package com.buelens.utilities;


import static org.junit.jupiter.api.Assertions.*;

import com.buelens.utilities.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StringUtilsTest {

    @Test
    public void shortenLong(){
        String longText = "Hello world";

        String expected = "Hel...";
        String actual = StringUtils.shorten(longText, 6, "...");

        assertEquals(expected, actual);
    }

    @Test
    public void shortenShort(){
        String shortText = "Hey";
        String expected = "Hey";
        String actual = StringUtils.shorten(shortText, 10, "...");

        assertEquals(expected, actual);
    }
}
